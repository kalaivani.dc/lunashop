<?php

return [
    'en' => [
        'display' => 'Singapore',
        'flag-icon' => 'sg'
    ],
    'bn' => [
        'display' => 'বাংলাদেশ',
        'flag-icon' => 'bn'
    ],
     'zh' => [
        'display' => '中国人',
        'flag-icon' => 'zh'
    ],
    'id' => [
        'display' => 'Indonesia',
        'flag-icon' => 'id'
    ],
    'my' => [
        'display' => 'Malaysia',
        'flag-icon' => 'my'
    ],
];
?>