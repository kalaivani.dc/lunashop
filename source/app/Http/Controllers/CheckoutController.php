<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;


use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class CheckoutController extends Controller
{
    public function checkout(Request $request)
    {
        $input = $request->all();

         return $this->view('front.checkout.checkout', [
            'cart'=>$input,
            'save'=>'/checkout?action=save'
        ]);

    }

    public function checkoutsave(Request $request)
    {
        $input = $request->all();

        //order
       $query="INSERT INTO orders(
        firstName, 
        lastName, 
        username, 
        address, 
        address2, 
        country, 
        state, 
        zip,
        user_id,
        phone_num,
        grand_total,
        created_by) VALUES(
        '".(isset($input['firstName'])?addslashes($input['firstName']):'')."', 
        '".(isset($input['lastName'])?addslashes($input['lastName']):'')."', 
        '".(isset($input['username'])?addslashes($input['username']):'')."',
        '".(isset($input['address'])?addslashes($input['address']):'')."', 
        '".(isset($input['address2'])?addslashes($input['address2']):'')."', 
        '".(isset($input['country'])?addslashes($input['country']):'')."', 
        '".(isset($input['state'])?addslashes($input['state']):'')."', 
        '".(isset($input['zip'])?addslashes($input['zip']):'')."',
        '".(isset($input['user_id'])?$input['user_id']:'')."',
        '".(isset($input['phone_num'])?$input['phone_num']:'')."',
        '".(isset($input['grand_total'])?$input['grand_total']:'')."',
        '".Auth::id()."')";

        DB::insert($query);
        $orderID = DB::connection()->getPdo()->lastInsertId();

  
        foreach($input['product'] as $key=>$value) {
            $query="INSERT INTO product_order(
            order_id, 
            product_id, 
            quantity, 
            variation,
            sub_total, 
            total) VALUES(
            '".$orderID."', 
            '".$key."',
            '".(isset($value['product']['quantity'])? $value['product']['quantity']: 0)."',
            '".(isset($value['variant'])? sizeof($value['variant']): 0)."',
            '".$value['product']['sub_total']."', 
            '".$value['product']['sub_total']."')";
            DB::insert($query);
         
            if (isset($value['variant'])) {
               
                foreach($value['variant'] as $k=>$v) {
 
                    foreach($v['item'] as $k2=>$v2) {
                        $query="INSERT INTO product_order_variation(
                        order_id, 
                        product_id, 
                        variant_id,
                        variant_type, 
                        variant_value,
                        variant_price,
                        variant_quantity) VALUES(
                        '".$orderID."', 
                        '".$key."',
                        '".$k."',
                        '".$v2['name']."',
                        '".$v2['value']."', 
                        '".$v2['regular']."',
                        '".$v['quantity']."')";
                        DB::insert($query);
                    }
                    
                    
                }
            }
        }

            $query="SELECT * FROM orders WHERE id = $orderID";
            $order = DB::select($query);
            
            $query="SELECT po.*, p.name, p.regular_price  FROM product_order po INNER JOIN product p ON p.id = po.product_id  WHERE order_id = ".$order[0]->id;
            $products = DB::select($query);
            $orderDetail = [];
            foreach($products as $key=>$value)
            {
                $orderDetail['product'][$value->product_id] = $value;
                
                
                $query="SELECT * FROM product_order_variation WHERE order_id = ".$order[0]->id."  AND product_id = ".$value->product_id;
                $variation = DB::select($query);
              foreach($variation as $k=>$v) {
                   $orderDetail['variant'][$value->product_id][$v->variant_id][] = $v;
               }
            }
            
        //  $email = new MailController;
        //  return $email->notifyAdmin($order[0], $orderDetail);
        
         return $this->view('front.checkout.thankyou', ['to'=>$order[0]->username, 'order'=>$order[0], 'detail'=>$orderDetail, 'sent'=>1]);


    }
}
