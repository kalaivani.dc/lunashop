<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use \App\Http\Controllers\UserController as UserController;
use \App\Http\Controllers\ModuleController;
// use \App\Http\Controllers\ConfigController as Config;

class GuestRouterController extends Controller
{
    public $module;
    public $user;
    public $role;
    public $action;
    public $method;
    public $guest;


     public function getRouter(Request $request, $guest)
    {

        /*initialize config*/
        // $config =  new Config();
       
        $this->method = \Route::current()->methods()[0];

        $modules = [];
        // for($i = 0; $i<$config->configuration()->tier; $i++) {
        //     $modules[] =  \Route::current()->parameter("tier_$i");
        // }
        for($i = 0; $i<3; $i++) {
            $modules[] =  \Route::current()->parameter("tier_$i");
        }
   
        //remove empty value
        $modules = array_filter($modules);

        $this->action = $request->query('action');
        
        if(isset($modules)) {

         //treat first param as main module and others sub
         $this->parentModule = $modules[0];

          if ($this->init($this->parentModule)) {
            //3rd param indicates guest/front end
            return $this->routeRequest($request, $modules, 1);

            }
         }
    }

    private function routeRequest($request, $modules = false, $guest = false)
    {

        //rearrange from main module to least sub module
        $modules = array_reverse($modules);

        if ($modules) {
           
            foreach ($modules as $key => $module) {

                if ($module) {

                    if(method_exists('\App\Http\Controllers\\'.ucfirst($this->parentModule).'Controller', "$module".ucfirst($this->action))){

                         // $breadcrumb = $this->getBreadcrumb($module, $this->action);
                         // var_dump("$module".ucfirst($this->action));exit;
                  
                         return $this->module->{"$module".ucfirst($this->action)}($request, 0, $guest);

                     } else {
                        abort(404);
                     }

                }

            }
            
        }
    }


    private function getBreadcrumb($module, $action)
    {
        $crumb = '<div class="breadcrumbs-top"><div class="breadcrumb-wrapper d-none d-sm-block"><ol class="breadcrumb p-0 mb-0 pl-1"><li class="breadcrumb-item"><a href="index.html"><i class="bx bx-home-alt"></i></a>';
        $a = new ModuleController();
        $bread = $a->getParentModules($a->getModuleId($module));
        foreach($bread as $key=>$value) {
            $crumb .='<li class="breadcrumb-item"><a href="#">'.$value['name'].'</a>';

        }
        $crumb .="</ol></div></div>";
        echo $crumb;exit;
        return $crumb;
    }
    public function init($module)
    {
        //get parent module
         $query="SELECT * FROM module WHERE name = '".strtolower($module)."'";

         $moduleData = DB::select($query);

         //if this is not the parent
         if (isset($moduleData[0])) {

         
             if ($moduleData[0]->parent != 0) {
                //get the parent module name
                 $query="SELECT name FROM module WHERE id = ".$moduleData[0]->parent;

                 $moduleData = DB::select($query);
          
                 $module = $moduleData[0]->name;

             }

            //initialize the module
            $moduleController = app('App\Http\Controllers\\' . ucfirst($module) . 'Controller');

            $this->module = new $moduleController;
            return $this->module;
        }
        echo $module;exit;
        echo "Module not found";
        return false;
    }
}
