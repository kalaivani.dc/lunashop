<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class RoleController extends Controller
{
    
    public function role($internal = false)
    {
        $query = "SELECT id, name FROM roles";
        $roles = DB::select($query);
        if ($internal) {
            return $roles;
        }
        return $this->view('role.view',
        [
            'roles'=>$roles]);
    }

    public function getRole()
    {
        $roles = $this->role(1);

        return $roles;
    }
}
