<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class ProductController extends Controller
{
     public function product(Request $request, $internal = false, $guest = false)
    {

        $extra = '';
        $id = $request->query('id');
        $input = $request->all();
        // echo "<pre>";
        // var_dump($input);exit;
        if ($id > 0) {

            $query = "SELECT * FROM product WHERE id = $id";
            $product = DB::select($query);


            $query = "SELECT * FROM variation v LEFT JOIN variation_promo vp ON v.id = vp.variation_id WHERE product_id = ".$product[0]->id;

            $variation = DB::select($query);

             return $this->view('product.product',
            [   
                'guest'=>$guest,
                'product'=>$product[0],
                'variation'=>$variation]);
            }
        $query = "SELECT * FROM product";

        $product = DB::select($query);

        $vendor = new VendorController();

        $vendor = $vendor->vendor($request, 1, $guest);
        

        if($internal) {
            return $product;
        }

        if (!$guest) {

            $user = new UserController(); 
            $role = $user->loggedInUser();
        

         $add = $this->site.$role->role.'/shop/product?action=add';
        }

        if (!$guest) {
            $view = 'product.view';
        } else {
            $view = 'front.product.view';

        }
         $add_title = 'product.addproduct';

         return $this->view($view,
        [
            'add'=>(isset($add)? $add: ''),
            'guest'=>$guest,
            'add_title'=>$add_title,
            'product'=>$product,
            'vendor'=>$vendor,
            'thead'=>['name'],
            // 'keys'=>[['id'=>1, 'name'=>'Brand 1'],['id'=>2, 'name'=>'Brand 2'],['id'=>3, 'name'=>'Brand 3']],
            'keys'=>[['id'=>1, 'name'=>'all']]
        ]);
    }

    public function product_post_4(Request $request, $internal = false)
    {

        $extra = '';
        $id = $request->query('id');
        $input = $request->all();

        ProcessRequests::dispatch('test', $input, 0,100);
        ProcessRequests::dispatch('test2', $input, 100,100);
        ProcessRequests::dispatch('test3', $input, 200,100);
        ProcessRequests::dispatch('test4', $input, 300,100);
        ProcessRequests::dispatch('test5', $input, 400,100);
        ProcessRequests::dispatch('test6', $input, 500,100);




// $url = 'http://localhost/nexus/admin/shop/brand';
// $nbPages = 30;

// $responses = Http::pool(function (Pool $pool) use ($url, $nbPages) {
//     return collect()
//         ->range(1, $nbPages)
//         ->map(fn ($page) => $pool->get($url . "?page={$page}"));
// });

// var_dump($responses);
        // $this->limit = 100;
        //  $offsetStr = '';
        //  $extra = "";
        //  if (isset($input['offset'])) {
        //     $this->offset = $input['offset']; 
        //  } else {
        //     $this->offset = 0;
        //  }

        // if (isset($input['vendor']) && !empty($input['vendor'])) {

        //     $extra.= (!empty($extra) ? ' AND': 'AND')." AND v.id = ".$input['vendor'];

        // }

        // if (isset($input['keyword']) && !empty($input['keyword'])) {

        //     $keyword = $input['keyword'];
        //     $extra .= (!empty($extra) ? ' AND': 'AND')."  p.name LIKE '%".$keyword."%'";
        //     // $extra .= " OR i.tracking_code LIKE '%".$keyword."%'";
        //     // $extra .= " OR i.order_id LIKE '%".$keyword."%'";

        // }

        // if ($id > 0) {
        //     $extra.= "WHERE id = $id";
        // }
        //  $query = "";
   
        // $query="SELECT [param] FROM product p INNER JOIN brand b ON b.id = p.brand_id INNER JOIN vendor v ON v.id = b.vendor_id WHERE 1 = 1 $extra";
        // $data = $this->paginate($query, 'p.id, p.name, p.brand_id', $this->offset, $this->limit);
      //   exit;
      //   foreach ($data['result'] as $key => $value)
      //    {
      //       $data['rowCount']++;
      //       $column[$value->brand_id][] = $this->setRow(
      //               $value->id,[
      //               $value->name
      //               ], $data['rowCount'], $value->id);
      //    }



      //    $totalkeys = array_keys($column);
      //    $rowCount = [];
      //    //count rows
      //    foreach ($totalkeys as $key => $value) {
      //       $rowCount[$value] = sizeof($column[$value]);
      //    }

        
      //   if (isset($input['paginate'])) {
      //       $data['paginate'] = (isset($input['paginate'])? $input['paginate'] : 0);
      //   }

      //   (isset($input['search'])? $data['search'] = $input['search']: '');
        
      //   $data['data'] = $column;
      //   $data['fresh'] = (isset($input['fresh'])? $input['fresh'] : 0);
      //   $data['totalRows'] = $data['totalRows'];
      //   $data['more'] = $data['more'];
      //   $data['offset'] = $data['offset'];
      //   $data['totalRows'] = $data['totalRows'];
      //   $data['rowCountByKey'] = $rowCount;


      //   unset($data['result']);

      // return $data;
    }

    //using ajax
     public function product_post(Request $request, $internal = false)
    {
        $input = $request->all();

        $extra = '';
        $id = $request->query('id');
        $input = $request->all();

        $this->limit = 100;
         $offsetStr = '';
         $extra = "";
         if (isset($input['offset'])) {
            $this->offset = $input['offset']; 
         } else {
            $this->offset = 0;
         }

        if (isset($input['vendor']) && !empty($input['vendor'])) {

            $extra.= (!empty($extra) ? ' AND': 'AND')." AND v.id = ".$input['vendor'];

        }

        if (isset($input['keyword']) && !empty($input['keyword'])) {

            $keyword = $input['keyword'];
            $extra .= (!empty($extra) ? ' AND': 'AND')."  p.name LIKE '%".$keyword."%'";
            // $extra .= " OR i.tracking_code LIKE '%".$keyword."%'";
            // $extra .= " OR i.order_id LIKE '%".$keyword."%'";

        }

        if ($id > 0) {
            $extra.= "WHERE id = $id";
        }
         $query = "";
   
        $query="SELECT [param] FROM product p INNER JOIN brand b ON b.id = p.brand_id INNER JOIN vendor v ON v.id = b.vendor_id WHERE 1 = 1 $extra";
        $data = $this->paginate($query, 'p.id, p.name, p.brand_id', $this->offset, $this->limit);
          $totalColumn = sizeof($data['result']);

        // foreach ($data['result'] as $key => $value)
        //  {
        //     $data['rowCount']++;
        //     //admin
           
        //     // $column[$value->brand_id][] = $this->setRow(
        //     //         $value->id,[
        //     //         '<a href="?id='.$value->id.'">'.$value->name.'</a>',
        //     //         ], $data['rowCount'], $value->id);

        
        //  }
ob_start();

?>
<?php
         // for($i = 0; $i < $totalColumn;$i++) {
         foreach ($data['result'] as $key => $value)
         
$data['rowCount']++;

            $id = $value->id;
            if ($data['rowCount']%3 == 0) {
                $column[$data['result'][$i]->brand_id][] 
               ?>
              </tr>
<tr class="parent-row">
               

               <?php
            } else {
                ?>
               

             <td>
              <div class="col-sm-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="new-arrival-product">
                                    <div class="new-arrivals-img-contnent">
                                        <img class="img-fluid" src="asset/assets/images_temp/product/1.jpg" alt="">
                                    </div>
                                    <div class="new-arrival-content text-center mt-3">
                                        <h4><a href="ecom-product-detail.html">Name <?php echo $data['rowCount']; ?></a></h4>
                                        <ul class="star-rating">
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star-half-empty"></i></li>
                                            <li><i class="fa fa-star-half-empty"></i></li>
                                        </ul>
                                        <span class="price">RM 12.90</span>
                                    </div>
                                </div>
                                 <input class="data-check-num" name="data[]" id="ship_<?php echo $id ?>" value="<?php echo $id ?>" type="checkbox">
                                <label class="data-check-num" for="ship_<?php echo $id ?>">
                                   <i class="fa fa-shopping-basket mr-2"></i>
                                </label>
                            </div>
                        </div>
                    </div>
          </td>
          <?php

         }
     }
?>


<?php
        $html = ob_get_contents();
        ob_end_clean();

         // $column[$data['result'][$i]->brand_id][] = $this->setRow2(
         //            $data['result'][$i]->id,[
         //            '<a href="?id='.$data['result'][$i]->id.'">'.$data['result'][$i]->name.'</a>',
         //            ], $data['rowCount'], $data['result'][$i]->id);


         $totalkeys = array_keys($column);
         $rowCount = [];
         //count rows
         foreach ($totalkeys as $key => $value) {
            $rowCount[$value] = sizeof($column[$value]);
         }

        if (isset($input['paginate'])) {
            $data['paginate'] = (isset($input['paginate'])? $input['paginate'] : 0);
        }

        (isset($input['search'])? $data['search'] = $input['search']: '');
      
        $data['data'] = $column;
        $data['fresh'] = (isset($input['fresh'])? $input['fresh'] : 0);
        $data['totalRows'] = $data['totalRows'];
        $data['more'] = $data['more'];
        $data['offset'] = $data['offset'];
        $data['totalRows'] = $data['totalRows'];
        $data['rowCountByKey'] = $rowCount;


        unset($data['result']);

      return $data;
    }

    //using websocket
    public function product_post_2(Request $request, $internal = false)
    {
        $input = $request->all();

        $connectionID = $input['id'];

        
        $extra = '';
        $id = $request->query('id');
        $input = $request->all();

        $this->limit = 100;
         $offsetStr = '';
         $extra = "";
         if (isset($input['offset'])) {
            $this->offset = $input['offset']; 
         } else {
            $this->offset = 0;
         }

        if (isset($input['vendor']) && !empty($input['vendor'])) {

            $extra.= (!empty($extra) ? ' AND': 'AND')." AND v.id = ".$input['vendor'];

        }

        if (isset($input['keyword']) && !empty($input['keyword'])) {

            $keyword = $input['keyword'];
            $extra .= (!empty($extra) ? ' AND': 'AND')."  p.name LIKE '%".$keyword."%'";
            // $extra .= " OR i.tracking_code LIKE '%".$keyword."%'";
            // $extra .= " OR i.order_id LIKE '%".$keyword."%'";

        }

        if ($id > 0) {
            $extra.= "WHERE id = $id";
        }
         $query = "";
   
        $query="SELECT [param] FROM product p INNER JOIN brand b ON b.id = p.brand_id INNER JOIN vendor v ON v.id = b.vendor_id WHERE 1 = 1 $extra";
        $data = $this->paginate($query, 'p.id, p.name, p.brand_id', $this->offset, $this->limit);

        if (sizeof($data['result']) <=0) {
            $column = [];
            return;
        }
        foreach ($data['result'] as $key => $value)
         {
            $data['rowCount']++;
            $column[$value->brand_id][] = $this->setRow(
                    $value->id,[
                    $value->name
                    ], $data['rowCount'], $value->id);
         }



         $totalkeys = array_keys($column);
         $rowCount = [];
         //count rows
         foreach ($totalkeys as $key => $value) {
            $rowCount[$value] = sizeof($column[$value]);
         }

        
        if (isset($input['paginate'])) {
            $data['paginate'] = (isset($input['paginate'])? $input['paginate'] : 0);
        }

        (isset($input['search'])? $data['search'] = $input['search']: '');
        
        $data['data'] = $column;
        $data['fresh'] = (isset($input['fresh'])? $input['fresh'] : 0);
        $data['totalRows'] = $data['totalRows'];
        $data['more'] = $data['more'];
        $data['offset'] = $data['offset'];
        $data['totalRows'] = $data['totalRows'];
        $data['rowCountByKey'] = $rowCount;


        unset($data['result']);
        event(new MessageTransport('data', ['id'=>$connectionID, 'result'=>$data]));
        exit;

    }

    public function productAdd(Request $request)
    {
        $logistic = new UserController($request);
        $role = new RoleController($request);
        $allRoles = $role->getRole();

        $brand = $this->brand($request, 1);



        return $this->view('product.create',
        [
            'brand'=>$brand]);
    }

     public function productSave(Request $request)
    {
        $input = $request->all();
        $userID = Auth::id();


        $query="INSERT INTO product(name, brand_id, regular_price, created_by)VALUES('".addslashes($input['name'])."', '".implode(',', $input['brand'])."','".$input['regular_price']."',$userID)";

        $insert = DB::insert($query);

        $productID = DB::connection()->getPdo()->lastInsertId();

        foreach ($input['variation'] as $key => $value) {

            $query="INSERT INTO variation(product_id, name, value, regular_price, created_by)VALUES($productID, '".addslashes($value['name'])."','".addslashes($value['value'])."','".$value['price']['regular']."',$userID)";
            $insert = DB::insert($query);
            $variationID = DB::connection()->getPdo()->lastInsertId();

            if ($value['price']['sale']) {
      
            $query="INSERT INTO variation_promo(variation_id, sale_price, start_date, end_date, created_by)VALUES($variationID, '".$value['price']['sale']."','".$value['sale']['start_date']."','".$value['sale']['end_date']."',$userID)";
            $insert = DB::insert($query);
            }

        }



        return redirect()->back()->with('success', 'Product Created');


    }
}
