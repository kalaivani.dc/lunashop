<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
        $this->middleware('auth')->except(['about','become_a_reseller', 'contact', 'store', 'privacy_policy', 'refund', 'terms', 'xe_currency', 'xe_currency_daily']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return $this->view('home', []);
    }
    public function about()
    {
        return $this->view('about', []);
    }
    public function become_a_reseller()
    {
        return $this->view('become_a_reseller', []);
    }
    public function contact()
    {
        return $this->view('contact', []);
    }
    public function store()
    {
        return $this->view('store', []);
    }
    public function privacy_policy()
    {
        return $this->view('privacy_policy', []);
    }
    public function refund()
    {
        return $this->view('refund', []);
    }
    public function terms()
    {
        return $this->view('terms', []);
    }
    public function xe_currency()
    {
      $ch = curl_init(); 
      $headers = array(
        'Content-Type:application/json',
        'Authorization: Basic '.base64_encode("nexustechnology877529484:vi5tokvei4p4iit0aos74clpcp")
      );
      
      //convert_from.json/?from=MYR&to=BDT,GBP&amount=110.23
      curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
      curl_setopt($ch,CURLOPT_URL,"https://xecdapi.xe.com/v1/currencies");
      curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
 
      $result=curl_exec($ch);
      curl_close($ch);
      
      $list = json_decode($result);
      
      foreach($list->currencies as $key=>$value) {
        
        $query="INSERT INTO currency_converter (iso, currency_name, is_obsolete) VALUES('".$value->iso."', '".addslashes($value->currency_name)."', '".$value->is_obsolete."')"; 
        DB::insert($query);
      }

      exit;
    }
    
    public function xe_currency_daily()
    {
      $ch = curl_init(); 
      $headers = array(
        'Content-Type:application/json',
        'Authorization: Basic '.base64_encode("nexustechnology877529484:vi5tokvei4p4iit0aos74clpcp")
      );

      curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
      curl_setopt($ch,CURLOPT_URL,"https://xecdapi.xe.com/v1/convert_from.json/?from=MYR&to=BDT,CNH,CNY,HKD,SGD,IDR&amount=1");
      curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
 
      $result=curl_exec($ch);
      curl_close($ch);
      
      $list = json_decode($result);
 
      foreach($list->to as $key=>$value) {
        
            $query="UPDATE currency_converter SET rate='".$value->mid."' WHERE iso = '".$value->quotecurrency."'";
            DB::update($query);

      }

    }

}
