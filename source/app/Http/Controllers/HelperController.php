<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Webklex\PDFMerger\Facades\PDFMergerFacade as PDFMerger;

use App\User;

Trait HelperController
{
  public $limit = 100;
  public $more = 0;
  public $offset = 0;


  public function mergepdf($urls){ 


  $pdf = PDFMerger::init();
  // $urls = ["https://coded.axisdigitalleap.com/coded/public/dhl_label/210186/label_1.pdf","https://coded.axisdigitalleap.com/coded/public/dhl_label/210188/label_1.pdf"];

    foreach ($urls  as $u => $url){
  
      $file_name = basename($url);
      $pdf->addPDF($file_name,'all');
      $absolutePath = public_path().DIRECTORY_SEPARATOR.'airwayprint'.DIRECTORY_SEPARATOR.date("Ymd",  strtotime("Now")).DIRECTORY_SEPARATOR;
  
        if (!file_exists($absolutePath)) {
          mkdir($absolutePath, 0777, true);
        }

      $fileName = time().'.pdf';
      $pdf->merge();
      $pdf->save($absolutePath."/".$fileName);
    
      if ($pdf -> save()){
        $output = Response::download($absolutePath."/".$fileName);
      }
      return  $output;  
    }
    // printer list
      // $printerlists = printer_list(PRINTER_ENUM_LOCAL);
      // foreach ($printerlist as $printerlist) {
      // $printers = $printerlist["NAME"] . '</LI>';
      // }
  }


  public function showCategoryTree($internal = false, $user = false, $role = false)
    {
       $modules = new ModuleController();
    
       $menus = $modules->read($internal, $user, $role);
       //user
       $user = new UserController();
       $role = $user->loggedInUser();
       $controller = new Controller();
       

       foreach($menus as $menu) {

        //check if path contains role restriction
       if (str_contains($menu['url'], '{{role}}')) { 
       
        $menuUrl = $controller->site.str_replace('{{role}}', $role->role, $menu['url']);

        
        } else {
        $menuUrl = $controller->site.$menu['url'];

        }



        ?>
        <li>
         <a href='<?php echo $menuUrl;?>'>
            <?php echo (!empty($menu['icon']) ? $menu['icon']: '<div class="fonticon-wrap">
                                                <i class="bx bx-menu-alt-right"></i>
                                            </div>');?>&nbsp;<span class="menu-title text-truncate" data-i18n="Setup">
                <?php echo $menu['name'];?></span></a>
                <?php
                if (isset($menu['children'])) {
                    foreach($menu['children'] as $children) {


                        if (str_contains($children['url'], '{{role}}')) { 
                       
                  
                        $menuChildUrl = $controller->site.str_replace('{{role}}', $role->role, $children['url']);
                        
                        } else {
                        $menuChildUrl = $controller->site.$children['url'];

                        }

                            if (str_contains($children['url'], '{{id}}')) { 
                   
                            $menuChildUrl = $controller->site.str_replace('{{id}}', Auth::id(), str_replace('{{role}}', $role->role, $children['url']));
             
                           }
  

                    ?>
                    <ul class="menu-content">
                      <li>
                         <a href='<?php echo $menuChildUrl;?>'>
                            <?php echo (!empty($children['icon']) ? $children['icon']: '<div class="fonticon-wrap">
                                                <i class="bx bx-chevrons-right"></i>
                                            </div>');?>&nbsp;<span class="menu-title text-truncate" data-i18n="Setup">
                                <?php echo $children['name'];?>
                            </span>
                        </a>
                        <?php
                        //grand child 1
                            if (isset($children['grand-child-1'])) {

                                foreach($children['grand-child-1'] as $grandChild) {

                   

                                    if (str_contains($grandChild['url'], '{{role}}')) { 
                                   
                                    $menuChildUrl = $controller->site.str_replace('{{role}}', $role->role, $grandChild['url']);
                                    
                                    } else {
                                    $menuChildUrl = $controller->site.$grandChild['url'];

                                    }
              

                                ?>
                                <ul class="menu-content">
                                  <li>
                                     <a href='<?php echo $menuChildUrl;?>'>
                                        <?php echo (!empty($grandChild['icon']) ? $grandChild['icon']: '<div class="fonticon-wrap">
                                                            <i class="bx bx-chevrons-right"></i>
                                                        </div>');?>&nbsp;<span class="menu-title text-truncate" data-i18n="Setup">
                                            <?php echo $grandChild['name'];?>
                                        </span>
                                    </a>
                                 </li>
                                 </ul>
                                <?php
                                }
                            }
                            ?>
                     </li>
                     </ul>
                    <?php
                    }
                }
                ?>
              
        </li>
        <?php
       }
    }

       public function showProductCategoryTree($internal = false, $user = false, $role = false, $guest = false)
    {
       $modules = new ModuleController();
    
       $menus = $modules->cats($internal, $user, $role);


       $crole = 'admin';
       if(!$guest) {
         //user
        $user = new UserController();
        $role = $user->loggedInUser();
        $crole = $role->role;
       }
      
       $controller = new Controller();
       
       foreach($menus as $menu) {

        //check if path contains role restriction
       if (str_contains($menu['url'], '{{role}}')) { 
       
        $menuUrl = $controller->site.str_replace('{{role}}', $crole, $menu['url']);

        
        } else {
        $menuUrl = $controller->site.$menu['url'];

        }



        ?>
        
        <li>
          <div class="timeline-badge info"></div>
         <a href='/category?id=<?php echo $menu['name'];?>' class="<?php echo (isset($menu['children'])? 'has-arrow': ''); ?> text-muted" href="javascript:void()" aria-expanded="false">
            <?php echo (!empty($menu['icon']) ? $menu['icon']: '');?>
              
				  
				<span class="nav-text"><h6 class="mb-0"><?php echo $menu['name'];?></h6></span>
				</a>
                <?php
                if (isset($menu['children'])) {
                    foreach($menu['children'] as $children) {


                        if (str_contains($children['url'], '{{role}}')) { 
                       
                  
                        $menuChildUrl = $controller->site.str_replace('{{role}}', $crole, $children['url']);
                        
                        } else {
                        $menuChildUrl = $controller->site.$children['url'];

                        }

                            if (str_contains($children['url'], '{{id}}')) { 
                   
                            $menuChildUrl = $controller->site.str_replace('{{id}}', Auth::id(), str_replace('{{role}}', $crole, $children['url']));
             
                           }
  

                    ?>
                    <ul aria-expanded="false">
                      <li>
                         <a href='/category?id=<?php echo $children['name'];?>'  class="<?php echo (isset($children['grand-child-1'])? 'has-arrow': ''); ?> ai-icon" href="javascript:void()" aria-expanded="false">
                            <?php echo (!empty($children['icon']) ? $children['icon']: '<div class="fonticon-wrap">
                                                <i class="bx bx-chevrons-right"></i>
                                            </div>');?>&nbsp;<span class="menu-title text-truncate" data-i18n="Setup">
                                <?php echo $children['name'];?>
                            </span>
                        </a>
                        <?php
                        //grand child 1
                            if (isset($children['grand-child-1'])) {

                                foreach($children['grand-child-1'] as $grandChild) {

                   

                                    if (str_contains($grandChild['url'], '{{role}}')) { 
                                   
                                    $menuChildUrl = $controller->site.str_replace('{{role}}', $crole, $grandChild['url']);
                                    
                                    } else {
                                    $menuChildUrl = $controller->site.$grandChild['url'];

                                    }
              

                                ?>
                                <ul aria-expanded="false">
                                  <li>
                                     <a href='/category?id=<?php echo $grandChild['name'];?>' class="<?php echo (isset($grandChild['grand-child-2'])? 'has-arrow': ''); ?> ai-icon" href="javascript:void()" aria-expanded="false">
                                        <?php echo (!empty($grandChild['icon']) ? $grandChild['icon']: '<div class="fonticon-wrap">
                                                            <i class="bx bx-chevrons-right"></i>
                                                        </div>');?>&nbsp;<span class="menu-title text-truncate" data-i18n="Setup">
                                            <?php echo $grandChild['name'];?>
                                        </span>
                                    </a>
                                 </li>
                                 </ul>
                                <?php
                                }
                            }
                            ?>
                     </li>
                     </ul>
                    <?php
                    }
                }
                ?>
              
        </li>
        <?php
       }
    }
    

    public function getInternalApiUrl()
    {
      // return 'http://localhost/eas-git/web/index.php?r=coded/';
      // return 'http://localhost/cms/cms/web/index.php?r=coded/';
      // return 'http://dev.axisdigitalleap.asia/web/index.php?r=coded/';
      return 'http://ebx.axisdigitalleap.com/web/index.php?r=coded/';
    }

    public function makeCurl($api, $param = false, $url = false)
    {
      if (!$url) {
      $url = $this->getInternalApiUrl();

      }

      $cSession = curl_init(); 
      curl_setopt($cSession,CURLOPT_URL,"{$url}{$api}{$param}");
      curl_setopt($cSession,CURLOPT_RETURNTRANSFER,true);
      curl_setopt($cSession,CURLOPT_HEADER, false); 
      $result=curl_exec($cSession);
      curl_close($cSession);

      if ($api == 'getproductlistingbysme') {
        return $result;
      }
      if (!$url) {

      $data = json_decode($result);
      } else {
        $data = $result;
      }
      return $data;
    }

    public function makePostCurl($url, $hash, $body, $skipHeader = false)
    {

$body = json_encode($body);

      $ch = curl_init($url);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_POSTFIELDS, ['data'=>urlencode($body)]);

      // execute!
      $response = curl_exec($ch);
// echo "<pre>";
// var_dump($response);
// exit;
      // close the connection, release resources used
      curl_close($ch);

     return $response;
      // $headers = array(
      //   'Authorization: ' . $hash,
      //   'Content-Type: application/json; charset=utf-8'
      // );

      // $ch = curl_init($url);
      // curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
      // curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
      // // curl_setopt($ch, CURLOPT_HEADER, TRUE); // Includes the header in the output
      // curl_setopt($ch, CURLOPT_POST, TRUE);
      // curl_setopt($ch, CURLOPT_POSTFIELDS, $body);
      // $result = curl_exec($ch);
      // $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
      // curl_close($ch);

      // $data = ['data'=>$result, 'lastFetched'=>date('Y-m-d H:i:s')];
      // return $data;
    }

    public static function makeStaticCurl($api, $param = false)
    {
      $url = 'http://ebx.axisdigitalleap.com/web/index.php?r=coded/';
      $cSession = curl_init(); 
      curl_setopt($cSession,CURLOPT_URL,"{$url}{$api}{$param}");
      curl_setopt($cSession,CURLOPT_RETURNTRANSFER,true);
      curl_setopt($cSession,CURLOPT_HEADER, false); 
      $result=curl_exec($cSession);
      curl_close($cSession);
      $data = json_decode($result);

      return $data;
    }

    private function VerifyAddItemBody()
    {
      $body = new \stdClass();
      $body->Item = new \stdClass();
    
      $body->Item->ItemSpecifics = '';
      $body->Item->ApplicationData = 'TheCodedProject';
      $body->Item->AutoPay = 'true';
      $body->Item->ShipToLocations = 'US';
      $body->Item->Site = 'US';
      $body->Item->SiteId = '0';
      $body->Item->StartPrice = array(
        'currencyID'=>'USD',
        'StartPrice'=>'00.0'
      );
      $body->Item->Title = '';
      
      $body->Item->BuyerResponsibleForShipping = 'false';
      $body->Item->CategoryMappingAllowed = 'true';
      $body->Item->ConditionID = '1000';
      $body->Item->Country = 'MY';
      $body->Item->CrossBorderTrade = 'US';
      $body->Item->Currency = 'USD';
      $body->Item->Description = '';
      $body->Item->DisableBuyerRequirements = 'true';
      $body->Item->eBayPlus = 'false';
      $body->Item->ExtendedSellerContactDetails = new \stdClass();
      $body->Item->ExtendedSellerContactDetails->ClassifiedAdContactByEmailEnabled = 'true';
      $body->Item->ExtendedSellerContactDetails->ContactHoursDetails = new \stdClass();
      $body->Item->ExtendedSellerContactDetails->ContactHoursDetails->Hours1AnyTime = 'false';
      $body->Item->ExtendedSellerContactDetails->ContactHoursDetails->Hours1Days = 'None';
      $body->Item->ExtendedSellerContactDetails->ContactHoursDetails->Hours1From = '10:30:00';
      $body->Item->ExtendedSellerContactDetails->ContactHoursDetails->Hours1To = '17:00:00';
      $body->Item->ExtendedSellerContactDetails->ContactHoursDetails->Hours2AnyTime = 'false';
      $body->Item->ExtendedSellerContactDetails->ContactHoursDetails->Hours2Days = 'None';
      $body->Item->ExtendedSellerContactDetails->ContactHoursDetails->Hours2From = '10:30:00';
      $body->Item->ExtendedSellerContactDetails->ContactHoursDetails->Hours2To = '17:00:00';
      $body->Item->ExtendedSellerContactDetails->ContactHoursDetails->TimeZoneID = 'Asia/Kuala_Lumpur';


      $body->Item->HitCounter = 'NoHitCounter';
      $body->Item->IncludeRecommendations = 'false';
      
      $body->Item->ListingDuration = 'GTC';
      $body->Item->ListingType = 'FixedPriceItem';
      $body->Item->Location = 'Selangor';
      $body->Item->PaymentMethods = 'PayPal';
      // $body->Item->PayPalEmailAddress = '';
      $body->Item->PictureDetails = new \stdClass();
      $body->Item->PictureDetails->GalleryType = 'Gallery';
      $body->Item->PictureDetails->PictureURL = 'https://i.postimg.cc/BtWkHKXT/11-Lalisse-Lavender-Hand-Cream-70m-L-2pcs-Lalisse-Intense-Rose-Hand-Cream-70m-L-1pc-Clean-Pure-Manuk.jpg,https://i.postimg.cc/BtWkHKXT/11-Lalisse-Lavender-Hand-Cream-70m-L-2pcs-Lalisse-Intense-Rose-Hand-Cream-70m-L-1pc-Clean-Pure-Manuk.jpg,https://i.postimg.cc/BtWkHKXT/11-Lalisse-Lavender-Hand-Cream-70m-L-2pcs-Lalisse-Intense-Rose-Hand-Cream-70m-L-1pc-Clean-Pure-Manuk.jpg';

      $body->Item->PostalCode = '';
      $body->Item->PrimaryCategory =  new \stdClass();
      $body->Item->PrimaryCategory->CategoryID = '';
      $body->Item->PrivateListing = 'false';
      $body->Item->Quantity = '1';

      $body->Item->ReturnPolicy = new \stdClass();
      $body->Item->ReturnPolicy->Description = 'ReturnsAccepted';
      $body->Item->ReturnPolicy->InternationalRefundOption = 'MoneyBack';
      $body->Item->ReturnPolicy->InternationalReturnsAcceptedOption = 'ReturnsAccepted';
      $body->Item->ReturnPolicy->InternationalReturnsWithinOption = 'Days_30';
      $body->Item->ReturnPolicy->InternationalShippingCostPaidByOption = 'Seller';
      $body->Item->ReturnPolicy->RefundOption = 'MoneyBack';
      $body->Item->ReturnPolicy->ReturnsAcceptedOption = 'ReturnsAccepted';
      $body->Item->ReturnPolicy->ReturnsWithinOption = 'Days_30';
      $body->Item->ReturnPolicy->ShippingCostPaidByOption = 'Buyer';

      // $body->Item->ShippingDetails = new \stdClass();
      // $body->Item->ShippingDetails->GlobalShipping = 'true';


      $body->Item->SellerProfiles = new \stdClass();
      $body->Item->SellerProfiles->SellerPaymentProfile = new \stdClass();
      $body->Item->SellerProfiles->SellerPaymentProfile->PaymentProfileID = '184096176010';
      $body->Item->SellerProfiles->SellerPaymentProfile->PaymentProfileName = 'PayPal Immediate Pay';

      $body->Item->SellerProfiles->SellerReturnProfile = new \stdClass();
      $body->Item->SellerProfiles->SellerReturnProfile->ReturnProfileID = '184096401010';
      $body->Item->SellerProfiles->SellerReturnProfile->ReturnProfileName = 'Return Policy';

      $body->Item->SellerProfiles->SellerShippingProfile = new \stdClass();
      $body->Item->SellerProfiles->SellerShippingProfile->ShippingProfileID = '184097426010';
      $body->Item->SellerProfiles->SellerShippingProfile->ShippingProfileName = 'Expedited Shipping';


      return $body;

    }

    public function actionBtn($action, $url)
    {
        $user = new UserController(); 
        $role = $user->loggedInUser();

        switch ($action) {
            case 'generate':
                return '<a  href="/backend'.$this->site.$role->role.$url.'"  class="dropdown-item" target="_blank"> <i class="
bx bx-plus-medical text-success"></i>&nbsp;<span class="badge badge-light-success">Generate</span></a>';
                break;
            case 'stockin':
                return '<a  href="/backend'.$this->site.$role->role.$url.'"  class="dropdown-item" target="_blank"> <i class="
bx bx-plus-medical text-success"></i>&nbsp;<span class="badge badge-light-success">Stock In</span></a>';
                break;
            case 'download2':
                return '<a href="/backend'.$this->site.$role->role.$url.'" class="dropdown-item  badge-light-success"> <i class="bx bx-download text-success"></i>&nbsp;<span class="badge badge-light-success">Download2</span></a>';
                break;
            case 'download':
                return '<a href="/backend'.$this->site.$role->role.$url.'" class="dropdown-item  badge-light-success"><i class="bx bx-download text-success"></i>&nbsp;<span class="badge badge-light-success">Download</span></a>';
                break;
            case 'update':
                return '<a href="/backend'.$this->site.$role->role.$url.'" class="dropdown-item  badge-light-warning" id="update" target="_blank"><i class="bx bx-pencil text-warning"></i>&nbsp;<span class="badge badge-light-warning">Update</span></a>';
                break;
            case 'changelog':
                return '<a href="/backend'.$this->site.$role->role.$url.'" class="dropdown-item  badge-light-info" target="_blank"><i class="fas fa-history fa-lg text-info"></i>&nbsp;<span class="badge badge-light-info">Changelog</span></a>';
                break;
            case 'delete':
                return '<a href="/backend'.$this->site.$role->role.$url.'" class="dropdown-item delete" target="_blank">Delete</a>';
                break;
            case 'edit':
                return '<a href="/backend'.$this->site.$role->role.$url.'" class="dropdown-item" target="_blank">Edit</a>';
                break;
            case 'upload':
                return '<a href="/backend'.$this->site.$role->role.$url.'" class="dropdown-item" target="_blank"><i class="fas fa-upload fa-lg text-info"></i>&nbsp;<span class="badge badge-light-info">Upload</span></a>';
                break;
            case 'clone':
                return '<a href="/backend'.$this->site.$role->role.$url.'"class="dropdown-item justify-content-between " target="_blank">Clone</a>';
                break;
            case 'view':
                if (str_contains($url, 'http')) { 
                   return $url;
                } else {
                    return '<a href="/backend'.$this->site.$role->role.$url.'" class="btn btn-primary btn-sm" target="_blank">View</a>';

                }
                break;
            case 'airway':
                if (str_contains($url, 'http')) { 
                   
                    return '<a href="'.$url.'" class="btn btn-primary btn-sm" target="_blank">Airway Bill</a>';
                } else {
                    return '<a href="/backend'.$this->site.$role->role.$url.'" class="btn btn-primary btn-sm" target="_blank">Airway Bill</a>';

                }
                break;

            default:
                // code...
                break;
        }
    }

    public function actionCol($actions)
    {
        ob_start();
        ?>
        <div class="dropdown my-auto">
            <i class="bx bx-dots-vertical-rounded font-medium-3 cursor-pointer dropdown-toggle nav-hide-arrow cursor-pointer" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" role="menu">
            </i>
            <span class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
            <?php 
            echo  $actions;
            ?>
            </span>
        </div>
        <?php
        $html = ob_get_contents();
        ob_end_clean();
        return $html;
    }

    public function paginate($query, $param, $offset, $limit = false, $orderBy = false, $order = false)
    {
        $output = [];
        $defaultParam = 'count(*) as count';
        $sort = '';

        if ($limit) {
          $this->limit = $limit;
        }

        if ($orderBy) {
            $sort = "ORDER BY $orderBy $order";
        }

        $query = str_replace('[param]', $defaultParam, $query);
        $totalCount = DB::select($query);
        $totalCount = $totalCount[0]->count;
       
        //replace param
        $query = str_replace($defaultParam, $param, $query);

        $query .=" $sort LIMIT $this->offset, $this->limit";

        $users = DB::select($query);


        //loaded
        $loadedRows = $this->offset + $this->limit;

        $rowCount =+ $this->offset;


        //balance rows
        $balance = $totalCount - $loadedRows;

        if ($balance > 0) {
            $this->more = 1;
        }

        $output['totalRows'] = $totalCount;
        $output['result'] = $users;
        $output['more'] = $this->more;
        $output['rowCount'] = $rowCount;
        $output['offset']= $loadedRows;

        return $output;
    }


    public function setRow($identifier, $data, $count, $trClass = false)
    {

        if (is_array($identifier)) {
            $id = $identifier[0];
            $checked = ($identifier[1] === 1? 'checked': 'no');
        } else {
             $checked = '';
             $id = $identifier;
        }

        ob_start();
        ?>
         <tr class="<?php echo $trClass;?> parent-row">
            <td valign="top">
                <input class="data-check-num" name="data[]" id="ship_<?php echo $id ?>" value="<?php echo $id ?>" type="checkbox" <?php echo $checked;?>>
                 <label class="data-check-num" for="ship_<?php echo $id ?>"><?php echo $count; ?></label>
            </td>

          <?php
          $totalColumn = sizeof($data);

          for($i = 0; $i < $totalColumn;$i++) {
            ?>
            <td valign="top"><?php echo $data[$i];?></td>
            <?php

          }?>
        </tr>
        <?php

        $html = ob_get_contents();
        ob_end_clean();
        return $this->minify_html($html);
    }

    
public function setGrid($identifier, $data, $count, $trClass = false)
    {

        if (is_array($identifier)) {
            $id = $identifier[0];
            $checked = ($identifier[1] === 1? 'checked': 'no');
        } else {
             $checked = '';
             $id = $identifier;
        }


        ob_start();

          $totalColumn = 1;//sizeof($data);
       
          for($i = 0; $i < $totalColumn;$i++) {
            ?>
            <td id="<?php echo $id;?>">
                <div class="card card-bg">
                    <div class="card-body">
                        <div class="media pt-3 pb-3">
                            <img src="<?php echo $data[5];?>" alt="image" class="mr-3 rounded" width="<?php echo $data[6];?>">
                            <div class="media-body row">
                               
                                   
                                
                                <div class="col-9">
                                    <h5 class="m-b-5 comic" data-item-name><?php echo $data[0];?></h5>
                                     
                                    <?php if(!empty($data[1])) {
                                        ?>
                                        <input type="hidden" data-item-price value="<?php echo $data[1];?>">
                                        <p class="mb-0"><span class="price">RM<?php echo $data[1];?></span></p>
                                        <?php
                                     }
                                     ?>
                                </div>
                                <div class="col-3">
                                    <div class="shopping-cart" data-cart-add>
                                       <a href="javascript:void(0)">
                                           <img src="/asset/home/buy.png" class="buy">
                                          </a>
                                     </div>
                                </div>
                                
                                    
                            </div>
                        </div>
                    </div>
                </div>
            </td>
            <?php

          }
          

        $html = ob_get_contents();
        ob_end_clean();
        return $this->minify_html($html);
    }



     public function setGridBlog($identifier, $data, $count, $trClass = false)
    {



        if (is_array($identifier)) {
            $id = $identifier[0];
            $checked = ($identifier[1] === 1? 'checked': 'no');
        } else {
             $checked = '';
             $id = $identifier;
        }

        ob_start();

          $totalColumn = 1;//sizeof($data);
       
          for($i = 0; $i < $totalColumn;$i++) {
            ?>

            <td>
                <div class="card">
                <div class="card-body">
                <div class="row88">
                    <div class="col-md-12">
                        <div class="new-arrival-product mb-4 mb-xxl-4 mb-md-0">
                            <div class="new-arrivals-img-contnent">
                                <a href="?id=<?php echo $id;?>">
                                <img class="img-fluid" src="http://online.printdepot.com.my/asset/front/images/product/2.jpg" alt="">
                            </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="new-arrival-content position-relative">
                            <h4><a href="?id=<?php echo $id;?>"><?php echo $data[0];?></a></h4>
                            <div class="comment-review star-rating">
                                <ul>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star-half-empty"></i></li>
                                    <li><i class="fa fa-star-half-empty"></i></li>
                                </ul>
                                <span class="review-text">(34 reviews) / </span><a class="product-review" href=""  data-toggle="modal" data-target="#reviewModal">Write a review?</a>
                              
                            </div>
                
                            <p>Author: <span class="item">Lee</span></p>
                        </div>
                    </div>
                </div>
                </div>
                </div>
            </td>
            <?php

          }?>
      
        <?php

        $html = ob_get_contents();
        ob_end_clean();
        return $this->minify_html($html);
    }

    public function setRowP($identifier, $data, $count, $trClass = false)
    {

        if (is_array($identifier)) {
            $id = $identifier[0];
            $checked = ($identifier[1] === 1? 'checked': 'no');
        } else {
             $checked = '';
             $id = $identifier;
        }

        ob_start();
        ?>
         <tr data-child="1" class="child-row">
            <td valign="top">
                <input  id="ship_<?php echo $id ?>" value="<?php echo $id ?>" type="checkbox" <?php echo $checked;?>>
                 <label for="ship_<?php echo $id ?>"><?php echo $count; ?></label>
            </td>

          <?php
          $totalColumn = sizeof($data);

          for($i = 0; $i < $totalColumn;$i++) {
            ?>
            <td style="background-color: yellow !important;" colspan="<?php echo $totalColumn-1;?>"><?php echo $data[$i];?></td>
            <?php

          }?>
        </tr>
        <?php

        $html = ob_get_contents();
        ob_end_clean();
        return $this->minify_html($html);
    }

    public function setRow5($identifier, $data, $count, $trClass = false)
    {

        if (is_array($identifier)) {
            $id = $identifier[0];
            $checked = ($identifier[1] === 1? 'checked': 'no');
        } else {
             $checked = '';
             $id = $identifier;
        }

        ob_start();
        ?>
         <tr class="<?php echo $trClass;?>">
            <td valign="top">
                <input class="data-check-num" name="data[]" id="ship_<?php echo $id ?>" value="<?php echo $id ?>" type="checkbox" <?php echo $checked;?>>
                 <label class="data-check-num" for="ship_<?php echo $id ?>"><?php echo $count; ?></label>
            </td>

          <?php

          $totalColumn = sizeof($data);

          for($i = 0; $i < $totalColumn;$i++) {
            if ($i == 0) {
                ?>
                 <td><?php echo $data[$i];?></td>
                <?php
            } else {
                
                echo $data[$i];
          
            }

          }?>
        </tr>
        <?php

        $html = ob_get_contents();

        ob_end_clean();
        return $this->minify_html($html);
    }

   

     //Viewing the userList
    private function minify_html($html)
    {
        $search = array(
            '/(\n|^)(\x20+|\t)/',
            '/(\n|^)\/\/(.*?)(\n|$)/',
            '/\n/',
            '/\<\!--.*?-->/',
            '/(\x20+|\t)/', # Delete multispace (Without \n)
            '/\>\s+\</', # strip whitespaces between tags
            '/(\"|\')\s+\>/', # strip whitespaces between quotation ("') and end tags
            '/=\s+(\"|\')/'
        ); # strip whitespaces between = "'
        $replace = array(
            "\n",
            "\n",
            " ",
            "",
            " ",
            "><",
            "$1>",
            "=$1"
        );

        $html = preg_replace($search, $replace, $html);
        return $html;
    }

    public function formatProducts($products, $marketplace, $declaredValue = false)
    {
        $formatProducts = [];

        if (sizeof($products) > 0)
        {
            foreach ($products as $key => $product)
            {

                switch ($marketplace) {
                    case 'Lazada':
                        if ($product->sku == '')
                        {
                            return;
                        }
                        $sku = $product->sku;
                        $listingName = $product->name;
                        $listingUrl = $product->product_detail_url;
                        //lazada API doesn't have quantity
                        $quantity = 1;

                        break;
                    case 'Shopee':
                        

                        if (!isset($product->item_sku)  && !isset($product->variation_sku) && !isset($product->model_sku))
                        {
                          return;

                        }
                        $sku = (!empty($product->variation_sku) ? $product->variation_sku: $product->item_sku);
                        if ($sku == '') {
                            $sku = $product->model_sku;
                        }
                        $listingName = $product->item_name;
                        $listingUrl = 'http://';
                        $quantity = $product->model_quantity_purchased;
                        

                        break;
                    default:
                        // code...
                        break;
                }


                //check if package
                if ((substr($sku, 0, strlen('PKG')) === 'PKG') === true)
                {
                    $type = 'package';
                    $query="SELECT v.sku,  'package' as type,p.brand_id,p.price,ppl.price as variant_price,ppl.total_price as variant_total_price, ppl.variant_id,ppl.quantity,p.id AS package_id FROM axis_package p INNER JOIN axis_package_product_list ppl ON p.id=ppl.package_id INNER JOIN axis_product v ON ppl.variant_id=v.id WHERE (p.sku = '".trim($sku)."' || p.manufacturer_code = '".trim($sku)."') AND p.status = 1";
                
                    $items = DB::select($query);


                } else {

                    $type = 'product';
                    $query="SELECT p.id as variant_id, p.name as product_name, 'product' as type, 1 as quantity, p.photo, p.selling_price as variant_price, p.sku, b.id as brand_id FROM `axis_product` p INNER JOIN axis_brand b ON p.brand_id = b.id WHERE (p.sku = '".trim($sku)."' || p.manufacturer_code = '".trim($sku)."') AND p.status = 1";

                    $items = DB::select($query);

                }

                if (sizeof($items) <= 0)
                {

                    return false;
                }
                
                if ($declaredValue > 0) {
                $declaredValuePerItem = $declaredValue/ (int)sizeof($items);

                } else {
                    $declaredValuePerItem = 0;
                }

                foreach ($items as $key => $value) {


                    //format product array
                    $formatProducts[] = array(
                        'po_product_id' => $value->variant_id,
                        'po_brand_id' => $value->brand_id,
                        'po_product_quantity' => ($quantity * $value->quantity),
                        'po_listing_title' => $listingName,
                        'po_listing_url' => $listingUrl,
                        'po_selling_mode' => 'Buy It Now',
                        'po_final_selling_price' => '',
                        'po_currency' => 'MYR',
                        'po_product_price' => $value->variant_price,
                        'po_stock_mode' => 'company',
                        'po_declare_value' => $declaredValuePerItem,
                        'po_status' => 'pending',
                        'po_type' => $type,
                        'po_sku'=> $sku,
                        'po_date' => date('d-m-Y')
                    );
                }

            

            }

            return $formatProducts;
        }

    }


    private function getMv2UserID($userID)
    {
        $query="SELECT mv2_userid FROM axis_user_detail WHERE user_id = $userID";
        $data = DB::select($query);
        return $data[0]->mv2_userid;

    }

    private function getShopID($shop)
    {
        $query="SELECT axis_shop_id FROM ebays WHERE account = '".$shop."'";
        $data = DB::select($query);
        return $data[0]->axis_shop_id;

    }

    private function getFreeGift($shop)
    {
        $formatProducts = [];

        $query="SELECT free_gift FROM ebays WHERE account = '".$shop."'";
        $data = DB::select($query);
        $freeGift = $data[0]->free_gift;
        if ($freeGift) {
        $type = 'free';
        $query="SELECT p.id as variant_id, p.name as product_name, 'product' as type, 1 as quantity, p.photo, p.selling_price as variant_price, p.sku, b.id as brand_id FROM `axis_product` p INNER JOIN axis_brand b ON p.brand_id = b.id WHERE sku = '".$freeGift."'";

        $item = DB::select($query);

        if (sizeof($item) > 0) {
            $item = $item[0];
            $formatProducts = array(
                'po_product_id' => $item->variant_id,
                'po_brand_id' => $item->brand_id,
                'po_product_quantity' => $item->quantity,
                'po_listing_title' => $item->product_name,
                'po_listing_url' => 'https://',
                'po_selling_mode' => 'Buy It Now',
                'po_final_selling_price' => '',
                'po_currency' => 'MYR',
                'po_product_price' => 0,
                'po_stock_mode' => 'company',
                'po_declare_value'=>0,
                'po_status' => 'pending',
                'po_type' => $type,
                'po_sku'=> $freeGift,
                'po_date' => date('d-m-Y')
            );
        }
        }


        return $formatProducts;

    }

    public function getMarketplaceID($marketplace = false)
    {
        $query = '';
        if ($marketplace) {
            $query .= "AND name = '$marketplace'";
        }
        $query = "SELECT * FROM axis_marketplace WHERE status = 1 $query";
        $list = DB::select($query);

       if (isset($marketplace)) {
        return $list[0];
       }

       return $list;
    }

    private function submitInvoice($invoice, $marketplace, $shop)
    {
        // if ($this->duplicateChecking($invoice['invoice_orderid'])) {

            $invoiceID = $this->insertInvoice($invoice);

            $this->insertShipment($invoice, $invoiceID);
            $this->insertProducts($invoice, $invoiceID);
        // }
    }

    private function duplicateChecking($orderID)
    {
        $query = "SELECT COUNT(*) as found FROM axis_invoice WHERE order_id = '".$orderID."' AND shipment_status ='completed'";
        $duplicate = DB::select($query);
        if ($duplicate[0]->found > 0) {
            return false;
        }
    }

    private function insertInvoice($invoice)
    {
    
        $query = "INSERT INTO axis_invoice (
                uid,
                marketplace,
                shipment_date,
                shipping_mode,
                shipment_status,
                shipment_mode,
                weight,
                shipping_fee,
                shipping_cost,
                currency,
                final_selling_price,
                admin_remark,
                order_id,
                date,
                submit_time,
                package_id,
                airwaybill_url
            )VALUES(
                '" . $invoice['invoice_uid'] . "','" 
                . $invoice['invoice_marketplace'] . "','" 
                . $invoice['invoice_shipment_date'] . "',
                '" . $invoice['invoice_shipping_mode'] . "','" 
                . $invoice['invoice_shipment_status'] . "','" 
                . $invoice['invoice_shipment_mode'] . "',
                '" . $invoice['invoice_weight'] . "','" 
                . $invoice['invoice_shipping_fee'] . "','" 
                . $invoice['invoice_shipping_cost'] . "','" 
                . $invoice['invoice_currency'] . "',
                '" . $invoice['invoice_final_selling_price'] . "','" 
                . $invoice['invoice_admin_remark'] . "',
                '" . $invoice['invoice_orderid'] . "','" 
                . $invoice['invoice_date'] . "','" 
                . $invoice['invoice_submit_time'] . "','" 
                . $invoice['invoice_package_id'] . "','" 
                . $invoice['airway'] . "'
            )";
            DB::insert($query);
            $invoiceID = DB::connection()->getPdo()->lastInsertId();
            return $invoiceID;

    }

    private function insertShipment($invoice, $invoice_id)
    {
         $query = "INSERT INTO axis_shipment (
            invoice_id,ebay_id,customer_name,
            customer_address1,customer_address2,city,
            state,postcode,country,
            customer_contact,customer_email,member_remark,
            status,date
        )VALUES(
            '" . $invoice_id . "','" . $invoice['shipment_ebay_id'] . "','" . addslashes(utf8_encode($invoice['shipment_customer_name'])) . "',
            '" . addslashes($invoice['shipment_customer_address1']) . "','" . addslashes($invoice['shipment_customer_address2']) . "','" . addslashes($invoice['shipment_city']) . "',
            '" . $invoice['shipment_state'] . "','" . $invoice['shipment_postcode'] . "','" . $invoice['shipment_country'] . "',
            '" . $invoice['shipment_customer_contact'] . "','" . $invoice['shipment_customer_email'] . "','Via API',
            '" . $invoice['shipment_status'] . "','" . $invoice['shipment_date'] . "'
        )";

        DB::insert($query);
    }
    private function insertProducts($invoice, $invoiceID)
    {

       if ($invoice['product_arr']) {
        foreach ($invoice['product_arr'] as $key => $product) {
           
           $query = "INSERT INTO axis_product_order (
                    invoice_id,
                    product_id,
                    type,
                    sku,
                    brand_id,
                    product_quantity,
                    listing_title,
                    listing_url,
                    selling_mode,
                    final_selling_price,
                    currency,
                    product_price,
                    stock_mode,
                    declare_value,
                    status,
                    date
                )VALUES(
                    '" . $invoiceID . "','" 
                    . $product['po_product_id'] . "','" 
                    . $product['po_type'] . "','" 
                    . $product['po_sku'] . "','" 
                    . $product['po_brand_id'] . "',
                    '" . $product['po_product_quantity'] . "','" 
                    . addslashes($product['po_listing_title']) . "','" 
                    . addslashes($product['po_listing_url']) . "',
                    '" . $product['po_selling_mode'] . "','" 
                    . $product['po_final_selling_price'] . "','" 
                    . $product['po_currency'] . "',
                    '" . $product['po_product_price'] . "','" 
                    . $product['po_stock_mode'] . "','" 
                    . $product['po_declare_value'] . "','" 
                    . $product['po_status'] . "','" 
                    . $product['po_date'] . "'
                )";
            DB::insert($query);

        }
       }

    }

    private function submitToMv2($invoice, $marketplace, $shop)
    {

         $dataPushCompile = http_build_query($invoice, '', '&');
         $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, 'https://axisnet.asia/memberv2/API/addorders.php');
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $dataPushCompile);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            $data = curl_exec($ch);
            curl_close($ch);

        $output = json_decode($data, true);

        // if ($output['status'] == 'success')
        // {
        //     $invoiceID = $output['invoice_id'];

        //     //keep the record in coded
        //     $ebay = new \App\shippedOrder;
        //     $ebay->invoice_id = $invoiceID;
        //     $ebay->order_id = $invoice['invoice_orderid'];
        //     $ebay->marketplace = $marketplace;
        //     $ebay->shop = $shop;
        //     $ebay->created_by = Auth::id();
        //     $ebay->save();
        // }
    }

    private function formatOrder($order, $shop, $userID, $products, $marketplace, $logistic, $airway, $countryCode = false)
    {
        $address = [];
       
       switch ($marketplace) {
           case 'Lazada':

           //address
                foreach ($order->address_shipping as $key => $value) {
                    if ($value != '') {
                        $address[$key] = $value;
                    }
                }
                $address['customer_name'] = $order->address_shipping->first_name;
                $address['full_address']  = $address['address1'].','.$address['city'].','.$address['address3'].','.$address['post_code'].','.$address['country'];
                $address['city'] = $order->address_shipping->city;
                $address['state'] = $order->address_shipping->address4;
                $address['zipcode'] = $order->address_shipping->post_code;
                $address['country'] = $countryCode;//$order->address_shipping->country;
                $address['phone'] = $order->address_shipping->phone;

                //order id 
                $orderID = $order->order_number;
                $total = $order->price;


               break;
            case 'Shopee':

                $address['customer_name'] = $order->recipient_address->name;
                $address['full_address'] = $order->recipient_address->full_address;
                $address['city'] = $order->recipient_address->city;
                $address['state'] = $order->recipient_address->state;
                $address['zipcode'] = $order->recipient_address->zipcode;
                $address['country'] = ($order->currency == 'MYR'? 'MY':'unknown');
                $address['phone'] = $order->recipient_address->phone;

                $orderID = $order->order_sn;
                $total = $order->total_amount;


               break;
           default:
               // code...
               break;
       }
        

       if ($products) {
         //assign free gift
        $freeGift = $this->getFreeGift($shop);

        if (sizeof($freeGift) > 0) {
            array_push($products, $freeGift);
        }
       }
       

        return array(
            'invoice_uid' => $this->getMv2UserID($userID),
            'invoice_marketplace' => $marketplace,
            'invoice_shipment_date' => date('d-m-Y') ,
            'invoice_shipping_mode' => $logistic,
            'invoice_shipment_status' => 'pending',
            'invoice_shipment_mode' => 'company',
            'invoice_weight' => '0',
            'invoice_shipping_fee' => '0.00',
            'invoice_shipping_cost' => '0.00',
            'invoice_currency' => 'MYR',
            'invoice_final_selling_price' => $total,
            'invoice_admin_remark' => 'via API',
            'invoice_orderid' => $orderID,
            'invoice_package_id' => '',
            'invoice_date' => date('d-m-Y') ,
            'invoice_submit_time' => date("h:i:sa") ,
            'shipment_ebay_id' => $this->getShopID($shop),
            'shipment_customer_name' => $address['customer_name'],
            'shipment_customer_address1' => $address['full_address'],
            'shipment_customer_address2' => $address['full_address'],
            'shipment_city' => $address['city'],
            'shipment_state' => $address['state'],
            'shipment_postcode' => $address['zipcode'],
            'shipment_country' => $address['country'],
            'shipment_customer_contact' => $address['phone'],
            'shipment_customer_email' => '',
            'shipment_status' => 'pending',
            'shipment_date' => date('d-m-Y') ,
            'product_arr' => $products,
            'airway' => $airway
        );
    }


     public function getShipmentStatus_temp($orderID)
    {

        $dataPushCompile = ['order_id' => $orderID ];
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://axisnet.asia/memberv2/API/getorderstatus_temp.php');
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $dataPushCompile);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $data = curl_exec($ch);
        curl_close($ch);
        

        $invoiceStatus = json_decode(rtrim($data, "'"));
        
        $output = [];
        if ($invoiceStatus) {
            foreach ($invoiceStatus as $key => $value) {
                $output = (array)$value;
            }
        }
        return $output;
    }
     public function getShipmentStatus($orderID)
    {
        //one order might be deleted and re-created, so take latest status
        $query = "SELECT shipment_status as status, tracking_code, id as invoice_id FROM axis_invoice WHERE order_id = $orderID ORDER BY id DESC LIMIT 1";
        $data =  DB::select($query);
        return $data;
    }
}
