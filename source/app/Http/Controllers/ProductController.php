<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class ProductController extends Controller
{
     public function productHome(Request $request, $internal = false, $guest = false)
    {
        
        return redirect('/category');

        $extra = '';
        $id = $request->query('id');
        $input = $request->all();
        $backend = $request->query('backend');
        if ($id > 0) {

            $query = "SELECT p.*, pp.sale_price, pp.start_date, pp.end_date, b.name as brand FROM product p LEFT JOIN product_promo pp ON p.id = pp.product_id LEFT JOIN brand b ON p.brand_id = b.id WHERE p.id = $id";
            $product = DB::select($query);
            
            //images

            if(($product[0]->featured_pic != '') && ($product[0]->gallery != '')) {
  
               
                $images = array_merge(explode(',', $product[0]->featured_pic), explode(',', $product[0]->gallery));

            } else if($product[0]->featured_pic != '') {
                $images = [$product[0]->featured_pic];
            } else {
                $images = [];
            }

            $query = "SELECT v.*, v.name as variation, vp.* FROM variation_value v LEFT JOIN variation_promo vp ON v.id = vp.variation_id WHERE product_id = ".$product[0]->id;

            $variation = DB::select($query);

            //category
            $query="SELECT id,name FROM category WHERE id IN(".$product[0]->category_id.")";
            $category = DB::select($query);


            $variation2 = [];
         
            foreach ($variation as $key => $value) {
                $variation2[$value->variation][] = $value;
            }

                 return $this->view('front.product.product',
                    [   
                        'guest'=>$guest,
                        'product'=>$product[0],
                        'category'=>$category,
                        'save'=>'/checkout',
                        'images'=>$images,
                        'variation'=>$variation2]);
            }
        $query = "SELECT * FROM product";

        $product = DB::select($query);
        

        if($internal) {
            return $product;
        }

        $cat = [];
        //category
            $query="SELECT id,name FROM category WHERE parent = 0 AND status = 1";
            $category = DB::select($query);
            
            foreach ($category as $k => $v) {
                $cat[] = ['id'=>$v->id, 'name'=>$v->name];
            }

        $view = 'product.view';
        $thead = ['name', 'regular', 'sale', 'start', 'end','sub category', 'variation', 'action'];
            $add_title = 'ADD PRODUCT';
            $search = 1;
            $keys = [$cat];
         
         return $this->view($view,
        [
            'add'=>(isset($add)? $add: ''),
            'guest'=>1,
            'main_title'=>'Product',
            'bread'=>[['#'=>'home'], ['#'=>'shop'], ['#'=>'product']],
            'add_title'=>$add_title,
            'product'=>$product,
            'vendor'=>[],
            'thead'=>$thead,
            'search'=>$search,
            'keys'=>$keys]);
    }
    
     public function product(Request $request, $internal = false, $guest = false)
    {

        $extra = '';
        $id = $request->query('id');
        $input = $request->all();
        $backend = $request->query('backend');
        if ($id > 0) {

            $query = "SELECT p.*, pp.sale_price, pp.start_date, pp.end_date, b.name as brand FROM product p LEFT JOIN product_promo pp ON p.id = pp.product_id LEFT JOIN brand b ON p.brand_id = b.id WHERE p.id = $id";
            $product = DB::select($query);
            
            //images

            if(($product[0]->featured_pic != '') && ($product[0]->gallery != '')) {
  
               
                $images = array_merge(explode(',', $product[0]->featured_pic), explode(',', $product[0]->gallery));

            } else if($product[0]->featured_pic != '') {
                $images = [$product[0]->featured_pic];
            } else {
                $images = [];
            }

            $query = "SELECT v.*, v.name as variation, vp.* FROM variation_value v LEFT JOIN variation_promo vp ON v.id = vp.variation_id WHERE product_id = ".$product[0]->id;

            $variation = DB::select($query);

            //category
            $query="SELECT id,name FROM category WHERE id IN(".$product[0]->category_id.")";
            $category = DB::select($query);
            

            $variation2 = [];
         
            foreach ($variation as $key => $value) {
                $variation2[$value->variation][] = $value;
            }

                // if ($guest) {

                //     return $this->view('product.product',
                //     [   
                //         'guest'=>$guest,
                //         'product'=>$product[0],
                //         'variation'=>$variation]);
                // } else {
                //      return $this->view('front.product.product',
                //     [   
                //         'guest'=>$guest,
                //         'product'=>$product[0],
                //         'variation'=>$variation]);
                // }
                 return $this->view('front.product.product',
                    [   
                        'guest'=>$guest,
                        'product'=>$product[0],
                        'category'=>$category,
                        'save'=>'http://online.printdepot.com.my/checkout',
                        'images'=>$images,
                        'variation'=>$variation2]);
            }
        $query = "SELECT * FROM product";

        $product = DB::select($query);

        if($internal) {
            return $product;
        }
        
        $query="SELECT p.id, p.featured_pic, p.gallery,p.name, p.category_id, p.brand_id, p.regular_price, pp.sale_price, pp.start_date, pp.end_date, c.parent as parent_id, c.name as category_name FROM product p LEFT JOIN brand b ON b.id = p.brand_id LEFT JOIN vendor v ON v.id = b.vendor_id LEFT JOIN product_promo pp ON pp.product_id = p.id LEFT JOIN category c ON c.id = p.category_id  WHERE 1 = 1 $extra ORDER BY p.regular_price ASC";

        $allproducts = DB::select($query);
        
         $lang = app()->getLocale();
    
        if($lang != 'my' && $lang != 'en') {
           
                    //conversion
            $query="SELECT rate FROM currency_converter WHERE lang = '".$lang."'";
         
            $rate = DB::select($query);
            $rate = $rate[0]->rate;
        } else {
           
            $rate = 1;
        }
        

 
        $cat = [];
        //category
            $query="SELECT id,name FROM category WHERE parent = 0 AND status = 1";
            $category = DB::select($query);
            
            foreach ($category as $k => $v) {
                $cat[] = ['id'=>$v->id, 'name'=>$v->name];
            }


        if (!$guest) {

            $user = new UserController(); 
            $role = $user->loggedInUser();
        

         $add = '/backend'.$this->site.$role->role.'/product?action=add';
        }

        if (!$guest) {
            $view = 'product.view';
        } else {
            $view = 'front.product.view';

        }
         

         if (!$guest) {
            $thead = ['name', 'regular', 'sale', 'start', 'end','sub category', 'variation', 'action'];
            $add_title = 'ADD PRODUCT';
            $search = 1;
            $keys = [$cat];
         } else {
            $thead = ['name'];
            $add_title='';
             $search = 0;
             $keys = ['all'];
         }
         
         
         $query="SELECT * FROM category";
    
         $category = DB::select($query);

         return $this->view($view,
        [
            'add'=>(isset($add)? $add: ''),
            'guest'=>$guest,
            'main_title'=>'Packages',
            'bread'=>[['#'=>'home'], ['#'=>'shop'], ['#'=>'product']],
            'add_title'=>$add_title,
            'product'=>$product,
            'products'=>$allproducts,
            'vendor'=>[],
            'thead'=>$thead,
            'search'=>$search,
            'rate'=>$rate,
            'category'=>$category,
            'keys'=>$keys]);
    }

    public function product_post_4(Request $request, $internal = false)
    {

        $extra = '';
        $id = $request->query('id');
        $input = $request->all();

        ProcessRequests::dispatch('test', $input, 0,100);
        ProcessRequests::dispatch('test2', $input, 100,100);
        ProcessRequests::dispatch('test3', $input, 200,100);
        ProcessRequests::dispatch('test4', $input, 300,100);
        ProcessRequests::dispatch('test5', $input, 400,100);
        ProcessRequests::dispatch('test6', $input, 500,100);




// $url = 'http://localhost/nexus/admin/shop/brand';
// $nbPages = 30;

// $responses = Http::pool(function (Pool $pool) use ($url, $nbPages) {
//     return collect()
//         ->range(1, $nbPages)
//         ->map(fn ($page) => $pool->get($url . "?page={$page}"));
// });

// var_dump($responses);
        // $this->limit = 100;
        //  $offsetStr = '';
        //  $extra = "";
        //  if (isset($input['offset'])) {
        //     $this->offset = $input['offset']; 
        //  } else {
        //     $this->offset = 0;
        //  }

        // if (isset($input['vendor']) && !empty($input['vendor'])) {

        //     $extra.= (!empty($extra) ? ' AND': 'AND')." AND v.id = ".$input['vendor'];

        // }

        // if (isset($input['keyword']) && !empty($input['keyword'])) {

        //     $keyword = $input['keyword'];
        //     $extra .= (!empty($extra) ? ' AND': 'AND')."  p.name LIKE '%".$keyword."%'";
        //     // $extra .= " OR i.tracking_code LIKE '%".$keyword."%'";
        //     // $extra .= " OR i.order_id LIKE '%".$keyword."%'";

        // }

        // if ($id > 0) {
        //     $extra.= "WHERE id = $id";
        // }
        //  $query = "";
   
        // $query="SELECT [param] FROM product p INNER JOIN brand b ON b.id = p.brand_id INNER JOIN vendor v ON v.id = b.vendor_id WHERE 1 = 1 $extra";
        // $data = $this->paginate($query, 'p.id, p.name, p.brand_id', $this->offset, $this->limit);
      //   exit;
      //   foreach ($data['result'] as $key => $value)
      //    {
      //       $data['rowCount']++;
      //       $column[$value->brand_id][] = $this->setRow(
      //               $value->id,[
      //               $value->name
      //               ], $data['rowCount'], $value->id);
      //    }



      //    $totalkeys = array_keys($column);
      //    $rowCount = [];
      //    //count rows
      //    foreach ($totalkeys as $key => $value) {
      //       $rowCount[$value] = sizeof($column[$value]);
      //    }

        
      //   if (isset($input['paginate'])) {
      //       $data['paginate'] = (isset($input['paginate'])? $input['paginate'] : 0);
      //   }

      //   (isset($input['search'])? $data['search'] = $input['search']: '');
        
      //   $data['data'] = $column;
      //   $data['fresh'] = (isset($input['fresh'])? $input['fresh'] : 0);
      //   $data['totalRows'] = $data['totalRows'];
      //   $data['more'] = $data['more'];
      //   $data['offset'] = $data['offset'];
      //   $data['totalRows'] = $data['totalRows'];
      //   $data['rowCountByKey'] = $rowCount;


      //   unset($data['result']);

      // return $data;
    }

    //using ajax
     public function product_post(Request $request, $internal = false)
    {
        $input = $request->all();

        $extra = '';
        $id = $request->query('id');
        $input = $request->all();

        $this->limit = 100;
         $offsetStr = '';
         $extra = "";
         if (isset($input['offset'])) {
            $this->offset = $input['offset']; 
         } else {
            $this->offset = 0;
         }

        if (isset($input['vendor']) && !empty($input['vendor'])) {

            $extra.= (!empty($extra) ? ' AND': 'AND')." AND v.id = ".$input['vendor'];

        }

        if (isset($input['keyword']) && !empty($input['keyword'])) {

            $keyword = $input['keyword'];
            $extra .= (!empty($extra) ? ' AND': 'AND')."  p.name LIKE '%".$keyword."%'";
            // $extra .= " OR i.tracking_code LIKE '%".$keyword."%'";
            // $extra .= " OR i.order_id LIKE '%".$keyword."%'";

        }

        if ($id > 0) {
            $extra.= "WHERE id = $id";
        }
         $query = "";
   
        $query="SELECT [param] FROM product p LEFT JOIN brand b ON b.id = p.brand_id LEFT JOIN vendor v ON v.id = b.vendor_id LEFT JOIN product_promo pp ON pp.product_id = p.id LEFT JOIN category c ON c.id = p.category_id  WHERE 1 = 1 $extra ORDER BY p.regular_price ASC";

        $data = $this->paginate($query, 'p.id, p.featured_pic, p.gallery,p.name, p.category_id, p.brand_id, p.regular_price, pp.sale_price, pp.start_date, pp.end_date, c.parent as parent_id, c.name as category_name', $this->offset, $this->limit);
          
        $img = 20;
        foreach ($data['result'] as $key => $value)
         {
             //cat id
                if($value->parent_id == 0) {
                    $cat = $value->category_id;
                } else {
                    $cat = $value->parent_id;
                }
            $data['rowCount']++;
            //reuse the img
            if($img == 40) {
                $img = 20;
            }
            $img++;
            if($value->featured_pic != '') {
                $featured = '/source/public/'.$value->featured_pic;
            } else {
               if($cat == 1) {
                   $featured = '/asset/home/vp.png';
                   $size = '50';
               } else {
                   $featured = '/asset/home/diamond.png';
                   $size = '75';
               }
                
            }

            //get variation
            $query="SELECT name,COUNT(*) as total_variation FROM variation_value WHERE product_id = $value->id GROUP BY name";
            $variation =  DB::select($query);
    
        $variants = '';
        if(sizeof($variation)> 0) {
            
        
        foreach ($variation as $k => $v) {
            $variants.= '<span class="bullet bullet-success bullet-sm"></span>&nbsp;';
        //   $variants .= '<span class="badge badge-light-success mr-1 mb-1">'.$v->name.'</span>';
        }
        } else {
            $variants = '-';
        }

            if ($input['guest'] == 1) {
            //   '<a href="?id='.$value->id.'">'.$value->name.'</a>',

                 $column['all'][] = $this->setGrid(
                            $value->id,[
                            $value->name,
                            $value->regular_price,
                            $value->sale_price,
                            $value->start_date,
                            $value->end_date,
                            $featured,
                            $size
                            ], $data['rowCount'], $value->id);
            } else {
               
                 $column[$cat][] = $this->setRow(
                            $value->id,[
                            '<a class="linkcolor" href="?action=edit&id='.$value->id.'">'.$value->name.'</a>',
                            $value->regular_price,
                            $value->sale_price,
                            $value->start_date,
                            $value->end_date,
                            $value->category_name,
                            $variants,
                            $this->actionCol(
                            $this->actionBtn('edit','/product?action=edit&id='.$value->id)
                            .$this->actionBtn('clone','/product?action=clone&id='.$value->id)
                            .$this->actionBtn('delete','/product?action=delete&id='.$value->id))
                            ], $data['rowCount'], $value->id);
            }
           

            


         }
         
     
    if ($input['guest'] == 1) {

         $column2 = [];

         foreach ($column as $key => $value) {
                $total = sizeof($value);
                $chunk = array_chunk($value, 3);
           
                foreach ($chunk as $k => $v) {

                $tr = '<tr class="">';

                    foreach ($v as $k2 => $v2) {
                        
                        $tr .=$v2;
                    }
                $tr .='</tr>';
                $column2[$key][] = $tr;


                }
           
         }
}
  
         $totalkeys = array_keys($column);
         $rowCount = [];
         //count rows
         foreach ($totalkeys as $key => $value) {
            $rowCount[$value] = sizeof($column[$value]);
         }

        
        if (isset($input['paginate'])) {
            $data['paginate'] = (isset($input['paginate'])? $input['paginate'] : 0);
        }

        (isset($input['search'])? $data['search'] = $input['search']: '');
        
        if ($input['guest'] == 1) {
        $data['data'] = $column2;

        } else {
        $data['data'] = $column;

        }

        $data['fresh'] = (isset($input['fresh'])? $input['fresh'] : 0);
        $data['totalRows'] = $data['totalRows'];
        $data['more'] = $data['more'];
        $data['offset'] = $data['offset'];
        $data['totalRows'] = $data['totalRows'];
        $data['rowCountByKey'] = $rowCount;


        unset($data['result']);

      return $data;
    }

    //using websocket
    public function product_post_2(Request $request, $internal = false)
    {
        $input = $request->all();

        $connectionID = $input['id'];

        
        $extra = '';
        $id = $request->query('id');
        $input = $request->all();

        $this->limit = 100;
         $offsetStr = '';
         $extra = "";
         if (isset($input['offset'])) {
            $this->offset = $input['offset']; 
         } else {
            $this->offset = 0;
         }

        if (isset($input['vendor']) && !empty($input['vendor'])) {

            $extra.= (!empty($extra) ? ' AND': 'AND')." AND v.id = ".$input['vendor'];

        }

        if (isset($input['keyword']) && !empty($input['keyword'])) {

            $keyword = $input['keyword'];
            $extra .= (!empty($extra) ? ' AND': 'AND')."  p.name LIKE '%".$keyword."%'";
            // $extra .= " OR i.tracking_code LIKE '%".$keyword."%'";
            // $extra .= " OR i.order_id LIKE '%".$keyword."%'";

        }

        if ($id > 0) {
            $extra.= "WHERE id = $id";
        }
         $query = "";
   
        $query="SELECT [param] FROM product p INNER JOIN brand b ON b.id = p.brand_id INNER JOIN vendor v ON v.id = b.vendor_id WHERE 1 = 1 $extra";
        $data = $this->paginate($query, 'p.id, p.name, p.brand_id', $this->offset, $this->limit);

        if (sizeof($data['result']) <=0) {
            $column = [];
            return;
        }
        foreach ($data['result'] as $key => $value)
         {
            $data['rowCount']++;
            $column[$value->brand_id][] = $this->setRow(
                    $value->id,[
                    $value->name
                    ], $data['rowCount'], $value->id);
         }



         $totalkeys = array_keys($column);
         $rowCount = [];
         //count rows
         foreach ($totalkeys as $key => $value) {
            $rowCount[$value] = sizeof($column[$value]);
         }

        
        if (isset($input['paginate'])) {
            $data['paginate'] = (isset($input['paginate'])? $input['paginate'] : 0);
        }

        (isset($input['search'])? $data['search'] = $input['search']: '');
        
        $data['data'] = $column;
        $data['fresh'] = (isset($input['fresh'])? $input['fresh'] : 0);
        $data['totalRows'] = $data['totalRows'];
        $data['more'] = $data['more'];
        $data['offset'] = $data['offset'];
        $data['totalRows'] = $data['totalRows'];
        $data['rowCountByKey'] = $rowCount;


        unset($data['result']);
        event(new MessageTransport('data', ['id'=>$connectionID, 'result'=>$data]));
        exit;

    }

    public function productAdd(Request $request)
    {
        $user = new UserController($request);
        $role = $user->loggedInUser();


        $brand = new BrandController($request);

        $brand = $brand->brand($request, 1);


        return $this->view('product.create',
        [
            'brand'=>$brand,
            'user'=>$role]);
    }




    public function productSave(Request $request)
    {
        $input = $request->all();
        $userID = Auth::id();

    
        $query="INSERT INTO product(name, tags, category_id, regular_price, content, created_by)VALUES('".addslashes($input['name'])."', '".$input['tags']."','".implode(',', $input['category'])."','".$input['regular_price']."','".(isset($input['content']) ? addslashes(nl2br($input['content'])) : '')."',$userID)";

        $insert = DB::insert($query);

        $productID = DB::connection()->getPdo()->lastInsertId();
        
        //sales
        if(isset($input['sale_price'])) {
            $query="INSERT INTO product_promo(product_id, sale_price, start_date, end_date) VALUES('".$productID."', '".$input['sale_price']."', '".(!empty($input['start_date'])? $input['start_date']: null)."', '".(!empty($input['end_date'])? $input['end_date']: null)."')";
            $insert = DB::insert($query);
        }
        
        $filePath = 'images'.DIRECTORY_SEPARATOR.$productID;
        $productPath = public_path($filePath.DIRECTORY_SEPARATOR.'product'.DIRECTORY_SEPARATOR);
  
    
        if(!\File::isDirectory($productPath)){
            \File::makeDirectory($productPath, 0777, true, true);
        }
        
        $year = date("Y");
        $month = date("m");
        $date = date("d");
        

        if(!\File::isDirectory($productPath.DIRECTORY_SEPARATOR.$year)){
            \File::makeDirectory($productPath.DIRECTORY_SEPARATOR.$year, 0777, true, true);
        }

        if(!\File::isDirectory($productPath.DIRECTORY_SEPARATOR.$year.DIRECTORY_SEPARATOR.$month)){
            \File::makeDirectory($productPath.DIRECTORY_SEPARATOR.$year.DIRECTORY_SEPARATOR.$month, 0777, true, true);
        }

        if(!\File::isDirectory($productPath.DIRECTORY_SEPARATOR.$year.DIRECTORY_SEPARATOR.$month.DIRECTORY_SEPARATOR.$date)){
            \File::makeDirectory($productPath.DIRECTORY_SEPARATOR.$year.DIRECTORY_SEPARATOR.$month.DIRECTORY_SEPARATOR.$date, 0777, true, true);
        }
        //upload featured picture
        $request->validate([
            'featured_pic' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
        
        $file = $request->file('featured_pic')->getClientOriginalName();
        $featuredFileName = pathinfo($file, PATHINFO_FILENAME);
        $extension = pathinfo($file, PATHINFO_EXTENSION);

        $featuredName = time().$featuredFileName.'.'.$request->featured_pic->extension();  

        $request->featured_pic->move(public_path($filePath.DIRECTORY_SEPARATOR.'product'.DIRECTORY_SEPARATOR.$year.DIRECTORY_SEPARATOR.$month.DIRECTORY_SEPARATOR.$date), $featuredName);

        


        if($request->hasfile('gallery'))
         {
            //upload gallery picture
            $this->validate($request, [
                    'gallery' => 'required',
                    'gallery.*' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048'
            ]);
         	$i = 0;
            foreach($request->file('gallery') as $file)
            {
            	$i++;
            	$galleryName = $file->getClientOriginalName();
                $name = time().$galleryName.$i.'.'.$file->extension();
                $file->move(public_path($filePath.DIRECTORY_SEPARATOR.'product'.DIRECTORY_SEPARATOR.$year.DIRECTORY_SEPARATOR.$month.DIRECTORY_SEPARATOR.$date), $name);  
                $data[] = $filePath.DIRECTORY_SEPARATOR.'product'.DIRECTORY_SEPARATOR.$year.DIRECTORY_SEPARATOR.$month.DIRECTORY_SEPARATOR.$date.DIRECTORY_SEPARATOR.$name;  
            }
         }
         
         $featured_pic = $filePath.DIRECTORY_SEPARATOR.'product'.DIRECTORY_SEPARATOR.$year.DIRECTORY_SEPARATOR.$month.DIRECTORY_SEPARATOR.$date.DIRECTORY_SEPARATOR.$featuredName;
         $gallery = '';
         if($request->file('gallery')) {
            $gallery = implode(',', $data);
            
         }
         if(isset($featured_pic)) {
              $query="UPDATE product SET featured_pic= '".$featured_pic."', gallery = '".$gallery."' WHERE id = $productID";
              DB::update($query);
         }
         
         
        

        foreach ($input['variation'] as $k => $v) {

            foreach ($v as $key => $value) {
               
               if (isset($value['value'])) {

               $query="INSERT INTO variation_value(product_id, name, value, regular_price, created_by)VALUES($productID, '".addslashes($v['variation'])."','".addslashes($value['value'])."','".(isset($value['price']['regular'])? $value['price']['regular']: 0)."',$userID)";

                $insert = DB::insert($query);
                $variationID = DB::connection()->getPdo()->lastInsertId();

                if ($value['price']['sale']) {
          
                $query="INSERT INTO variation_promo(variation_id, sale_price, start_date, end_date, created_by)VALUES($variationID, '".$value['price']['sale']."','".(!empty($value['sale']['start_date'])? $value['sale']['start_date']: null)."','".(!empty($value['sale']['end_date'])? $value['sale']['end_date']: null)."',$userID)";
                $insert = DB::insert($query);
                }
               }

            }
            

        }


        return redirect('/backend/admin/product?action=edit&id='.$productID);


    }
    
    
     public function productClone(Request $request)
    {
        $user = new UserController($request);
        $role = $user->loggedInUser();
        $id = $request->query('id');
        $userID = Auth::ID();

        $query= "INSERT INTO product(name, tags, category_id, regular_price, content, created_by)SELECT name, tags, category_id, regular_price, content, $userID FROM  product WHERE id = ".$id ;
        DB::insert($query);
        $lastID = DB::connection()->getPdo()->lastInsertId();
        
        $query= "INSERT INTO variation_value(variation_type_id, product_id, name, value, regular_price, created_by)SELECT variation_type_id, $lastID, name, value, regular_price, created_by FROM  variation_value WHERE product_id = ".$id ;
        DB::insert($query);

        
        
        
        return redirect()->to('/backend/admin/product?action=edit&id='.$lastID);
        
    }

     public function productEdit(Request $request)
    {
        $user = new UserController($request);
        $role = $user->loggedInUser();
        $id = $request->query('id');



        $brand = new BrandController($request);

        $brand = $brand->brand($request, 1);

        $query = "SELECT p.*, pp.sale_price, pp.start_date, pp.end_date, b.name as brand FROM product p LEFT JOIN product_promo pp ON p.id = pp.product_id LEFT JOIN brand b ON p.brand_id = b.id WHERE p.id = $id";
        $product = DB::select($query);

        //images

            if(($product[0]->featured_pic != '') && ($product[0]->gallery != '')) {
  
               
                $images = array_merge(explode(',', $product[0]->featured_pic), explode(',', $product[0]->gallery));

            } else if($product[0]->featured_pic != '') {
                $images = [$product[0]->featured_pic];
            } else {
                $images = [];
            }


        $query = "SELECT v.*, v.id as variant_id, v.name as variation, vp.* FROM variation_value v LEFT JOIN variation_promo vp ON v.id = vp.variation_id WHERE product_id = ".$product[0]->id;

        $variation = DB::select($query);

        //category
        $query="SELECT id,name FROM category WHERE id IN(".$product[0]->category_id.")";
        $category = DB::select($query);


        $variation2 = [];
        
        foreach ($variation as $key => $value) {
        
            $variation2[$value->variation][] = $value;
        }


        return $this->view('product.edit',
        [
            'brand'=>$brand,
            'user'=>$role,
            'product'=>$product[0],
            'id'=>$id,
            'images'=>$images,
            'variation'=>$variation2]);
    }
    
    public function productDeleteVariant(Request $request)
    {
        $variant = $request->query('id');
        $query="DELETE FROM variation_value WHERE id = ".$variant;
        DB::delete($query);
        
        return redirect()->back()->with('success', 'Variant Deleted');
    }
    
    public function productDeleteVariation(Request $request)
    {
        $variant = $request->query('id');
        $query="DELETE FROM variation_value WHERE name = '".$variant."'";
        DB::delete($query);
        
        return redirect()->back()->with('success', 'Variant Deleted');
    }
    public function productDeleteImage(Request $request)
    {
        $input = $request->all();
        $userID = Auth::id();
        $productID = $request->query('id');
        $image = base64_decode($request->query('src'));
       
        $query="SELECT featured_pic, gallery FROM product WHERE id = $productID";
         $product = DB::select($query);
          //images

            if(($product[0]->featured_pic != '') && ($product[0]->gallery != '')) {
  
               
                $images = array_merge(explode(',', $product[0]->featured_pic), explode(',', $product[0]->gallery));

            } else if($product[0]->featured_pic != '') {
                $images = [$product[0]->featured_pic];
            } else {
                $images = [];
            }
        
        //unset
        foreach($images as $key=>$value) {
            if($value==$image) {
                unset($images[$key]);
            }
        }
        
        
        if($product[0]->featured_pic == $image) {
            $query="UPDATE product SET featured_pic = '' WHERE id = $productID";
        } else {
            //remove featured
            unset($images[0]);
            $images = implode(',', $images);

            $query="UPDATE product SET gallery = '".$images."' WHERE id =". $productID;
        }
        //update
        
        DB::update($query);
         return redirect()->back()->with('success', 'Image Deleted');
        
    }
    
    public function productDelete(Request $request)
    {
         $input = $request->all();
        $userID = Auth::id();
        $productID = $request->query('id');
        $query="DELETE FROM product WHERE id = $productID";
         DB::delete($query);
         return redirect()->back()->with('success', 'Product Deleted');
        
    }
    public function productUpdate(Request $request)
    {
        $input = $request->all();
        $userID = Auth::id();
        $productID = $request->query('id');


        $query="UPDATE product SET name = '".addslashes($input['name'])."', regular_price = '".$input['regular_price']."', content= '".(isset($input['content']) ? addslashes(nl2br($input['content'])) : '')."' WHERE id = $productID";
        DB::update($query);

        $filePath = 'images'.DIRECTORY_SEPARATOR.$productID;
        $productPath = public_path($filePath.DIRECTORY_SEPARATOR.'product'.DIRECTORY_SEPARATOR);
  
    
        if(!\File::isDirectory($productPath)){
            \File::makeDirectory($productPath, 0777, true, true);
        }
        
        $year = date("Y");
        $month = date("m");
        $date = date("d");
        

        if(!\File::isDirectory($productPath.DIRECTORY_SEPARATOR.$year)){
            \File::makeDirectory($productPath.DIRECTORY_SEPARATOR.$year, 0777, true, true);
        }

        if(!\File::isDirectory($productPath.DIRECTORY_SEPARATOR.$year.DIRECTORY_SEPARATOR.$month)){
            \File::makeDirectory($productPath.DIRECTORY_SEPARATOR.$year.DIRECTORY_SEPARATOR.$month, 0777, true, true);
        }

        if(!\File::isDirectory($productPath.DIRECTORY_SEPARATOR.$year.DIRECTORY_SEPARATOR.$month.DIRECTORY_SEPARATOR.$date)){
            \File::makeDirectory($productPath.DIRECTORY_SEPARATOR.$year.DIRECTORY_SEPARATOR.$month.DIRECTORY_SEPARATOR.$date, 0777, true, true);
        }

        if($request->hasfile('featured_pic'))
         {
        $file = $request->file('featured_pic')->getClientOriginalName();
        $featuredFileName = pathinfo($file, PATHINFO_FILENAME);
        $extension = pathinfo($file, PATHINFO_EXTENSION);

        $featuredName = time().$featuredFileName.'.'.$request->featured_pic->extension();  

        $request->featured_pic->move(public_path($filePath.DIRECTORY_SEPARATOR.'product'.DIRECTORY_SEPARATOR.$year.DIRECTORY_SEPARATOR.$month.DIRECTORY_SEPARATOR.$date), $featuredName);
        
        $featured_pic = $filePath.DIRECTORY_SEPARATOR.'product'.DIRECTORY_SEPARATOR.$year.DIRECTORY_SEPARATOR.$month.DIRECTORY_SEPARATOR.$date.DIRECTORY_SEPARATOR.$featuredName;
        
        $query="UPDATE product SET featured_pic= '".$featured_pic."' WHERE id = $productID";
         DB::update($query);
         }

        


        if($request->hasfile('gallery'))
         {
            //upload gallery picture
            $this->validate($request, [
                    'gallery' => 'required',
                    'gallery.*' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048'
            ]);
         	$i = 0;
            foreach($request->file('gallery') as $file)
            {
            	$i++;
            	$galleryName = $file->getClientOriginalName();
                $name = time().$galleryName.$i.'.'.$file->extension();
                $file->move(public_path($filePath.DIRECTORY_SEPARATOR.'product'.DIRECTORY_SEPARATOR.$year.DIRECTORY_SEPARATOR.$month.DIRECTORY_SEPARATOR.$date), $name);  
                $data[] = $filePath.DIRECTORY_SEPARATOR.'product'.DIRECTORY_SEPARATOR.$year.DIRECTORY_SEPARATOR.$month.DIRECTORY_SEPARATOR.$date.DIRECTORY_SEPARATOR.$name;  
            }
         }
         
         
         $gallery="";
         if($request->hasfile('gallery')) {
            $gallery = implode(',', $data);
            
            $query="UPDATE product SET gallery = '".$gallery."' WHERE id = $productID";
            DB::update($query);
         }
         
 
        $remainingVariants = [];
         //update existing 
        if(isset($input['variation'])) {

        foreach ($input['variation'] as $k => $v) {

            foreach ($v as $key => $value) {

               if (isset($value['value'])) {
                   
                   

                $query = "UPDATE variation_value SET name = '".addslashes($v['variation'])."', value = '".addslashes($value['value'])."', regular_price = '".(isset($value['price']['regular'])? $value['price']['regular']: 0)."' WHERE id = $key";

                $update = DB::update($query);

                if ($value['price']['sale']) {

                    $query="SELECT * FROM variation_promo WHERE variation_id = ".$key;
                    $isExist = DB::select($query);
                  
                  if(sizeof($isExist) > 0) {
                      $query = "UPDATE variation_promo SET sale_price = '".$value['price']['sale']."', start_date = '".(!empty($value['sale']['start_date'])? $value['sale']['start_date']: null)."', end_date = '".(!empty($value['sale']['end_date'])? $value['sale']['end_date']: null)."' WHERE variation_id = $key";
                       $update = DB::update($query);
                  } else {
                      $query="INSERT INTO variation_promo(variation_id, sale_price, start_date, end_date)VALUES('".$key."', '".$value['price']['sale']."', '".$value['sale']['start_date']."', '".$value['sale']['end_date']."')";
             
              
                      DB::insert($query);
                  }
                    

                   
                }


               }

            }
            

        }

        }

        //new variants
        if(isset($input['new_variation'])) {
            
        
        foreach ($input['new_variation'] as $k => $v) {
            foreach ($v as $key => $value) {
               
               if (isset($value['value'])) {

               $query="INSERT INTO variation_value(product_id, name, value, regular_price, created_by)VALUES($productID, '".addslashes($v['variation'])."','".addslashes($value['value'])."','".(isset($value['price']['regular'])? $value['price']['regular']: 0)."',$userID)";

                $insert = DB::insert($query);
                $variationID = DB::connection()->getPdo()->lastInsertId();

                if ($value['price']['sale']) {
          
                $query="INSERT INTO variation_promo(variation_id, sale_price, start_date, end_date, created_by)VALUES($variationID, '".$value['price']['sale']."','".(!empty($value['sale']['start_date'])? $value['sale']['start_date']: null)."','".(!empty($value['sale']['end_date'])? $value['sale']['end_date']: null)."',$userID)";
                $insert = DB::insert($query);
                }
               }

            }
            

        }
        }

        return redirect()->back()->with('success', 'Product Updated');


    }
}
