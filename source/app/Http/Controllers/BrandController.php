<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class BrandController extends Controller
{
    public function brand(Request $request, $internal = false)
    {
        $extra = '';
        $id = $request->query('id');
        if ($id > 0) {
            $extra.= "WHERE id = $id";
        }
        $query = "SELECT * FROM brand $extra";
        $brand = DB::select($query);

 
        if($internal) {
            return $brand;
        }

         return $this->view('brand.view',
        [
            'brand'=>$brand]);
    }
    public function brandAdd(Request $request)
    {
        $logistic = new UserController($request);
        $role = new RoleController($request);
        $allRoles = $role->getRole();

        

        $vendor = $this->vendor($request, 1);
        return $this->view('brand.create',
        [
            'vendor'=>$vendor]);
    }

     public function brandSave(Request $request)
    {
        $input = $request->all();
        $userID = Auth::id();
        $query="INSERT INTO brand(name, vendor_id, created_by)VALUES('".addslashes($input['name'])."', '".implode(',', $input['vendor'])."',$userID)";
        $insert = DB::insert($query);

        return redirect()->back()->with('success', 'Brand Created');


    }
}
