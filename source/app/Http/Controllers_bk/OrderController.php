<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class OrderController extends Controller
{
    public function email_order(Request $request)
    {
        $extra = '';
        $id = $request->query('id');
        $preview = $request->query('preview');
        $input = $request->all();

        if(!empty($id)) {
   
            $query="SELECT * FROM orders WHERE id = $id";
            $order = DB::select($query);
            
            $query="SELECT po.*, p.name  FROM product_order po INNER JOIN product p ON p.id = po.product_id  WHERE order_id = ".$order[0]->id;
            $products = DB::select($query);
            $orderDetail = [];
            foreach($products as $key=>$value)
            {
                $orderDetail['product'][$value->product_id] = $value;
                
                
                $query="SELECT * FROM product_order_variation WHERE order_id = ".$order[0]->id."  AND product_id = ".$value->product_id;
                $variation = DB::select($query);
                $orderDetail['variant'][$value->product_id] = $variation;
            }
            
            
            $email = new MailController;
            return $email->sendEmail($order[0], $orderDetail);

        }

    }
    
     public function order(Request $request)
    {
        $extra = '';
        $id = $request->query('id');
        $preview = $request->query('preview');
        $input = $request->all();

        if(!empty($id)) {
   
            $query="SELECT * FROM orders WHERE id = $id";
            $order = DB::select($query);
            
            $query="SELECT po.*, p.name  FROM product_order po INNER JOIN product p ON p.id = po.product_id  WHERE order_id = ".$order[0]->id;
            $products = DB::select($query);
            $orderDetail = [];
            foreach($products as $key=>$value)
            {
                $orderDetail['product'][$value->product_id] = $value;
                
                
                $query="SELECT * FROM product_order_variation WHERE order_id = ".$order[0]->id."  AND product_id = ".$value->product_id;
                $variation = DB::select($query);
                $orderDetail['variant'][$value->product_id] = $variation;
            }
            

            return $this->view('order.order',
                    [   
                        'skipHeader'=>1,
                        'order'=>$order[0],
                        'products'=>$products,
                        'detail'=>$orderDetail,
                        'save'=>'https://nexustech.online/blog']);

        }

         return $this->view('order.view',
        [
            'thead'=>[
                'id',
                'firstName',
                'lastName',
                'username',
                'address',
                'status'],
            'keys'=>['all']]);
    }

    //using ajax
    public function order_post(Request $request)
    {
        $input = $request->all();
        $query="SELECT * FROM orders";
        $orders = DB::select($query);

         $this->limit = 100;
         $offsetStr = '';
         $extra = "";
         if (isset($input['offset'])) {
            $this->offset = $input['offset']; 
         } else {
            $this->offset = 0;
         }

        $query="SELECT [param] FROM orders o";
        $data = $this->paginate($query, 'o.*', $this->offset, $this->limit);

        foreach ($data['result'] as $key => $value)
         {
            $data['rowCount']++;
            $column['all'][] = $this->setRow(
                $value->id,[
                '<a class="linkcolor" href="?id='.$value->id.'">'.$value->id.'</a>',
                $value->firstName,
                $value->lastName,
                $value->username,
                $value->address,
                $value->status], $data['rowCount'], $value->id);

        }

         $totalkeys = array_keys($column);
         $rowCount = [];
         //count rows
         foreach ($totalkeys as $key => $value) {
            $rowCount[$value] = sizeof($column[$value]);
         }

         if (isset($input['paginate'])) {
            $data['paginate'] = (isset($input['paginate'])? $input['paginate'] : 0);
        }

        (isset($input['search'])? $data['search'] = $input['search']: '');

        $data['data'] = $column;
        $data['fresh'] = (isset($input['fresh'])? $input['fresh'] : 0);
        $data['totalRows'] = $data['totalRows'];
        $data['more'] = $data['more'];
        $data['offset'] = $data['offset'];
        $data['totalRows'] = $data['totalRows'];
        $data['rowCountByKey'] = $rowCount;


        unset($data['result']);

        return $data;
    }
}
