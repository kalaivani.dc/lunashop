<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class CategoryController extends Controller
{

    
      public function category(Request $request, $internal = false, $guest = false)
    {

        $extra = '';
        $id = $request->query('id');
        $input = $request->all();
        $backend = $request->query('backend');
        $cat = 0;
        $output = [];
 
        if(!empty($id)) {
             //get category
            $query="SELECT id FROM category WHERE name LIKE '%$id%'";
    
            $category = DB::select($query);

            $cat = $category[0]->id;

        } 
        if (!$guest) {

            $user = new UserController(); 
            $role = $user->loggedInUser();
        

         $add = '/backend'.$this->site.$role->role.'/category?action=add';
        }

        if (!$guest) {

            $view = 'category.view';
        } else {
            $view = 'front.category.view';

        }

        
         

         if (!$guest) {
            $thead = ['name', 'regular', 'sale', 'start', 'end', 'variation'];
            $add_title = 'ADD CATEGORY';
            $keys = ['all'];
            $search = 1;
         } else {
            $thead = ['name'];
            $add_title='';
             $search = 0;
            $keys = ['all'];

         }
         
         return $this->view($view,
        [
            'add'=>(isset($add)? $add: ''),
            'main_title'=>ucfirst($id),
            'bread'=>[['#'=>'home'], ['#'=>'shop'], ['#'=>'category'], ['#'=>'product']],
            'guest'=>$guest,
            'add_title'=>$add_title,
            'cat'=>$cat,
            'category'=>$output,
            'vendor'=>[],
            'thead'=>['name'],
            'search'=>$search,
            'keys'=>$keys]);
    }

 public function categoryAdd(Request $request)
    {
        $user = new UserController($request);
        $role = $user->loggedInUser();


        return $this->view('category.create',
        [
            'user'=>$role]);
    }
    
    public function categorySave(Request $request)
    {
        $input = $request->all();
        $userID = Auth::id();
        // echo "<pre>";
        // var_dump($input);
        // exit;
        
         $query="INSERT INTO category(name, status, parent, content, created_by)VALUES('".addslashes($input['name'])."','".$input['status']."','".implode(',', $input['category'])."', '".(isset($input['content']) ? addslashes(nl2br($input['content'])) : '')."',$userID)";

        $insert = DB::insert($query);

        $catID = DB::connection()->getPdo()->lastInsertId();
        
        if($request->hasfile('featured_pic'))
         {
        $filePath = 'images'.DIRECTORY_SEPARATOR.$catID;
        $productPath = public_path($filePath.DIRECTORY_SEPARATOR.'category'.DIRECTORY_SEPARATOR);
  
    
        if(!\File::isDirectory($productPath)){
            \File::makeDirectory($productPath, 0777, true, true);
        }
        
        $year = date("Y");
        $month = date("m");
        $date = date("d");
        

        if(!\File::isDirectory($productPath.DIRECTORY_SEPARATOR.$year)){
            \File::makeDirectory($productPath.DIRECTORY_SEPARATOR.$year, 0777, true, true);
        }

        if(!\File::isDirectory($productPath.DIRECTORY_SEPARATOR.$year.DIRECTORY_SEPARATOR.$month)){
            \File::makeDirectory($productPath.DIRECTORY_SEPARATOR.$year.DIRECTORY_SEPARATOR.$month, 0777, true, true);
        }

        if(!\File::isDirectory($productPath.DIRECTORY_SEPARATOR.$year.DIRECTORY_SEPARATOR.$month.DIRECTORY_SEPARATOR.$date)){
            \File::makeDirectory($productPath.DIRECTORY_SEPARATOR.$year.DIRECTORY_SEPARATOR.$month.DIRECTORY_SEPARATOR.$date, 0777, true, true);
        }
        //upload featured picture
        $request->validate([
            'featured_pic' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
        
        $file = $request->file('featured_pic')->getClientOriginalName();
        $featuredFileName = pathinfo($file, PATHINFO_FILENAME);
        $extension = pathinfo($file, PATHINFO_EXTENSION);

        $featuredName = time().$featuredFileName.'.'.$request->featured_pic->extension();  

        $request->featured_pic->move(public_path($filePath.DIRECTORY_SEPARATOR.'category'.DIRECTORY_SEPARATOR.$year.DIRECTORY_SEPARATOR.$month.DIRECTORY_SEPARATOR.$date), $featuredName);

        
        $featured_pic = $filePath.DIRECTORY_SEPARATOR.'category'.DIRECTORY_SEPARATOR.$year.DIRECTORY_SEPARATOR.$month.DIRECTORY_SEPARATOR.$date.DIRECTORY_SEPARATOR.$featuredName;
        if(isset($featured_pic)) {
              $query="UPDATE category SET featured_pic= '".$featured_pic."' WHERE id = $catID";
              DB::update($query);
         }
         }
         
          return redirect()->back()->with('success', 'Category Created');
        
        
    }
    
    private function getCategory($parentID)
    {
         $query="SELECT id, name, status FROM category WHERE parent IN($parentID)";
    
        $category = DB::select($query);
        return $category;
    }

//using ajax
     public function categoryList(Request $request)
    {
        $input = $request->all();

        $extra = '';
        $input = $request->all();

         if (isset($input['keyword']) && !empty($input['keyword'])) {

            $extra.= " AND c.name LIKE '%".$input['keyword']."%'";

        }

        $query="SELECT *  FROM category c WHERE 1 = 1 $extra";

        $result = DB::select($query);
        $category = '<ul>';
        foreach ($result as $key => $value) {
            $category .= '<li><input type="checkbox" name="category[]" value="'.$value->id.'">'.$value->name.'</li>';
        }
        $category .= '</ul>';
   
        return $category;
    }
    
    public function category_tree()
    {
        $output = [];
        $query="SELECT id, name, status FROM category WHERE parent = 0";
    
            $category = DB::select($query);

 
            foreach($category as $key=>$value) {
                
                $query="SELECT COUNT(id) as total FROM product WHERE category_id IN($value->id)";
                $products = DB::select($query);
            
                $parent = new \stdClass();
                $parent->text = $value->name;
                $parent->href = '#'.str_replace(' ', '-', $value->name);
                $parent->tags = [$products[0]->total];
                $parent->status = $value->status;
                
                
 
                $child_1 = $this->getCategory($value->id);
                if(sizeof($child_1) > 0) {
                    foreach($child_1 as $k2=>$v2) {
                        
                         $query="SELECT COUNT(id) as total FROM product WHERE sub_category IN($v2->id)";
                         $products = DB::select($query);
                         
                        $child = new \stdClass();
                                $child->text =  $v2->name;
                                $child->href = '#'.str_replace(' ', '-', $v2->name);
                                $child->tags = [$products[0]->total];
                                $child->status = $v2->status;
                        
                        
                        $grand_child_1 = $this->getCategory($v2->id);
                        // $grand_child_arr = [];
                        
                        if(sizeof($grand_child_1) > 0) {
                            
                                
                                
                            foreach($grand_child_1 as $k3=>$v3) {
                                
                                 $query="SELECT COUNT(id) as total FROM product WHERE sub_category IN($v3->id)";
                                $products = DB::select($query);
                                
                                $grand_child = new \stdClass();
                                $grand_child->text =  $v3->name;
                                $grand_child->href = '#'.str_replace(' ', '-', $v3->name);;
                                $grand_child->tags = [$products[0]->total];
                                $grand_child->status = $v3->status;

                                
                               
                                $child->nodes[] = $grand_child;
                                
                            
                                

    
                            }

                                
                          
                        } 
                        
                        $parent->nodes[] = $child;
                    }
                    
                }
                
                $output[] = $parent;
             
                
               
            }
            
            return $output;

            
    }
     //using ajax
     public function category_post(Request $request, $internal = false)
    {
        $input = $request->all();

        $extra = '';
        $input = $request->all();

        $this->limit = 100;
         $offsetStr = '';
         $extra = "";
         if (isset($input['offset'])) {
            $this->offset = $input['offset']; 
         } else {
            $this->offset = 0;
         }

         if (isset($input['id']) && !empty($input['id'])) {

            $extra.= " AND c.id = ".$input['id'];

        }

        
         $query = "";
   
        $query="SELECT [param] FROM category c WHERE 1 = 1 $extra";


        $data = $this->paginate($query, 'c.*', $this->offset, $this->limit);
          

        foreach ($data['result'] as $key => $value)
         {
            $data['rowCount']++;
             $column['all'][] = $this->setRow(
                            $value->id,[
                            '<a href="?id='.$value->id.'">'.$value->name.'</a>'
                            ], $data['rowCount'], $value->id);

         }

         $totalkeys = array_keys($column);
         $rowCount = [];
         //count rows
         foreach ($totalkeys as $key => $value) {
            $rowCount[$value] = sizeof($column[$value]);
         }

        
        if (isset($input['paginate'])) {
            $data['paginate'] = (isset($input['paginate'])? $input['paginate'] : 0);
        }

        (isset($input['search'])? $data['search'] = $input['search']: '');
        
        $data['data'] = $column;
        $data['fresh'] = (isset($input['fresh'])? $input['fresh'] : 0);
        $data['totalRows'] = $data['totalRows'];
        $data['more'] = $data['more'];
        $data['offset'] = $data['offset'];
        $data['totalRows'] = $data['totalRows'];
        $data['rowCountByKey'] = $rowCount;


        unset($data['result']);

      return $data;
    }

    //using ajax
     public function product_post(Request $request, $internal = false)
    {
        $input = $request->all();

        $extra = '';
        $id = $request->query('id');
        $input = $request->all();
        $column = [];
        $column2 = [];
        
        $this->limit = 100;
         $offsetStr = '';
         $extra = "";
         if (isset($input['offset'])) {
            $this->offset = $input['offset']; 
         } else {
            $this->offset = 0;
         }

         if (isset($input['id']) && !empty($input['id'])) {

            $extra.= " AND p.category_id = ".$input['id'];

        }

        if ($id > 0) {
            $extra.= "WHERE id = $id";
        }
         $query = "";
   
        $query="SELECT [param] FROM product p LEFT JOIN brand b ON b.id = p.brand_id LEFT JOIN vendor v ON v.id = b.vendor_id LEFT JOIN product_promo pp ON pp.product_id = p.id WHERE 1 = 1 $extra";

        $data = $this->paginate($query, 'p.id, p.featured_pic, p.name, p.brand_id, p.regular_price, pp.sale_price, pp.start_date, pp.end_date', $this->offset, $this->limit);
          
        $img = 20;
        foreach ($data['result'] as $key => $value)
         {
            $data['rowCount']++;
            if($img == 40) {
                $img = 20;
            }
            $img++;
            if($value->featured_pic != '') {
                $featured = '/source/public/'.$value->featured_pic;
            } else {
                $featured = 'http://online.printdepot.com.my/asset/app-assets/images/banner/banner-'.$img.'.jpg';
            }

            if ($input['guest'] == 1) {
                 $column['all'][] = $this->setGrid(
                            $value->id,[
                            '<a href="/product?id='.$value->id.'">'.$value->name.'</a>',
                            $value->regular_price,
                            $value->sale_price,
                            $value->start_date,
                            $value->end_date,
                            $featured
                            ], $data['rowCount'], $value->id);
            } else {
                 $column[$value->brand_id][] = $this->setRow(
                            $value->id,[
                            '<a href="?id='.$value->id.'">'.$value->name.'</a>',
                            $value->regular_price,
                            $value->sale_price,
                            $value->start_date,
                            $value->end_date,
                            ''
                            ], $data['rowCount'], $value->id);
            }
           

            


         }
         
     
    if ($input['guest'] == 1) {

         $column2 = [];

         foreach ($column as $key => $value) {
                $total = sizeof($value);
                $chunk = array_chunk($value, 3);
           
                foreach ($chunk as $k => $v) {

                $tr = '<tr class=" parent-row">';

                    foreach ($v as $k2 => $v2) {
                        
                        $tr .=$v2;
                    }
                $tr .='</tr>';
                $column2[$key][] = $tr;


                }
           
         }
}
  
         $totalkeys = array_keys($column);
         $rowCount = [];
         //count rows
         foreach ($totalkeys as $key => $value) {
            $rowCount[$value] = sizeof($column[$value]);
         }

        
        if (isset($input['paginate'])) {
            $data['paginate'] = (isset($input['paginate'])? $input['paginate'] : 0);
        }

        (isset($input['search'])? $data['search'] = $input['search']: '');
        
        if ($input['guest'] == 1) {
        $data['data'] = $column2;

        } else {
        $data['data'] = $column;

        }
        $data['fresh'] = (isset($input['fresh'])? $input['fresh'] : 0);
        $data['totalRows'] = $data['totalRows'];
        $data['more'] = $data['more'];
        $data['offset'] = $data['offset'];
        $data['totalRows'] = $data['totalRows'];
        $data['rowCountByKey'] = $rowCount;


        unset($data['result']);

      return $data;
    }


}
