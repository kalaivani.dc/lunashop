<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Mail;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Mail\MyEmail;
// use \PDF;

class MailController extends Controller {
    
   public function notifyAdmin($order, $detail) {
      $to_email =$order->username;
      $mail = new MyEmail;
            Mail::to($to_email)->cc('nexustech183@gmail.com')->send($mail->from('nexustech183@gmail.com')->subject("New Order Invoice #".$order->id)->view('order.mail', ['to'=>$to_email, 'order'=>$order, 'detail'=>$detail]));

    
        return $this->view('front.checkout.thankyou', ['to'=>$to_email, 'order'=>$order, 'detail'=>$detail, 'sent'=>1]);
        
   }
   public function html_email() {
      $data = array('name'=>"Virat Gandhi");
      Mail::send('mail', $data, function($message) {
         $message->to('abc@gmail.com', 'Tutorials Point')->subject
            ('Laravel HTML Testing Mail');
         $message->from('xyz@gmail.com','Virat Gandhi');
      });
      echo "HTML Email Sent. Check your inbox.";
   }
   public function attachment_email() {
      $data = array('name'=>"Virat Gandhi");
      Mail::send('mail', $data, function($message) {
          
         $message->to('kalaivani.dc@gmail.com', 'Test Email')->subject
            ('Laravel Testing Mail with Attachment');
         $message->attach('http://online.printdepot.com.my/asset/home/assets/img/logo-207x41.png');
         $message->attach('http://online.printdepot.com.my/asset/home/assets/img/logo-207x41.png');
         $message->from('nexustech183@gmail.com','Nexus Tech');
      });
      echo "Email Sent with attachment. Check your inbox.";
   }
    public function sendEmail($order, $detail) {

        $to_email = $order->username;
   
        $mail = new MyEmail;
        Mail::to($to_email)->cc('nexustech183@gmail.com')->send($mail->from('nexustech183@gmail.com')->attach('http://online.printdepot.com.my/asset/home/assets/img/logo-207x41.png')->subject("Invoice #".$order->id)->view('order.mail', ['to'=>$to_email, 'order'=>$order, 'detail'=>$detail]));
    
    
        return $this->view('order.order', ['to'=>$to_email, 'order'=>$order, 'detail'=>$detail, 'sent'=>1]);
    }
}