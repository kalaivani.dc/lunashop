<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;


use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class CheckoutController extends Controller
{
    public function checkout(Request $request)
    {
        $input = $request->all();

         return $this->view('front.checkout.checkout', [
            'cart'=>$input,
            'save'=>'http://online.printdepot.com.my/checkout?action=save'
        ]);

    }

    public function checkoutsave(Request $request)
    {
        $input = $request->all();
   
        //order
       $query="INSERT INTO orders(
        firstName, 
        lastName, 
        username, 
        address, 
        address2, 
        country, 
        state, 
        zip,
        grand_total,
        created_by) VALUES(
        '".addslashes($input['firstName'])."', 
        '".addslashes($input['lastName'])."',
        '".$input['username']."',
        '".addslashes($input['address'])."', 
        '".addslashes($input['address2'])."', 
        '".addslashes($input['country'])."', 
        '".addslashes($input['state'])."', 
        '".addslashes($input['zip'])."',
        '".$input['grand_total']."',
        '".Auth::id()."')";

        DB::insert($query);
        $orderID = DB::connection()->getPdo()->lastInsertId();
        
        foreach($input['product'] as $key=>$value) {
            $query="INSERT INTO product_order(
            order_id, 
            product_id, 
            quantity, 
            sub_total, 
            total) VALUES(
            '".$orderID."', 
            '".$key."',
            '".$value['qty']."',
            '".$value['sub_total']."', 
            '".$value['total']."')";
            DB::insert($query);
            
            if (isset($value['variant'])) {
               
                foreach($value['variant'] as $k=>$v) {
                
                    $query="INSERT INTO product_order_variation(
                    order_id, 
                    product_id, 
                    variant_type, 
                    variant_value,
                    variant_price) VALUES(
                    '".$orderID."', 
                    '".$key."',
                    '".$v['name']."',
                    '".$v['value']."', 
                    '".$v['regular']."')";
                    DB::insert($query);
                    
                }
            }
        }
       
       
            $query="SELECT * FROM orders WHERE id = $orderID";
            $order = DB::select($query);
            
            $query="SELECT po.*, p.name  FROM product_order po INNER JOIN product p ON p.id = po.product_id  WHERE order_id = ".$order[0]->id;
            $products = DB::select($query);
            $orderDetail = [];
            foreach($products as $key=>$value)
            {
                $orderDetail['product'][$value->product_id] = $value;
                
                
                $query="SELECT * FROM product_order_variation WHERE order_id = ".$order[0]->id."  AND product_id = ".$value->product_id;
                $variation = DB::select($query);
                $orderDetail['variant'][$value->product_id] = $variation;
            }
            
         $email = new MailController;
         return $email->notifyAdmin($order[0], $orderDetail);


    }
}
