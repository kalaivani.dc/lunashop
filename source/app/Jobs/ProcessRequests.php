<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Events\MessageTransport;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProcessRequests implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    use \App\Http\Controllers\HelperController;

    public $data;
    public $input;
    public $response;


    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data, $input, $offset, $limit)
    {
        $this->data = $data;
        $this->input = $input;
        $this->offset = $offset;
        $this->limit = $limit;

        
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Request $request, $internal = false)
    {
        $input = $request->all();

        $connectionID = $input['id'];

        $extra = '';
        $id = $request->query('id');

        var_dump($this->offset);
        $this->limit = 100;
         $offsetStr = '';
         $extra = "";
         // if (isset($input['offset'])) {
         //    $this->offset = $input['offset']; 
         // } else {
         //    $this->offset = 0;
         // }

        if (isset($input['vendor']) && !empty($input['vendor'])) {

            $extra.= (!empty($extra) ? ' AND': 'AND')." AND v.id = ".$input['vendor'];

        }

        if (isset($input['keyword']) && !empty($input['keyword'])) {

            $keyword = $input['keyword'];
            $extra .= (!empty($extra) ? ' AND': 'AND')."  p.name LIKE '%".$keyword."%'";
            // $extra .= " OR i.tracking_code LIKE '%".$keyword."%'";
            // $extra .= " OR i.order_id LIKE '%".$keyword."%'";

        }

        if ($id > 0) {
            $extra.= "WHERE id = $id";
        }
         $query = "";
   
        $query="SELECT [param] FROM product p INNER JOIN brand b ON b.id = p.brand_id INNER JOIN vendor v ON v.id = b.vendor_id WHERE 1 = 1 $extra";
        $data = $this->paginate($query, 'p.id, p.name, p.brand_id', $this->offset, $this->limit);

        if (sizeof($data['result']) <=0) {
            $column = [];
            return;
        }
        foreach ($data['result'] as $key => $value)
         {
            $data['rowCount']++;
            $column[$value->brand_id][] = $this->setRow(
                    $value->id,[
                    $value->name
                    ], $data['rowCount'], $value->id);
         }



         $totalkeys = array_keys($column);
         $rowCount = [];
         //count rows
         foreach ($totalkeys as $key => $value) {
            $rowCount[$value] = sizeof($column[$value]);
         }

        
        if (isset($input['paginate'])) {
            $data['paginate'] = (isset($input['paginate'])? $input['paginate'] : 0);
        }

        (isset($input['search'])? $data['search'] = $input['search']: '');
        
        $data['data'] = $column;
        $data['fresh'] = (isset($input['fresh'])? $input['fresh'] : 0);
        $data['totalRows'] = $data['totalRows'];
        $data['more'] = $data['more'];
        $data['offset'] = $data['offset'];
        $data['totalRows'] = $data['totalRows'];
        $data['rowCountByKey'] = $rowCount;

        unset($data['result']);
        event(new MessageTransport('data', ['id'=>$connectionID, 'result'=>$data]));

    }
}
