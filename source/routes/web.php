<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ConfigController as Config;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Translation
Route::get('lang/{lang}', ['as' => 'lang.switch', 'uses' => 'App\Http\Controllers\LanguageController@switchLang']);

Route::get('/', [App\Http\Controllers\ProductController::class, 'productHome']);

Route::get('sendbasicemail',[App\Http\Controllers\MailController::class, 'basic_email']);
Route::get('sendhtmlemail',[App\Http\Controllers\MailController::class, 'html_email']);
Route::get('sendattachmentemail',[App\Http\Controllers\MailController::class, 'attachment_email']);



Route::get('/home', [App\Http\Controllers\HomeController::class, 'index']);

Route::get('/about', [App\Http\Controllers\HomeController::class, 'about']);
Route::get('/become-a-reseller', [App\Http\Controllers\HomeController::class, 'become_a_reseller']);
Route::get('/contact', [App\Http\Controllers\HomeController::class, 'contact']);
Route::get('/store', [App\Http\Controllers\HomeController::class, 'store']);
Route::get('/privacy-policy', [App\Http\Controllers\HomeController::class, 'privacy_policy']);
Route::get('/refund', [App\Http\Controllers\HomeController::class, 'refund']);
Route::get('/terms', [App\Http\Controllers\HomeController::class, 'terms']);
Route::get('/xe', [App\Http\Controllers\HomeController::class, 'xe_currency']);
Route::get('/xe_daily', [App\Http\Controllers\HomeController::class, 'xe_currency_daily']);

Auth::routes();



//bootstrap
Route::middleware(['auth'])->group(function () {

    Route::prefix('backend/{role}')->group(function () {

        /*initialize config*/
        $config =  new Config();
        $tier = '';

        for($i = 0; $i < 3; $i++)
        {
            $tier .= "{tier_$i}/";
  
            Route::prefix($tier)->group(function () {
                routerCallback();
                   
            });

        }

    });

});

    //front end
    /*initialize config*/
    $config =  new Config();
    $tier = '';

    for($i = 0; $i < 3; $i++)
    {
        $tier .= "{tier_$i}/";

        Route::prefix($tier)->group(function () {
            guestRouterCallback();
               
        });

    }


function routerCallback()
{

    Route::get('/', [App\Http\Controllers\RouterController::class, 'getRouter']);
    Route::post('/', [App\Http\Controllers\RouterController::class, 'getRouter']);
    Route::get('q={id}', [App\Http\Controllers\RouterController::class, 'getRouter']);

    Route::get('action={action}', [App\Http\Controllers\RouterController::class, 'getRouter']);
    Route::get('action={action}&q={id}&r={id2}&s={id3}', [App\Http\Controllers\RouterController::class, 'getRouter']);

    Route::post('action={action}', [App\Http\Controllers\RouterController::class, 'getRouter']);
    Route::post('action={action}&q={id}&r={id2}&s={id3}', [App\Http\Controllers\RouterController::class, 'getRouter']);
}


function guestRouterCallback()
{

    Route::get('/', [App\Http\Controllers\GuestRouterController::class, 'getRouter']);
    Route::post('/', [App\Http\Controllers\GuestRouterController::class, 'getRouter']);
    Route::get('q={id}', [App\Http\Controllers\GuestRouterController::class, 'getRouter']);

    Route::get('action={action}', [App\Http\Controllers\GuestRouterController::class, 'getRouter']);
    Route::get('action={action}&q={id}&r={id2}&s={id3}', [App\Http\Controllers\GuestRouterController::class, 'getRouter']);

    Route::post('action={action}', [App\Http\Controllers\GuestRouterController::class, 'getRouter']);
    Route::post('action={action}&q={id}&r={id2}&s={id3}', [App\Http\Controllers\GuestRouterController::class, 'getRouter']);
}