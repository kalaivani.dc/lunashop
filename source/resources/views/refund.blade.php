@extends('layouts.app2')


@section('styles')

@endsection

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
<h1>Return and Refund Policy</h1>
<hr>



<h2>NO REFUND POLICY</h2>


<p style="text-align:justify;">Lunashop digital Products have a strict no-refund policy. Please be sure the products are right for you before purchasing. Users are solely responsible for confirming that their devices are compatible with the Products they purchase. All virtual item purchased are final, non-refundable and non-returnable. We do not offer refunds or exchanges for the incorrect purchase of Lunashop Products, including due to compatibility issues.</p>




<h2>DISCLAIMER OF WARRANTIES</h2>


<p style="text-align:justify;">LUNASHOP DOES NOT GUARANTEE, REPRESENT, OR WARRANT THAT YOUR USE OF THE PRODUCT AND/OR WEBSITE WILL BE UNINTERRUPTED OR ERROR-FREE, AND YOU AGREE THAT FROM TIME TO TIME WE MAY REMOVE THE PRODUCT FOR INDEFINITE PERIODS OF TIME, OR CANCEL THE PRODUCT IN ACCORDANCE WITH THESE TERMS AND CONDITIONS.</p>
<p style="text-align:justify;">We have no control over the quality, fitness, safety, reliability, legality, or any other aspect of any Product that is purchased using our Product. We are not required to issue refunds if a purchase turns out to not meet your expectations, or if the third party providers do not fulfil their commitments, although we will make reasonable efforts to assist you in these matters. We have no obligation, and cannot guarantee that, we will resolve any disputes related to any transaction to your satisfaction.</p>
<p style="text-align:justify;">We have the absolute discretion to alter the Terms & Conditions, in any way in respect of the Program, including terms of ordering, Coin usage procedures for Reward, or Reward in any form, without prior notice, and even if the changes can affect the value of the accumulated Coin. You must check our present Terms & Conditions and details and other information for the Program through the Member Web Portal or by contacting our Customer Support at lunashop.my1@gmail.com.</p>
<p style="text-align:justify;">Our website contents are provided based on “as is” and “as available”. We hereby firmly state that we do not make any guarantees or collaterals whether expressly or implicitly, in respect to the merchantability of a product or suitability of our product that we provide for a specific purpose.</p>
<p style="text-align:justify;">From time to time, our Product may be delayed, interrupted or disrupted for an indeterminate period of time. In addition, except as otherwise required by applicable law or regulation, Lunashop may terminate your use of Product or impose limits on the type and/or amount of transactions you are allowed to make with the Product at any time at its sole discretion without prior notice. Lunashop and its affiliates shall not be liable for any claim arising from or related to Lunashop arising from any such delay, interruption, disruption, limitation, or suspension.</p>

<hr>

<h2>LIMITATION OF LIABILITY</h2>


<p style="text-align:justify;">YOU EXPRESSLY UNDERSTAND AND AGREE THAT TO THE FULLEST EXTENT PERMITTED BY APPLICABLE LAW, WE AND OUR AFFILIATES (AND OUR AND THEIR RESPECTIVE EMPLOYEES, DIRECTORS, AGENTS AND REPRESENTATIVES), PARTNERS AND LICENSORS SHALL NOT BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, PUNITIVE, CONSEQUENTIAL OR EXEMPLARY DAMAGES, INCLUDING BUT NOT LIMITED TO, DAMAGES FOR LOSS OF PROFITS, GOODWILL, USE, DATA COST OF PROCUREMENT OF SUBSTITUTE PRODUCTS, OR OTHER INTANGIBLE LOSSES ARISING OUT OF OR IN CONNECTION WITH: (i) THE USE OR INABILITY TO USE THE PRODUCT; (ii) ANY CHANGES MADE TO THE PRODUCT OR ANY TEMPORARY OR PERMANENT CESSATION OF THE PRODUCT OR ANY PART THEREOF; (iii) THE UNAUTHORIZED ACCESS TO OR ALTERATION OF YOUR TRANSMISSIONS OR DATA; (iv) THE DELETION OF, CORRUPTION OF, OR FAILURE TO STORE AND/OR SEND OR RECEIVE YOUR TRANSMISSIONS OR DATA ON OR THROUGH THE PRODUCT; (v) STATEMENTS OR CONDUCT OF ANY THIRD PARTY ON THE PRODUCT; (vi) PROGRAM OR YOUR PARTICIPATION IN THE PROGRAM, (vii) DELAY, FAILURE OR DECISION BY US IN THE OPERATION OF THE PROGRAM OR ANY CHANGES TO THE TERMS AND CONDTIONS ON THE REDEMPTION AND USAGE OF THE POINTS (viii) ILLEGITIMATE USE OF YOUR MEMBER CARD OR PIN, (ix) OFFERS, DESCRIPTION, STATEMENTS OR CLAIMS ABOUT THE PROGRAM, BRANDS OR PARTNERSHIP OR ANY INDIVIDUAL, (x) PURCHASES, REDEMPTIONS OR USAGE OF GOODS FROM THE BRAND OR PARTNERSHIP, INCLUDING OTHER REWARD, WHETHER PROVIDED BY US OR ONE OF OUR AFFILIATES, BRAND OR PARTNER. OUR BRAND AND PARTNERS ARE NOT ACCOUNTABLE FOR THE PROGRAM AND (xi) ANY OTHER MATTER RELATING TO THE PRODUCT AND/OR WEBSITE.</p>
<p style="text-align:justify;">TO THE EXTENT PERMITTED BY APPLICABLE LAW, IN NO EVENT WILL THE AGGREGATE LIABILITY OF LUNASHOP OR OUR AFFILIATES (AND OUR AND THEIR RESPECTIVE EMPLOYEES, DIRECTORS, AGENTS AND REPRESENTATIVES), PARTNERS AND LICENSORS ARISING OUT OF OR IN CONNECTION WITH THESE TERMS AND CONDITIONS OR THE TRANSACTIONS CONTEMPLATED HEREBY, WHETHER IN CONTRACT, TORT (INCLUDING NEGLIGENCE, PRODUCT LIABILITY OR OTHER THEORY), WARRANTY, OR OTHERWISE, EXCEED THE AMOUNT OF FEES EARNED BY US IN CONNECTION WITH YOUR USE OF PRODUCT AND/OR WEBSITE DURING THE THREE (3) MONTH PERIOD IMMEDIATELY PRECEDING THE EVENT GIVING RISE TO THE CLAIM FOR LIABILITY.</p>

________________________________________


<p style="text-align:justify;">Contact Us</p>


<p style="text-align:justify;">Don't hesitate to contact us if you have any inquiry.</p>
<p style="text-align:justify;">Via Email: lunashop.my1@gmail.com</p>


            </div>
        </div>
    </div>
</div>

@endsection

