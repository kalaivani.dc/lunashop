@extends('layouts.app')


@section('assets')

@endsection

@section('content')

<div class="content-header row">
    <div class="content-header-left col-12 mb-2 mt-1">
        <div class="breadcrumbs-top">
            <h5 class="content-header-title float-left pr-1 mb-0 d-none d-sm-block">Add Product</h5>
            <h5 class="content-header-title pr-1 mb-0 d-block d-sm-none"> {{__('module.edit')}}</h5>
                <div class="breadcrumb-wrapper d-none d-sm-block">
                    <ol class="breadcrumb p-0 mb-0 pl-1">
                        <li class="breadcrumb-item"><a href="/<?php echo $user->role?>/module/main"><i class="bx bx-home-alt"></i></a> </li>
                        <li class="breadcrumb-item"><a href="/<?php echo $user->role;?>/module/main">{{__('module.module')}}</a></li>
                        <li class="breadcrumb-item active">Add Module</li>
                    </ol>
                </div>
        </div>
    </div>
</div>

<div class="card">
    <div class="card-header text-left">
    
    </div>
<form action="<?php echo $site;?><?php echo $user->role;?>/shop/product?action=save" method="POST" enctype="multipart/form-data">@csrf
    <div class="card-body">
        <div class="row col-12">
            <div class="col-md-4">
                <div class="form-group">
                <label>Product Name</label>
                <input type="text" name="name" class="form-control">
                </div>
            </div>
             <div class="col-md-4">
                <div class="form-group">
                <label>Brand</label>
                <select class="form-control" name="brand[]">
                <?php 
                if(isset($brand)) {
                    foreach ($brand as $key => $value) {
                       ?>
                       <option value="<?php echo $value->id;?>"><?php echo $value->name;?></option>
                       <?php
                    }
                }
                ?>
                </select>
            </div>
            </div>

             <div class="col-md-4">
                <div class="form-group">
                <label>Price</label>
                <input type="text" class="form-control" name="regular_price" placeholder="Price">
                </div>
             </div>
            
          
        </div>

    </div>

    <!--variation-->
    <div class="col-12">
    <div class="card">
        <div class="card-header">
        Variation
        </div>
        <div class="card-body">
            <div class="col-12">

            <div class="form-group table-responsive">
                <table class="table-stripped variation-wrapper" data-product_wrapper>

                    <tr id="1">
                        <th style="width: 10px;">

                        <span class="numberCircle" data-number><span>1</span></span>
                        </th>
                        <th data-delete-product>
                        <i class='bx bx-trash'></i></th>
                        <th><input type="text" data-product-name id="product_name[1]" class="form-control" name="product_name[1]" placeholder="Color" required></th>

                        <th>
                        <button data-add-product type="button" class="btn btn-outline-primary mr-1 mb-1 form-control" style="margin-top:15px;">
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-plus-circle" viewBox="0 0 16 16">
                        <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/>
                        <path d="M8 4a.5.5 0 0 1 .5.5v3h3a.5.5 0 0 1 0 1h-3v3a.5.5 0 0 1-1 0v-3h-3a.5.5 0 0 1 0-1h3v-3A.5.5 0 0 1 8 4z"/>
                        </svg><span class="align-middle ml-25"></span></button>
                        </th>
                    </tr>

                <!--variation 2-->
                <tr  class="variation_wrapper2">
                    <td colspan="8">
                        <table class="table-stripped">
                            <tr>
                                <th colspan="5"></th>
                                <th>
                                <button data-add-productspecs type="button" class="btn btn-outline-primary mr-1 mb-1">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-plus-circle" viewBox="0 0 16 16">
                                <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/>
                                <path d="M8 4a.5.5 0 0 1 .5.5v3h3a.5.5 0 0 1 0 1h-3v3a.5.5 0 0 1-1 0v-3h-3a.5.5 0 0 1 0-1h3v-3A.5.5 0 0 1 8 4z"/>
                                </svg><span class="align-middle ml-25">Attribute</span></button>
                                </th>
                            </tr>

                            <tr><th>Attribute</th></tr>
                            <tr>
                                <td></td>

                                <td><span>Value</span></td>


                                <td><span>Regular Price</span></td>
                                <td><span>Promo Price</span></td>
                                <td><span>Promo Start Date</span></td>
                                <td><span>Promo End Date</span></td>
                            </tr>
                            <tr data-variant-wrapper id="1">

                                <td data-delete-variant>
                                <i class='bx bx-trash'></i>
                                </td>

                                <td>

                                <input data-variation-value placeholder="yellow" type="text" name="variation[1][1][value]" class="form-control">
                                </td>
                                <td>

                               
                                <input data-regular-price placeholder="" type="text" name="variation[1][1][price][regular]" class="form-control">
                                </td>
                                <td>


                                <input data-sale-price placeholder="" type="text" name="variation[1][1][price][sale]" class="form-control">
                                </td>

                                <td>

                                <input data-sale-start type="date" name="variation[1][1][sale][start_date]" class="form-control">
                                </td>
                                <td>

                                <input data-sale-end type="date" name="variation[1][1][sale][end_date]" class="form-control">
                                
                                </td>
                            </tr>
                        </table>
                    </td>

                </tr>


                </table>

            </div>
            </div>
            </div>
        </div>
    </div>

    <div class="mb-4 ml-1 d-flex">
       <a href="/<?php echo $user->role;?>/module" style="margin-right:0.1rem">
            <button type="button" class="btn btn-dark wow zoomIn">{{__('academy.back')}} </button>
        </a>
        <button type="submit" class="btn btn-primary subtotal-preview-btn" data-submit>{{__('module.submit')}}</button>
  </div>
</form>
</div>

@endsection
@section('scripts')
<script type="text/javascript">
        $(function() {





            $('.loading').addClass('hidden');

            $('[data-size2]').on('click', function() {
                //  get the size
                var $this = this;
                // $($this).html('');
              $('[data-size] option:selected').each(function(index, value) {

                  $($this).append('<option value="'+$(value).val()+'">'+$(value).text()+'</option>');
                });

            });

    
            $("body").on("click", "[data-add-productspecs]", function() {

                var id = $('[data-product_wrapper]').length;

                var $trLast =  $($(this).parents('.variation_wrapper2')).find('table tr:last');
                $trNew = $trLast.clone();

                 //get total attribute to determine id
                var attrID = $($(this).parents('[data-product_wrapper]')).find('.variation_wrapper2 [data-variant-wrapper]').length;

                attrID++;

                $.each($($trNew), function(key, value) {
                   
                   // $(value).find('td:nth-child(1) input').attr('name', 'variation['+count2+'][name]');
                   $(value).find('td:nth-child(2) input').attr('name', 'variation['+id+']['+attrID+'][value]');
                   $(value).find('td:nth-child(3) input').attr('name', 'variation['+id+']['+attrID+'][price][regular]');
                   $(value).find('td:nth-child(4) input').attr('name', 'variation['+id+']['+attrID+'][price][sale]');
                   $(value).find('td:nth-child(5) input').attr('name', 'variation['+id+']['+attrID+'][sale][start_date]');
                   $(value).find('td:nth-child(6) input').attr('name', 'variation['+id+']['+attrID+'][sale][end_date]');


                });
                
                $trLast.after($trNew);
             });

                
                var id = 0;
               $("body").on("click", "[data-add-product]", function() {
                
                var $trLast =  $($(this).parents('[data-product_wrapper]'));
                id = $('[data-product_wrapper]').length;
                id++;

                //get total attribute to determine id
                 var attrID = $($(this).parents('[data-product_wrapper]')).find('.variation_wrapper2 [data-variant-wrapper]').length;
        

                $trNew = $trLast.clone();
                $trNew.find('tr').attr('id', id);

                //variation id
                $($trNew).find('tr [data-number]').text(id);

                /**change element**/
                $($trNew).find('tr th [data-product-name]').attr('name', 'product_name['+id+']');

                //existing
                var existing = $($trNew).find('.variation_wrapper2 [data-variant-wrapper] td');
                $($trNew).find('.variation_wrapper2 [data-variant-wrapper]').css('background', 'yellow');
                var existing_no = 0;
                $.each($(existing), function(key, value) {
                    existing_no++;
             
                    console.log(key);
                    console.log(value);

                   $(value).find('[data-variation-value] input').attr('name', 'variation['+id+']['+key + parseInt(1)+'][value]');
                   $(value).find('[data-regular-price] input').attr('name', 'variation['+id+']['+key + parseInt(1)+'][price][regular]');
                   $(value).find('[data-sale-price] input').attr('name', 'variation['+id+']['+key + parseInt(1)+'][price][sale]');
                   $(value).find('[data-sale-start] input').attr('name', 'variation['+id+']['+key + parseInt(1)+'][sale][start_date]');
                   $(value).find('[data-sale-end] input').attr('name', 'variation['+id+']['+key + parseInt(1)+'][sale][end_date]');

                });
                
                //add new variation
                $($trNew).find('.variation_wrapper2 [data-variation-value]').attr('name', 'variation['+id+']['+attrID+'][value]');
                $($trNew).find('.variation_wrapper2 [data-regular-price]').attr('name', 'variation['+id+']['+attrID+'][price][regular]');
                $($trNew).find('.variation_wrapper2 [data-sale-price]').attr('name', 'variation['+id+']['+attrID+'][price][sale]');
                $($trNew).find('.variation_wrapper2 [data-sale-start]').attr('name', 'variation['+id+']['+attrID+'][sale][start_date]');
                $($trNew).find('.variation_wrapper2 [data-sale-end]').attr('name', 'variation['+id+']['+attrID+'][sale][end_date]');
                $($trNew).find('.variation_wrapper2 [data-same-price]').attr('name', 'variation['+id+']['+attrID+'][price]').attr('id', 'checkbox['+id+']');




                /**ends**/

                // $trLast.after($trNew);
                $($trNew).insertBefore($trLast);
                
               




             });

               $("body").on('click', "[data-delete-product]", function() {
            
                $($(this).parents('[data-product_wrapper]')).remove();
               });

               $("body").on('click', "[data-delete-variant]", function() {
     
                $($(this).parents('[data-variant-wrapper]')).remove();
               });

           
        });
    </script>


<script>

$("#new_category_btn2").on('click',function(e){
    
    // e.preventDefault();
  
    
  // $("#newCat").toggleClass('hidden');
                  $.ajax({
                url: "/productsByCat",
                method: "POST",
                data:{
                    "_token": "{{ csrf_token() }}",
                    'id':userID,
                    'dataType':'html'
                },
                success: function (response) {
                    console.log(response);

                },
            });

});

var category = '';
var storeID = 0;
$('#category').on('keypress', function(e) {
 
        if ($(this).val().length > 1) {
          console.log($(this).val());
          storeID = $('#storeid').val();

          category = $(this).val();
          /*fetch category*/
                  $.ajax({
                url: "/category",
                method: "POST",
                data:{
                    "_token": "{{ csrf_token() }}",
                    'keyword':category,
                    'store':storeID,
                    'dataType':'html'
                },
                success: function (response) {
                    console.log(response);
                    $('#categorySuggestion').html(response);

                }
            });
          /*fetch category*/

        }

         if(e.which == 13) {
        alert('You pressed enter!');
    }
      });



$('body').on('click', '[data-business-policy-save]', function(e) {
 
    e.preventDefault = false;
    var status = false;
    if (status == false) {

        $('input[name=business_policy]').val($('.editor').text());
        e.preventDefault = false;
    }
    
});


$('body').on('click', '[data-blog-save]', function(e) {
 
     e.preventDefault = false;
    var status = false;
    if (status == false) {

        var quill = new Quill ('.editor');
        var quillHtml = quill.root.innerHTML.trim();
        $('#content').val(quillHtml);
        e.preventDefault = false;
    }

  
});

//save product
$('body').on('click', '[data-product-save]', function(e) {
 
     e.preventDefault = false;
    var status = false;
    if (status == false) {

        var quill = new Quill ('.editor');
        var quillHtml = quill.root.innerHTML.trim();
        $('#content').val(quillHtml);
        e.preventDefault = false;
    }

  
});



$('body').on('click', '#searchbtn', function() {

    $('.loading').removeClass('hidden');

    var keyword = $('#searchbar').val();

    $('.search-bar-wrapper').css({
        "margin-top": "0",
         "position": "relative"
    });
    if($(window).innerWidth() <= 768) {

    $('.search-bar-wrapper').css({
        "margin-top": "0",
         "position": "relative",
         "top":"0"
    });
    }

    $('#home-video').css({
        "margin-top": "0",
         "position": "relative"
    });

        $('.search-bar').css({
        "margin-top": "5%",
         "position": "relative"
    });

        $('.one').slideUp();
        $('.two').slideUp();
        $('#products').removeClass('hidden').slideDown();
        if (keyword != '') {
                $.ajax({
                url: "/productsByCat",
                method: "POST",
                data:{
                    "_token": "{{ csrf_token() }}",
                    'keyword':keyword,
                    'dataType':'html'
                },
                success: function (response) {
                    console.log(response);

                 $('.loading').addClass('hidden');
                
                 $('#products').html(response).removeClass('hidden').slideDown();
                    
                     $('.search-bar-wrapper').css({
                        "margin-top": "0",
                         "position": "relative"
                    });

                    $('#home-video').css({
                        "margin-top": "0",
                         "position": "relative"
                    });

                        $('.search-bar').css({
                        "margin-top": "5%",
                         "position": "relative"
                    });

                $('.one').slideUp();
                $('.two').slideUp();
                $('#products').removeClass('hidden').slideDown();

                },
            });
        } else {
          
            $('.loading').addClass('hidden');
             
                    var timerInterval
    Swal.fire({
      title: 'Type something to  search...',
      timer: 2000,
      confirmButtonClass: 'btn pink-danger',
      buttonsStyling: false,
      onBeforeOpen: function () {
        Swal.showLoading()
        timerInterval = setInterval(function () {
          Swal.getContent().querySelector('strong')
            .textContent = Swal.getTimerLeft()
        }, 100)
      },
      onClose: function () {
        clearInterval(timerInterval)
      }
    }).then(function (result) {
      if (
        // Read more about handling dismissals
        result.dismiss === Swal.DismissReason.timer
      ) {
        console.log('I was closed by the timer')
      }
    })
             

        }


});


$('.facebook-sync').on('click', function () {
    Swal.fire({
      position: 'top-start',
      icon: 'success',
      title: 'Your page has been synced',
      showConfirmButton: false,
      timer: 1500,
      confirmButtonClass: 'btn btn-primary',
      buttonsStyling: false,
    })

});



$('.instagram-sync').on('click', function () {
   Swal.fire({
      position: 'top-start',
      icon: 'success',
      title: 'Your page has been synced',
      showConfirmButton: false,
      timer: 1500,
      confirmButtonClass: 'btn btn-primary',
      buttonsStyling: false,
    })

});

$('.search-menu .nav-item a').on('click', function() {

    $('.loading').removeClass('hidden');

    if($(window).innerWidth() <= 768) {
        
    $('.search-bar-wrapper').css({
        "margin-top": "0",
         "position": "relative",
         "top":"0"
    });
    }

    var catID = $(this).attr('href').split('#')[1];


    $.ajax({
        url: "/productsByCat",
        method: "POST",
        data:{
            "_token": "{{ csrf_token() }}",
            'id':catID,
            'dataType':'html'
        },
        success: function (response) {

         $('.loading').addClass('hidden');
        
         $('#products').html(response).removeClass('hidden').slideDown();
            
             $('.search-bar-wrapper').css({
                "margin-top": "0",
                 "position": "relative"
            });

            $('#home-video').css({
                "margin-top": "0",
                 "position": "relative"
            });

                $('.search-bar').css({
                "margin-top": "5%",
                 "position": "relative"
            });

        $('.one').slideUp();
        $('.two').slideUp();
        $('#products').removeClass('hidden').slideDown();

        },
    });



});
</script>
@endsection