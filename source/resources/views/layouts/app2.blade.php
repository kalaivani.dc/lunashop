<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>LUNASHOP</title>
    <!-- Favicon icon -->

     <link rel="apple-touch-icon" href="/asset/home/luna_rose_logo.png">
    <link rel="shortcut icon" type="image/x-icon" href="/asset/home/luna_rose_logo.png">

        <!-- Facebook/LinkedIn Share -->
        <meta property="og:url" content="https://lunashop.my/" />
        <meta property="og:type" content="website" />
        <meta property="og:title" content="Lunashop- The Top Up World" />
        <meta property="og:description" content="Mobile game top up site" />
        <meta property="og:image" content="http://lunashop.my/asset/home/luna_rose_logo.png" />
        <meta property="og:image:width" content="1200px" />
        <meta property="og:image:height" content="630px" />
        
        
        
        
    {{Html::style('front/vendor/bootstrap-select/dist/css/bootstrap-select.min.css')}}
    

    {{Html::style('front/css/style.css')}}
<link href="https://fonts.googleapis.com/css?family=Luckiest+Guy" rel="stylesheet">

    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@100;200;300;400;500;600;700;800;900&family=Roboto:wght@100;300;400;500;700;900&display=swap" rel="stylesheet">
 <!--page specific styles-->
    @yield('styles')
    <style type="text/css">
    
.widget-timeline.style-1 .timeline .timeline-badge.info + .timeline-panel {
    border-color: #434345;
}
.widget-timeline .timeline-badge.info:after {
    background-color: #434345 !important;
    box-shadow: 0 5px 10px 0 rgba(30, 167, 197, 0.2);
}
.widget-timeline .timeline-badge.info {
    border-color: #434345;
}
.widget-timeline.style-1 .timeline .timeline-badge.info + .timeline-panel:after {
    background-color: #FFD523 !important;
    display:none;
}
[data-sidebar-style="full"][data-layout="vertical"] .deznav .metismenu > li > a {
    border-radius:0;
    /*left: 10px;*/
    top: -5px;
}
.metismenu h6.mb-0 {
    left:10px;
    position:relative;
}
[data-sidebar-style="mini"][data-layout="vertical"] .deznav .metismenu > li.mm-active > a {
    background:none !important;
}
 .header .header-content, [data-header-position="fixed"] .header {
     background-color:#fff;
 }
 

 .header-right .nav-item .nav-link {
        color: #464a53 !important;
    }
@media only screen and (max-width: 600px){

    
    .metismenu h6.mb-0 {
        left:40px;
        position:relative;
    }
    .header-right .nav-item .nav-link {
        color:#fff !important;
    }
    
    [data-sidebar-position="fixed"][data-layout="vertical"] .nav-header,.header .header-content, [data-header-position="fixed"] .header {
      
     background: url(/asset/home/top_board.png);
    background-size: cover;
    background-repeat: no-repeat;
     background-color:none;
      
  }

  [data-sidebar-style="overlay"] .footer {
    padding-left: 0;
    position: fixed;
    background: url(/asset/home/top_board.png);
    background-size: cover;
    background-repeat: no-repeat;
    bottom: 0;
    background-color:none;
    width:100%;
}

}  
    .widget-timeline .timeline:before {
        left:30px;
    }
    .widget-timeline .timeline > li > .timeline-badge {
        left: 20px;
    }

.authincation-content {
    background-color: #434345 !important;
}
.nav-header .brand-title {
  
    max-width: 120px;
    margin-top: 0px;
}

[data-cart] input {
    outline: none;
    border: none;
}
.list-group-item.active {
    z-index: 2;
    color: #fff;
    background-color: #434345 !important;
    border-color: #434345 !important;
}
.list-group-item.active input {
    color: #fff !important;
}
 .btn-primary {
     background-color: #434345 !important;
    border-color: #434345 !important;
    border-radius:0;
 }
  .badge-primary  {
      background-color: #f82249;
  }
  div[data-key-count] {
    width: 30px;
    border-radius: 15px;
    position: relative;
    left: 20px;
    background-color: red;
    padding: 0;
    color: white;
    font-size: 12px;
    text-align: center;
    top: -50px;
}
[data-sidebar-style="full"][data-layout="vertical"] .deznav .metismenu > li.mm-active > a {
    background: none;
    color: #eb1b23 !important;
    border-left: 2px solid #FFD523 !important;
    border-radius: 0;
}

span.nav-text:hover,span.nav-text:focus, span.nav-text:active, span.nav-text:visited, span.menu-title.text-truncate:hover, span.menu-title.text-truncate:active, span.menu-title.text-truncate:visited {
    color: #eb1b23 !important;
}
[data-headerbg="color_1"] .nav-header .hamburger.is-active .line, [data-headerbg="color_1"] .nav-header .hamburger .line {
    background: #434345 !important;
}
[data-sidebar-style="full"][data-layout="vertical"] .menu-toggle .nav-header .nav-control .hamburger .line,[data-headerbg="color_1"] .nav-header .hamburger.is-active .line, 
[data-headerbg="color_1"] .nav-header .hamburger .line,[data-sidebar-style="mini"][data-layout="vertical"] .deznav .metismenu > li:hover > a {
    background-color: #434345 !important;
}
/*[data-sidebar-style="full"][data-layout="vertical"] .deznav .metismenu > li > a {*/
/*    padding: 5px !important;*/

/*}*/
          /*--------------------------------------------------------------
# Portfolio
--------------------------------------------------------------*/
#portfolio-flters {
  list-style: none;
  margin-bottom: 20px;
}

#portfolio-flters li {
 cursor: pointer;
    display: inline-block;
    margin: 10px 5px;
    font-size: 15px;
    font-weight: 500;
    line-height: 1;
    color: #7e7e7e;
    transition: all 0.3s;
    padding: 30px;
    border-radius: 50px;
    font-family: "Poppins", sans-serif;
    text-align: center;
    height: 30px;
    background-color: #fff;
    border: 1px solid #7e7e7e;
}
/*#portfolio-flters ul {*/
/*    left: -5%;*/
/*    position: relative;*/
/*}*/
#portfolio-flters li:hover, #portfolio-flters li.filter-active {
  background: #EA7A9A;
  color: #fff;
  border: 1px solid #EA7A9A;
}

.portfolio .portfolio-item {
  margin-bottom: 30px;
}

.portfolio .portfolio-item .portfolio-img {
  overflow: hidden;
}

.portfolio .portfolio-item .portfolio-img img {
  transition: all 0.6s;
}

.portfolio .portfolio-item .portfolio-info {
  opacity: 0;
  position: absolute;
  left: 15px;
  bottom: 0;
  z-index: 3;
  right: 15px;
  transition: all 0.3s;
  background: rgba(55, 81, 126, 0.8);
  padding: 10px 15px;
}

.portfolio .portfolio-item .portfolio-info h4 {
  font-size: 18px;
  color: #fff;
  font-weight: 600;
  color: #fff;
  margin-bottom: 0px;
}

.portfolio .portfolio-item .portfolio-info p {
  color: #f9fcfe;
  font-size: 14px;
  margin-bottom: 0;
}

.portfolio .portfolio-item .portfolio-info .preview-link, .portfolio .portfolio-item .portfolio-info .details-link {
  position: absolute;
  right: 40px;
  font-size: 24px;
  top: calc(50% - 18px);
  color: #fff;
  transition: 0.3s;
}

.portfolio .portfolio-item .portfolio-info .preview-link:hover, .portfolio .portfolio-item .portfolio-info .details-link:hover {
  color: #47b2e4;
}

.portfolio .portfolio-item .portfolio-info .details-link {
  right: 10px;
}

.portfolio .portfolio-item:hover .portfolio-img img {
  transform: scale(1.15);
}

.portfolio .portfolio-item:hover .portfolio-info {
  opacity: 1;
}

/*--------------------------------------------------------------
# Portfolio Details
--------------------------------------------------------------*/
.portfolio-details {
  padding-top: 40px;
}

.portfolio-details .portfolio-details-slider img {
  width: 100%;
}

.portfolio-details .portfolio-details-slider .swiper-pagination {
  margin-top: 20px;
  position: relative;
}

.portfolio-details .portfolio-details-slider .swiper-pagination .swiper-pagination-bullet {
  width: 12px;
  height: 12px;
  background-color: #fff;
  opacity: 1;
  border: 1px solid #47b2e4;
}

.portfolio-details .portfolio-details-slider .swiper-pagination .swiper-pagination-bullet-active {
  background-color: #47b2e4;
}

.portfolio-details .portfolio-info {
  padding: 30px;
  box-shadow: 0px 0 30px rgba(55, 81, 126, 0.08);
}

.portfolio-details .portfolio-info h3 {
  font-size: 22px;
  font-weight: 700;
  margin-bottom: 20px;
  padding-bottom: 20px;
  border-bottom: 1px solid #eee;
}

.portfolio-details .portfolio-info ul {
  list-style: none;
  padding: 0;
  font-size: 15px;
}

.portfolio-details .portfolio-info ul li + li {
  margin-top: 10px;
}

.portfolio-details .portfolio-description {
  padding-top: 30px;
}

.portfolio-details .portfolio-description h2 {
  font-size: 26px;
  font-weight: 700;
  margin-bottom: 20px;
}

.portfolio-details .portfolio-description p {
  padding: 0;
}

.hidden {
    display: none;
    
}
.strike {
    text-decoration: line-through;
    font-size:20px;
}
.front thead, .front .dt-buttons.btn-group.flex-wrap {
     display: none;
}
.fullheight {
height: 100% !important;
}

a.linkcolor {
color: #727E8C !important;
text-decoration: underline;
}
tr.red a.linkcolor {
color: #fff !important;
}

.dt-buttons.btn-group, #target_filter label, #target_filter input[type=search] {
    display:none;
}

table.dataTable tbody td {
    background:none !important;
}
.table tbody tr td {
    vertical-align: top;
    border:none;
}
.card {
    /*border-radius: 2.25rem;*/
    /*box-shadow: 0px 10px 20px #17181b;*/
    /*background-color: #08090c;*/
    /*border-radius: 10px;*/
    /*background: radial-gradient(#272727, #1b1b1b);*/
}

/* .box {*/
/*  position: relative;*/
/*  max-width: 600px;*/
/*  width: 90%;*/
  /*height: 400px;*/
  /*background: #fff;*/
  /*box-shadow: 0 0 15px rgba(0,0,0,.1);*/
/*}*/

/* common */
.ribbon {
  width: 150px;
  height: 150px;
  overflow: hidden;
  position: absolute;
}
.ribbon::before,
.ribbon::after {
  position: absolute;
  z-index: -1;
  content: '';
  display: block;
  border: 5px solid #2980b9;
}
.ribbon span {
  position: absolute;
  display: block;
  width: 225px;
  padding: 15px 0;
  /*background-color: #3498db;*/
  background-color: #fce043;
  background-image: linear-gradient(315deg, #fce043 0%, #fb7ba2 74%);
  box-shadow: 0 5px 10px rgba(0,0,0,.1);
  color: #fff;
  font: 700 18px/1 'Lato', sans-serif;
  text-shadow: 0 1px 1px rgba(0,0,0,.2);
  text-transform: uppercase;
  text-align: center;
}

/* top left*/
.ribbon-top-left {
  top: -10px;
  left: -10px;
}
.ribbon-top-left::before,
.ribbon-top-left::after {
  border-top-color: transparent;
  border-left-color: transparent;
}
.ribbon-top-left::before {
  top: 0;
  right: 0;
}
.ribbon-top-left::after {
  bottom: 0;
  left: 0;
}
.ribbon-top-left span {
  right: -25px;
  top: 30px;
  transform: rotate(-45deg);
}

/* top right*/
.ribbon-top-right {
  top: -10px;
  right: -10px;
}
.ribbon-top-right::before,
.ribbon-top-right::after {
  border-top-color: transparent;
  border-right-color: transparent;
}
.ribbon-top-right::before {
  top: 0;
  left: 0;
}
.ribbon-top-right::after {
  bottom: 0;
  right: 0;
}
.ribbon-top-right span {
  left: -25px;
  top: 30px;
  transform: rotate(45deg);
}

/* bottom left*/
.ribbon-bottom-left {
  bottom: -10px;
  left: -10px;
}
.ribbon-bottom-left::before,
.ribbon-bottom-left::after {
  border-bottom-color: transparent;
  border-left-color: transparent;
}
.ribbon-bottom-left::before {
  bottom: 0;
  right: 0;
}
.ribbon-bottom-left::after {
  top: 0;
  left: 0;
}
.ribbon-bottom-left span {
  right: -25px;
  bottom: 0px;
  transform: rotate(225deg);
}

/* bottom right*/
.ribbon-bottom-right {
  bottom: -10px;
  right: -10px;
}
.ribbon-bottom-right::before,
.ribbon-bottom-right::after {
  border-bottom-color: transparent;
  border-right-color: transparent;
}
.ribbon-bottom-right::before {
  bottom: 0;
  left: 0;
}
.ribbon-bottom-right::after {
  top: 0;
  right: 0;
}
.ribbon-bottom-right span {
  left: -25px;
  bottom: 30px;
  transform: rotate(-225deg);
}
/**mobile only*/
@media only screen and (max-width: 600px){
table.dataTable tbody td {

    display: block;
}
table.dataTable tbody td {
    padding: 0 !important;
}
.table tbody tr td {
    border:none !important;
}
.hide-for-small {
    display:none !important;
}

.hide-not-for-small {
    display: flex;
}
.mobile-title.hide-not-for-small {
    display: block;
}
.header-right .notification_dropdown .nav-link i {
    font-size: 28px;
}
}
/**dekstop*/
@media only screen and (min-width: 601px){

.hide-for-small {
    display:inline-flex;
}

.hide-not-for-small {
    display:none !important;
}
.mobile-title.hide-not-for-small {
    display: none !important;
}
.desktop-total.hide-for-small {
    display: block;
}
}

/*override*/
.dataTables_wrapper .dataTables_paginate .paginate_button.previous, .dataTables_wrapper .dataTables_paginate .paginate_button.next, .pagination .page-item.active .page-link, .dataTables_wrapper .dataTables_paginate .paginate_button:hover, .dataTables_wrapper .dataTables_paginate .paginate_button.current:hover, .dataTables_wrapper .dataTables_paginate .paginate_button.current, .authincation-content, [data-sidebar-style="mini"][data-layout="vertical"] .deznav .metismenu > li.mm-active > a {
    background-color:none !important;
    border-color: none !important;
}
a.btn.btn-primary {
    background-color:#434345 !important;
    border-color:#434345 !important;
}
.new-arrival-content .price, .authincation-content, .text-primary {
    color:#434345 !important;
}
.bg-primary {
    background-color: #eb1b23 !important;
    color:#fff !important;
}
[data-sidebar-style="mini"][data-layout="vertical"] .deznav .metismenu > li.mm-active > a {
    color: #FFD523 !important;
}
.form-control {
    border-radius: 0 !important;
    margin-bottom:10px;
}

#main-wrapper {
    background: url(/asset/home/game_bg.jpg);
    /*background-size:cover;*/
    /*background: radial-gradient(#272727, #1b1b1b);*/
    background-position: center;
}
.deznav {
    background: url(/asset/home/deznav.jpg);
    background-size:contain;
}
.profile-info h4 {
    color: #fff !important;
}
.profile {
    background-color:#ffcc53;
}
.profile .credit {
    box-shadow: -5px -5px 0 #f3793b, 1px -1px 0 #f3793b, -1px 1px 0 #f3793b, 1px 1px 0 #f3793b;
    background-color:#fad56c;
    color:#3457D5 !important;
    font-weight:bold;
    border-radius:15px;
}

.card.step {
    background:none !important;
}
.form-control {
    border-radius:15px !important;
}
.fcomic2.item{
     font-size: 16pt;
}
.card-bg input[type=text] {
     position: relative;
     top:-30px;
     width:200px;
     font-weight:bold !important;
     color:black !important;
}
.bg-item {
   position: relative;
    left: -100px;
}

@media only screen and (max-width: 700px){
    
    .bg-item {
       position: relative;
       left: -30px;
    }
    
}

@media only screen and (max-width: 600px){
    
    .bg-item {
       position: relative;
       left: 0;
    }
    
    .media img.mr-3.rounded {
        right: 0;
    }
    
}



.fcomic {
 font-size: 26pt;
  font-family: 'Luckiest Guy';
  color: #fff;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
  text-shadow:-5px -5px 0 #f3793b, 1px -1px 0 #f3793b, -1px 1px 0 #f3793b, 1px 1px 0 #f3793b;
  position:relative;
  line-height: 30px;
  top:-20px;
}
.fcomic2{
    font-size: 12pt;
    font-family: 'Luckiest Guy';
    color: #fff;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
    text-shadow: -5px -5px 0 #f3793b, 1px -1px 0 #f3793b, -1px 1px 0 #f3793b, 1px 1px 0 #f3793b;
}

.comic {
 font-size: 37pt;
  font-family: 'Luckiest Guy';
  color: #fff;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
  text-shadow:-5px -5px 0 #f3793b, 1px -1px 0 #f3793b, -1px 1px 0 #f3793b, 1px 1px 0 #f3793b;
  position:relative;
  /*top:20px;*/


}


.profile .comic {
     font-size: 20pt;
}
.luna-img {
    display:none;
}
.new-arrival-content a {
    color: #e8c661;
}
.price, .authincation-content, .text-primary {
 
   text-shadow: 0 0 3vw #CF9FFF;
   text-align:center;
   
}
.rounded-circle-step {
    background-color: #eb1b23 !important;
    color: #fff !important;
    width: 50px;
    height: 50px;
    font-size: 30px;
    border-radius: 25px;
    top: 20px;
}
[data-cart-add] i.fa.fa-shopping-basket {
    color: rgb(76, 129, 71);
    font-size: 20px;
    position: relative;
    left: -50px;
    bottom: -30px;
}
.media img.mr-3.rounded {
    position: relative;
    right: -20px;
    bottom: -20px;
}

.media-body {
    left: 10px;
    position: relative;
}
.buy {
    border-radius: 3px;
    position: relative;
    width: 50px;
    left: -60px;
    top: 20px;
}
.content-body {
    /*padding-bottom:150px;*/
}
.price {
    font-size: 20px;
    position: relative;
    top: -20px;
    color: #3457D5 !important;
    font-weight: bold;
    background-color: #fad56c;
    border-radius: 30px;
    padding: 0 15px;
}
.card-body {
    padding:0.5rem !important;
}


.header-right .notification_dropdown .nav-link {
    /*background:none;*/
}
.scoreboard {
    background: url(/asset/home/scoreboard.png);
    background-size: cover;
    height: 350px;
    width: 100%;
}
div#DZ_W_TimeLine11Home {
    width:300px;
}
#target_info, #target_paginate {
    display:none !important;
}
.header-right .notification_dropdown .nav-link {
    background: none !important;
    left:-10px;
}
.header-right .notification_dropdown .nav-link .badge {
    top:5px !important;
    height: 20px;
    width: 20px;
    font-size: 10px;
    line-height: 17px;
}
.arrow {
    width: 50px;
    top: -35px;
    left: 60px;
    position: relative;
}
[data-cart] table {
    border-collapse: collapse;
    left: -50px;
    position: relative;
    top: 30px;
    background: none;
}

.photo-content .cover-photo {
   
    background-size: cover;
    background-position: center;
        min-height: 250px;
    width: 100%;
    border-radius:15px;
}
@media only screen and (max-width: 600px){
    .photo-content .cover-photo {
        min-height:100px !important;    
    }
    
}
</style>
<style type="text/css">

[data-cart] input {
   width: 90px;
    text-align: right;
}
    .ribbon span {
        background-image: url(/asset/home/sidebar.png);
    background-size: cover;
    }
    .new-arrival-content .price {
        float:left;
    }
    
    * {
  box-sizing: border-box;
}

:root {
  --gold: #ffb338;
  --light-shadow: #77571d;
  --dark-shadow: #3e2904;
}
body {
  margin: 0;
}
.p-wrapper{
    background: radial-gradient(#272727, #1b1b1b);
}
a {
    color: #272727 !important;
    
}
.wrapper {
  
  display: grid;
  grid-template-areas: 'overlap';
  place-content: center;
  text-transform: uppercase;
  /*height: 100vh;*/
  border-radius:15px;
}
.wrapper > div {
  background-clip: text;  
  -webkit-background-clip: text;
  color: #363833;
  font-family: 'Poppins', sans-serif;
  font-weight: 900;
  font-size: 50px;
  grid-area: overlap;
  letter-spacing: 1px;
  -webkit-text-stroke: 4px transparent;
}
div.bg {
  background-image: repeating-linear-gradient( 105deg, 
    var(--gold) 0% , 
    var(--dark-shadow) 5%,
    var(--gold) 12%);
  color: transparent;
  filter: drop-shadow(5px 15px 15px black);
  transform: scaleY(1.05);
  transform-origin: top;
}
div.fg{
  background-image: repeating-linear-gradient( 5deg,  
    var(--gold) 0% , 
    var(--light-shadow) 23%, 
    var(--gold) 31%);
  color: #1e2127;
  transform: scale(1);
}
.card-bg {
    background: url(/asset/home/card-bg.png);
    background-size:contain;
    background-repeat:no-repeat;
    height: 150px;
}

/*--------------------------------------------------------------
# Footer
--------------------------------------------------------------*/
#footer {
  background: #101522;
  padding: 0 0 25px 0;
  color: #eee;
  font-size: 14px;
}

#footer .footer-top {
  background: #040919;
  padding: 60px 0 30px 0;
}

#footer .footer-top .footer-info {
  margin-bottom: 30px;
}

#footer .footer-top .footer-info h3 {
  font-size: 26px;
  margin: 0 0 20px 0;
  padding: 2px 0 2px 0;
  line-height: 1;
  font-family: "Raleway", sans-serif;
  font-weight: 700;
  color: #fff;
}

#footer .footer-top .footer-info img {
  /*height: 40px;*/
  margin-bottom: 10px;
  text-align:center;
}

#footer .footer-top .footer-info p {
  font-size: 14px;
  line-height: 24px;
  margin-bottom: 0;
  font-family: "Raleway", sans-serif;
  color: #fff;
}

#footer .footer-top .social-links a {
  display: inline-block;
  background: #222636;
  color: #eee !important;
  line-height: 1;
  margin-right: 4px;
  border-radius: 50%;
  width: 36px;
  height: 36px;
  transition: 0.3s;
  display: inline-flex;
  align-items: center;
  justify-content: center;
}

#footer .footer-top .social-links a i {
  line-height: 0;
  font-size: 16px;
}

#footer .footer-top .social-links a:hover {
    background: #f3793b;
    color: #fff;
}

#footer .footer-top h4 {
  font-size: 14px;
  font-weight: bold;
  color: #fff;
  text-transform: uppercase;
  position: relative;
  padding-bottom: 12px;
  border-bottom: 2px solid #3457D5;
}

#footer .footer-top .footer-links {
  margin-bottom: 30px;
}
#footer a {
      color: #eee !important;
}
#footer .footer-top .footer-links ul {
  list-style: none;
  padding: 0;
  margin: 0;
}

#footer .footer-top .footer-links ul i {
  padding-right: 5px;
  color: #f82249;
  font-size: 18px;
}

#footer .footer-top .footer-links ul li {
  border-bottom: 1px solid #262c44;
  padding: 10px 0;
}

#footer .footer-top .footer-links ul li:first-child {
  padding-top: 0;
}

#footer .footer-top .footer-links ul a {
  color: #eee !important;
}

#footer .footer-top .footer-links ul a:hover {
  color: #f82249;
}

#footer .footer-top .footer-contact {
  margin-bottom: 30px;
}

#footer .footer-top .footer-contact p {
  line-height: 26px;
}

#footer .footer-top .footer-newsletter {
  margin-bottom: 30px;
}

#footer .footer-top .footer-newsletter input[type=email] {
  border: 0;
  padding: 6px 8px;
  width: 65%;
}

#footer .footer-top .footer-newsletter input[type=submit] {
  background: #f82249;
  border: 0;
  width: 35%;
  padding: 6px 0;
  text-align: center;
  color: #fff;
  transition: 0.3s;
  cursor: pointer;
}

#footer .footer-top .footer-newsletter input[type=submit]:hover {
  background: #e0072f;
}

#footer .copyright {
  text-align: center;
  padding-top: 30px;
}

#footer .credits {
  text-align: center;
  font-size: 13px;
  color: #ddd;
}
 
</style>

</head>

<body>

    <!--*******************
        Preloader start
    ********************-->
    <div id="preloader">
        <div class="sk-three-bounce">
            <div class="sk-child sk-bounce1"></div>
            <div class="sk-child sk-bounce2"></div>
            <div class="sk-child sk-bounce3"></div>
        </div>
    </div>
    <!--*******************
        Preloader end
    ********************-->


    <!--**********************************
        Main wrapper start
    ***********************************-->
    <div id="main-wrapper">

        <!--**********************************
            Nav header start
        ***********************************-->
        <div class="nav-header">
             
            <a href="/" class="brand-logo">
              
                <!--<img class="brand-title" src="https://nexustech.online/asset/home/demo_cropped.png" alt="">-->
                <img class="logo-abbr" src="/asset/home/luna_rose_logo.png" alt="">
                <img class="logo-compact" src="/asset/home/luna_rose_logo.png" alt="">
                <img class="brand-title" src="/asset/home/luna_rose_white.png" alt="" >
            </a>

          

            <div class="nav-control">
                <div class="hamburger">
                    <span class="line"></span><span class="line"></span><span class="line"></span>
                </div>
            </div>
        </div>
        <!--**********************************
            Nav header end
        ***********************************-->
        
       


        
        
        <!--**********************************
            Header start
        ***********************************-->
        <div class="header">
            <div class="header-content">
                <nav class="navbar navbar-expand">
                    <div class="collapse navbar-collapse justify-content-between">
                        <div class="header-left">
                            <div class="dashboard_bar">
                                <?php echo (isset($main_title)? $main_title: '');?>
                            </div>
                        </div>
                         <ul class="navbar-nav header-right">
                           <?php
                           $flags = [
                                'sg'=>'/asset/flags/singapore.png',
                                'bn'=>'/asset/flags/bangladesh.png',
                                'my'=>'/asset/flags/malaysia.png',
                                'zh'=>'/asset/flags/china.png',
                                'id'=>'/asset/flags/indonesia.png',
                               ];
                           ?>
                        
                           <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <img width="30px" src="<?php echo $flags[Config::get('languages')[App::getLocale()]['flag-icon']];?>">  {{ Config::get('languages')[App::getLocale()]['display'] }}
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                            @foreach (Config::get('languages') as $lang => $language)
                                @if ($lang != App::getLocale())
                                        <a class="dropdown-item" href="{{ route('lang.switch', $lang) }}"><img width="30px" src="<?php echo $flags[$language['flag-icon']];?>"> {{$language['display']}}</a>
                                @endif
                            @endforeach
                            </div>
                    </li>
                           
                             <li class="nav-item dropdown notification_dropdown">
                                <a class="nav-link" href="javascript:void(0)">
                                   <!--<i class="fa fa-shopping-basket" style="color:rgb(76, 129, 71);"></i>-->
                                   <img src="/asset/home/noti.png" style="width:40px !important;">
                                    <span class="badge light text-white bg-primary rounded-circle hidden" data-cart-wrapper>1</span>
                                </a>
                                <!--<div class="dropdown-menu dropdown-menu-right rounded">-->
                                <!--    <div id="DZ_W_TimeLine11Home" class="widget-timeline dz-scroll style-1 p-3 height370 ps ps--active-y">-->
                                <!--        <form action="/checkout" method="POST">-->
                                <!--            @csrf-->
                                <!--            <ul class="contacts" data-cart>-->
                                        
                                <!--            </ul>-->
                                <!--            <div class="shopping-cart mt-3">-->
                                           
                                <!--                     <button type="submit" style="background:#434345;border-color: #434345;color:#fff;" class="btn subtotal-preview-btn" data-submit><i class="fa fa-shopping-basket mr-2"></i>Checkout</button>-->
                                              
                                <!--            </div>-->
                                <!--        </form>-->
                                <!--    </div>-->
                                <!--</div>-->
                            </li>
                            
                           
    
                             <li class="hide-for-small nav-item">
                                <a class="nav-link" href="/product">{{ __('Packages') }}</a>
                            </li>
                            <li class="hide-for-small nav-item">
                                <a class="nav-link" href="/about">{{ __('About') }}</a>
                            </li>
                           <li class="hide-for-small nav-item dropdown header-profile">
                                @guest
                                @else
                                <a class="nav-link" href="javascript:void(0)" role="button" data-toggle="dropdown">
                                    <img src="/asset/front/images/profile/profile-circle.svg" width="20" alt=""/>
                                    <div class="header-info">
                                        <span class="text-black"><strong>
                                           
                                            
                                            {{ Auth::user()->name }}</strong></span>
                                        <p class="fs-12 mb-0">Admin</p>
                                       
                                    </div>
                                </a>
                                 @endguest
                                <div class="dropdown-menu dropdown-menu-right">
                                  
                                   
                                     @guest
                                    <!-- <a href="/login" class="dropdown-item ai-icon">-->
                                    <!--    <svg id="icon-logout" xmlns="http://www.w3.org/2000/svg" class="text-danger" width="18" height="18" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><path d="M9 21H5a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h4"></path><polyline points="16 17 21 12 16 7"></polyline><line x1="21" y1="12" x2="9" y2="12"></line></svg>-->
                                    <!--    <span class="ml-2">Login </span>-->
                                    <!--</a>-->
                                    <!--<a href="/register" class="dropdown-item ai-icon">-->
                                    <!--    <svg id="icon-logout" xmlns="http://www.w3.org/2000/svg" class="text-danger" width="18" height="18" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><path d="M9 21H5a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h4"></path><polyline points="16 17 21 12 16 7"></polyline><line x1="21" y1="12" x2="9" y2="12"></line></svg>-->
                                    <!--    <span class="ml-2">Register </span>-->
                                    <!--</a>-->
                                     @else
                                      <a href="/home" class="dropdown-item ai-icon">
                                        <svg id="icon-inbox" xmlns="http://www.w3.org/2000/svg" class="text-success" width="18" height="18" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><path d="M4 4h16c1.1 0 2 .9 2 2v12c0 1.1-.9 2-2 2H4c-1.1 0-2-.9-2-2V6c0-1.1.9-2 2-2z"></path><polyline points="22,6 12,13 2,6"></polyline></svg>
                                        <span class="ml-2">Dashboard </span>
                                    </a>
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                                       onclick="event.preventDefault();
                                                                     document.getElementById('logout-form').submit();" class="dropdown-item ai-icon">
                                        <svg id="icon-logout" xmlns="http://www.w3.org/2000/svg" class="text-danger" width="18" height="18" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><path d="M9 21H5a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h4"></path><polyline points="16 17 21 12 16 7"></polyline><line x1="21" y1="12" x2="9" y2="12"></line></svg>
                                        <span class="ml-2">Logout </span>
                                    </a>
                                    @endguest
                                </div>
                            </li>
                                   
                       
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
        <!--**********************************
            Header end ti-comment-alt
        ***********************************-->

        <!--**********************************
            Sidebar start
        ***********************************-->
        <div class="deznav">
           <div class="widget-timeline style-1">
                <div class="add-menu-sidebar">
                                         
                <ul class="timeline" id="menu">
                    
                     <li>
                         @guest
                         
                        @else
                         <div class="timeline-badge info"></div>
                         <a class="text-muted" href="/home" aria-expanded="false">
                          
                            <span class="nav-text"><h6 class="mb-0">Dashboard</h6></span>
                        </a>
                        @endguest
                        
                    </li>

                     <?php
                        if (isset($cats)) {
                            echo $cats;
                        } ?>
                    
                </ul>
                </div>
                
               
                <div class="copyright">
                    <p>© 2023 All Rights Reserved</p>
                    <p>Made with <span class="heart"></span> by Nexus Tech</p>
                </div>

            </div>
            
        </div>
        <!--**********************************
            Sidebar end
        ***********************************-->

        <!--**********************************
            Content body start
        ***********************************-->
        <div class="content-body">
            <div class="container-fluid">
                <!-- Add Order -->
                <div class="modal fade" id="addOrderModalside">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Add Menus</h5>
                                <button type="button" class="close" data-dismiss="modal"><span>&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <form>
                                    <div class="form-group">
                                        <label class="text-black font-w500">Food Name</label>
                                        <input type="text" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label class="text-black font-w500">Order Date</label>
                                        <input type="date" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label class="text-black font-w500">Food Price</label>
                                        <input type="text" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <button type="button" class="btn btn-primary">Submit</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <!--<div class="page-titles">-->
                <!--    <ol class="breadcrumb">-->
                        <?php 
                        // if(isset($bread)) {
                           
                        //     foreach($bread as $key=>$value) {
                        //         foreach($value as $k=>$v) {
                            ?>
                             <!--<li class="breadcrumb-item"><a href="<?php #echo $k;?>"><?php #echo $v;?></a></li>-->
                            <?php
                        //         }
                        //     }
                        // }
                        
                        ?>
                       
                        
                <!--    </ol>-->
                <!--</div>-->
            
                <div id="app">
                   

                    <main class="py-12">
                       
                            @yield('content')
                   
                         
                    </main>

                </div>
    
            </div>
        </div>
       
         <footer id="footer">
    <div class="footer-top">
      <div class="container">
        <div class="row">
           
          <div class="col-lg-4 col-md-6 footer-info">
               <div class="col-12 text-center">
                 <img src="/asset/home/luna_rose_logo.png">
            </div>
             
           
          </div>

          <div class="col-lg-4 col-md-6 footer-links">
            <h4>Useful Links</h4>
            <ul>
              <li><i class="bi bi-chevron-right"></i> <a href="/">Home</a></li>
              <li><i class="bi bi-chevron-right"></i> <a href="/about">About Us</a></li>
              <li><i class="bi bi-chevron-right"></i> <a href="/become-a-reseller">Become a Reseller</a></li>
              <li><i class="bi bi-chevron-right"></i> <a href="/contact">Contact Us</a></li>
              <!--<li><i class="bi bi-chevron-right"></i> <a href="/login">Login/Register</a></li>-->
              <li><i class="bi bi-chevron-right"></i> <a href="/store">Our Store </a></li>
              <li><i class="bi bi-chevron-right"></i> <a href="/privacy-policy">Privacy Policy</a></li>
              <li><i class="bi bi-chevron-right"></i> <a href="/refund">Refund & Return Policy</a></li>
              <li><i class="bi bi-chevron-right"></i> <a href="/terms">Terms & Conditions</a></li>
            </ul>
          </div>


          <div class="col-lg-4 col-md-6 footer-contact">
              <h4>Owned By</h4>
              <img src="http://lunashop.my/asset/home/luna_logo_white.png">
              <br/>
            <h4>Contact Us</h4>
            <p>
             Lunashop World Sdn Bhd<br/>
              No. 1-2-1B, 2nd Floor, Block B, Kolam Centre Phase II,<br/> Jalan Lintas, Luyang,<br/> 88300 Kota Kinabalu,<br/> Sabah, Malaysia. 
              <strong>Phone:</strong> <a href="tel:+60164009418">+60164009418</a><br>
              <strong>Email:</strong> <a href="mailto:lunashop.my1@gmail.com ">lunashop.my1@gmail.com </a><br>
            </p>

            <div class="social-links">

              <a href="https://www.facebook.com/lunashop.my/" class="facebook" target="_blank"><img src="/asset/home/social_icons/fb.png" width="20"></a>
              <a href=" http://m.me/lunashop.my" class="facebook" target="_blank"><img src="/asset/home/social_icons/messager.png" width="20"></a>
              
             

              <a href="https://instagram.com/lunashop.my?igshid=MzRlODBiNWFlZA" class="instagram" target="_blank"><img src="/asset/home/social_icons/insta.png" width="20"></a>
              <a href="https://www.lunashop.my/" class="google-plus"><img src="/asset/home/social_icons/website.png" width="20"></a>
              <a href="https://shopee.com.my/lunashop.my1" class="linkedin" target="_blank"><img src="/asset/home/social_icons/shopee.png" width="20"></a>
              <a href="https://wa.me/+60164009418" class="linkedin" target="_blank"><img src="/asset/home/social_icons/whatsapp.png" width="20"></a>

            </div>

          </div>

        </div>
      </div>
    </div>

  </footer><!-- End  Footer -->
  
        <div class="footer">
            <div class="copyright">
                <div class="row">
                    <div class="col-4 text-center">
                        <a href="/">
                       <img src="/asset/home/home.png"  width="55"> 
                       </a>
                    </div>
                     <div class="col-4 text-center">
                       <a href="/category"><img src="/asset/home/game.png" width="55"></a> 
                    </div>
                     <div class="col-4 text-center">
                       <a href="/about"><img src="/asset/home/info.png" width="50" style="position:relative;top:5px;"></a>
                    </div>
                     <!--<div class="col-3">-->
                         
                       
                        <!--  @guest-->
                        <!--    @if (Route::has('login'))-->
                               <!--<a href="{{ route('login') }}">-->
                               <!--    <img src="/asset/home/login.png" width="50" alt="login"></a>-->
                        <!--    @endif-->
                        
                            
                        <!--@else-->
                   <!--     <a href="{{ route('logout') }}"-->
                   <!--onclick="event.preventDefault();-->
                   <!--              document.getElementById('logout-form').submit();">-->
                   <!--        <img src="/asset/home/login.png" width="50" alt="logout"> -->
                   <!--        </a>-->
                   <!--        <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">-->
                   <!--             @csrf-->
                   <!--         </form>-->
                   <!--     @endguest-->

                   <!-- </div>-->
                </div>
            </div>
        </div>
        <!--**********************************
            Footer end
        ***********************************-->

        <!--**********************************
           Support ticket button start
        ***********************************-->

        <!--**********************************
           Support ticket button end
        ***********************************-->


</div>
    <!--**********************************
        Main wrapper end
    ***********************************-->

    <!--**********************************
        Scripts
    ***********************************-->

    <script type="text/javascript">
      var csrfToken = "{{ csrf_token() }}";

    </script>


    <!-- Required vendors -->
    {{Html::script('app-assets/vendors/js/vendors.min.js')}}
    {{Html::script('app-assets/fonts/LivIconsEvo/js/LivIconsEvo.tools.js')}}
    {{Html::script('app-assets/fonts/LivIconsEvo/js/LivIconsEvo.defaults.js')}}
    {{Html::script('app-assets/fonts/LivIconsEvo/js/LivIconsEvo.min.js')}}
    {{Html::script('app-assets/vendors/js/forms/repeater/jquery.repeater.min.js')}}
    {{Html::script('app-assets/js/scripts/configs/vertical-menu-dark.js')}}

    {{Html::script('app-assets/js/scripts/components.js')}}


    {{Html::script('assets/theme/vendor/aos/aos.js')}}
    {{Html::script('assets/theme/vendor/glightbox/js/glightbox.min.js')}}
    {{Html::script('assets/theme/vendor/isotope-layout/isotope.pkgd.min.js')}}
    {{Html::script('assets/theme/vendor/swiper/swiper-bundle.min.js')}}
    {{Html::script('assets/theme/vendor/waypoints/noframework.waypoints.js')}} 
    {{Html::script('assets/theme/js/main.js')}}


      <!--do not change this-->
      <script src="{{ '/asset/front/vendor/global/global_modified.min.js'}}" defer></script>
       <script src="{{ '/asset/front/vendor/bootstrap-select/dist/js/bootstrap-select.min.js'}}" defer></script> 
      <script src="{{ '/asset/front/js/custom.min.js'}}" defer></script>
      <script src="{{ '/asset/front/js/deznav-init.js'}}" defer></script>
<script>
   $(function() {
    //   localStorage.destroy();
        if(JSON.parse(localStorage.getItem("cart")) == null) {
            var cart = [];

            localStorage.clear();

        } else {
            var cart = JSON.parse(localStorage.getItem("cart"));

        }

        
        var qty = 0;
        var sub_total2 = 0.00;
        var total2 = 0.00;
        var obj = {};
        var sub_total = 0.00;
        var total = 0.00;
        var cart = [];

       
        if(cart.length > 0) {
            shoppingCart();
        }
        
    

         var variants = [];
        function updateCart(id, name, regular, sale, qty)
        {
            if(!id) {
                var id = $('[data-main-product-name]').attr('id');
                var name = $('[data-main-product-name]').text();
                var regular = $('#pp').val();
                var sale = $('#ps').val();
                var qty = $('#qty').val();
            }
           
            
            sub_total = 0.00;
            total = 0.00;
            
            if(jQuery.inArray(id, Object.keys(cart)) != -1) {
         
                if(cart[id]['variant'].length <= 0) {
                  
                    cart[id]['quantity'] = parseInt(cart[id]['quantity']) + parseInt(qty);
                }
            } else {
                if($('[data-variation]').length >0) {
                    cart[id] = {'name':name, 'regular':regular, 'sale':sale, 'variant':[], 'variants':[]};
                } else {
                    if(id != '') {
                        cart[id] = {'name':name, 'regular':regular, 'sale':sale,'quantity':qty, 'variant':[], 'variants':[]};
                    }
                    
                }
                
            }
            

            var variant_name = '';
            var variant_value = '';
            var a = [];
            var b ='';
            var c = [];
            
            
            if($('[data-variation]').length > 0) {
           
                $.each($('[data-variation]'), function(key, value) {
                 
                    variant_name = $(value).attr('id');
                    variant_value = $(value).val();
                    custom_variant = variant_name+'_'+variant_value;
    
                    b += variant_value;
              
                    a.push({'item': $(value).attr('id'),
                            'value': $(value).val(),
                            'regular':$(value).find('option:selected').data('price')});
                });
                
    
                if(cart[id]['variant'].length <= 0) {
                     c[b] = [{'item':a, 'quantity':qty}];
                     cart[id]['variant'].push(c);
                     cart[id]['variants'].push(b);
                } else {
                    
      
                if(jQuery.inArray(b, cart[id]['variants']) != -1) {
                    $.each(cart[id]['variant'], function(key, value) {
                        
                        if(typeof(cart[id]['variant'][key][b])!='undefined') {
                             cart[id]['variant'][key][b][0]['quantity'] = parseInt(cart[id]['variant'][key][Object.keys(value)][0]['quantity']) + parseInt(qty);
                        }
                    
                              
                   
                    });
                    
                } else {
                     c[b] = [{'item':a, 'quantity':qty}];
                     cart[id]['variant'].push(c);
                     cart[id]['variants'].push(b);
                }
                
               
                }
            
            }
            
            console.log(cart);
            localStorage.setItem("cart", JSON.stringify(cart));
         

        }
        
        
        $('body').on('click', '[data-cart-add]', function() {

            var $item = $(this).parents('[data-block-wrapper]');
            var id = $item.attr('id');
            var name = $item.find('[data-item-name]').text();
            var regular = $item.find('[data-item-price]').val();
            var sale = 0;
            var qty = 1;
            
            updateCart(id, name, regular, sale, qty);
            shoppingCart();
        
            
        });
        
        function shoppingCart()
        {
            var cartTable = '<table style="border-collapse: collapse;">';
            var grandTotal = 0;
            var sub_total = 0;
            
            $.each(cart, function(k, v) {
                if(v != null) {
                    
                    cartTable += '<tr><th><input style="color:#eb1b23;" type="text" name="product['+k+'][product][name]" value="'+v.name+'" readonly></th>';
                    if(v.variant.length <= 0) {
                        
                         
                         sub_total += (v.regular * parseInt(v.quantity));
                         
                        cartTable += '<th><input style="color:#eb1b23;" type="text" name="product['+k+'][product][quantity]" value="'+v.quantity+'" readonly></th>';
                    }
                    
                    if(v.variant.length <= 0) {
                        cartTable += '<th><input style="color:#eb1b23;" type="text" name="product['+k+'][product][regular]" value="'+v.regular+'" readonly></th>';
                    }
                    cartTable += '</tr>';
                                 
                      
                        
                        $.each(v.variant, function(k2, v2) {
                         
                            $.each(Object.values(v2), function(k3, v3) {
               
                                    var sub_total = 0;
                                    $.each(v3[0].item, function(k4, v4) {
                                   
                                    sub_total += (v4.regular * parseInt(v3[0].quantity));
                                    
                                        
                                    cartTable += '<tr>';

                                    cartTable += '<td><div class="timeline-badge info">';
            						cartTable += '</div>';
            						cartTable += '<a class="timeline-panel text-muted" href="#">';
            						cartTable += '<span><input type="hidden" name="product['+k+'][variant]['+Object.keys(v2)+'][item]['+k4+'][name]" value="'+v4.item+'"><input type="text" name="product['+k+'][variant]['+Object.keys(v2)+'][item]['+k4+'][value]" value="'+v4.value+'" readonly></span>';
            		
            						cartTable +='</a></td>';
            						cartTable +='<td><input type="hidden" name="product['+k+'][variant]['+Object.keys(v2)+'][quantity]" value='+v3[0].quantity+'> x'+v3[0].quantity+'</td>';
                                    cartTable += '<td><input style="text-align:right;" type="text" name="product['+k+'][variant]['+Object.keys(v2)+'][item]['+k4+'][regular]" value="'+v4.regular+'" readonly></td>';
                                    cartTable += '</tr>';
                                    
                                   
                                });
                                grandTotal += parseInt(sub_total).toFixed(2);
                                cartTable += '<tr><td colspan="2">Sub total</td><td colspan="2" class="text-right">'+sub_total+'</td></tr>';
                                 cartTable += '<tr><td colspan="3"><hr></td></tr>';
                            });
                     });
                     
                     if(v.variant.length <= 0) {
                         
                         grandTotal = parseInt(grandTotal) + parseInt(sub_total);
                          cartTable += '<tr><td class="text-right">Sub total</td><td></td><td><input type="text" name="product['+k+'][product][sub_total]" value='+sub_total+'></td></tr>';
                                 cartTable += '<tr><td></td><td><hr></td></tr>';
                         
                     }
                    
                    
                    
                }
                
            });
            
           
                                 
            cartTable += '<tr><td class="text-right">Grand total</td><td colspan="2" class="text-right"><input type="text" name="grand_total" value='+grandTotal.toFixed(2)+'></td></tr>';
            cartTable += '<table>';
            
            $('[data-cart]').html(cartTable);
            
            var bell = JSON.parse(localStorage.getItem("cart"));
            
            bell2 = [];
            $.each(bell, function(x, y) {
                if(y!=null) {
          
                    bell2.push(y);
                }
           
            });
            $('[data-cart-wrapper]').removeClass('hidden').text(bell2.length);
           
            
            
            
            
        }
        
        
        
        
        


    });
</script>
@yield('scripts')




</body>

</html>