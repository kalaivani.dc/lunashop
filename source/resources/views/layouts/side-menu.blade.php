<!-- BEGIN: Main Menu-->
     <div class="main-menu menu-fixed menu-light menu-accordion menu-shadow" data-scroll-to-active="true">
        <div class="navbar-header">
            <ul class="nav navbar-nav flex-row">

            
                
                <li class="nav-item mr-auto"><a class="navbar-brand" href="/">
                    <div class="brand-logo">
                    <img src="/asset/home/luna.png" style="width:100%;top: -5px;position: relative;">
                    </div>
                    <h2 class="brand-text mb-0">LUNASHOP</h2>
                </a></li>
                <li class="nav-item nav-toggle"><a class="nav-link modern-nav-toggle pr-0" data-toggle="collapse"><i class="bx bx-x d-block d-xl-none font-medium-4 primary"></i><i class="toggle-icon bx-disc font-medium-4 d-none d-xl-block primary bx" data-ticon="bx-disc"></i></a></li>
            </ul>
        </div>
        <div class="shadow-bottom"></div>
        <div class="main-menu-content">
         
        <!--menu starts-->
        <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation" data-icon-style="lines">
            <?php
            $marketplace = \Route::current()->parameter('sub');
            $role = \Route::current()->parameter('role');
            $site = '/';
            ?>
            <?php
            if (isset($menu)) {
                echo $menu;
            } 
            else {
             ?>
              <li>
               <a class="d-flex align-items-center" href="<?php echo $site;?><?php echo  $role;?>/dashboard"><i class="bx bx-home-circle"></i><span class="menu-item text-truncate" data-i18n="Listings">Dashboard</span></a>
               </li>
              <?php 
               
               
                 if (isset($stores)) {
                   
                     $stores2 = $stores;
                     

                    if (isset($stores['id'])) {
                        $stores2 = $stores['id'];
                    }
                    $i = 0;
                    foreach ($stores2 as $key => $store) {
                       
                 ?>
                    <?php
                    if ($marketplace == 'shopee') {
                    ?>
                    <li class="nav-item"><a href="#"><i class="bx bx-store-alt"></i><span class="menu-title text-truncate" data-i18n="{{ $stores['name'][$i] }}">{{ $stores['name'][$i] }}</span></a>
                    <?php
                        // echo $stores['name'][$i];
                    } else {
                    ?>
                    <li class="nav-item"><a href="#"><i class="bx bx-store-alt"></i><span class="menu-title text-truncate" data-i18n="{{ $store }}">{{ $store }}</span></a>
                    <?php
                        // echo $store;
                    }
                    ?>
                    </a>
                        <ul class="menu-content">
                        <li>
                            <a class="d-flex align-items-center" href="<?php echo $site;?><?php echo  $role;?>/marketplace/<?php echo  $marketplace;?>?tab=launchpack&q=<?php echo $store;?>"><i class="bx bx-list-ul"></i><span class="menu-item text-truncate" data-i18n="Listings">Launchpack</span></a>
                        </li>
                        <li>
                            <a class="d-flex align-items-center" href="<?php echo $site;?><?php echo  $role;?>/marketplace/<?php echo  $marketplace;?>?tab=batch&q=<?php echo $store;?>"><i class="bx bx-list-ul"></i><span class="menu-item text-truncate" data-i18n="Listings">Batch</span></a>
                        </li>
                        <li>
                            <a class="d-flex align-items-center" href="<?php echo $site;?><?php echo  $role;?>/marketplace/<?php echo  $marketplace;?>?tab=listings&q=<?php echo $store;?>"><i class="bx bx-detail"></i><span class="menu-item text-truncate" data-i18n="Listings">Listings</span></a>
                        </li>
                        <li>
                            <a class="d-flex align-items-center" href="<?php echo $site;?><?php echo  $role;?>/marketplace/<?php echo  $marketplace;?>?tab=orders&q=<?php echo $store;?>"><i class="bx bx-cart-alt"></i><span class="menu-item text-truncate" data-i18n="Orders">Orders</span></a>
                        </li>
                        <li>
                            <a class="d-flex align-items-center" href="<?php echo $site;?><?php echo  $role;?>/marketplace/<?php echo $marketplace;?>?tab=invoice&q=<?php echo $store;?>"><i class="bx bx-receipt"></i><span class="menu-item text-truncate" data-i18n="Invoice">Invoice</span></a>
                        </li>

                    </ul>
                       </li>
                      
             <?php   
             $i++;
                     
            }
          }
          ?>
          
          <?php
        }
            ?>
            </ul>
        <!--menu ends-->
        </div>
    </div>
    <!-- END: Main Menu-->