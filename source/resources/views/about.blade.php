@extends('layouts.app2')


@section('styles')

@endsection

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                           <p style="text-align:justify;">
             
                            Founded in 2020, Lunashop has been providing game credits top up for leading games like Mobile Legends: Bang Bang (Moonton), Free Fire (Garena) and PUBG Mobile (Tencent), starting at Shopee Malaysia platform and now moving to our very own website; Lunashop.my.</p>
                            <p style="text-align:justify;">
                            Here at Lunashop, we have been and will always continue to cater more game related products. Our beliefs are to give our customers a one-stop center of game credits top up, through continuously expanding our product varieties and availability.</p>
                            <p style="text-align:justify;">
                            Lunashop has recently integrated with the most credible and safe payment gateway (Razer Merchant Services) to provide regionally localized payment methods. Till today, we have hundreds of localized payment methods including abroad from Malaysia, to further give our customers an enhanced shopping freedom.</p>
                            <p style="text-align:justify;">
                            To enhance your shopping experience, our website is specifically designed to give you a seamless checkout system and a near instant delivery on digital products. Be assured that we will continually upgrade our services to provide all our customers with the best shopping experience you can find anywhere.</p>
                            <p style="text-align:justify;">
                            Enjoy a hassle-free purchase experience when buying game credits at Lunashop, the one-stop center of your top up world. We accept almost all types of payments and offer various game products.
                            </p>
                            <p style="text-align:justify;">
                            What are you waiting for? Let’s shop now! 
                            
                            Xoxo.
                            </p>

            </div>
        </div>
    </div>
</div>

@endsection

