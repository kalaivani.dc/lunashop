@extends('layouts.app2')


@section('styles')

@endsection

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <h1>User Terms and Conditions</h1>
<hr>

<p style="text-align:justify;">This web page represents our Terms and Conditions on the Use of the Lunashop ("Terms"), located at https://www.lunashop.my and the tools provided by Lunashop and/or its affiliates and governs all products offered by Lunashop as particularly described below (“Products”). These Terms and Conditions apply to your use of all of Lunashop Products and website developed by LUNA TOP UP WORLD. You acknowledge that you are aware of the contents of and agree to be legally bound by these Terms and Conditions. The terms, "we" and "our" as used in this Term refer to Lunashop. Our Product permit you to make purchases at our Website using your mobile account or payment channels.</p>
<p style="text-align:justify;">We may amend these Term at any time by posting the amended terms on our Website. We may or may not post notices on the homepage of our Website when such changes occur. By assessing, using and making purchases using our Product, you are agreeing to these Terms.</p>
<ol>
<li>Lunashop reserves the right to refuse to provide the Product to anyone for any reason at any time. Your use of the Product and/or website is at your sole risk. In circumstances where you are authorized both to make purchases for the organization, you may make purchases using a corporate or business mobile account or payment instrument and shall agree to these Terms on its behalf.</li>
<li>You expressly understand and agree that Lunashop shall not be liable for any direct, indirect, incidental, special, consequential or exemplary damages, or damages for loss of profits, goodwill, use, data or other intangible losses resulting from the use of or inability to use the Product and/or website.</li>
<li>In no event shall Lunashop or our merchants be liable for lost profits or any special, incidental or consequential damages arising out of or in connection with our website, our Product or these Terms (however arising including negligence). You agree to indemnify and hold us and (as applicable) our parent, subsidiaries, affiliates, officers, directors, agents, and employees, harmless from any claim or demand, including reasonable attorneys´ fees, made by any third party due to or arising out of your breach of these Terms or the documents it incorporates by reference, or your violation of any law or the rights of a third party or your use of the Product and/or our website.</li>
<li>The Product may provide, or third parties may provide, links to other World Wide Web (www) sites or resources. Because Lunashop has no control over such websites and resources, you acknowledge and accept that Lunashop is not responsible for the availability of such external websites or resources, and do not endorse and is not responsible or liable for any information, data, text, software, music, sound, photographs, graphics, video, messages, tags, or other materials ("Content"), advertising, products or other materials on or available from such websites or resources. As such, you also acknowledge and accept that Lunashop does not and is not obligated to examine, evaluate or screen any of these external resources and does not warrant or endorse any of the information, content, offers or claims of these third parties. You further acknowledge and agree that Lunashop shall not be responsible or liable, directly or indirectly, for any damage or loss caused or alleged to be caused by or in connection with use of or reliance on any such Content, goods available on or through any such website or resource.</li>
<li>You agree not to alter, modify, reproduce, duplicate, copy, sell, resell or exploit any portion of the Product, use of the Product, or access to the Product without the express written permission by Lunashop.</li>
<li>The entire content of this website which consists of inter alia text, video (of any format, streaming or otherwise), audio clips (of any format, streaming or otherwise), data assemblages, graphics, logos, buttons, icons and any software (the "Site Content") is proprietary to Lunashop or its content provider or other third parties and is protected under international and domestic copyright laws. The arrangement and/or compilation of the Website Content is proprietary to Lunashop and is protected under international and domestic copyright laws.</li>
<li>Verbal or written abuse of any kind (including but not limited to threats of abuse or retribution and defamatory statements) of any Lunashop’s customer, employee, member, or officer will result in immediate account termination.</li>
<li>The failure of Lunashop to exercise or enforce any right or provision of the Terms and Conditions shall not constitute a waiver of such right or provision.</li>
<li>Using our Product, you agree that:
<ul>
a.	transactions made via Lunashop cannot be refuted once initiated.
b.	you are solely responsible in the usage of your mobile account or payment channel and that Lunashop shall have no liability to you or any third party for any unauthorized use or access of Lunashop through your mobile account or payment channels or in the event your mobile account or your account has been compromised. In such circumstances, it is your responsibility to inform your mobile operator or your payment channel of such instances.
c.	any taxes, duties, currency exchange fees, data charges and any other applicable charges in relation to your purchases via Lunashop shall be borne by you.
d.	Lunashop, or your mobile operator if made purchase via your mobile account or your chosen payment channel provided at Lunashop.com has the sole discretion to accept or decline a transaction performed by you through Lunashop for whatsoever reason, and that, should this occur, Lunashop shall not be held liable to you.
e.	you have consented to be contacted by Lunashop, and to receive notices electronically, including but not limited to by text message, from us. You agree that we may make any notices that we may be required by law to make in electronic format. These communications will be deemed to be in writing and received by you when sent to you.
f.	you may be required to register with the Website. You agree to keep your password confidential and will be responsible for all use of your account and password. We reserve the right to remove, reclaim, or change a username you select if we determine, in our sole discretion, that such username is inappropriate, obscene, or otherwise objectionable.
10.	As a user of our Product, you agree not to:
a.	use the Product in any way that violates any applicable law or for any unlawful purpose.
b.	disparage, tarnish, or otherwise harm, Lunashop, our websites, or our Product.
c.	make any unauthorized use or access of the Product, including collecting usernames and/or email addresses of users by electronic or other means for the purpose of sending unsolicited email, or creating user account by automated means or under false pretences.
d.	use a buying agent or purchasing agent to make purchases using our Products and thereafter charge other directly or indirectly for such uses.
e.	attempt to impersonate another user or person or use the username of another user.
f.	use any information obtained from the Website in order to harass, abuse, or harm another person.
g.	systematically retrieve data or other content from the Website to create or compile, directly or indirectly, a collection, compilation, database, or directory without written permission from us.
h.	circumvent, disable, or otherwise interfere with security-related features of the Website, including features that prevent or restrict the use or copying of any content or enforce limitations on the use of the Website and/or the content contained therein.
i.	engage in unauthorized framing of or linking to the Website/Product.
j.	trick, defraud, or mislead us and other users, especially in any attempt to learn sensitive account information such as user passwords;
k.	make improper use of our support services or submit false reports of abuse or misconduct.
l.	engage in any automated use of the system, such as using scripts to send comments or messages, or using any data mining, robots, or similar data gathering and extraction tools.
m.	interfere with, disrupt, or create an undue burden on the Website or the networks or Product connected to the Website.
n.	decipher, decompile, disassemble, or reverse engineer any of the software comprising or in any way making up a part of the Website.
o.	attempt to bypass any measures of the Website designed to prevent or restrict access to the Website, or any portion of the Website.
p.	harass, annoy, intimidate, or threaten any of our employees or agents engaged in providing any portion of the Product to you.
</ul>
</li>
<li>Lunashop reserves the right to deny payments from suspicious buyers. In the event that an order or payment is tagged as being suspicious, delivery of item(s) will be delayed until Lunashop can successfully verify its legitimacy, be it by phone or a request for verifying information. The determination of what amounts to suspiciousness shall be at the sole and absolute discretion of Lunashop. You acknowledge and accept that under the laws of certain jurisdiction, Lunashop may be obligated to report any suspicious transactions to the relevant authorities.</li>
<li>Unless you notify Lunashop to the contrary on the day of delivery and such notification is confirmed by email, the Product shall be deemed to have been accepted by you as being in good order and in accordance with the terms and conditions under which the Product is offered by Lunashop.</li>
<li>You undertake to make all payments promptly and in accordance with any rules, regulations or guidelines issued by Lunashop from time to time and shall not be entitled to withhold payment of all or any of the price.</li>
<li>All transactions made in accordance with these Terms and Conditions are final. The Product that has been delivered are strictly non-refundable.</li>
<li>If you have any complaints about our website and/or Product, you should direct them to us via e-mail at lunashop.my1@gmail.com.</li>
</li>Lunashop Products
<ul>
<li>Lunashop Credits are not legal tender. It is a virtual credit which allow users to reload and spend on any online and mobile games available at Lunashop.com and Lunashop content partners payment platform.</li>
<li>Lunashop voucher is a game voucher which carries Lunashop Credits in the form of a physical voucher or softpin. Each Lunashop vouchers comprise of a unique serial number and pin code which allow users to reload Lunashop Credits into their Lunashop account or perform flash top up into games which is available at Lunashop.com and Lunashop content partners payment platform.</li>
<li>Lunashop Reward Program Membership (“Program”) implemented and operated by Luna Top Up World. Under the Program, if you have signed up for the Program as a Lunashop Reward member (“Coin”), we allow you to collect Coin through the purchase of Lunashop Credits at Lunashop.com portal website and to use the Coin you have collected to redeem Lunashop Credits, merchandise, gift certificates and other reward (“Reward”) that is provided by suppliers, manufacturers, retailers, and content providers (“Partners”) being offered by us from time to time.</li>
</ul>
</o>
<p style="text-align:justify;">Registration, membership, and all benefits will be subjected to the Terms and Conditions of the specific Program. By registering in the Program or Coin collection, you agree that you have read and understood the Terms & Conditions and are bound by all the Terms & Conditions, that may be subjected to change from time to time, and also agree for us to collect and use your personal information in accordance with our Privacy Policy. The Terms & Conditions herein is between us and you and relate to your participation in the Program, your right to collect, use, redeem the Reward and your right to other benefits of the Program. If you are dealing with us in regards to the internet, you hereby allow the forming of a contractual agreement through electronic communication. We have the final authority in the interpretation of the Terms & Conditions and other related questions in relation to the Program or the Reward. Our failure to carry out any of the provisions of the Terms & Conditions at any time may not be interpreted as a relinquishment of rights or the rights of all parties to conduct violations over any provision or any other provision of this Terms & Conditions. You may not divert or transfer your rights or obligations under the Terms & Conditions without prior written permission from us.</p>


<p style="text-align:justify;">Participation in the Program</p>
i.	Lunashop membership only applies to individuals, not to companies, organizations or any other body.
ii.	To register in the Program, you must (i) register or sign up to acquire a Lunashop account (“Account”), (ii) inform us, a valid email address, and (iii) be at least 18 (eighteen)years old and if you are under the age of 18 (or equivalent minimum age in the relevant jurisdiction), we require you to obtain permission from your legal guardian to register for an account and that the legal guardian must agree to these Terms and Conditions. If you are the legal guardian of a minor who is registering for an Account, you must accept these Terms and Conditions on the minor’s behalf and you will be responsible for all use of the Account, including any transactions made by the minor, whether the minor’s account is now open or created later and whether or not the minor is supervised by you during his or her use of our Product.
iii.	If we accept your registration application, we will sign you up in the Program as a Lunashop Reward Program member (“Member”), you must provide any change to your personal data to us, such as name, mailing address, email address, and phone number by contacting our Customer Support at lunashop.my1@gmail.com or you may renew your personal data through https://www.lunashop.my (“Member Web Portal“).
iv.	In the event, your account is lost and you forgot Your email and access code, you will lose all of Your Coin.
v.	Lunashop reserves the right to terminate your membership at any time for whatsoever reason Lunashop deem fit.
PIN (Personal Identification Number)
vi.	Email is required to access the Member Account and to redeem your Coin, or when contacting Customer Support at lunashop.my1@gmail.com
vii.	You are responsible for ensuring that you keep your email confidential at all times and to notify us over any illegal use of your email or when your email is compromised.
viii.	We are not responsible or accountable in any way over losses arising from your failure to fulfil these conditions.
Coin Acquisition
ix.	Coins are acquired for every purchase of Lunashop Credits made by using the payment channels provided at https://www.lunashop.my website.
x.	To acquire Coin you must perform a transaction purchase at www.Lunashop.com
xi.	Coin will then be automatically added to your Lunashop membership Account no later than 24 hours after the transaction has been initiated.
xii.	You will acquire Lunashop Coin for every transaction that is perform at Lunashop with the term of 0.01% of the transaction value. Further Lunashop reserves the right to change the provision at any time.
Coin Expiration
Coin expires 12 months after the date it is acquired.
Coin Usage
Coin does not have any cash value, monetary value, or other values, and cannot be converted to any currency. Coin are calculated according to the Reward Program that we provide from time to time. Lunashop reserves the right to set the amount of Coin required to redeem or convert into any goods or benefits that will be exchanged and to alter the amount at any time, without prior notice.

<hr>


<h2>INTELLECTUAL PROPERTY RIGHTS</h2>


<p style="text-align:justify;">If you purchase any digital content from the website, it is exclusively for your personal consumption. You have no right to reproduce it for any reason. Except for the licenses granted in these Terms and Conditions, you have no right, title or interest in or to Lunashop or its Product. The content and information of this website is protected by intellectual property rights law and owned by Lunashop and/or the proprietary property of its suppliers, affiliates, or licensors. Without Lunashop’s prior written permission, you may not copy, reproduce, publish, distribute, transmit, display, license, sell, circulate, create derivative works from, or otherwise exploit the content and information of this website to any third party (including, without limitation, the display and distribution of the material via a third party websites or other networked computer environment).</p>
<p style="text-align:justify;">Unauthorized use of this website, including without limitation, unauthorized entry into the Lunashop systems, misuse of passwords or misuse of any information posted on the website is strictly prohibited. In addition, use of this website is unauthorized in any jurisdiction where the use of this website may violate any applicable legal requirements.</p>
<p style="text-align:justify;">All trademarks, product marks, trade names, logos and icons (collectively "Trademarks") displayed on our website are registered and unregistered Trademarks of Lunashop and others. Nothing contained in our website should be construed as granting, by implication, estoppel, or otherwise, any licence or right to use any Trademark displayed on our website without the written permission of Lunashop or such third party that may own the Trademarks displayed on our website. Your use of the Trademarks displayed on our website, or any other content on our website, is strictly prohibited.</p>

<hr>

<h2>DISCLAIMER OF WARRANTIES</h2>


<p style="text-align:justify;">LUNASHOP DOES NOT GUARANTEE, REPRESENT, OR WARRANT THAT YOUR USE OF THE PRODUCT AND/OR WEBSITE WILL BE UNINTERRUPTED OR ERROR-FREE, AND YOU AGREE THAT FROM TIME TO TIME WE MAY REMOVE THE PRODUCT FOR INDEFINITE PERIODS OF TIME, OR CANCEL THE PRODUCT IN ACCORDANCE WITH THESE TERMS AND CONDITIONS.</p>
<p style="text-align:justify;">We have no control over the quality, fitness, safety, reliability, legality, or any other aspect of any Product that is purchased using our Product. We are not required to issue refunds if a purchase turns out to not meet your expectations, or if the third party providers do not fulfil their commitments, although we will make reasonable efforts to assist you in these matters. We have no obligation, and cannot guarantee that, we will resolve any disputes related to any transaction to your satisfaction.</p>
<p style="text-align:justify;">We have the absolute discretion to alter the Terms & Conditions, in any way in respect of the Program, including terms of ordering, Coin usage procedures for Reward, or Reward in any form, without prior notice, and even if the changes can affect the value of the accumulated Coin. You must check our present Terms & Conditions and details and other information for the Program through the Member Web Portal or by calling our Customer Support at lunashop.my1@gmail.com.</p>
<p style="text-align:justify;">Our website contents are provided based on “as is” and “as available”. We hereby firmly state that we do not make any guarantees or collaterals whether expressly or implicitly, in respect to the merchantability of a product or suitability of our product that we provide for a specific purpose.</p>
<p style="text-align:justify;">From time to time, our Product may be delayed, interrupted or disrupted for an indeterminate period of time. In addition, except as otherwise required by applicable law or regulation, Lunashop may terminate your use of Product or impose limits on the type and/or amount of transactions you are allowed to make with the Product at any time at its sole discretion without prior notice. Lunashop and its affiliates shall not be liable for any claim arising from or related to Lunashop arising from any such delay, interruption, disruption, limitation, or suspension.</p>

<hr>


<h2>LIMITATION OF LIABILITY</h2>


<p style="text-align:justify;">YOU EXPRESSLY UNDERSTAND AND AGREE THAT TO THE FULLEST EXTENT PERMITTED BY APPLICABLE LAW, WE AND OUR AFFILIATES (AND OUR AND THEIR RESPECTIVE EMPLOYEES, DIRECTORS, AGENTS AND REPRESENTATIVES), PARTNERS AND LICENSORS SHALL NOT BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, PUNITIVE, CONSEQUENTIAL OR EXEMPLARY DAMAGES, INCLUDING BUT NOT LIMITED TO, DAMAGES FOR LOSS OF PROFITS, GOODWILL, USE, DATA COST OF PROCUREMENT OF SUBSTITUTE PRODUCTS, OR OTHER INTANGIBLE LOSSES ARISING OUT OF OR IN CONNECTION WITH: (i) THE USE OR INABILITY TO USE THE PRODUCT; (ii) ANY CHANGES MADE TO THE PRODUCT OR ANY TEMPORARY OR PERMANENT CESSATION OF THE PRODUCT OR ANY PART THEREOF; (iii) THE UNAUTHORIZED ACCESS TO OR ALTERATION OF YOUR TRANSMISSIONS OR DATA; (iv) THE DELETION OF, CORRUPTION OF, OR FAILURE TO STORE AND/OR SEND OR RECEIVE YOUR TRANSMISSIONS OR DATA ON OR THROUGH THE PRODUCT; (v) STATEMENTS OR CONDUCT OF ANY THIRD PARTY ON THE PRODUCT; (vi) PROGRAM OR YOUR PARTICIPATION IN THE PROGRAM, (vii) DELAY, FAILURE OR DECISION BY US IN THE OPERATION OF THE PROGRAM OR ANY CHANGES TO THE TERMS AND CONDTIONS ON THE REDEMPTION AND USAGE OF THE POINTS (viii) ILLEGITIMATE USE OF YOUR MEMBER CARD OR PIN, (ix) OFFERS, DESCRIPTION, STATEMENTS OR CLAIMS ABOUT THE PROGRAM, BRANDS OR PARTNERSHIP OR ANY INDIVIDUAL, (x) PURCHASES, REDEMPTIONS OR USAGE OF GOODS FROM THE BRAND OR PARTNERSHIP, INCLUDING OTHER REWARD, WHETHER PROVIDED BY US OR ONE OF OUR AFFILIATES, BRAND OR PARTNER. OUR BRAND AND PARTNERS ARE NOT ACCOUNTABLE FOR THE PROGRAM AND (xi) ANY OTHER MATTER RELATING TO THE PRODUCT AND/OR WEBSITE.</p>
<p style="text-align:justify;">TO THE EXTENT PERMITTED BY APPLICABLE LAW, IN NO EVENT WILL THE AGGREGATE LIABILITY OF LUNASHOP OR OUR AFFILIATES (AND OUR AND THEIR RESPECTIVE EMPLOYEES, DIRECTORS, AGENTS AND REPRESENTATIVES), PARTNERS AND LICENSORS ARISING OUT OF OR IN CONNECTION WITH THESE TERMS AND CONDITIONS OR THE TRANSACTIONS CONTEMPLATED HEREBY, WHETHER IN CONTRACT, TORT (INCLUDING NEGLIGENCE, PRODUCT LIABILITY OR OTHER THEORY), WARRANTY, OR OTHERWISE, EXCEED THE AMOUNT OF FEES EARNED BY US IN CONNECTION WITH YOUR USE OF PRODUCT AND/OR WEBSITE DURING THE THREE (3) MONTH PERIOD IMMEDIATELY PRECEDING THE EVENT GIVING RISE TO THE CLAIM FOR LIABILITY.</p>

<hr>

<h2>NO REFUND POLICY</h2>


<p style="text-align:justify;">Lunashop digital Products have a strict no-refund policy. Please be sure the products are right for you before purchasing. Users are solely responsible for confirming that their devices are compatible with the Products they purchase. All virtual item purchased are final, non-refundable and non-returnable. We do not offer refunds or exchanges for the incorrect purchase of Lunashop Products, including due to compatibility issues.</p>

<hr>


<h2>TERMINATION</h2>


<p style="text-align:justify;">Lunashop may at any time, under certain circumstances and without prior notice, immediately terminate or suspend all or a portion of your Account and/or access to the Product. Cause for such termination shall include: (a) violations of this Agreement or any other policies or guidelines that are referenced herein and/or posted on the Product; (b) a request by you to cancel or terminate your Account; (c) a request and/or order from law enforcement, a judicial body, or other government agency; (d) where provision of the Product to you is or may become unlawful; (e) unexpected technical or security issues or problems; (f) your participation in fraudulent or illegal activities; or (g) failure to pay any fees owed by you in relation to the Product, provided that in the case of non-material breach, Lunashop will be permitted to terminate only after giving you 30 days’ notice and only if you have not cured the breach within such 30-day period. Any such termination or suspension shall be made by Lunashop in its sole discretion and Lunashop will not be responsible to you or any third party for any damages that may result or arise out of such termination or suspension of your Account and/or access to the Product.</p>
<p style="text-align:justify;">In addition, Lunashop may terminate your Account upon 30 days’ prior notice via email to the address associated with your Account if (a) your Account has been inactive for one (1) year; or (b) there is a general discontinuance of the Product or any part thereof. Notice of general discontinuance of Product will be provided as set forth herein, unless it would not be reasonable to do so due to circumstances arising from legal, regulatory, or governmental action; to address user security, user privacy, or technical integrity concerns; to avoid disruptions to other users; or due to a natural disaster, a catastrophic event, war, or other similar occurrence outside of Lunashop’s reasonable control.</p>
<p style="text-align:justify;">Should you breach any of these Terms and Conditions, Lunashop shall have the exclusive sole and absolute right to terminate, discontinue or withdraw the provision of the Product to you. In the event of a breach by you, Lunashop reserves its right to pursue any remedy or relief in so far as permitted by law, which includes but is not limited to injunction, damages and/or specific performance.</p>

<hr>


<h2>PRIVACY POLICY</h2>


<p style="text-align:justify;">Please refer to our privacy policy.</p>

<hr>


<h2>MISCELLANEOUS</h2>


<p style="text-align:justify;">Lunashop is not a bank, e-money issuer, or money transferor and does not require the approval of the relevant Authorities to operate. Your prepaid or post-paid mobile account is not a bank account, e-money account, payment card, or other regulated financial instrument. Lunashop does not process or store your credit or debit card number. This Agreement shall be governed by and construed according to the laws of Malaysia without regard to its conflicts-of-law provisions, and you and Lunashop agree to submit to the exclusive jurisdiction of the Malaysia courts. Nothing in these Terms and Conditions is intended to or creates any type of joint venture, employee-employer, creditor-debtor, escrow, partnership, or any fiduciary relationship between you, us, or our affiliates. Lunashop may assign this Agreement, any of its terms, and any of Lunashop obligations, in whole or in part, at any time, with or without notice to you, but you may not assign this Agreement, or any part of it, to any other party without Lunashop’s prior written approval. Any attempt by you to do so is void.</p>
<p style="text-align:justify;">Notwithstanding any law, rule or regulation to the contrary, you agree that any claim or cause of action you may have arising out of these Terms and Conditions must be filed within one (1) year after such claim or cause of action first could be filed or be forever barred. We will not be considered to have waived any of our rights or remedies, or portion of them, unless the waiver is in writing and signed by us. Our failure to enforce the strict performance of any provision of these Terms and Conditions will not constitute a waiver of our right to subsequently enforce such provision or any other provisions.</p>
<p style="text-align:justify;">These Terms and Conditions constitute the entire agreement between you and Lunashop, and supersede and cancel all prior and contemporaneous agreements, claims, representations, and understandings (including, but not limited to, any prior versions of the Terms and Conditions). No modification or amendment of this Agreement will be binding on Lunashop unless set forth in writing signed by us.</p>
<p style="text-align:justify;">If any part of this Agreement is held by a court of competent jurisdiction to be invalid or unenforceable, the remaining terms and conditions of this Agreement will remain in full force and effect and, upon our request, the court will construe any invalid or unenforceable portions in a manner that most closely reflects the economic, legal and business objectives of the original language. If such construction is not possible, the provision will be severed from this Agreement and the rest of the Agreement will remain in full force and effect.</p>
<p style="text-align:justify;">These Terms and Conditions were written in English. To the extent any translated version of this Agreement conflicts with the English version, the English version controls and prevails. We reserve the right to change, modify or otherwise alter these Terms and Conditions at any time. You can find the most recent version on our website. Such modifications shall become effective immediately upon the posting thereof.</p>



<p style="text-align:justify;">Contact Us</p>


<p style="text-align:justify;">Don't hesitate to contact us if you have any inquiry.</p>
<p style="text-align:justify;">Via Email: lunashop.my1@gmail.com</p>



            </div>
        </div>
    </div>
</div>

@endsection

