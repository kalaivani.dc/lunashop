@extends('layouts.app2')


@section('styles')

@endsection

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
 <h4>Become a Reseller</h4>
            <p>
             We welcome resellers with special discount. To be a reseller, contact us at:
 
              <strong>Phone:</strong> <a href="tel:+60164009418">+60164009418</a><br>
              <strong>Email:</strong> <a href="mailto:lunashop.my1@gmail.com ">lunashop.my1@gmail.com </a><br>
            </p>

            </div>
        </div>
    </div>
</div>

@endsection

