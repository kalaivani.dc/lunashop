@extends('layouts.app2')


@section('styles')
<style>
input {
    margin: 0;
    font-family: inherit;
    font-size: inherit;
    line-height: inherit;
    outline: none;
    border: none;
    background:none;
}
</style>
@endsection

@section('content')

                  
                        <div class="card">
                            <div class="card-body">
                                 

                                    <form action="<?php echo (isset($save)?$save:'')?>" method="POST">
                                            @csrf
                                        <div class="row">
                                            <div class="col-lg-6 col-sm-12">
                                                <h4 class="mb-3">Billing/Shipping address</h4>
                                                  <div class="row">
                                                <div class="col-md-12 mb-3">
                                                    <label for="firstName">First name</label>
                                                    <input type="text" class="form-control" id="firstName" name="firstName" placeholder="" value="" required="">
                                                    <div class="invalid-feedback">
                                                        Valid first name is required.
                                                    </div>
                                                </div>
                                                <div class="col-md-12 mb-3">
                                                    <label for="lastName">Last name</label>
                                                    <input type="text" class="form-control" id="lastName" name="lastName" placeholder="" value="" required="">
                                                    <div class="invalid-feedback">
                                                        Valid last name is required.
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="mb-3">
                                                <label for="username">Email</label>
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">@</span>
                                                    </div>
                                                    <input type="text" class="form-control" id="username" name="username" placeholder="you@example.com" required="">
                                                    <div class="invalid-feedback d-block">
                                                        Your email is required.
                                                    </div>
                                                </div>
                                            </div>

                                           

                                            <div class="mb-3">
                                                <label for="address">Address</label>
                                                <input type="text" class="form-control" id="address" name="address" placeholder="1234 Main St" required="">
                                                <div class="invalid-feedback">
                                                    Please enter your shipping address.
                                                </div>
                                            </div>

                                            <div class="mb-3">
                                                <label for="address2">Address 2 <span
                                                        class="text-muted">(Optional)</span></label>
                                                <input type="text" class="form-control" id="address2"  name="address2" placeholder="Apartment or suite">
                                            </div>

                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <label for="country">Country</label>
                                                    <select class="d-block w-100 default-select" id="country" name="country" required="">
                                                        <option value="">Choose...</option>
                                                        <option>Malaysia</option>
                                                    </select>
                                                    <div class="invalid-feedback">
                                                        Please select a valid country.
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 mb-3">
                                                    <label for="state">State</label>
                                                    <select class="d-block default-select w-100" id="state" name="state" required="">
                                                        <option value="">Choose...</option>
                                                        <option>Selangor</option>
                                                        <option>Perak</option>
                                                    </select>
                                                    <div class="invalid-feedback">
                                                        Please provide a valid state.
                                                    </div>
                                                </div>
                                                <div class="col-lg-12 mb-3">
                                                    <label for="zip">Zip</label>
                                                    <input type="text" class="form-control" id="zip" name="zip" placeholder="" required="">
                                                    <div class="invalid-feedback">
                                                        Zip code required.
                                                    </div>
                                                </div>
                                            </div>
                                            </div>
                                             <div class="col-lg-6 col-sm-12">
                                                <h4 class="d-flex justify-content-between align-items-center mb-3">
                                                    <span class="text-muted">Your cart</span>
                                                    <span class="badge badge-primary badge-pill text-white"><?php echo sizeof($cart['product']);?></span>
                                                </h4>
                                                <ul class="list-group mb-3">
                                            <?php
                                            $grand = 0;
                                               
                                            foreach ($cart['product'] as $key => $value) {
                                      
                                                $sub = 0;
                                       
                                                  
                                              ?>
                                               <li class="list-group-item d-flex justify-content-between lh-condensed">
                                                <div>
                                                    <h6 class="my-0"><input type="text" name="product[<?php echo $key;?>][name]" value="<?php echo $value['product']['name'];?>"></h6>
                                                    
                                                </div>
                                                <?php if(!isset($value['variant'])) {
                                                $sub+= ($value['product']['quantity']* $value['product']['regular']);
                                                
                                                $grand += $sub;
                                                ?>
                                              <span class="text-muted"><input class="text-muted" type="text" name="product[<?php echo $key;?>][quantity]" value="<?php echo $value['product']['quantity'];?>"></span>
                                              
                                                <span class="text-muted"><input class="text-muted" type="text" name="product[<?php echo $key;?>][regular]" value="<?php echo $value['product']['regular'];?>"></span>
                                                <?php } ?>
                                                </li>
                                              <?php
                                              
                                              
                                                
                                                if(isset($value['variant'])) {
                                          
                                                    ?>
                                                     <ul class="list-group mb-3">
                                                        <?php 
                                                        $count = 0;
                                                        $variation = 0;
                                                
                                                        foreach($value['variant'] as $k=>$v) {
                                                            $variation++;
                                                            $variation_sub = 0;
                                                         ?>
                                                         
                                           <li class="list-group-item d-flex justify-conten<t-between lh-condensed">
                                           <div>
                                                                   
                                           <h6 class="my-0">Variation <?php echo $variation;?></h6>
                                           <small class="text-muted"></small>
                                           </div>
                                           <span class="text-muted"></span>
                                         </li>
                                                            
                                                         <?php
                                                            
                                                             foreach($v['item'] as $k2=>$v2) {
                                                                 
                                                             
                                                            $count++;
                                                            $sub+= ($v['quantity']* $v2['regular']);
                                                            $variation_sub+= ($v['quantity']* $v2['regular']);
                                                            ?>
                                                            <li class="list-group-item d-flex justify-conten<t-between lh-condensed">
                                                                <div>
                                                                   
                                                                    <h6 class="my-0"><input style="color:#f82249" type="text" name="product[<?php echo $key;?>][variant][<?php echo $k;?>][item][<?php echo $count;?>][value]" value="<?php echo $v2['value'];?>"></h6>
                                                                    
                                                                    <small class="text-muted"><input type="text" name="product[<?php echo $key;?>][variant][<?php echo $k;?>][item][<?php echo $count;?>][name]" value="<?php echo $v2['name'];?>"></small>
                                                                </div>
                                                                
                                                                
                                                               
                                                                
                                                                <span class="text-muted"><input type="text" name="product[<?php echo $key;?>][variant][<?php echo $k;?>][item][<?php echo $count;?>][regular]" value="<?php echo $v2['regular'];?>"></span>
                                                            </li>
                                                            <?php
                                                        }
                                                        ?>
                                                         
                                                         </li>
                                                         <li class="list-group-item d-flex justify-conten<t-between lh-condensed">
                                           <div>
                                                                   
                                           <h6 class="my-0" style="color:#f82249">Sub total </h6>
                                           <small class="text-muted">x
                                               <input type="text" name="product[<?php echo $key;?>][variant][<?php echo $k;?>][quantity]" value="<?php echo $v['quantity'];?>">
                                           </small>
                                           </div>
                                           <span class="text-muted"><?php echo $variation_sub;?></span>
                                         </li>
                                                        <?php
                                                        }
                                                        
                                                        ?>
                                                    </ul>
                                                    <?php
                                                }
                                                
                                                ?>
                                                 <li class="list-group-item d-flex justify-content-between ">
                                                    <div class="text-dark">
                                                         <span>Sub Total (RM)</span>
                                             
                                                    </div>
                                                    <span class="text-dark"><input type="text" name="product[<?php echo $key;?>][sub_total]" value="<?php echo $sub;?>">
                                                    </span>
                                                </li>
                                                <li class="list-group-item d-flex justify-content-between active">
                                                    <div class="text-white">
                                                        <small>TOTAL</small>
                                             
                                                    </div>
                                                    
                                                    <?php 
                                                    $grand += $sub;?>
                                                
                                                    <input type="text" name="product[<?php echo $key;?>][total]" value="<?php echo $sub;?>"></span>
                                                </li>
                                                <?php
                                               
                                            }
                                            ?>
                                           
                                           
                                           
                                            <li class="list-group-item d-flex justify-content-between">
                                                <span>Total (RM)</span>
                                                <strong><input type="text" name="grand_total" value="<?php echo $grand;?>"></span></strong>
                                            </li>
                                        </ul>
                                        
                                                <div class="row">
                                                    <div class="col-12" style="margin-bottom:200px;">
                                                        <button class="btn btn-primary btn-lg btn-block" type="submit">PLACE ORDER</button>
                                                    </div>
                                                </div>
                                                
                                            </div>
                                            
                                        </div>
                                    </form>
                               
                            </div>
                        </div>
              
               
@endsection

@section('scripts')

    <script src="https://nexustech.online/asset/front/vendor/bootstrap-select/dist/js/bootstrap-select.min.js"></script>

@endsection
