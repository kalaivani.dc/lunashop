@extends('layouts.app2')


@section('styles')

@endsection

@section('content')
   <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <?php 
                                if(!empty($product->sale_price)) {
                                    ?>
                                    <div class="ribbon ribbon-top-left"><span>SALE</span></div>
                                    <?php
                                }
                                ?>
                                
                                <div class="row">
                                        <div class="col-xl-3 col-lg-6  col-md-6 col-xxl-5 ">
                                        <!-- Tab panes -->
                                        <div class="tab-content">
                                             <?php
                                                    if ($images != '') {
                                                       
                                                        foreach ($images as $key => $value) {
                                                          ?>
                                                           <div role="tabpanel" class="tab-pane fade show <?php echo ($key== 0 ? 'active':'');?>" id="key_<?php echo $key;?>">
                                                            <img class="img-fluid" src="/source/public/<?php echo $value;?>" alt="">
                                                        </div>
                                                          <?php
                                                        }
                                                    }
                                             ?>
                                                        
                                           
                                           
                                        </div>
                                        <div class="tab-slide-content new-arrival-product mb-4 mb-xl-0">
                                            <!-- Nav tabs -->
                                            <ul class="nav slide-item-list mt-3" role="tablist">
                                                <?php
                                                    if ($images != '') {
                                                         foreach ($images as $key => $value) {
                                                             ?>
                                                             <li role="presentation">
                                                                <a href="#key_<?php echo $key;?>" role="tab" data-toggle="tab"><img class="img-fluid" src="/source/public/<?php echo $value;?>" alt="" width="50"></a>
                                                            </li>
                                                             <?php
                                                         }
                                                    }
                                                ?>
                                              
                                            </ul>
                                        </div>
                                    </div>
                                    <!--content-->
                                    <div class="col-xl-9 col-lg-6  col-md-6 col-xxl-7 col-sm-12">
                                        <div class="product-detail-content">
                                            <!--Product details-->
                                            <div class="new-arrival-content pr">
                                                <h4 data-main-product-name id="<?php echo $product->id;?>"><?php echo ucfirst($product->name);?></h4>
                                                <!--<div class="comment-review star-rating">-->
                                                <!--    <ul>-->
                                                <!--        <li><i class="fa fa-star"></i></li>-->
                                                <!--        <li><i class="fa fa-star"></i></li>-->
                                                <!--        <li><i class="fa fa-star"></i></li>-->
                                                <!--        <li><i class="fa fa-star-half-empty"></i></li>-->
                                                <!--        <li><i class="fa fa-star-half-empty"></i></li>-->
                                                <!--    </ul>-->
                                                <!--    <span class="review-text">(34 reviews) / </span><a class="product-review" href=""  data-toggle="modal" data-target="#reviewModal">Write a review?</a>-->
                                                <!--</div>-->
                                                <div class="d-table mb-2">
                                                   <!--product price-->
                                                   <input type="hidden" id="pp" value="<?php echo $product->regular_price;?>">
                                                   <input type="hidden" id="ps" value="<?php echo $product->sale_price;?>">

                                                    <p class="float-left d-block <?php echo (empty($product->sale_price)? 'price': '');?>">
                                                      
                                                        <span data-product-price class="<?php echo (!empty($product->sale_price)? 'strike': 'price');?>">
                                                            RM<?php echo $product->regular_price;?>
                                                        </span>
                                                    </p>
                                                <!--product sale-->
                                                <span data-pricing>

                                                    <p class="<?php echo !empty($product->sale_price)? '': 'hidden';?>">
                                                      
                                                        <span class="sale <?php echo (!empty($product->sale_price)? 'price': 'strike');?>" data-product-sale="<?php echo $product->sale_price;?>">RM
                                                        <?php echo $product->sale_price;?>
                                                        
                                                        
                                                            <?php 
                                                            if (!empty($product->start_date)) {

                                                                $start = strtotime(now());
                                                                $end = strtotime($product->end_date);
                                                                $datediff = $end - $start;
                                                                $day = 'day';
                                                                $diff = round($datediff / (60 * 60 * 24));
                                                                if ($diff>1) {
                                                                    $day = 'days';
                                                                }
                                                                ?>
                                                                <span class="badge badge-danger light"><img class="img-fluid" src="http://online.printdepot.com.my/asset/front/images/fire.png" width="40" height="40">
                                                                <?php
                                                                if($product->start_date != '0000-00-00 00:00:00') {
                                                                  ?>
                                                                   ending in <?php echo $diff.' '.$day;?>
                                                                  <?php
                                                                }
                                                                ?>
                                                               </span>
                                                                </span>
                                                                <?php
                                                            }

                                                            
                                                            ?>
                                                        
                                                    </p>
                                                       
                                                </span>
                                                </div>
                                                <p>Availability: <span class="item"> In stock
                                                <!--    <i class="fa fa-shopping-basket"></i></span>-->
                                                <!--</p>-->
                                                <!--<p>Product code: <span class="item">0405689</span> </p>-->
                                                <!--<p>Brand: <span class="item"><?php #echo $product->brand;?></span></p>-->
                                                <?php
                                                if(isset($category)) {
                                                    ?>
                                                    <p>Category:&nbsp;&nbsp;
                                                        <?php
                                                        foreach ($category as $key => $value) {
                                                           ?>
                                                          <a href="/category?id=<?php echo $value->name;?>"><span class="badge badge-info light">
                                                              <?php echo $value->name;?>
                                                          </span></a>
                                                           <?php
                                                        }
                                                        ?>
                                                    </p>
                                                    <?php
                                                }

                                                ?>
                                                
                                            <?php
                                                if(isset($product->tags)) {
                                                    $tags = explode(',', $product->tags);
                         
                                                    if ($tags[0] != '') {
                                                        ?>
                                                        <p>Tags:&nbsp;&nbsp;
                                                        <?php
                                                   
                                                        foreach ($tags as $key => $value) {
                                                           ?>
                                                          <a href="/tag?id=<?php echo $value;?>"><span class="badge badge-success light">
                                                              <?php echo $value;?>
                                                          </span></a>
                                                           <?php
                                                        }
                                                        ?>
                                                    </p>
                                                    <?php
                                                }
                                                }

                                                ?>
                                                
                                               
                                                <?php 

                                                   if (isset($variation)) {
                                                    ?>
                                                    <div class="row">
                                                        
                                                    <?php
                                                    foreach ($variation as $key => $value) {
                                                        
                                                        ?>
                                                        <div class="col-5">
                                                           <?php echo $key;?>
                                                        </div>

                                                        <div class="col-7">
                                                            <select class="form-control" data-variation id="<?php echo $key;?>">
                                                            <?php 
                                                            foreach ($value as $k => $v) {
                                                                ?>
                                                                <option data-price="<?php echo $v->regular_price;?>" data-sale="<?php echo $v->sale_price;?>" value="<?php echo $v->value;?>">
                                                                <?php echo $v->value;?>
                                                                </option>
                                                                <?php
                                                            }
                                                            ?>
                                                            </select>
                                                        </div>
                                                        <?php
                                                    }

                                                    ?>
                                                   
                                                    </div>

                                                <?php }?>
                                                
                                                
                                                <!--Quantity start-->
                                                <div class="row">
                                                    <div class="col-5">
                                                        Quantity
                                                    </div>
                                                        
                                                    <div class="col-7">
                                                         <input type="number" id="qty" name="num" class="form-control input-btn input-number" value="1">
                                                    </div>
                                                </div>
                                                 <!--Quantity End-->
                                                 
                                                 <div class="row">
                                                  
                                                        
                                                    <div class="col-6">
                                                          <div class="shopping-cart mt-3" data-cart-add>
                                                            <a class="btn btn-primary btn-lg" href="javascript:void(0)"><i
                                                                    class="fa fa-shopping-basket mr-2"></i>Add
                                                                to cart</a>
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                              
                                               
                                               
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                   <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                <div class="row">
                                    <div class="col-12  mb-2">
                                        <?php
                                        echo nl2br($product->content);
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
                      
                 



@endsection

@section('scripts')
<script type="text/javascript">
    $(function() {

    //   localStorage.destroy();
        if(JSON.parse(localStorage.getItem("cart")) == null) {
            var cart = [];

            localStorage.clear();

        } else {
            var cart = JSON.parse(localStorage.getItem("cart"));

        }

        
        var qty = 0;
        var sub_total2 = 0.00;
        var total2 = 0.00;
        var obj = {};
        var sub_total = 0.00;
        var total = 0.00;
        var cart = [];

       
        if(cart.length > 0) {
            shoppingCart();
        }
        
    

         var variants = [];
        function updateCart(first)
        {
            
            var id = $('[data-main-product-name]').attr('id');
            var name = $('[data-main-product-name]').text();
            var regular = $('#pp').val();
            var sale = $('#ps').val();
            var qty = $('#qty').val();
            
            sub_total = 0.00;
            total = 0.00;
            
            if(jQuery.inArray(id, Object.keys(cart)) != -1) {
                console.log('main product in array');
                if(cart[id]['variant'].length <= 0) {
                    console.log(cart[id]['quantity']);
                    cart[id]['quantity'] = parseInt(cart[id]['quantity']) + parseInt(qty);
                }
            } else {
                if($('[data-variation]').length >0) {
                    cart[id] = {'name':name, 'regular':regular, 'sale':sale, 'variant':[], 'variants':[]};
                } else {
                    cart[id] = {'name':name, 'regular':regular, 'sale':sale,'quantity':qty, 'variant':[], 'variants':[]};
                }
                
            }
            

            var variant_name = '';
            var variant_value = '';
            var a = [];
            var b ='';
            var c = [];
            
            
            if($('[data-variation]').length > 0) {
           
                $.each($('[data-variation]'), function(key, value) {
                 
                    variant_name = $(value).attr('id');
                    variant_value = $(value).val();
                    custom_variant = variant_name+'_'+variant_value;
    
                    b += variant_value;
              
                    a.push({'item': $(value).attr('id'),
                            'value': $(value).val(),
                            'regular':$(value).find('option:selected').data('price')});
                });
                
    
                if(cart[id]['variant'].length <= 0) {
                     c[b] = [{'item':a, 'quantity':qty}];
                     cart[id]['variant'].push(c);
                     cart[id]['variants'].push(b);
                } else {
                    
      
                if(jQuery.inArray(b, cart[id]['variants']) != -1) {
                    $.each(cart[id]['variant'], function(key, value) {
                        
                        if(typeof(cart[id]['variant'][key][b])!='undefined') {
                             cart[id]['variant'][key][b][0]['quantity'] = parseInt(cart[id]['variant'][key][Object.keys(value)][0]['quantity']) + parseInt(qty);
                        }
                    
                              
                   
                    });
                    
                } else {
                     c[b] = [{'item':a, 'quantity':qty}];
                     cart[id]['variant'].push(c);
                     cart[id]['variants'].push(b);
                }
                
               
                }
            
            }
            
            console.log(cart);
            localStorage.setItem("cart", JSON.stringify(cart));
         

        }
        
        
        $('[data-cart-add]').on('click', function() {
            
            updateCart();
            // localStorage.setItem("cart", JSON.stringify(cart));
            // localStorage.setItem("items", JSON.stringify(items));
            //  $('[data-product-price]').text('RM'+total).removeClass('hidden');
            shoppingCart();
        
            
        });
        
        function shoppingCart()
        {
            var cartTable = '<table style="border-collapse: collapse;">';
            var grandTotal = 0;
            
            $.each(cart, function(k, v) {
                if(v != null) {
                    
                    cartTable += '<tr><th><input style="color:#eb1b23;" type="text" name="product['+k+'][product][name]" value="'+v.name+'" readonly></th>';
                    if(v.variant.length <= 0) {
                        
                         var sub_total = 0;
                         sub_total += (v.regular * parseInt(v.quantity));
                         
                        cartTable += '<th><input style="color:#eb1b23;" type="text" name="product['+k+'][product][quantity]" value="'+v.quantity+'" readonly></th>';
                    }
                    
                    if(v.variant.length <= 0) {
                        cartTable += '<th><input style="color:#eb1b23;" type="text" name="product['+k+'][product][regular]" value="'+v.regular+'" readonly></th>';
                    }
                    cartTable += '</tr>';
                                 
                      
                        
                        $.each(v.variant, function(k2, v2) {
                         
                            $.each(Object.values(v2), function(k3, v3) {
               
                                    var sub_total = 0;
                                    $.each(v3[0].item, function(k4, v4) {
                                   
                                    sub_total += (v4.regular * parseInt(v3[0].quantity));
                                    
                                        
                                    cartTable += '<tr>';

                                    cartTable += '<td><div class="timeline-badge info">';
            						cartTable += '</div>';
            						cartTable += '<a class="timeline-panel text-muted" href="#">';
            						cartTable += '<span><input type="hidden" name="product['+k+'][variant]['+Object.keys(v2)+'][item]['+k4+'][name]" value="'+v4.item+'"><input type="text" name="product['+k+'][variant]['+Object.keys(v2)+'][item]['+k4+'][value]" value="'+v4.value+'" readonly></span>';
            		
            						cartTable +='</a></td>';
            						cartTable +='<td><input type="hidden" name="product['+k+'][variant]['+Object.keys(v2)+'][quantity]" value='+v3[0].quantity+'> x'+v3[0].quantity+'</td>';
                                    cartTable += '<td><input style="text-align:right;" type="text" name="product['+k+'][variant]['+Object.keys(v2)+'][item]['+k4+'][regular]" value="'+v4.regular+'" readonly></td>';
                                    cartTable += '</tr>';
                                    
                                   
                                });
                                grandTotal += parseInt(sub_total).toFixed(2);
                                cartTable += '<tr><td colspan="2">Sub total</td><td colspan="2" class="text-right">'+sub_total+'</td></tr>';
                                 cartTable += '<tr><td colspan="3"><hr></td></tr>';
                            });
                     });
                     
                     if(v.variant.length <= 0) {
                         
                         grandTotal += parseInt(sub_total).toFixed(2);
                         cartTable += '<tr><td>Sub total</td><td></td><td>'+sub_total+'</td></tr>';
                                 cartTable += '<tr><td></td><td><hr></td></tr>';
                     }
                    
                    
                    
                }
                
            });
            cartTable += '<tr><td colspan="2">Grand total</td><td class="text-right">'+grandTotal+'</td></tr>';
            cartTable += '<table>';
            
            $('[data-cart]').html(cartTable);
            
             var bell = JSON.parse(localStorage.getItem("cart"));
            
            bell2 = [];
            $.each(bell, function(x, y) {
                if(y!=null) {
          
                    bell2.push(y);
                }
           
            });
            $('[data-cart-wrapper]').removeClass('hidden').text(bell2.length);
           
            
            
            
            
        }
        
        
        
        
        


    });
</script>

@endsection
