@extends('layouts.app2')

@section('styles')
    @include('layouts.datatable-header-css')
@endsection

@section('content')
                <!-- row -->
                <form action="/checkout?action=save" method="POST">
                                            @csrf
                <div class="row">
                    <div class="col-xl-12">
                       <div class="card card-bg">
                           
                           <div class="card-body">
                              <div class="row">
                                  <div class="col-2">
                                      <span class="badge light text-white bg-primary rounded-circle-step">1</span>
                                   
                                  </div>
                                  <div class="col-8 bg-item">
                                      
                                     <p class="fcomic2">{{__('front.enter')}}</p>
                                     <p class="fcomic">{{__('front.game_id')}}</p>
                                      <input type="text" class="form-control" value="" name="user_id" required>
                                  </div>
                              </div>
                           </div>
                       </div> 
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-xl-12">
                       <div class="card card-bg">
                           
                           <div class="card-body">
                              <div class="row">
                                  <div class="col-2">
                                      <span class="badge light text-white bg-primary rounded-circle-step">2</span>
                                   
                                  </div>
                                  <div class="col-8 bg-item">
                                     <p class="fcomic2">{{__('front.select')}}</p>
                                     <p class="fcomic">{{__('front.recharge')}}</p>

                                  </div>
                              </div>
                           </div>
                       </div> 
                    </div>
                </div>
                        
                <div class="row">
                    <div class="col-xl-12">
                        <div class="profile-news">
                                <div class="row"> 
                           <?php 
                           foreach($products as $key=>$value) {
                               
                                //cat id
                            if($value->parent_id == 0) {
                                $cat = $value->category_id;
                            } else {
                                $cat = $value->parent_id;
                            }
                            
                               if($value->featured_pic != '') {
                                $featured = '/source/public/'.$value->featured_pic;
                            } else {
                               if($cat == 1) {
                                   $featured = '/asset/home/vp.png';
                                   $size = '50';
                               } else {
                                   $featured = '/asset/home/diamond.png';
                                   $size = '75';
                               }
                                
                            }
                            
                           
            
                               ?>
                               <div data-block-wrapper class="col-lg-4 col-md-6 col-sm-6" id="<?php echo $value->id;?>">
                                        <div class="card card-bg">
                                            <div class="card-body">
                                                <div class="media pt-3 pb-3">
                                                    <img src="<?php echo $featured;?>" alt="image" class="mr-3 rounded" width="<?php echo $size;?>">
                                                    <div class="media-body row">
                                                       
                                                           
                                                        
                                                        <div class="col-9">
                                                            <h5 class="m-b-5 comic" data-item-name><?php echo $value->name;?></h5>
                                                             
                                                            <?php if(!empty($value->regular_price)) {
                                                                ?>
                                                                <input type="hidden" data-item-price value="<?php echo $value->regular_price;?>">
                                                                <p class="mb-0"><span class="price">{{__('front.symbol')}}<?php echo ($value->regular_price * $rate);?></span></p>
                                                                <?php
                                                             }
                                                             ?>
                                                        </div>
                                                        <div class="col-3">
                                                            <div class="shopping-cart" data-cart-add>
                                                               <a href="javascript:void(0)">
                                                                   <img src="/asset/home/buy.png" class="buy">
                                                                  </a>
                                                             </div>
                                                        </div>
                                                        
                                                            
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                               <?php
                           }
                            
                           ?>
                                    
                   </div>
                          
                        </div>
                    </div>
                  
                </div>
               <div class="row">
                    <div class="col-xl-12">
                       <div class="card card-bg">
                           
                           <div class="card-body">
                              <div class="row">
                                  <div class="col-2">
                                      <span class="badge light text-white bg-primary rounded-circle-step">3</span>
                                   
                                  </div>
                                  <div class="col-8 bg-item">
                                     <p class="fcomic2">{{__('front.select')}}</p>
                                     <p class="fcomic">{{__('front.payment')}}</p>
                             
                                      <input type="text" class="form-control" value="" name="phone_num" placeholder="{{__('front.phone')}} ({{__('front.optional')}})">
                                     
                                  </div>
                              </div>
                           </div>
                       </div> 
                    </div>
                </div>
                
                  <div class="row">
                    <div class="col-xl-12">
                       <div class="card">
                           
                           <div class="card-body">
                              <div class="row">
                                  <div class="col-2">
                                      <span class="badge light text-white bg-primary rounded-circle-step">4</span>
                                   
                                  </div>
                                  <div class="col-8 bg-item">
                                    
                                     <p class="fcomic">{{__('front.buy')}}</p>
                                       
                                            <ul class="contacts" data-cart>
                                        
                                            </ul>
                                  
                                            <div class="shopping-cart mt-3">
                                           
                                                     <button type="submit" style="background:#434345;border-color: #434345;color:#fff;" class="btn subtotal-preview-btn text-right" data-submit><i class="fa fa-shopping-basket mr-2"></i>{{__('front.checkout')}}</button>
                                              
                                            </div>
                                        
                                        
                                      
                                  </div>
                              </div>
                           </div>
                       </div> 
                    </div>
                </div>
                </form>

@endsection

@section('scripts')
@include('layouts.datatable-footer-js')

{{Html::script('assets/js/plugin-coded.js')}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/3.0.4/socket.io.js"> </script>

<script>
// var dt = new Date();
//        // var time = dt.getHours() + ":" + dt.getMinutes() + ":" + dt.getSeconds();
// var time = dt.getSeconds();

//     if (!localStorage.getItem("sessionxxx")) {
//         var userID = time;
//         localStorage.setItem("sessionxxx", userID);

//     } else {
//         var userID = localStorage.getItem("sessionxxx");

//     }

//    var sock = io("http://127.0.0.1:3444", {
//        transports: ['websocket'],
//        query: 'token='+userID
//    });

//     sock.on("connect", function() {
//         console.log('connected to server');

//     });
//         var table = [];
//         var client = {

//             fireAjax: function(offset, paginate, callback, userID) {

//                 $.ajax({
//                     url: "<?php echo $site;?><?php echo (isset($user->role)? '/'.$user->role: '');?>shop/product_post",
//                     method: "POST",
                    
//                     data: {
//                         "_token": csrfToken,
//                         "offset": (offset ? offset : 0),
//                         id:userID,
//                     },
//                     dataType: 'json',
//                     error: function(msg, b, c) {

//                         console.debug('error');
//                     },
//                     dataType: 'json',
//                     success: function(result) {

//                         callback(result);
//                         return false;
//                     },
//                     type: 'POST'
//                 });

//             },
//             target: function(data, userID, keyCount = false) {

//                 var response = data.result;
//         $.each(response.data, function(key, value) {

//         var table_rows = $.trim(value);
//         if (!table[key]) {

//         table[key] = $('.filter-' + key + ' #target').DataTable({
//         'iDisplayLength':100,
//         //  "pageLength": 0,
//         // "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
//         "bDestroy": true,
//         "responsive": true,
//         "scrollX": true,
//         dom: 'Bfrtip',
//          "columnDefs": [ {
//           "targets": [0],
//           "orderable": false,
//           'sortable':false
//         } ],
//         buttons: [
//             {
//                 extend :'copy',
//                 exportOptions: {
//                     columns: ':visible:not(.not-exported)',
//                     modifier: { page: 'current' }
//                 },
//                 title: 'Copy'
//             },
//             {
//                 extend :'csvHtml5',
//                 exportOptions: {
//                     columns: ':visible:not(.not-exported)',
//                     modifier: { page: 'current' },
//                     stripHtml: true
//                 },
//                 title: 'CSV export'
//             },
//             {
//                 extend : 'excel',
//                 exportOptions: {
//                     columns: ':visible:not(.not-exported)',
//                 },
//                 title: 'Excel export'
//             },
//             {
//                 extend : 'pdf' ,
//                 pageSize: 'LETTER',
//                 orientation: 'landscape',
//                 exportOptions: {
//                     columns: ':visible:not(.not-exported)',
//                     modifier: { page: 'current' },
//                 },
//                 title: 'Pdf export',
//             },
//              //,'pageLength'
//         ]
//         });
//         }

//          if(keyCount == 'undefined') {
//             keyCount = [key];
//             keyCount[key] = response['rowCountByKey'][key];

//         }


//         keyCount = client.sumByKey(key, $('#portfolio-flters li[data-filter=".filter-' + key + '"] [data-key-count]'), response['rowCountByKey'][key]);

//         table[key].rows.add($(table_rows)).draw();
//         });

        
//         //pagination
//         // if (data.result) {
//         // $('[data-loading-spinner]').removeClass('hidden');


//         // console.log(data.result.offset);
//         // client.updateTable(data.result.offset, 2, userID);
//         // }


//      $('[data-loading-spinner]').addClass('hidden');
 
//             },
//             user: function()
//             {
//                if (!localStorage.getItem("sessionxxx")) {
//                     var userID = time;
//                     localStorage.setItem("sessionxxx", userID);

//                 } else {
//                     var userID = localStorage.getItem("sessionxxx");

//                 } 

//                 return userID;
//             },
//              updateTable: function(offset, $paginate, userID) {

//                 client.fireAjax(offset, $paginate, client.target, userID);

//             },
//             sumByKey: function(key, element, total) {
                
//                 var eThis = $(element).find('p');
//                 var eVal = parseFloat((eThis.text() == '' ? 0 : eThis.text()));
//                 var sum = eVal + total;

//                 element.html('<p>'+sum+'<p>');


//                 return element;
            

//             }

//         }


//     $.ajax({
//          url: "<?php echo $site;?><?php echo (isset($user->role)? '/'.$user->role: '');?>shop/product_post",

//         method: "POST",
//         data:{
//             "_token": "{{ csrf_token() }}",
//             'id':userID,
//             'dataType':'html'
//         },
//         success: function (response) {
//             console.log(response);

//         },
//     });

//     sock.on('nexus_database_action-channel-one:App\\Events\\MessageTransport', function (data) {
//         console.log('chanel 1');
//         console.log(data.result.data);
//         client.target(data, data.id);
//         $("#team1_score").append(data.id);

//         });
      


// $(function() {
//     //make the first child active as 'All' is  removed
//     $('#portfolio-flters li:nth-child(1)').addClass('filter-active');
//     var activeKey = $('li.filter-active').data('filter').split('filter-');
//     $('input[name=active_tab]').val(activeKey[1]);
//     //hide all content
//     $('.portfolio-item').css({ 'display': 'none' });
//     //show the first one
//     $('.portfolio-item.filter-'+activeKey[1]).css({ 'display': 'block' });
//     //adjust the layout
//     $('.filter-active').trigger('click');
//     $('.row.portfolio-container.aos-init.aos-animate').addClass('fullheight');
// });



</script>

<script type="text/javascript">


      $(function() {

        // initialize the datalist plugin on page load
    //   var table =  $('body').datalist({
    //         url: "<?php echo $site;?><?php echo (isset($user->role)? '/'.$user->role: '');?>product/product_post",
    //         'guest':"<?php echo $guest;?>"
    //         // 'id':userID
    //     });
    

        $('#vendor').on('change', function() {

            var vendor = $(this).val();
            $('body').datalist({
             url: "<?php echo $site;?><?php echo (isset($user->role)? '/'.$user->role: '');?>product/product_post",
             'search':1,
            'fresh':1,
            'vendor': vendor,
            'guest':"<?php echo $guest;?>"

            // 'id':userID
            

            });
        });

        //on search
        $('#search').on('click', function() {

            if (sessionStorage.getItem("more") == 1) {
                sessionStorage.setItem("stop_current_loop", 1);
            }
            

            var vendor = $('input[id=vendor]').val();
            var keyword = $('input[name=keyword]').val();


            $('body').datalist({
             url: "<?php echo $site;?><?php echo (isset($user->role)? '/'.$user->role: '');?>product/product_post",
             'search':1,
            'fresh':1,
            'vendor': vendor,
            'keyword':keyword,
            'guest':"<?php echo $guest;?>"

            // 'id':userID
            

            });
        });



//     $("#new_category_btn2").on('click',function(e){
    

    //     $.ajax({
    //      url: "<?php echo $site;?><?php echo (isset($user->role)? '/'.$user->role: '');?>shop/product_post",

    //     method: "POST",
    //     data:{
    //         "_token": "{{ csrf_token() }}",
    //         'id':userID,
    //         'dataType':'html'
    //     },
    //     success: function (response) {
    //         console.log(response);

    //     },
    // });

// });

     });

</script>

@endsection
