@extends('layouts.app2')

@section('styles')
    @include('layouts.datatable-header-css')
    <style>
        .photo-content .cover-photo {
    background: url('<?php echo $featured;?>'');
    background-size: cover;
    background-position: center;
    min-height: 250px;
    width: 100%;
}
    </style>
@endsection

@section('content')


                <!-- row -->
                <div class="row">
                      
                    <div class="col-lg-12">
                        <div class="profile card card-body px-3 pt-3 pb-0">
                            <div class="profile-head">
                                <div class="photo-content">
                                    
                                    <div class="cover-photo" style="background: url('/source/public/<?php echo $featured;?>');background-size: cover;background-position: center;">
                                       
                                    </div>
                                </div>
                                <div class="profile-info">
									
									<div class="profile-details">
										<div class="profile-name px-3 pt-2">
										<h4 class="m-b-5 comic"><?php echo $name;?></h4>
											<p class="credit">CREDITS</p>
										</div>
									
									</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
               <form action="/checkout?action=save" method="POST">
                                            @csrf
                <div class="row">
                    <div class="col-xl-12">
                       <div class="card card-bg">
                           
                           <div class="card-body">
                              <div class="row">
                                  <div class="col-2">
                                      <span class="badge light text-white bg-primary rounded-circle-step">1</span>
                                   
                                  </div>
                                  <div class="col-8 bg-item">
                      
                                     <p class="fcomic2">{{__('front.enter')}}</p>
                                     <p class="fcomic">{{__('front.game_id')}}</p>
                                      <input type="text" class="form-control" value="" name="user_id" required>
                                  </div>
                              </div>
                           </div>
                       </div> 
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-xl-12">
                       <div class="card card-bg">
                           
                           <div class="card-body">
                              <div class="row">
                                  <div class="col-2">
                                      <span class="badge light text-white bg-primary rounded-circle-step">2</span>
                                   
                                  </div>
                                  <div class="col-8  bg-item">
                                     <p class="fcomic2">{{__('front.select')}}</p>
                                     <p class="fcomic">{{__('front.recharge')}}</p>
                                     <p>
                                         <img src="/asset/home/arrow.png" class="arrow">
                                     </p>
                                  </div>
                              </div>
                           </div>
                       </div> 
                    </div>
                </div>
                        
                <div class="row">
                    <div class="col-xl-12">
                        <div class="profile-news">
                                <div class="row"> 
                           <?php 
                           foreach($products as $key=>$value) {
                               
                                //cat id
                            if($value->parent_id == 0) {
                                $cat = $value->category_id;
                            } else {
                                $cat = $value->parent_id;
                            }
                            
                               if($value->featured_pic != '') {
                                $featured = '/source/public/'.$value->featured_pic;
                            } else {
                               if($cat == 1) {
                                   $featured = '/asset/home/vp.png';
                                   $size = '50';
                               } else {
                                   $featured = '/asset/home/diamond.png';
                                   $size = '75';
                               }
                                
                            }
                            
                           
            
                               ?>
                               <div data-block-wrapper class="col-lg-4 col-md-6 col-sm-6" id="<?php echo $value->id;?>">
                                        <div class="card card-bg">
                                            <div class="card-body">
                                                <div class="media pt-3 pb-3">
                                                    <img src="<?php echo $featured;?>" alt="image" class="mr-3 rounded" width="<?php echo $size;?>">
                                                    <div class="media-body row">
                                                       
                                                           
                                                        
                                                        <div class="col-9">
                                                            <h5 class="m-b-5 comic" data-item-name><?php echo $value->name;?></h5>
                                                             
                                                            <?php if(!empty($value->regular_price)) {
                                                                ?>
                                                                <input type="hidden" data-item-price value="<?php echo $value->regular_price;?>">
                                                                <p class="mb-0"><span class="price">{{__('front.symbol')}}<?php echo ($value->regular_price * $rate);?></span></p>
                                                                <?php
                                                             }
                                                             ?>
                                                        </div>
                                                        <div class="col-3">
                                                            <div class="shopping-cart" data-cart-add>
                                                               <a href="javascript:void(0)">
                                                                   <img src="/asset/home/buy.png" class="buy">
                                                                  </a>
                                                             </div>
                                                        </div>
                                                        
                                                            
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                               <?php
                           }
                            
                           ?>
                                    
                   </div>
                          
                        </div>
                    </div>
                  
                </div>
               <div class="row">
                    <div class="col-xl-12">
                       <div class="card card-bg">
                           
                           <div class="card-body">
                              <div class="row">
                                  <div class="col-2">
                                      <span class="badge light text-white bg-primary rounded-circle-step">3</span>
                                   
                                  </div>
                                  <div class="col-8 bg-item">
                                      
                                    <p class="fcomic2">{{__('front.select')}}</p>
                                     <p class="fcomic">{{__('front.payment')}}</p>
                                     
                                    
                                     
                                  </div>
                              </div>
                           </div>
                       </div> 
                    </div>
                </div>
                
                  <div class="row">
                    <div class="col-xl-12">
                       <div class="card">
                           
                           <div class="card-body">
                              <div class="row">
                                  <div class="col-2">
                                      <span class="badge light text-white bg-primary rounded-circle-step">4</span>
                                   
                                  </div>
                                  <div class="col-8">
                                
                                     <p class="fcomic">{{__('front.buy')}}</p>
                                       
                                        <div class="row">
                                                <div class="col-12">
                                                     <input type="text" class="form-control" value="" name="phone_num" placeholder="{{__('front.phone')}} ({{__('front.optional')}})">
                                                </div>
                                            </div>
                                            
                                        <div class="row">
                                                <div class="col-12">
                                                    <ul class="contacts" data-cart>
                                                
                                                    </ul>
                                             </div>
                                        </div>
                                            
                                           
                                            

                                         
                                            <div class="row">
                                                <div class="col-12">
                                            <div class="shopping-cart mt-3">
                                           
                                                     <button type="submit" style="background:#434345;border-color: #434345;color:#fff;" class="btn subtotal-preview-btn text-right" data-submit><i class="fa fa-shopping-basket mr-2"></i>{{__('front.checkout')}}</button>
                                              
                                            </div>
                                             </div>
                                              </div>
                                        
                                        
                                      
                                  </div>
                              </div>
                           </div>
                       </div> 
                    </div>
                </div>
                </form>



@endsection

@section('scripts')
@include('layouts.datatable-footer-js')

{{Html::script('assets/js/plugin-coded.js')}}

<script type="text/javascript">


      $(function() {

        // initialize the datalist plugin on page load
    //   var table =  $('body').datalist({
    //         url: "<?php echo $site;?><?php echo (isset($user->role)? '/'.$user->role: '');?>category/product_post",
    //         'guest':"<?php echo $guest;?>",
    //         'id':<?php echo $cat;?>
    //     });
    

        


     });

</script>
@endsection
