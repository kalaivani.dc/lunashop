@extends('layouts.app2')

@section('styles')
    @include('layouts.datatable-header-css')
@endsection

@section('content')


                <!-- row -->
                <div class="row">
                      
                   
                        
                         <?php
              
                            foreach($games as $key=>$game) {
                                ?>
                                 <div class="col-6">
                                 <div class="profile card card-body px-3 pt-3 pb-0">
                                     <a href="/category?id=<?php echo $game->name;?>">
                                    <div class="profile-head">
                                        <div class="photo-content">
                                            
                                            <div class="cover-photo" style="background: url('/source/public/<?php echo $game->featured_pic;?>');background-size: cover;background-position: center;">
                                               
                                            </div>
                                        </div>
                                       
                                        <div class="profile-info">
        									
        									<div class="profile-details">
        										<div class="profile-name pt-2">
        										<h4 class="m-b-5 comic">
        										<?php echo $game->name;?>
        										</h4>
        										
        										</div>
        									
        									</div>
                                        </div>
                                    </div>
                                    </a>
                                </div>
                                 </div>
                                <?php
                            }   
                                
                        ?>
                       
                   
                </div>
              



@endsection

@section('scripts')
@include('layouts.datatable-footer-js')

{{Html::script('assets/js/plugin-coded.js')}}

<script type="text/javascript">



</script>
@endsection
