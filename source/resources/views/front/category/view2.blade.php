@extends('layouts.app2')

@section('styles')
    @include('layouts.datatable-header-css')
    <style>
        .photo-content .cover-photo {
    background: url(<?php echo $featured;?>);
    background-size: cover;
    background-position: center;
    min-height: 250px;
    width: 100%;
}
    </style>
@endsection

@section('content')

    <div class="cover-photo" style="background: url('<?php echo $featured;?>');background-size: cover;background-position: center;min-height: 250px; width: 100%;"></div>
</div>


     @include('table.form')

@endsection

@section('scripts')
@include('layouts.datatable-footer-js')

{{Html::script('assets/js/plugin-coded.js')}}

<script type="text/javascript">


      $(function() {

        // initialize the datalist plugin on page load
       var table =  $('body').datalist({
            url: "<?php echo $site;?><?php echo (isset($user->role)? '/'.$user->role: '');?>category/product_post",
            'guest':"<?php echo $guest;?>",
            'id':<?php echo $cat;?>
        });
    

        


     });

</script>
@endsection
