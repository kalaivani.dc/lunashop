@extends('layouts.app')


@section('assets')
    {{Html::style('assets/css/vendors/css/forms/select/select2.min.css')}}
    {{Html::style('https://fonts.googleapis.com/css?family=Rubik:300,400,500,600%7CIBM+Plex+Sans:300,400,500,600,700')}}
@endsection

@section('content')
<style>
h5{text-transform:capitalize;}
.arrow:before {
    position: absolute;
    top: 26%;
    right: 20px;
    font-family: 'boxicons';
    content: "\e9b2";
    transition: all 200ms linear 0s;
    font-size: 1.2rem;
    font-weight: 600;
}
.collapsible {box-shadow: 0 3px 12px 0 rgb(0 0 0 / 10%) !important;}
@media all and (max-width: 767px){
   h5{text-align:center;} 
   .col-4{-webkit-flex: 100%;-ms-flex: 100%;flex: 100%;max-width: 100%;margin-bottom:0.5rem;}
   .collapsible .col-8{-webkit-flex: 80%;-ms-flex: 80%;flex: 80%;max-width: 80%; }
   .collapsible .col-4{-webkit-flex: 20%;-ms-flex: 20%;flex: 20%;max-width: 20%; }
}
</style>

<div class="content-header row">
    <div class="content-header-left col-12 mb-2 mt-1">
        <div class="breadcrumbs-top">
            <h5 class="content-header-title float-left pr-1 mb-0 d-none d-sm-block"> {{__('module.edit')}} <?php echo $module['name'];?></h5>
            <h5 class="content-header-title pr-1 mb-0 d-block d-sm-none"> {{__('module.edit')}} <?php echo $module['name'];?></h5>
                <div class="breadcrumb-wrapper d-none d-sm-block">
                    <ol class="breadcrumb p-0 mb-0 pl-1">
                        <li class="breadcrumb-item"><a href="/<?php echo $user->role?>/module/main"><i class="bx bx-home-alt"></i></a> </li>
                        <li class="breadcrumb-item"><a href="/<?php echo $user->role;?>/module/main">{{__('module.module')}}</a></li>
                        <li class="breadcrumb-item active">{{__('module.edit')}} <?php echo $module['name'];?></li>
                    </ol>
                </div>
        </div>
    </div>
</div>

<div class="card">
    <div class="card-header text-left">
        <div class="col-6">
        <h5 class="d-none d-sm-block">Edit <?php echo $module['name'];?></h5>
        </div>
        <div class="col-6 d-flex justify-content-end">
        <a href="<?php echo $site;?><?php echo $user->role;?>/module?tab=add" target="_blank"><button type="button" class="btn btn-success glow d-none d-sm-block mr-1" style="float:right;"><div class="fonticon-wrap"><i class="bx bx-plus"></i>{{__('module.addmodule')}}</div></button></a>
        <a href="<?php echo $site;?><?php echo $user->role;?>/module?tab=add" target="_blank"><button type="button" class="btn btn-success glow d-block d-sm-none mr-1" style="float:right;"><div class="fonticon-wrap"><i class="bx bx-plus"></i></div></button></a>
        <a href="<?php echo $site;?><?php echo $user->role;?>/module" target="_blank"><button type="button" class="btn btn-secondary glow d-none d-sm-block" style="float:right;"><div class="fonticon-wrap"><i class="bx bx-list-ul"></i>{{__('module.allmodule')}}</div></button></a>
        <a href="<?php echo $site;?><?php echo $user->role;?>/module" target="_blank"><button type="button" class="btn btn-secondary glow d-block d-sm-none" style="float:right;"><div class="fonticon-wrap"><i class="bx bx-list-ul"></i></div></button></a>
        </div>
    </div>

<div class="card-body">
    <form action="<?php echo $site;?><?php echo $user->role;?>/module?tab=update&q=<?php echo $id;?>" method="POST" enctype="multipart/form-data">
        @csrf
    <div class="row">
        <div class="col-4 col-sm-4 wow fadeInLeft">
            <label>{{__('module.namess')}}</label>
            <input type="text" name="name" value="<?php echo $module['name'];?>" class="form-control" required="">
        </div>
        <div class="col-4 col-sm-4 wow fadeInLeft">
            <label>{{__('module.path')}}</label>
            <input type="text" name="path" value="<?php echo $module['url'];?>" class="form-control" required="">
        </div>
        <div class="col-4 col-sm-4 wow fadeInLeft radio-glow">
            <label>{{__('module.status')}}</label>
                <ul class="list-unstyled mb-0 mt-50">
                    <li class="d-inline-block mr-2 mb-1">
                        <fieldset>
                            <div class="radio radio-primary radio-glow">
                                <input type="radio" name="status" id="status1" value="1" <?php echo ($module['status'] == 1? 'checked': '')?>>
                                <label for="status1">{{__('module.active')}}</label>
                            </div>
                        </fieldset>
                    </li>
                    <li class="d-inline-block mr-2 mb-1">
                        <fieldset>
                            <div class="radio radio-danger radio-glow">
                                <input type="radio" name="status" id="status0" value="0" <?php echo ($module['status'] == 0? 'checked': '')?>>
                                <label for="status0">{{__('module.inactive')}}</label>
                            </div>
                        </fieldset>
                     </li>
                </ul>
        </div>
    </div>
    <hr style="border-style: dashed;">
    <div class="row">
        <div class="col-12 col-sm-12 wow fadeInLeft">
            <label>{{__('module.submenu')}}</label>
        </div>
                <?php
                    if (!empty($children)) {
                ?>
                    <?php
                        foreach ($children as $key => $value) {
                    ?>
        <div class="col-4 col-sm-4 wow fadeInLeft">
            <div class="collapsible">
                        <div class="card collapse-header">
                            <div id="headingCollapse<?php echo $value->id;?>" class="card-header arrow" data-toggle="collapse" role="button" data-target="#collapse<?php echo $value->id;?>" aria-expanded="true" aria-controls="collapse<?php echo $value->id;?>">
                                <span class="collapse-title"><?php echo $value->name;?></span>
                            </div>
                            <div id="collapse<?php echo $value->id;?>" role="tabpanel" aria-labelledby="headingCollapse<?php echo $value->id;?>" class="collapse" style="">
                                <div class="card-body row">
                                    <div class="col-8 col-sm-8 wow fadeInLeft pl-0">
                                        <input type="text" name="menu[<?php echo $value->id;?>]" value="<?php echo $value->name;?>" class="form-control">
                                    </div>
                                    <div class="col-4 col-sm-4 wow fadeInLeft pr-0">
                                        <a href="<?php echo $site;?><?php echo $user->role;?>/module/?tab=delete&q=<?php echo $value->id;?>"><button class="btn btn-icon btn-danger" type="button" data-repeater-delete=""><i class="bx bx-x"></i><span class="d-lg-inline-block d-none">{{__('module.delete')}}</span> </button></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
        </div> 
                    <?php
                        }
                    ?>
                <?php
                    }
                ?>
    </div>
    <hr style="border-style: dashed;">
    <div class="row">
        <div class="col-sm-12 d-flex justify-content-end">
            <button type="submit" class="btn btn-primary btn-small subtotal-preview-btn mr-1">{{__('module.update')}}</button>
            <a href="/<?php echo $user->role;?>/module/main/delete/<?php echo $module['id'];?>" class="btn btn-danger btn-small subtotal-preview-btn">{{__('module.delete')}}</a></td>
        </div>
    </div>
    </form>
</div>
</div>
@endsection
