<div class="card shadow-none border">
   
        <div class="card-header border-bottom">

            <div class="row"  style="width: 100%;">
                
                <div class="col-5 text-right">
                    @include('table.action_buttons')

                </div>

                <div class="col-12">
                    @include('table.mobile_keys')
                    
                </div>
                <div class="col-12">
                    @include('table.desktop_keys')
                    
                </div>
                <div class="col-12">
                    @include('table.toggle_column')
                    
                </div>
                

            </div>

        </div>
 
    <form id="form" action="<?php echo (isset($save)?$save:'')?>" method="POST">
        @csrf
        <div class="card-body">
            <div class="col-12 text-center hidden" data-loading-spinner>
                <div class="spinner-border" role="status">
                <span class="sr-only">Loading...</span>
                </div>
            </div>



            <div class="row portfolio-container" data-aos="fade-up" data-aos-delay="200">
                <?php
 
                if (isset($keys)) {

                foreach ($keys as $b => $brand) {

                    if (is_array($brand)) {
                        ?>
                        <div class="col-lg-12 col-md-6 portfolio-item filter-<?php echo (isset($brand['id'])? $brand['id']: $brand);?>" data-filter-<?php echo (isset($brand['id'])? $brand['id']: $brand);?>>
                        <?php
                    } else {
   

                        ?>
                        <div class="col-lg-12 col-md-6 portfolio-item filter-<?php echo (isset($brand->id)? $brand->id: $brand);?>" data-filter-<?php echo (isset($brand->id)? $brand->id: $brand);?>>
                        <?php
                    }
                ?>
                

                <input type="hidden" name="active_tab" value="">
                @include('table.table')

                </div>
                    <?php
                }
                }
                ?>
            </div>

            <?php 
            if (isset($save) && !isset($update_shipping_fee)) {
                ?>
                <button type="submit" class="btn btn-success subtotal-preview-btn" data-submit>{{__('product.submit')}}</button>
                <?php
            }
            ?>

             <?php 
            if (isset($update_shipping_fee)) {
                ?>
                <button type="submit" class="btn btn-secondary  subtotal-preview-btn" data-form-submit>{{__('shipment.update')}}</button>
                <?php
            }

            if (isset($marketplace)) {
                ?>
                <button type="button" class="btn btn-warning subtotal-preview-btn">
                <a target="_blank" href="https://ebx360.com/sme/marketplace/<?php echo $marketplace;?>/autoAwb?q=<?php echo $account;?>">Generate AWB</a></button>
                <?php
            }
            ?>
            

        </div>
    </form>
</div>