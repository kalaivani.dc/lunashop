<div class="row">
  <div class="col-2">
    <!--total-->
    <div class="fonticon-wrap">
      Total <span id="totalRows"></span>
    </div>
  </div>
  <div class="col-2">
    <?php 
    if (isset($add2) && !empty($add2)) {
      ?>
      <a href="<?php echo $add2;?>" target="_blank">
        <button class="btn btn-warning float-right" style= "width:max-content">{{__($add_title2)}}</button>
      </a>
      <?php
    }
    ?>
  </div>
  <div class="col-2">
    <?php 
    if (isset($addfiles)&& !empty($addfiles)) {
      ?>
      <a href="<?php echo $addfiles;?>" target="_blank">
        <button class="btn btn-light-success float-right" style= "width:max-content">{{__($addfiles_title)}}</button>
      </a>
      <?php
    }
    ?>
  </div>
  <div class="col-2">
    <?php 
    if (isset($add) && !empty($add)) {
      ?>
      <a href="<?php echo $add;?>" target="_blank">
        <button class="btn btn-success float-right" style= "width:max-content">{{__($add_title)}}</button>
      </a>
      <?php
    }
    ?>
  </div>



</div>



