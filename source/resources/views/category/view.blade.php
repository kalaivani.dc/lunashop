@extends('layouts.app')

@section('styles')
    @include('layouts.datatable-header-css')
    {{Html::style('app-assets/css-rtl/plugins/extensions/ext-component-treeview.css')}}

@endsection

@section('content')



         <div class="col-lg-12  col-sm-12">
                            <div class="card">
                                <div class="card-header">
                                    <div class="row">
                                       <div class="col-lg-6  col-6">
                                            <h6 class="card-title">Category
                                    </h6>
                                        </div>
                                          <div class="col-lg-6  col-6">
                                                <?php 
                                                if (isset($add) && !empty($add_title)) {
                                                  ?>
                                                  <a href="<?php echo $add;?>" target="_blank">
                                                    <button class="btn btn-success float-right" style= "width:max-content">{{__($add_title)}}</button>
                                                  </a>
                                                  <?php
                                                }
                                                ?>
                                            </div>
                                    </div>
                                    
                                </div>
                                <div class="card-body">
                                    <div class="form-group">
                                        <div class="seachbox mb-2">
                                            <input type="text" class="form-control" placeholder="Search" id="input-search" name="input-search">
                                        </div>
                                    </div>
                                    <div class="row mb-1">
                                        <div class="col-sm-12">
                                            <div class="form-group d-flex flex-wrap align-items-center mb-0">
                                                <div class="checkbox mr-1 mb-50">
                                                    <input type="checkbox" class="checkbox__input" id="chk-ignore-case" value="false">
                                                    <label for="chk-ignore-case">Ignore Case</label>
                                                </div>
                                                <div class="checkbox mr-1 mb-50">
                                                    <input type="checkbox" class="checkbox__input" id="chk-exact-match" value="false">
                                                    <label for="chk-exact-match">Exact Match</label>
                                                </div>
                                                <div class="checkbox mr-1 mb-50">
                                                    <input type="checkbox" class="checkbox__input" id="chk-reveal-results" value="false">
                                                    <label for="chk-reveal-results">Reveal Results</label>
                                                </div>
                                                <div class="searchable-action">
                                                    <button type="button" class="btn btn-primary btn-sm mr-1 mb-50" id="btn-search">
                                                        Search
                                                    </button>
                                                    <button type="button" class="btn btn-light-primary btn-sm mb-50" id="btn-clear-search">
                                                        Clear
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-8 col-sm-12">
                                            <div id="searchable-tree"></div>
                                        </div>
                                        <div class="col-md-4 col-sm-12">
                                            <div id="search-output"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
@endsection



@section('scripts')

    @include('layouts.datatable-footer-js')
    {{Html::script('assets/js/plugin-coded.js')}}
    {{Html::script('app-assets/vendors/js/ui/jquery.sticky.js')}}
    {{Html::script('app-assets/vendors/js/extensions/bootstrap-treeview.min.js')}}
    <!--{{Html::script('app-assets/js/scripts/extensions/ext-component-treeview.js')}}-->

    <script type="text/javascript">
     $(document).ready(function () {
  // color variable define here
  var $primary = '#5A8DEE',
    $danger = '#FF5B5C',
    $warning = '#FDAC41',
    $primary_light = '#6999f3',
    $warning_light = '#FFEED9',
    $dark_primary = '#2c6de9',
    $hover_warning = '#fed8a6',
    $white = '#fff';
    var $searchableTree = '';
        
      $.ajax({
         url: "<?php echo $site;?>backend/admin/category/category_tree",

        method: "POST",
        data:{
            "_token": "{{ csrf_token() }}",
            'dataType':'json'
        },
        success: function (response) {

              // Searchable Tree options define here
             categoryTree(response);

        },
    });

  function categoryTree(response) {

          
        $searchableTree = $('#searchable-tree').treeview({
                   selectedBackColor: [$primary],
                    color: [$primary],
                    showBorder: true,
                    showTags: true,
                    data: response,
              });

  }

  // Searchable Tree's search and checkbox value save into variable here
  var search = function (e) {

    var pattern = $('#input-search').val();
    var options = {
      ignoreCase: $('#chk-ignore-case').is(':checked'),
      exactMatch: $('#chk-exact-match').is(':checked'),
      revealResults: $('#chk-reveal-results').is(':checked')
    };
    // seachable tree checkbox and search value pass here
    var results = $searchableTree.treeview('search', [pattern, options]);
    // Searchable Tree's search Output  print into html
    var output = '<p>' + results.length + ' matches found</p>';
    $.each(results, function (index, result) {
      output += '<p>- ' + result.text + '</p>';
    });
    $('#search-output').html(output);
  }
  // Searchable Tree's seach btn's action define here
  $('#btn-search').on('click', search);
  $('#input-search').on('keyup', search);
  // Searchable Tree's  clear btn's  action define here
  $('#btn-clear-search').on('click', function (e) {
    $searchableTree.treeview('clearSearch');
    $('#input-search').val('');
    $('#search-output').html('');
  });


});

    </script>

@endsection
