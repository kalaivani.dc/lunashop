@extends('layouts.app2')


@section('styles')

@endsection

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
 <h4>Contact Us</h4>
            <p>
             Lunashop World Sdn Bhd<br/>
              No. 1-2-1B, 2nd Floor, Block B, Kolam Centre Phase II,<br/> Jalan Lintas, Luyang,<br/> 88300 Kota Kinabalu,<br/> Sabah, Malaysia. 
              <strong>Phone:</strong> <a href="tel:+60164009418">+60164009418</a><br>
              <strong>Email:</strong> <a href="mailto:lunashop.my1@gmail.com ">lunashop.my1@gmail.com </a><br>
            </p>

            </div>
        </div>
    </div>
</div>

@endsection

