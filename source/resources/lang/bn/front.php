<?php

return [
    'symbol' => '৳',
    'game_id'=>'খেলার সনাক্তকরণ নম্ব',
    'recharge'=>'রিচার্জ',
    'payment'=>'পেমেন্ট',
    'phone'=>'ফোন নম্বর',
    'optional'=>'ঐচ্ছিক',
    'buy'=>'কেনা',
    'checkout'=>'বেতন',
    'select'=>'নির্বাচন করুন',
    'enter'=>'প্রবেশ করা'
];