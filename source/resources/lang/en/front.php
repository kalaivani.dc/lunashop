<?php

return [
    'symbol' => 'S$',
    'game_id'=>'Game id',
    'recharge'=>'Recharge',
    'payment'=>'Payment',
    'phone'=>'Phone number',
    'optional'=>'Optional',
    'buy'=>'Buy',
    'checkout'=> 'Checkout',
    'select'=>'Select',
    'enter'=>'Enter'
];