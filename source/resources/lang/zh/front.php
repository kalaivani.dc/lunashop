<?php

return [
    'symbol' => '짜',
    'game_id'=>'游戏编号',
    'recharge'=>'充值',
    'payment'=>'支付',
    'phone'=>'电话号码',
    'optional'=>'选修的',
    'buy'=>'买',
    'checkout'=>'支付',
    'select'=>'选择',
    'enter'=>'进入'
];