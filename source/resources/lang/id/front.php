<?php

return [
    'symbol' => 'Rs',
    'game_id'=>'id game',
    'recharge'=>'Isi ulang',
    'payment'=>'Pembayaran',
    'phone'=>'Nomor telepon',
    'optional'=>'Opsional',
    'buy'=>'membeli',
    'checkout'=>'membayar',
    'select'=>'Pilih',
    'enter'=>'Memasuki',
];