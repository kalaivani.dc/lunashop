<?php

return [
    'symbol' => 'RM',
    'game_id'=>'Id permainan',
    'recharge'=>'Cas semula',
    'payment'=>'Pembayaran',
    'phone'=>'Nombor telefon',
    'optional'=>'Pilihan',
    'select'=>'Pilih',
    'enter'=>'Masukkan',
    'checkout'=>'Checkout',
    'buy'=>'Membeli'
];