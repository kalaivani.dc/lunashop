@extends('layouts.app2')


@section('styles')

@endsection

@section('content')
   <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <?php 
                                if(!empty($product->sale_price)) {
                                    ?>
                                    <div class="ribbon ribbon-top-left"><span>SALE</span></div>
                                    <?php
                                }
                                ?>
                                
                                <div class="row">
                                        <div class="col-xl-3 col-lg-6  col-md-6 col-xxl-5 ">
                                        <!-- Tab panes -->
                                        <div class="tab-content">
                                             <?php
                                                    if ($images != '') {
                                                       
                                                        foreach ($images as $key => $value) {
                                                          ?>
                                                           <div role="tabpanel" class="tab-pane fade show <?php echo ($key== 0 ? 'active':'');?>" id="key_<?php echo $key;?>">
                                                            <img class="img-fluid" src="/source/public/<?php echo $value;?>" alt="">
                                                        </div>
                                                          <?php
                                                        }
                                                    }
                                             ?>
                                                        
                                           
                                           
                                        </div>
                                        <div class="tab-slide-content new-arrival-product mb-4 mb-xl-0">
                                            <!-- Nav tabs -->
                                            <ul class="nav slide-item-list mt-3" role="tablist">
                                                <?php
                                                    if ($images != '') {
                                                         foreach ($images as $key => $value) {
                                                             ?>
                                                             <li role="presentation">
                                                                <a href="#key_<?php echo $key;?>" role="tab" data-toggle="tab"><img class="img-fluid" src="/source/public/<?php echo $value;?>" alt="" width="50"></a>
                                                            </li>
                                                             <?php
                                                         }
                                                    }
                                                ?>
                                              
                                            </ul>
                                        </div>
                                    </div>
                                    <!--content-->
                                    <div class="col-xl-9 col-lg-6  col-md-6 col-xxl-7 col-sm-12">
                                        <div class="product-detail-content">
                                            <!--Product details-->
                                            <div class="new-arrival-content pr">
                                                <h4 data-main-product-name id="<?php echo $product->id;?>"><?php echo ucfirst($product->name);?></h4>
                                                <!--<div class="comment-review star-rating">-->
                                                <!--    <ul>-->
                                                <!--        <li><i class="fa fa-star"></i></li>-->
                                                <!--        <li><i class="fa fa-star"></i></li>-->
                                                <!--        <li><i class="fa fa-star"></i></li>-->
                                                <!--        <li><i class="fa fa-star-half-empty"></i></li>-->
                                                <!--        <li><i class="fa fa-star-half-empty"></i></li>-->
                                                <!--    </ul>-->
                                                <!--    <span class="review-text">(34 reviews) / </span><a class="product-review" href=""  data-toggle="modal" data-target="#reviewModal">Write a review?</a>-->
                                                <!--</div>-->
                                                <div class="d-table mb-2">
                                                   <!--product price-->
                                                   <input type="hidden" id="pp" value="<?php echo $product->regular_price;?>">
                                                   <input type="hidden" id="ps" value="<?php echo $product->sale_price;?>">

                                                    <p class="float-left d-block <?php echo (empty($product->sale_price)? 'price': '');?>">
                                                      
                                                        <span data-product-price class="<?php echo (!empty($product->sale_price)? 'strike': 'price');?> hidden">
                                                            RM<?php echo $product->regular_price;?>
                                                        </span>
                                                    </p>
                                                <!--product sale-->
                                                <span data-pricing>

                                                    <p class="<?php echo !empty($product->sale_price)? '': 'hidden';?>">
                                                      
                                                        <span class="sale <?php echo (!empty($product->sale_price)? 'price': 'strike');?>" data-product-sale="<?php echo $product->sale_price;?>">RM
                                                        <?php echo $product->sale_price;?>
                                                        
                                                        
                                                            <?php 
                                                            if (!empty($product->start_date)) {

                                                                $start = strtotime(now());
                                                                $end = strtotime($product->end_date);
                                                                $datediff = $end - $start;
                                                                $day = 'day';
                                                                $diff = round($datediff / (60 * 60 * 24));
                                                                if ($diff>1) {
                                                                    $day = 'days';
                                                                }
                                                                ?>
                                                                <span class="badge badge-danger light"><img class="img-fluid" src="http://online.printdepot.com.my/asset/front/images/fire.png" width="40" height="40">
                                                                <?php
                                                                if($product->start_date != '0000-00-00 00:00:00') {
                                                                  ?>
                                                                   ending in <?php echo $diff.' '.$day;?>
                                                                  <?php
                                                                }
                                                                ?>
                                                               </span>
                                                                </span>
                                                                <?php
                                                            }

                                                            
                                                            ?>
                                                        
                                                    </p>
                                                       
                                                </span>
                                                </div>
                                                <p>Availability: <span class="item"> In stock
                                                <!--    <i class="fa fa-shopping-basket"></i></span>-->
                                                <!--</p>-->
                                                <!--<p>Product code: <span class="item">0405689</span> </p>-->
                                                <!--<p>Brand: <span class="item"><?php #echo $product->brand;?></span></p>-->
                                                <?php
                                                if(isset($category)) {
                                                    ?>
                                                    <p>Category:&nbsp;&nbsp;
                                                        <?php
                                                        foreach ($category as $key => $value) {
                                                           ?>
                                                          <a href="/category?id=<?php echo $value->name;?>"><span class="badge badge-info light">
                                                              <?php echo $value->name;?>
                                                          </span></a>
                                                           <?php
                                                        }
                                                        ?>
                                                    </p>
                                                    <?php
                                                }

                                                ?>
                                                
                                            <?php
                                                if(isset($product->tags)) {
                                                    $tags = explode(',', $product->tags);
                         
                                                    if ($tags[0] != '') {
                                                        ?>
                                                        <p>Tags:&nbsp;&nbsp;
                                                        <?php
                                                   
                                                        foreach ($tags as $key => $value) {
                                                           ?>
                                                          <a href="/tag?id=<?php echo $value;?>"><span class="badge badge-success light">
                                                              <?php echo $value;?>
                                                          </span></a>
                                                           <?php
                                                        }
                                                        ?>
                                                    </p>
                                                    <?php
                                                }
                                                }

                                                ?>
                                                
                                               
                                                <?php 

                                                   if (isset($variation)) {
                                                    ?>
                                                    <div class="row">
                                                        
                                                    <?php
                                                    foreach ($variation as $key => $value) {
                                                        
                                                        ?>
                                                        <div class="col-5">
                                                           <?php echo $key;?>
                                                        </div>

                                                        <div class="col-7">
                                                            <select class="form-control" data-variation id="<?php echo $key;?>">
                                                            <?php 
                                                            foreach ($value as $k => $v) {
                                                                ?>
                                                                <option data-price="<?php echo $v->regular_price;?>" data-sale="<?php echo $v->sale_price;?>" value="<?php echo $v->value;?>">
                                                                <?php echo $v->value;?>
                                                                </option>
                                                                <?php
                                                            }
                                                            ?>
                                                            </select>
                                                        </div>
                                                        <?php
                                                    }

                                                    ?>
                                                   
                                                    </div>

                                                <?php }?>
                                                
                                                
                                                <!--Quantity start-->
                                                <div class="row">
                                                    <div class="col-5">
                                                        Quantity
                                                    </div>
                                                        
                                                    <div class="col-7">
                                                         <input type="number" id="qty" name="num" class="form-control input-btn input-number" value="1">
                                                    </div>
                                                </div>
                                                 <!--Quantity End-->
                                                 
                                                 <div class="row">
                                                  
                                                        
                                                    <div class="col-6">
                                                          <div class="shopping-cart mt-3" data-cart-add>
                                                            <a class="btn btn-primary btn-lg" href="javascript:void(0)"><i
                                                                    class="fa fa-shopping-basket mr-2"></i>Add
                                                                to cart</a>
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                              
                                               
                                               
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                   <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                <div class="row">
                                    <div class="col-12  mb-2">
                                        <?php
                                        echo nl2br($product->content);
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
                      
                 



@endsection

@section('scripts')
<script type="text/javascript">
    $(function() {

      localStorage.clear();
        if(JSON.parse(localStorage.getItem("cart")) == null) {
            var cart = [];
            var items = [];

            localStorage.clear();

        } else {
            var cart = JSON.parse(localStorage.getItem("cart"));
            var items = JSON.parse(localStorage.getItem("items"));

        }

        
        var qty = 0;
        var sub_total2 = 0.00;
        var total2 = 0.00;
        var obj = {};
        var sub_total = 0.00;
        var total = 0.00;

        
        //update cart
        updateCart(cart.length);
       
        if(cart.length > 0) {
            shoppingCart();
        }
        
        
        //on qty change
        $("#qty").on('keyup change click', function () {
  
          var total = sub_total * $(this).val();
        //   $('[data-product-price]').text('RM'+total);
          updateCart();
        });
        
        $('[data-variation]').on('change', function() {
            updateCart();
        });
        
        function updateCart(first)
        {
            var id = $('[data-main-product-name]').attr('id');
            var name = $('[data-main-product-name]').text();
            var qty = $('#qty').val();
            sub_total = 0.00;
            total = 0.00;
            
            if(jQuery.inArray(id, items) != -1) {
                console.log('in array');

            
            $.each($('[data-variation]'), function(key, value) {
                sub_total+= parseFloat($(value).find('option:selected').data('price'));

                
                //attach to the right product inside temporary cart
                $.each(cart, function(k, v) {
                    $.each(v.product, function(k2, v2) {
                 
                    if (id == v2.id) {
                        
                     $.each(v2.variant, function(k3, v3) {
                    
                    
                            //if same variant type exists
                            if($(value).attr('id') == v3[0].item)
                            {
                               
                                //update the value
                              cart[k]['product'][0]['variant'][k3][0]['value'] = $(value).val();
                              cart[k]['product'][0]['variant'][k3][0]['regular'] = $(value).find('option:selected').data('price');
                            } 
                         });
                               
                          
                                
                             
                             
                        } 
                   
                   });
                   
                    cart[k]['product'][0]['qty'] = qty;
                    cart[k]['product'][0]['per_product'] = sub_total;
                    cart[k]['product'][0]['sub'] = sub_total * qty;
                    console.log('sub '+sub_total);
                    
                });
                
               
            });
            
               
            } else {
                console.log('NOT in array');
   
                cart.push({'product':[{'id':id, 'name': name, 'qty': qty, 'variant':[], 'sub': 0}]});
            

                items.push(id);
                

            
            $.each($('[data-variation]'), function(key, value) {
                sub_total+= parseFloat($(value).find('option:selected').data('price'));
 
                
                //attach to the right product inside temporary cart
                $.each(cart, function(k, v) {
                    $.each(v.product, function(k2, v2) {
                 
                    if (id == v2.id) {
                         cart[k]['product'][0]['variant'].push([{
                                  'item': $(value).attr('id'),
                                  'value': $(value).val(),
                                  'regular':$(value).find('option:selected').data('price')
                                 }]);
                        cart[k]['product'][0]['qty'] = qty;
                        cart[k]['product'][0]['per_product'] = sub_total;
                        cart[k]['product'][0]['sub'] = sub_total * qty;
                             
                        } 
                   });
                    
                });
                
               
            });
             
                
            }
            
            


            
            
            qty = $('#qty').val();
            total = sub_total * qty;
            total = total.toFixed(2)
            // $('[data-product-price]').text('RM'+total);
            
            sub_total2 = sub_total;
            total2 = total;
            
             $('[data-product-price]').text('RM'+total).removeClass('hidden');

        }
        
        
        $('[data-cart-add]').on('click', function() {
            
            localStorage.setItem("cart", JSON.stringify(cart));
            localStorage.setItem("items", JSON.stringify(items));
             $('[data-product-price]').text('RM'+total).removeClass('hidden');
            shoppingCart();
        
            
        });
        
        function shoppingCart()
        {
            if(JSON.parse(localStorage.getItem("cart")) != null) {
      
         
            $('[data-cart-wrapper]').removeClass('hidden').text(JSON.parse(localStorage.getItem("cart")).length);
           
            
            var cartTable = '<table style="border-collapse: collapse;">';
            var grandTotal = 0;
            
            $.each(JSON.parse(localStorage.getItem("cart")), function(k, v) {
               
                $.each(v.product, function(k2, v2) {
                console.log(v2);
                     cartTable += '<tr><th><input style="color:#eb1b23;" type="text" name="product['+v2.id+'][product]" value="'+v2.name+'" readonly></th></tr>';
                    
                     $.each(v2.variant, function(k3, v3) {
                        cartTable += '<tr>';

                        cartTable += '<td><div class="timeline-badge info">';
						cartTable += '</div>';
						cartTable += '<a class="timeline-panel text-muted" href="#">';
						cartTable += '<span><input type="hidden" name="product['+v2.id+'][variant]['+v3[0].item+'][name]" value="'+v3[0].item+'"><input type="text" name="product['+v2.id+'][variant]['+v3[0].item+'][value]" value="'+v3[0].value+'" readonly></span>';
		
						cartTable +='</a></td>';
                        cartTable += '<td><input style="text-align:right;" type="text" name="product['+v2.id+'][variant]['+v3[0].item+'][regular]" value="'+v3[0].regular+'" readonly></td>';
                        cartTable += '</tr>';
                     });
                     
                     cartTable += '<tr><th>Quantity</th><th style="text-align:right;"><input style="text-align:right;" type="number"  name="product['+v2.id+'][qty]"value="'+v2.qty+'"></th></tr>';
                     cartTable += '<tr><th>Sub Total</th><th style="text-align:right;">'+v2.per_product+'</th></tr>';
                     cartTable += '<tr><th>Total</th><th style="text-align:right;">'+v2.sub+'</th></tr>';
                     grandTotal = v2.sub.toFixed(2);
                    
                });
                
            });
            
            
            cartTable += '<tr><th>Grand Total</th><th style="text-align:right;">'+grandTotal+'</th></tr>';
            cartTable += '<table>';
            
            $('[data-cart]').html(cartTable);
            
            }
        }
        
        
        
        
        


    });
</script>

@endsection
