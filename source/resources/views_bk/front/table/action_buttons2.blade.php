<div class="row">
  <div class="col-8">
    <!--total-->
    <div class="fonticon-wrap">
       <i class="bx bx-heart" style="color: red;"></i><span id="totalRows"></span>
    </div>
  </div>

  <div class="col-4">
    <?php 
    if (isset($add) && !empty($add)) {
      ?>
      <a href="<?php echo $add;?>" target="_blank">
        <button class="btn btn-success float-right">{{__($add_title)}}</button>
      </a>
      <?php
    }
    ?>
  </div>


</div>



