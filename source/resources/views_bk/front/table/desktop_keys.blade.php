<div id="portfolio-flters" class="btn-group mr-1 mb-1 d-none d-sm-block">
    <ul  class="d-flex" data-aos="fade-up" data-aos-delay="100">
        <?php
        if (isset($keys)) {
            foreach ($keys as $b => $brand) {

                if (is_array($brand)) {
                ?>
                 <li data-filter=".filter-<?php echo (isset($brand['id'])? $brand['id']: $brand);?>" class="dropdown-item"><?php echo (isset($brand['name'])? $brand['name']: $brand);?><div data-key-count></div></li>
                <?php
            } else {
                ?>
                <li data-filter=".filter-<?php echo (isset($brand->id)? $brand->id: $brand);?>">
                    <?php echo (isset($brand->name)? $brand->name: $brand);?><div data-key-count></div>
                </li>
                <?php
            }
            }
        }
        ?>
    </ul>
</div>