

<div class="dropdown">
    <button type="button" class="btn btn-outline-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        Toggle Column
    </button>
    <div class="dropdown-menu dropdown-menu-right" data-column-toggle>

        <?php 
        $count = 0;
            foreach ($thead as $key => $value) {
            $count++;
            ?>
            <a data-column="<?php echo $count;?>" class="dropdown-item" href="javascript:void(0);">{{__($value)}}</a>
            <?php
            }
        ?>
    </div>
</div>