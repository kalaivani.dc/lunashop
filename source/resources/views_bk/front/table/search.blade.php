 <div class="row">
    <?php
    if (isset($vendor)) {?>
    <div class="col-md-3">
            <label>Vendor</label>

        <select id="vendor" class="form-control">
            <option value="">Select a vendor</option>
            
            <?php foreach ($vendor as $key => $value) {?>
            

            <option value="<?php echo $value->id;?>"><?php echo $value->name;?></option>
            <?php  }?>
           
        
        </select>
    </div>
     <?php  } ?>
<div class="col-md-3">
            <label>Keyword</label>

            <input type="text" name="keyword" class="form-control" placeholder="product, brand, vendor">
        </div>
     
 <?php 
    if (isset($dates)) {?>
        <div class="col-md-3">
            <label>Start Date</label>

            <input type="date" name="start" class="form-control">
        </div>
        <div class="col-md-3">
            <label>End Date</label>

            <input type="date" name="end" class="form-control">
        </div>
        <div class="col-md-3">
            <label>Region</label>

            <select name="region" class="form-control">
                <option value="all">All</option>
                <option  value="local">Local</option>
                <option selected value="international">International</option>

                
            </select>
        </div>

        <div class="col-md-3">
            <label>Member</label>

            <?php 
            if (isset($members)) {?>
  
               <select id="member" class="form-control">
                    <option value="">Select a member</option>

                    <?php foreach($members as $user) { ?>
                   
                       
                        <option  value="<?php echo $user->mv2_userid;?>"><?php echo $user->name;?></option><?php }?>
                        
                    
               </select>
          
            <?php } ?>
        </div>

        

        <?php 
        if (isset($couriers)) {?>

        
        <div class="col-md-3">
            <label>Courier</label>
           <select name="courier" class="form-control">
                <option value="">Select a courier</option>

                <?php foreach($couriers as $courier) { ?>
               
                   
                    <option  value="<?php echo $courier;?>"><?php echo $courier;?></option><?php }?>
                    
                
           </select>
        </div>
        <?php

        }


        ?>
        

       
<?php } ?>
<div class="col-md-3">
            <label>Action</label>

            <input type="button" value="Search" id="search" class="form-control btn btn-warning">

        </div>

</div>