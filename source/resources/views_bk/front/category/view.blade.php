@extends('layouts.app2')

@section('styles')
    @include('layouts.datatable-header-css')
@endsection

@section('content')
     @include('table.form')

@endsection

@section('scripts')
@include('layouts.datatable-footer-js')

{{Html::script('assets/js/plugin-coded.js')}}

<script type="text/javascript">


      $(function() {

        // initialize the datalist plugin on page load
       var table =  $('body').datalist({
            url: "<?php echo $site;?><?php echo (isset($user->role)? '/'.$user->role: '');?>category/product_post",
            'guest':"<?php echo $guest;?>",
            'id':<?php echo $cat;?>
        });
    

        


     });

</script>
@endsection
