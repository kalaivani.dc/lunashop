@extends('layouts.app2')


@section('styles')
<style>
   .timeline-badge {
    /*border-radius: 50%;*/
    /*height: 22px;*/
    /*left: 0;*/
    /*position: absolute;*/
    /*top: 10px;*/
    /*width: 22px;*/
    /*border-width: 2px;*/
    /*border-style: solid;*/
    /*background: #fff;*/
    /*padding: 4px;*/
}
.timeline-badge.success {
    border-color: #e3f9e9;
}
.timeline-badge:after {
    /*content: "";*/
    /*width: 10px;*/
    /*height: 10px;*/
    /*border-radius: 100%;*/
    /*display: block;*/
}
.timeline-badge.success:after {
    background-color: #2BC155 !important;
    box-shadow: 0 5px 10px 0 rgba(43, 193, 85, 0.2);
}
 .timeline-badge.success + .timeline-panel {
    background: #e3f9e9;
    border-color: #e3f9e9;
}
.timeline-panel {
    border-radius: 1.25rem;
    padding: 15px 20px;
    position: relative;
    display: block;
    margin-bottom: 40px;
    border-width: 1px;
    border-style: solid;
}
</style>
@endsection

@section('content')
<?php 
if(isset($sent)) {
    ?>
    <div class="row">
    <div class="col-12">
       <div class="timeline-badge success">
			</div>
			<a class="timeline-panel text-muted" href="#">
				<span>THANK YOU</span>
				<h6 class="mb-0">Your order is received</h6>
			</a>
    </div>
</div>
    <?php
}

?>


<div class="row">
                    <div class="col-lg-12">

                        <div class="card">
                            <div class="card-header"> Invoice <strong><?php echo $order->created_date;?></strong> <span class="float-right">
                                    <strong>Status:</strong> <?php echo $order->status;?></span> </div>
                            <div class="card-body">
                                <div class="row mb-5">
                                    <div class="mt-4 col-xl-3 col-lg-3 col-md-6 col-sm-12">
                                        <h6>From:</h6>
                                        <div> <strong>PREMIO PRINTDEPOT SDN BHD</strong> </div>
                                        <div>MR. JOEY WONG</div>
                                        <div>1st & 2nd Flr,No 1, Jalan SS 6/5B,</div>
                                        <div>Dataran Glomac, Kelana Jaya,</div>
                                        <div>47301 Petaling Jaya, Selangor Dahrul Ehsan</div>
                
                                    </div>
                                    <div class="mt-4 col-xl-3 col-lg-3 col-md-6 col-sm-12">
                                        <h6>To:</h6>
                                        
                                        
                                        <div> <strong>Attn: <?php echo $order->firstName;?> <?php echo $order->lastName;?></strong> </div>
                                        <div><?php echo $order->address;?></div>
                                        <div><?php echo $order->address2;?>, <?php echo $order->zip;?>, <?php echo $order->state;?></div>
                                        <div><?php echo $order->country;?></div>
                                        <div>Email: <?php echo $order->username;?></div>
                                    </div>
                                    <div class="mt-4 col-xl-6 col-lg-6 col-md-12 col-sm-12 d-flex justify-content-lg-end justify-content-md-center justify-content-xs-start">
                                        <div class="row align-items-center">
											<div class="col-sm-9"> 
												<div class="brand-logo mb-3">
													<img class="logo-abbr mr-2" src="http://online.printdepot.com.my/asset/home/assets/img/logo-207x41.png" alt="">
									
												</div>
                                                <!--<span>Please send exact amount: <strong class="d-block">0.15050000 BTC</strong>-->
                                                <!--    <strong>1DonateWffyhwAjskoEwXt83pHZxhLTr8H</strong></span><br>-->
                                                <!--<small class="text-muted">Current exchange rate 1BTC = $6590 USD</small>-->
                                            </div>
                                            <div class="col-sm-3 mt-3"> 
                                            <!--<img src="https://nexustech.online/asset/front/images/qr.png" class="img-fluid width110">-->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="table-responsive">
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th class="center">#</th>
                                                <th>Item</th>
                                                <th>Description</th>
                                                <th class="right">Unit Cost(RM)</th>
                                                <th class="center">Qty</th>
                                                <th class="right">Total(RM)</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            if(isset($detail)) {
                                                $count = 0;
                                                foreach($detail['product'] as $key=>$value) {
                                                    $count++;
                                                   ?>
                                                   <tr>
                                                        <td class="center"><?php echo $count;?></td>
                                                        <td class="left strong"><?php echo $value->name;?></td>
                                                        <td></td>
                                                        <td class="right"><?php echo $value->sub_total;?></td>
                                                        <td class="center"><?php echo $value->quantity;?></td>
                                                        <td class="right"><?php echo $value->total;?></td>
                                                    </tr>
                                                    
                                                   <?php
                                                   
                                                   if(isset($detail['variant'][$value->product_id])) {
                                                                ?>
                                                              
                                                                <?php
                                                                $i=0;
                                                               foreach($detail['variant'][$value->product_id] as $k=>$v) {
                                                                $i++;
                                                                   ?>
                                                                   <tr>
                                                                       <td></td>
                                                                       <td><span class="bullet bullet-success bullet-sm"></span> <?php echo $v->variant_type;?></td>
                                                                       <td><?php echo $v->variant_value;?></td>
                                                                       <td><?php echo $v->variant_price;?></td>
                                                                       
                                                                   </tr>
                                                                   <?php
                                                               }
                                                               ?>
                                                               
                                                               <?php
                                                           }
                                                }
                                                
                                               
                                                           
                                                           
                                                          
                                            }
                                            
                                            ?>
                                            
                                           
                                        </tbody>
                                    </table>
                                </div>
                                <div class="row">
                                    <div class="col-lg-4 col-sm-5"> </div>
                                    <div class="col-lg-4 col-sm-5 ml-auto">
                                        <table class="table table-clear">
                                            <tbody>
                                                <!--<tr>-->
                                                <!--    <td class="left"><strong>Subtotal</strong></td>-->
                                                <!--    <td class="right">$8.497,00</td>-->
                                                <!--</tr>-->
                      
                                                <tr>
                                                    <td class="left"><strong>Total</strong></td>
                                                    <td class="right"><strong>RM<?php echo $order->grand_total;?></strong><br>
                                                     
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                
                                
                
                            </div>
                        </div>
                    </div>
                </div>
                

@endsection

@section('scripts')

@endsection
