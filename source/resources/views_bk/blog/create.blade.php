@extends('layouts.app')


@section('assets')
@endsection

@section('content')

<div class="content-header row">
    <div class="content-header-left col-12 mb-2 mt-1">
        <div class="breadcrumbs-top">
            <h5 class="content-header-title float-left pr-1 mb-0 d-none d-sm-block">Add Product</h5>
            <h5 class="content-header-title pr-1 mb-0 d-block d-sm-none"> {{__('module.edit')}}</h5>
                <div class="breadcrumb-wrapper d-none d-sm-block">
                    <ol class="breadcrumb p-0 mb-0 pl-1">
                        <li class="breadcrumb-item"><a href="/<?php echo $user->role?>/home"><i class="bx bx-home-alt"></i></a> </li>
                        <li class="breadcrumb-item"><a href="/<?php echo $user->role;?>/home">{{__('module.module')}}</a></li>
                        <li class="breadcrumb-item active">Add Module</li>
                    </ol>
                </div>
        </div>
    </div>
</div>

<div class="card">
    <div class="card-header text-left">
    
    </div>
<form action="/backend/<?php echo $user->role;?>/blog?action=save" method="POST" enctype="multipart/form-data">@csrf
    <div class="card-body">
        <div class="row col-12">
            <div class="col-md-3">
                <div class="form-group">
                <label>Blog Title</label>
                <input type="text" name="name" class="form-control">
                </div>
            </div>
            
        </div>

    </div>


    <!--description-->
       <div class="col-12">
        <div class="card">

         <div class="card-body">
             <textarea name="content" id="content" class="hidden"></textarea>
                       <section class="full-editor">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Description</h4>
                                </div>
                                <div class="card-body">
                                  
                                    <div class="row">
                                      <div class="col-sm-12">
                                            <div id="full-wrapper">
                                                <div id="full-container">
                                                    <div class="editor">
                                                        
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
             
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                                </div>
                            </div>
                        </div>

                    

    <div class="mb-4 ml-1 d-flex">

        <button type="submit" class="btn btn-primary subtotal-preview-btn" data-submit>{{__('submit')}}</button>
  </div>
</form>
</div>

@endsection
@section('scripts')

<script type="text/javascript">

$(function() {

    
    //save product
    $('body').on('click', '[data-submit]', function(e) {
     
         e.preventDefault = false;
        var status = false;
        if (status == false) {

            var quill = new Quill ('.editor');
            var quillHtml = quill.root.innerHTML.trim();
            $('#content').val(quillHtml);
            e.preventDefault = false;
        }

      
    });


});
</script>
@endsection