@extends('layouts.app2')

@section('styles')
    @include('layouts.datatable-header-css')
@endsection

@section('content')
     @include('table.form')

@endsection

@section('scripts')
@include('layouts.datatable-footer-js')

{{Html::script('assets/js/plugin-coded.js')}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/3.0.4/socket.io.js"> </script>

<script>
// var dt = new Date();
//        // var time = dt.getHours() + ":" + dt.getMinutes() + ":" + dt.getSeconds();
// var time = dt.getSeconds();

//     if (!localStorage.getItem("sessionxxx")) {
//         var userID = time;
//         localStorage.setItem("sessionxxx", userID);

//     } else {
//         var userID = localStorage.getItem("sessionxxx");

//     }

//    var sock = io("http://127.0.0.1:3444", {
//        transports: ['websocket'],
//        query: 'token='+userID
//    });

//     sock.on("connect", function() {
//         console.log('connected to server');

//     });
//         var table = [];
//         var client = {

//             fireAjax: function(offset, paginate, callback, userID) {

//                 $.ajax({
//                     url: "<?php echo $site;?><?php echo (isset($user->role)? '/'.$user->role: '');?>shop/product_post",
//                     method: "POST",
                    
//                     data: {
//                         "_token": csrfToken,
//                         "offset": (offset ? offset : 0),
//                         id:userID,
//                     },
//                     dataType: 'json',
//                     error: function(msg, b, c) {

//                         console.debug('error');
//                     },
//                     dataType: 'json',
//                     success: function(result) {

//                         callback(result);
//                         return false;
//                     },
//                     type: 'POST'
//                 });

//             },
//             target: function(data, userID, keyCount = false) {

//                 var response = data.result;
//         $.each(response.data, function(key, value) {

//         var table_rows = $.trim(value);
//         if (!table[key]) {

//         table[key] = $('.filter-' + key + ' #target').DataTable({
//         'iDisplayLength':100,
//         //  "pageLength": 0,
//         // "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
//         "bDestroy": true,
//         "responsive": true,
//         "scrollX": true,
//         dom: 'Bfrtip',
//          "columnDefs": [ {
//           "targets": [0],
//           "orderable": false,
//           'sortable':false
//         } ],
//         buttons: [
//             {
//                 extend :'copy',
//                 exportOptions: {
//                     columns: ':visible:not(.not-exported)',
//                     modifier: { page: 'current' }
//                 },
//                 title: 'Copy'
//             },
//             {
//                 extend :'csvHtml5',
//                 exportOptions: {
//                     columns: ':visible:not(.not-exported)',
//                     modifier: { page: 'current' },
//                     stripHtml: true
//                 },
//                 title: 'CSV export'
//             },
//             {
//                 extend : 'excel',
//                 exportOptions: {
//                     columns: ':visible:not(.not-exported)',
//                 },
//                 title: 'Excel export'
//             },
//             {
//                 extend : 'pdf' ,
//                 pageSize: 'LETTER',
//                 orientation: 'landscape',
//                 exportOptions: {
//                     columns: ':visible:not(.not-exported)',
//                     modifier: { page: 'current' },
//                 },
//                 title: 'Pdf export',
//             },
//              //,'pageLength'
//         ]
//         });
//         }

//          if(keyCount == 'undefined') {
//             keyCount = [key];
//             keyCount[key] = response['rowCountByKey'][key];

//         }


//         keyCount = client.sumByKey(key, $('#portfolio-flters li[data-filter=".filter-' + key + '"] [data-key-count]'), response['rowCountByKey'][key]);

//         table[key].rows.add($(table_rows)).draw();
//         });

        
//         //pagination
//         // if (data.result) {
//         // $('[data-loading-spinner]').removeClass('hidden');


//         // console.log(data.result.offset);
//         // client.updateTable(data.result.offset, 2, userID);
//         // }


//      $('[data-loading-spinner]').addClass('hidden');
 
//             },
//             user: function()
//             {
//                if (!localStorage.getItem("sessionxxx")) {
//                     var userID = time;
//                     localStorage.setItem("sessionxxx", userID);

//                 } else {
//                     var userID = localStorage.getItem("sessionxxx");

//                 } 

//                 return userID;
//             },
//              updateTable: function(offset, $paginate, userID) {

//                 client.fireAjax(offset, $paginate, client.target, userID);

//             },
//             sumByKey: function(key, element, total) {
                
//                 var eThis = $(element).find('p');
//                 var eVal = parseFloat((eThis.text() == '' ? 0 : eThis.text()));
//                 var sum = eVal + total;

//                 element.html('<p>'+sum+'<p>');


//                 return element;
            

//             }

//         }


//     $.ajax({
//          url: "<?php echo $site;?><?php echo (isset($user->role)? '/'.$user->role: '');?>shop/product_post",

//         method: "POST",
//         data:{
//             "_token": "{{ csrf_token() }}",
//             'id':userID,
//             'dataType':'html'
//         },
//         success: function (response) {
//             console.log(response);

//         },
//     });

//     sock.on('nexus_database_action-channel-one:App\\Events\\MessageTransport', function (data) {
//         console.log('chanel 1');
//         console.log(data.result.data);
//         client.target(data, data.id);
//         $("#team1_score").append(data.id);

//         });
      


// $(function() {
//     //make the first child active as 'All' is  removed
//     $('#portfolio-flters li:nth-child(1)').addClass('filter-active');
//     var activeKey = $('li.filter-active').data('filter').split('filter-');
//     $('input[name=active_tab]').val(activeKey[1]);
//     //hide all content
//     $('.portfolio-item').css({ 'display': 'none' });
//     //show the first one
//     $('.portfolio-item.filter-'+activeKey[1]).css({ 'display': 'block' });
//     //adjust the layout
//     $('.filter-active').trigger('click');
//     $('.row.portfolio-container.aos-init.aos-animate').addClass('fullheight');
// });



</script>

<script type="text/javascript">


      $(function() {

        // initialize the datalist plugin on page load
       var table =  $('body').datalist({
            url: "<?php echo $site;?><?php echo (isset($user->role)? '/'.$user->role: '');?>category/category_post",
            'guest':"<?php echo $guest;?>"
            'id':<?php echo $cat;?>
        });
    

        


     });

</script>
@endsection
