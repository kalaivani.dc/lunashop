<!--<div class="card">-->
   
        <?php 
        if (isset($keys) && $keys[0]!='all') {
        ?>
       <div class="content-header-left col-12 mb-2 mt-1" style="padding-left:0;">
             <?php 
                if(isset($main_title)) {

                ?>
                
                    <div class="breadcrumbs-top  py-2">
                        <h5 class="content-header-title float-left pr-1 mb-0"><?php echo $main_title;?></h5>
                        <div class="breadcrumb-wrapper d-none d-sm-block">
                            <ol class="breadcrumb p-0 mb-0 pl-1">
                                <li class="breadcrumb-item"><a href="index.html"><i class="bx bx-home-alt"></i></a>
                                </li>
                                <li class="breadcrumb-item"><a href="#">Extensions</a>
                                </li>
                                <li class="breadcrumb-item active">Drag &amp; Drop
                                </li>
                            </ol>
                        </div>
                    </div>
             
                   
                    <?php 
                }
            ?>
    </div>
        
    <?php } ?>
    <!--</div>-->
    
    <?php 

    if (!isset($guest) || (isset($guest)&&$guest!=1)) {
        ?>
        <div class="card">
            <div class="card-header">
               <div class="row add-item-row">
                    
                    <div class="col-md-8 col-sm-6">
                    @include('table.mobile_keys')
                    @include('table.desktop_keys')
                    </div>
                    <div class="col-md-4 text-right add-item  d-none d-sm-block">
      
                        <?php 
                        if (isset($add) && !empty($add_title)) {
                          ?>
                          <a href="<?php echo $add;?>" target="_blank">
                            <button class="btn btn-success float-right" style= "width:max-content;top: 10px;position: relative;">{{__($add_title)}}</button>
                          </a>
                          <?php
                        }
                        ?>
                    
                </div>
                <div class="col-sm-6 text-right add-item d-sm-none">
                     <a href="<?php echo $add;?>" target="_blank">
                <button data-add-product class="btn btn-icon btn-success" type="button">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-plus-circle" viewBox="0 0 16 16">
                                <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/>
                                <path d="M8 4a.5.5 0 0 1 .5.5v3h3a.5.5 0 0 1 0 1h-3v3a.5.5 0 0 1-1 0v-3h-3a.5.5 0 0 1 0-1h3v-3A.5.5 0 0 1 8 4z"/>
                                </svg></button></a>
                                </div>
                    
                </div>
            </div>
            <div class="card-body">
        <?php
    }
    ?>
    <form id="form" action="<?php echo (isset($save)?$save:'')?>" method="POST">
        @csrf
       
            <div class="col-12 text-center hidden" data-loading-spinner>
                <div class="spinner-border" role="status">
                <span class="sr-only">Loading...</span>
                </div>
            </div>


<div class="col-12" >
            <div class="row portfolio-container" data-aos="fade-up" data-aos-delay="200">
                <?php
 
                if (isset($keys)) {

                foreach ($keys as $b => $brand) {

                    if (is_array($brand)) {
         
                        foreach ($brand as $k => $v) {
                     
                        ?>
                        <div class="col-lg-12 col-md-12 portfolio-item filter-<?php echo (isset($v['id'])? $v['id']: $v);?>" data-filter-<?php echo (isset($v['id'])? $v['id']: $v);?>>
                            <input type="hidden" name="active_tab" value="">
                            @include('table.table')

                            </div>
                        <?php
                        }
                    } else {
   

                        ?>
                        <div class="col-lg-12 col-md-12 portfolio-item filter-<?php echo (isset($brand->id)? $brand->id: $brand);?>" data-filter-<?php echo (isset($brand->id)? $brand->id: $brand);?>>
                            <input type="hidden" name="active_tab" value="">
                    @include('table.table')

                    </div>
                        <?php
                        }
                    ?>
                    

                    
                        <?php
                    }
                    }
                ?>
            </div>
        </div>

            <?php 
            if (isset($save) && !isset($update_shipping_fee)) {
                ?>
                <button type="submit" class="btn btn-success subtotal-preview-btn" data-submit>{{__('product.submit')}}</button>
                <?php
            }
            ?>

             <?php 
            if (isset($update_shipping_fee)) {
                ?>
                <button type="submit" class="btn btn-secondary  subtotal-preview-btn" data-form-submit>{{__('shipment.update')}}</button>
                <?php
            }

            if (isset($marketplace)) {
                ?>
                <button type="button" class="btn btn-warning subtotal-preview-btn">
                <a target="_blank" href="https://ebx360.com/sme/marketplace/<?php echo $marketplace;?>/autoAwb?q=<?php echo $account;?>">Generate AWB</a></button>
                <?php
            }
            ?>
            
    </form>
    <?php
    if (!isset($guest)) {
        ?>
        </div>
    </div>
        <?php
    }
    ?>
