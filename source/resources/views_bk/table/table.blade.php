<table id="target" class="table <?php echo (isset($guest)? ($guest!=1? '': 'front'): '');?>" style="width:100%">
    <thead class="thead-dark hidden">
        <tr>
            <?php 
            if (!isset($guest)|| (isset($guest) && $guest != 1)) {
                ?>
                <th>

                <input class="data-check-num" type="checkbox" id="ship_all">
                <label class="data-check-num" for="ship_all">#</label>


                </th>
                <?php
            }
            ?>
            

            <?php 

            foreach ($thead as $key => $value) {
            ?>
            <th <?php echo (isset($colspan) ? 'colspan='.$colspan : '');?>>{{__($value)}}</th>
            <?php
            }
            ?>

        </tr>
       
    </thead>
    <tbody></tbody>
</table>