<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
    <meta name="description" content="Frest admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities.">
    <meta name="keywords" content="admin template, Frest admin template, dashboard template, flat admin template, responsive admin template, web app">
    <meta name="author" content="NEXUS">
    <title>Dashboard - NEXUS</title>
    <link rel="apple-touch-icon" href="/asset/home/nexus_fav.png">
    <link rel="shortcut icon" type="image/x-icon" href="/asset/home/nexus_fav.png">
    <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500,600%7CIBM+Plex+Sans:300,400,500,600,700" rel="stylesheet">

    {{Html::style('app-assets/vendors/css/vendors.min.css')}}
    {{Html::style('app-assets/vendors/css/charts/apexcharts.css')}}


    {{Html::style('app-assets/css/bootstrap.css')}}
    {{Html::style('app-assets/css/bootstrap-extended.css')}}
    {{Html::style('app-assets/css/colors.css')}}
    {{Html::style('app-assets/css/components.css')}}

    {{Html::style('app-assets/css/themes/dark-layout.css')}}
    {{Html::style('app-assets/css/themes/semi-dark-layout.css')}}
    {{Html::style('app-assets/css/core/menu/menu-types/vertical-menu.css')}}

   


    {{Html::style('assets/css/style.css')}}

 <style type="text/css">

  div[data-key-count] {
    width: 30px;
    border-radius: 15px;
    position: relative;
    left: 20px;
    background-color: red;
    padding: 0;
    color: white;
    font-size: 12px;
    text-align: center;
    top: -30px;
}
          /*--------------------------------------------------------------
# Portfolio
--------------------------------------------------------------*/
#portfolio-flters {
  list-style: none;
  /*margin-bottom: 20px;*/
}

#portfolio-flters li {
 cursor: pointer;
    display: inline-block;
    margin: 10px 5px;
    font-size: 15px;
    font-weight: 500;
    line-height: 1;
    color: #444444;
    transition: all 0.3s;
/*    padding: 2px;*/
/*    border-radius: 50px;*/
    font-family: "Poppins", sans-serif;
    text-align: center;
/*    height: 30px;*/
    background-color: darkgrey;
}

#portfolio-flters li:hover, #portfolio-flters li.filter-active {
  background: #47b2e4;
  color: #fff;
}

.portfolio .portfolio-item {
  margin-bottom: 30px;
}

.portfolio .portfolio-item .portfolio-img {
  overflow: hidden;
}

.portfolio .portfolio-item .portfolio-img img {
  transition: all 0.6s;
}

.portfolio .portfolio-item .portfolio-info {
  opacity: 0;
  position: absolute;
  left: 15px;
  bottom: 0;
  z-index: 3;
  right: 15px;
  transition: all 0.3s;
  background: rgba(55, 81, 126, 0.8);
  padding: 10px 15px;
}

.portfolio .portfolio-item .portfolio-info h4 {
  font-size: 18px;
  color: #fff;
  font-weight: 600;
  color: #fff;
  margin-bottom: 0px;
}

.portfolio .portfolio-item .portfolio-info p {
  color: #f9fcfe;
  font-size: 14px;
  margin-bottom: 0;
}

.portfolio .portfolio-item .portfolio-info .preview-link, .portfolio .portfolio-item .portfolio-info .details-link {
  position: absolute;
  right: 40px;
  font-size: 24px;
  top: calc(50% - 18px);
  color: #fff;
  transition: 0.3s;
}

.portfolio .portfolio-item .portfolio-info .preview-link:hover, .portfolio .portfolio-item .portfolio-info .details-link:hover {
  color: #47b2e4;
}

.portfolio .portfolio-item .portfolio-info .details-link {
  right: 10px;
}

.portfolio .portfolio-item:hover .portfolio-img img {
  transform: scale(1.15);
}

.portfolio .portfolio-item:hover .portfolio-info {
  opacity: 1;
}

/*--------------------------------------------------------------
# Portfolio Details
--------------------------------------------------------------*/
.portfolio-details {
  padding-top: 40px;
}

.portfolio-details .portfolio-details-slider img {
  width: 100%;
}

.portfolio-details .portfolio-details-slider .swiper-pagination {
  margin-top: 20px;
  position: relative;
}

.portfolio-details .portfolio-details-slider .swiper-pagination .swiper-pagination-bullet {
  width: 12px;
  height: 12px;
  background-color: #fff;
  opacity: 1;
  border: 1px solid #47b2e4;
}

.portfolio-details .portfolio-details-slider .swiper-pagination .swiper-pagination-bullet-active {
  background-color: #47b2e4;
}

.portfolio-details .portfolio-info {
  padding: 30px;
  box-shadow: 0px 0 30px rgba(55, 81, 126, 0.08);
}

.portfolio-details .portfolio-info h3 {
  font-size: 22px;
  font-weight: 700;
  margin-bottom: 20px;
  padding-bottom: 20px;
  border-bottom: 1px solid #eee;
}

.portfolio-details .portfolio-info ul {
  list-style: none;
  padding: 0;
  font-size: 15px;
}

.portfolio-details .portfolio-info ul li + li {
  margin-top: 10px;
}

.portfolio-details .portfolio-description {
  padding-top: 30px;
}

.portfolio-details .portfolio-description h2 {
  font-size: 26px;
  font-weight: 700;
  margin-bottom: 20px;
}

.portfolio-details .portfolio-description p {
  padding: 0;
}

.hidden {
    display: none;
    
}
.strike {
    text-decoration: line-through;
    font-size:20px;
}
.front thead, .front .dt-buttons.btn-group.flex-wrap {
     display: none;
}
.fullheight {
height: 100% !important;
}

a.linkcolor {
color: #727E8C !important;
text-decoration: underline;
}
tr.red a.linkcolor {
color: #fff !important;
}


/**mobile only*/
@media only screen and (max-width: 600px){
table.dataTable tbody td {

    display: block;
}
.hide-for-small {
    display:none !important;
}

.hide-not-for-small {
    display: flex;
}
.mobile-title.hide-not-for-small {
    display: block;
}
.header-right .notification_dropdown .nav-link i {
    font-size: 28px;
}
}


#target_wrapper table.dataTable, #target_wrapper .dataTables_scrollHeadInner {
    width: 100% !important;
}

/**checkbox**/


input[id^=ship_] {
  position: absolute;
  left: -9999px;
}

label.data-check-num {
  display: block;
  position: relative;
  /*margin: 20px;*/
  padding: 5px 5px 5px 52px;
  border: 3px solid #fff;
  border-radius: 100px;
  color: #fff;
  background-color: #A3AFBD;
  white-space: nowrap;
  cursor: pointer;
  user-select: none;
  transition: background-color .2s, box-shadow .2s;
}

label.data-check-num::before {
  content: '';
  display: block;
  position: absolute;
  top: 10px;
  bottom: 10px;
  left: 10px;
  width: 32px;
  border: 3px solid #fff;
  border-radius: 100px;
  transition: background-color .2s;
}

label.data-check-num:first-of-type {
  /*transform: translateX(-40px);*/
}

label.data-check-num:last-of-type {
  /*transform: translateX(40px);*/
}

label.data-check-num:hover, input[id^=ship_]:focus + label.data-check-num {

  background-color: #39DA8A;
    box-shadow: 0 2px 6px 0 rgb(57 218 138 / 60%);
}

input[id^=ship_]:checked + label.data-check-num {
  background-color: #39DA8A;
  box-shadow: 0 2px 6px 0 rgb(57 218 138 / 60%);
}

input[id^=ship_]:checked + label.data-check-num::before {
  background-color: #fff;
}
</style>
 <style type="text/css">
    .table .thead-dark th {
        background-color:#fff;
        border-color:#3457D5;
        color: #3457D5;
        text-transform: capitalize;
        font-size: 14px;
        font-weight: 500;
    }
    input[id^=ship_]:checked + label.data-check-num {
        background-color:#3457D5;
        box-shadow: none;
    }
    .btn-secondary {
        background: none !important;
        color: #3457D5;
        border: 1px solid #3457D5;
    }
    body.semi-dark-layout .main-menu {
     background-color: none; 
    background-image: url(https://nexustech.online/asset/home/sidebar.png);
    background-size: cover;
   }
   .main-menu.menu-dark .navigation > li {
    background: rgba(90, 141, 238, 0.15);
    margin-bottom: 10px;
    border-radius:15px;
   }
   .main-menu.menu-dark .navigation li a {
       color:#ccc;
   }
   .main-menu.menu-dark .navigation > li > ul > li:first-child > a {
       border-top:none;
   }
   .btn-secondary {
       border-color:#3457D5 !important;
   }
   .main-menu .main-menu-content {
       top:30px;
   }
   .main-menu .navbar-header .navbar-brand .brand-text, i.toggle-icon.bx-disc.font-medium-4.d-none.d-xl-block.primary.bx {
       color:#ccc;
   }
   #portfolio-flters li:hover, #portfolio-flters li.filter-active {
       background-color:#3457D5;
   }
   #portfolio-flters li {
       background: rgba(90, 141, 238, 0.15);
       color: #3457D5;
   }

   .vertical-layout.vertical-menu-modern .main-menu.menu-dark {
       border: 1px solid #3457D5 !important;
   }
   .header-navbar.fixed-top {
       background-color:#3457D5;
   }
   div.dataTables_wrapper div.dataTables_filter {
    text-align: right;
    position: relative;
    top: -10px;
    }
    #target_wrapper .dt-buttons.btn-group.flex-wrap {
            position: inherit;
        top: 50px;
    }
    .header-navbar .navbar-wrapper {
     background-image:url(https://nexustech.online/asset/home/sidebar.png);
     background-size:cover;
    }
    .dataTables_filter input[type=search] {
    padding:20px !important;
    border-color:#3457D5;
}
.dataTables_filter label{
    color:#3457D5;
}
div.dataTables_wrapper div.dataTables_filter {
    top:-20px;
}
.pagination .page-item.previous.disabled .page-link, .pagination .page-item.next.disabled .page-link, .pagination .page-item.first.disabled .page-link, .pagination .page-item.last.disabled .page-link,.pagination .page-item:nth-last-child(-n+2) .page-link {
    background:none;
    color: #3457D5 !important;
    border:none;
    border-bottom: 1px solid #3457D5 !important;
    border-radius:0;
}

.pagination .page-item.active, .pagination .page-item.active .page-link, .pagination .page-item.active .page-link:hover {
    background-color:#3457D5 !important;
    color:#fff !important;
}
.add-item-row {
    width:100%;
}
.btn-success {
    color: #3457D5;
}
.btn-secondary:hover, .btn-secondary.hover {
    background-color:#3457D5 !important;
}
.btn-success:hover, .btn-success.hover {
    color: #3457D5;
}

.vertical-layout.vertical-menu-modern.menu-expanded .footer {
    margin-left:0;
}
footer.footer {
    padding-left:0;
}
.header-navbar .navbar-container ul.nav li > a.nav-link {
    color:#fff;
}
label.data-check-num:hover, input[id^=ship_]:focus + label.data-check-num {
    background-color:#3457D5;
    box-shadow: 0 2px 6px 0 rgb(90 141 238 / 60%);
}
a.delete, a.delete:hover, a.delete:focus {
    background-color:#FF5B5C !important;
    color:#fff;
}

.badge.badge-secondary,.btn-primary, .btn-primary:hover, .btn-primary.hover {
    background-color:#3457D5 !important;
}
    /**mobile only*/
@media only screen and (max-width: 600px){
    #target_wrapper .dt-buttons.btn-group.flex-wrap,div.dataTables_wrapper div.dataTables_filter{
        top:0 !important;
    } 
    div.dataTables_wrapper div.dataTables_filter {
        text-align: left;
    }
    #portfolio-flters li {
        width: 100%;
    padding: 0.8rem 1.6rem;
    clear: both;
    font-weight: 400;
    color: #475F7B;
    text-align: inherit;
    white-space: nowrap;
    background-color: transparent;
    border: 0;
    position: relative;
    left:-15%;
    text-transform: capitalize;
}
#portfolio-flters li:hover, #portfolio-flters li.filter-active {
    background:none;
    color: #3457D5;
}
.add-item {
    position: relative;
    top: -37px;
    right: -55%;
}
.add-item-row {
    width:auto;
}

}

</style>
    @yield('assets')

</head>
<!-- END: Head-->

<!-- BEGIN: Body-->

 @if(Request::segment(1) != 'login' && Request::segment(1) != 'register'&& Request::segment(1) != 'password')

        <body
        class="vertical-layout vertical-menu-modern semi-dark-layout 2-columns navbar-sticky footer-static"
        data-open="click"
        data-menu="vertical-menu-modern"
        data-col="2-columns">
    @else
        <body
        class="vertical-layout vertical-menu-modern boxicon-layout no-card-shadow content-left-sidebar todo-application navbar-sticky footer-static loginBg"
        data-open="click"
        data-menu="vertical-menu-modern"
        data-col="content-left-sidebar">


    @endif

    <!-- BEGIN: Header-->
    <div class="header-navbar-shadow"></div>
    <nav class="header-navbar main-header-navbar navbar-expand-lg navbar navbar-with-menu fixed-top ">
        <div class="navbar-wrapper">
            <div class="navbar-container content">
                <div class="navbar-collapse" id="navbar-mobile">
                    <div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
                        <ul class="nav navbar-nav">
                            <li class="nav-item mobile-menu d-xl-none mr-auto"><a class="nav-link nav-menu-main menu-toggle hidden-xs" href="javascript:void(0);"><i class="ficon bx bx-menu"></i></a></li>
                        </ul>
                        
                    </div>
                    <ul class="nav navbar-nav float-right" style="background: rgba(90, 141, 238, 0.15);color: #fff;border-radius: 25px;">
                        
                         @guest
                                @if (Route::has('login'))
                                   <li class="dropdown dropdown-language nav-item">
                                        <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                                    </li>
                                @endif

                                @if (Route::has('register'))
                                   <li class="dropdown dropdown-language nav-item">
                                        <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                    </li>
                                @endif
                            @else
                        <li class="dropdown dropdown-user nav-item"><a class="dropdown-toggle nav-link dropdown-user-link" href="javascript:void(0);" data-toggle="dropdown">
        <div class="user-nav d-sm-flex d-none"><span class="user-name">{{ Auth::user()->name }}</span>
        <span class="user-status text-muted">Admin</span></div><span><img class="round" src="https://nexustech.online/asset/assets/images/portrait/small/avatar-s-11.jpg" alt="avatar" height="40" width="40"></span>
    </a>

                            <div class="dropdown-menu dropdown-menu-right pb-0"><a class="dropdown-item" href=""><i class="bx bx-user mr-50"></i> Edit Profile</a>
                                <div class="dropdown-divider mb-0"></div><a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();"><i class="bx bx-power-off mr-50"></i> Logout</a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                        @csrf
                                    </form>
                            </div>
                        </li>
                            @endguest

                       
                        
                    </ul>
                </div>
            </div>
        </div>
    </nav>
    <!-- END: Header-->


    <!-- BEGIN: Main Menu-->
    <div class="main-menu menu-fixed menu-dark menu-accordion menu-shadow" data-scroll-to-active="true">
             @include('layouts.side-menu')
        
        <div class="shadow-bottom"></div>
       
    </div>
    <!-- END: Main Menu-->

    <!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="content-wrapper">
            <div class="content-header row">
            </div>
            <div class="content-body">
                @yield('content')
            </div>
        </div>
    </div>
    <!-- END: Content-->

    <!-- demo chat-->
    
    <div class="sidenav-overlay"></div>
    <div class="drag-target"></div>

    <!-- BEGIN: Footer-->
    <footer class="footer footer-static footer-light">
        <p class="clearfix mb-0"><span class="float-left d-inline-block">2023 &copy; Nexus Tech</span><span class="float-right d-sm-inline-block d-none">Crafted with<i class="bx bxs-heart pink mx-50 font-small-3"></i>by<a class="text-uppercase" href="#" target="_blank">Nexus Tech</a></span>
            <button class="btn btn-primary btn-icon scroll-top" type="button"><i class="bx bx-up-arrow-alt"></i></button>
        </p>
    </footer>
    <!-- END: Footer-->

    <script type="text/javascript">
      var csrfToken = "{{ csrf_token() }}";

    </script>

    {{Html::script('app-assets/vendors/js/vendors.min.js')}}
    {{Html::script('app-assets/fonts/LivIconsEvo/js/LivIconsEvo.tools.js')}}
    {{Html::script('app-assets/fonts/LivIconsEvo/js/LivIconsEvo.defaults.js')}}
    {{Html::script('app-assets/fonts/LivIconsEvo/js/LivIconsEvo.min.js')}}

    {{Html::script('app-assets/js/scripts/configs/vertical-menu-dark.js')}}
    {{Html::script('app-assets/js/core/app-menu.js')}}
    {{Html::script('app-assets/js/core/app.js')}}
    {{Html::script('app-assets/js/scripts/components.js')}}
    <!--  {{Html::script('app-assets/js/scripts/footer.js')}}-->

    <!--{{Html::script('app-assets/js/scripts/pages/dashboard-ecommerce.js')}}-->


    @yield('scripts')



</body>
<!-- END: Body-->

</html>