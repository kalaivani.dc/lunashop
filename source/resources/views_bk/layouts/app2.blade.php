<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>DEMO -NEXUS</title>
    <!-- Favicon icon -->

     <link rel="apple-touch-icon" href="/asset/home/nexus_fav.png">
    <link rel="shortcut icon" type="image/x-icon" href="/asset/home/nexus_fav.png">

        <!-- Facebook/LinkedIn Share -->
        <meta property="og:url" content="https://nexustech.online/product" />
        <meta property="og:type" content="website" />
        <meta property="og:title" content="DEMO - Nexus Tech" />
        <meta property="og:description" content="an ecommerce site built on top of laravel and dataTable from developer to developer" />
        <meta property="og:image" content="https://nexustech.online/asset/front/images/site-promo.png" />
        <meta property="og:image:width" content="1200px" />
        <meta property="og:image:height" content="630px" />
        
        
        
        
    {{Html::style('front/vendor/bootstrap-select/dist/css/bootstrap-select.min.css')}}

    {{Html::style('front/css/style.css')}}


    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@100;200;300;400;500;600;700;800;900&family=Roboto:wght@100;300;400;500;700;900&display=swap" rel="stylesheet">
 <!--page specific styles-->
    @yield('styles')
    <style type="text/css">
    
.widget-timeline.style-1 .timeline .timeline-badge.info + .timeline-panel {
    border-color: #434345;
}
.widget-timeline .timeline-badge.info:after {
    background-color: #434345 !important;
    box-shadow: 0 5px 10px 0 rgba(30, 167, 197, 0.2);
}
.widget-timeline .timeline-badge.info {
    border-color: #434345;
}
.widget-timeline.style-1 .timeline .timeline-badge.info + .timeline-panel:after {
    background-color: #FFD523 !important;
    display:none;
}
[data-sidebar-style="full"][data-layout="vertical"] .deznav .metismenu > li > a {
    border-radius:0;
    /*left: 10px;*/
    top: -5px;
}
.metismenu h6.mb-0 {
    left:10px;
    position:relative;
}
[data-sidebar-style="mini"][data-layout="vertical"] .deznav .metismenu > li.mm-active > a {
    background:none !important;
}
@media only screen and (max-width: 600px){
    .metismenu h6.mb-0 {
        left:40px;
        position:relative;
    }

}  
    .widget-timeline .timeline:before {
        left:30px;
    }
    .widget-timeline .timeline > li > .timeline-badge {
        left: 20px;
    }

.authincation-content {
    background-color: #434345 !important;
}
.nav-header .brand-title {
  
    max-width: 120px;
    margin-top: 0px;
}

[data-cart] input {
    outline: none;
    border: none;
}
.list-group-item.active {
    z-index: 2;
    color: #fff;
    background-color: #434345 !important;
    border-color: #434345 !important;
}
.list-group-item.active input {
    color: #fff !important;
}
 .btn-primary {
     background-color: #434345 !important;
    border-color: #434345 !important;
    border-radius:0;
 }
  .badge-primary  {
      background-color: #f82249;
  }
  div[data-key-count] {
    width: 30px;
    border-radius: 15px;
    position: relative;
    left: 20px;
    background-color: red;
    padding: 0;
    color: white;
    font-size: 12px;
    text-align: center;
    top: -50px;
}
[data-sidebar-style="full"][data-layout="vertical"] .deznav .metismenu > li.mm-active > a {
    background: none;
    color: #eb1b23 !important;
    border-left: 2px solid #FFD523 !important;
    border-radius: 0;
}

span.nav-text:hover,span.nav-text:focus, span.nav-text:active, span.nav-text:visited, span.menu-title.text-truncate:hover, span.menu-title.text-truncate:active, span.menu-title.text-truncate:visited {
    color: #eb1b23 !important;
}
[data-headerbg="color_1"] .nav-header .hamburger.is-active .line, [data-headerbg="color_1"] .nav-header .hamburger .line {
    background: #434345 !important;
}
[data-sidebar-style="full"][data-layout="vertical"] .menu-toggle .nav-header .nav-control .hamburger .line,[data-headerbg="color_1"] .nav-header .hamburger.is-active .line, 
[data-headerbg="color_1"] .nav-header .hamburger .line,[data-sidebar-style="mini"][data-layout="vertical"] .deznav .metismenu > li:hover > a {
    background-color: #434345 !important;
}
/*[data-sidebar-style="full"][data-layout="vertical"] .deznav .metismenu > li > a {*/
/*    padding: 5px !important;*/

/*}*/
          /*--------------------------------------------------------------
# Portfolio
--------------------------------------------------------------*/
#portfolio-flters {
  list-style: none;
  margin-bottom: 20px;
}

#portfolio-flters li {
 cursor: pointer;
    display: inline-block;
    margin: 10px 5px;
    font-size: 15px;
    font-weight: 500;
    line-height: 1;
    color: #7e7e7e;
    transition: all 0.3s;
    padding: 30px;
    border-radius: 50px;
    font-family: "Poppins", sans-serif;
    text-align: center;
    height: 30px;
    background-color: #fff;
    border: 1px solid #7e7e7e;
}
/*#portfolio-flters ul {*/
/*    left: -5%;*/
/*    position: relative;*/
/*}*/
#portfolio-flters li:hover, #portfolio-flters li.filter-active {
  background: #EA7A9A;
  color: #fff;
  border: 1px solid #EA7A9A;
}

.portfolio .portfolio-item {
  margin-bottom: 30px;
}

.portfolio .portfolio-item .portfolio-img {
  overflow: hidden;
}

.portfolio .portfolio-item .portfolio-img img {
  transition: all 0.6s;
}

.portfolio .portfolio-item .portfolio-info {
  opacity: 0;
  position: absolute;
  left: 15px;
  bottom: 0;
  z-index: 3;
  right: 15px;
  transition: all 0.3s;
  background: rgba(55, 81, 126, 0.8);
  padding: 10px 15px;
}

.portfolio .portfolio-item .portfolio-info h4 {
  font-size: 18px;
  color: #fff;
  font-weight: 600;
  color: #fff;
  margin-bottom: 0px;
}

.portfolio .portfolio-item .portfolio-info p {
  color: #f9fcfe;
  font-size: 14px;
  margin-bottom: 0;
}

.portfolio .portfolio-item .portfolio-info .preview-link, .portfolio .portfolio-item .portfolio-info .details-link {
  position: absolute;
  right: 40px;
  font-size: 24px;
  top: calc(50% - 18px);
  color: #fff;
  transition: 0.3s;
}

.portfolio .portfolio-item .portfolio-info .preview-link:hover, .portfolio .portfolio-item .portfolio-info .details-link:hover {
  color: #47b2e4;
}

.portfolio .portfolio-item .portfolio-info .details-link {
  right: 10px;
}

.portfolio .portfolio-item:hover .portfolio-img img {
  transform: scale(1.15);
}

.portfolio .portfolio-item:hover .portfolio-info {
  opacity: 1;
}

/*--------------------------------------------------------------
# Portfolio Details
--------------------------------------------------------------*/
.portfolio-details {
  padding-top: 40px;
}

.portfolio-details .portfolio-details-slider img {
  width: 100%;
}

.portfolio-details .portfolio-details-slider .swiper-pagination {
  margin-top: 20px;
  position: relative;
}

.portfolio-details .portfolio-details-slider .swiper-pagination .swiper-pagination-bullet {
  width: 12px;
  height: 12px;
  background-color: #fff;
  opacity: 1;
  border: 1px solid #47b2e4;
}

.portfolio-details .portfolio-details-slider .swiper-pagination .swiper-pagination-bullet-active {
  background-color: #47b2e4;
}

.portfolio-details .portfolio-info {
  padding: 30px;
  box-shadow: 0px 0 30px rgba(55, 81, 126, 0.08);
}

.portfolio-details .portfolio-info h3 {
  font-size: 22px;
  font-weight: 700;
  margin-bottom: 20px;
  padding-bottom: 20px;
  border-bottom: 1px solid #eee;
}

.portfolio-details .portfolio-info ul {
  list-style: none;
  padding: 0;
  font-size: 15px;
}

.portfolio-details .portfolio-info ul li + li {
  margin-top: 10px;
}

.portfolio-details .portfolio-description {
  padding-top: 30px;
}

.portfolio-details .portfolio-description h2 {
  font-size: 26px;
  font-weight: 700;
  margin-bottom: 20px;
}

.portfolio-details .portfolio-description p {
  padding: 0;
}

.hidden {
    display: none;
    
}
.strike {
    text-decoration: line-through;
    font-size:20px;
}
.front thead, .front .dt-buttons.btn-group.flex-wrap {
     display: none;
}
.fullheight {
height: 100% !important;
}

a.linkcolor {
color: #727E8C !important;
text-decoration: underline;
}
tr.red a.linkcolor {
color: #fff !important;
}

.dt-buttons.btn-group, #target_filter label, #target_filter input[type=search] {
    display:none;
}

table.dataTable tbody td {
    background:none !important;
}
.table tbody tr td {
    vertical-align: top;
}

/* .box {*/
/*  position: relative;*/
/*  max-width: 600px;*/
/*  width: 90%;*/
  /*height: 400px;*/
  /*background: #fff;*/
  /*box-shadow: 0 0 15px rgba(0,0,0,.1);*/
/*}*/

/* common */
.ribbon {
  width: 150px;
  height: 150px;
  overflow: hidden;
  position: absolute;
}
.ribbon::before,
.ribbon::after {
  position: absolute;
  z-index: -1;
  content: '';
  display: block;
  border: 5px solid #2980b9;
}
.ribbon span {
  position: absolute;
  display: block;
  width: 225px;
  padding: 15px 0;
  /*background-color: #3498db;*/
  background-color: #fce043;
  background-image: linear-gradient(315deg, #fce043 0%, #fb7ba2 74%);
  box-shadow: 0 5px 10px rgba(0,0,0,.1);
  color: #fff;
  font: 700 18px/1 'Lato', sans-serif;
  text-shadow: 0 1px 1px rgba(0,0,0,.2);
  text-transform: uppercase;
  text-align: center;
}

/* top left*/
.ribbon-top-left {
  top: -10px;
  left: -10px;
}
.ribbon-top-left::before,
.ribbon-top-left::after {
  border-top-color: transparent;
  border-left-color: transparent;
}
.ribbon-top-left::before {
  top: 0;
  right: 0;
}
.ribbon-top-left::after {
  bottom: 0;
  left: 0;
}
.ribbon-top-left span {
  right: -25px;
  top: 30px;
  transform: rotate(-45deg);
}

/* top right*/
.ribbon-top-right {
  top: -10px;
  right: -10px;
}
.ribbon-top-right::before,
.ribbon-top-right::after {
  border-top-color: transparent;
  border-right-color: transparent;
}
.ribbon-top-right::before {
  top: 0;
  left: 0;
}
.ribbon-top-right::after {
  bottom: 0;
  right: 0;
}
.ribbon-top-right span {
  left: -25px;
  top: 30px;
  transform: rotate(45deg);
}

/* bottom left*/
.ribbon-bottom-left {
  bottom: -10px;
  left: -10px;
}
.ribbon-bottom-left::before,
.ribbon-bottom-left::after {
  border-bottom-color: transparent;
  border-left-color: transparent;
}
.ribbon-bottom-left::before {
  bottom: 0;
  right: 0;
}
.ribbon-bottom-left::after {
  top: 0;
  left: 0;
}
.ribbon-bottom-left span {
  right: -25px;
  bottom: 0px;
  transform: rotate(225deg);
}

/* bottom right*/
.ribbon-bottom-right {
  bottom: -10px;
  right: -10px;
}
.ribbon-bottom-right::before,
.ribbon-bottom-right::after {
  border-bottom-color: transparent;
  border-right-color: transparent;
}
.ribbon-bottom-right::before {
  bottom: 0;
  left: 0;
}
.ribbon-bottom-right::after {
  top: 0;
  right: 0;
}
.ribbon-bottom-right span {
  left: -25px;
  bottom: 30px;
  transform: rotate(-225deg);
}
/**mobile only*/
@media only screen and (max-width: 600px){
table.dataTable tbody td {

    display: block;
}
.hide-for-small {
    display:none !important;
}

.hide-not-for-small {
    display: flex;
}
.mobile-title.hide-not-for-small {
    display: block;
}
.header-right .notification_dropdown .nav-link i {
    font-size: 28px;
}
}
/**dekstop*/
@media only screen and (min-width: 601px){

.hide-for-small {
    display:inline-flex;
}

.hide-not-for-small {
    display:none !important;
}
.mobile-title.hide-not-for-small {
    display: none !important;
}
.desktop-total.hide-for-small {
    display: block;
}
}

/*override*/
.dataTables_wrapper .dataTables_paginate .paginate_button.previous, .dataTables_wrapper .dataTables_paginate .paginate_button.next, .pagination .page-item.active .page-link, .dataTables_wrapper .dataTables_paginate .paginate_button:hover, .dataTables_wrapper .dataTables_paginate .paginate_button.current:hover, .dataTables_wrapper .dataTables_paginate .paginate_button.current, .authincation-content, [data-sidebar-style="mini"][data-layout="vertical"] .deznav .metismenu > li.mm-active > a {
    background-color:none !important;
    border-color: none !important;
}
a.btn.btn-primary {
    background-color:#434345 !important;
    border-color:#434345 !important;
}
.new-arrival-content .price, .authincation-content, .text-primary {
    color:#434345 !important;
}
.bg-primary {
    background-color: #eb1b23 !important;
    color:#fff !important;
}
[data-sidebar-style="mini"][data-layout="vertical"] .deznav .metismenu > li.mm-active > a {
    color: #FFD523 !important;
}
.form-control {
    border-radius: 0 !important;
    margin-bottom:10px;
}
</style>
<style type="text/css">
    .ribbon span {
        background-image: url(https://nexustech.online/asset/home/sidebar.png);
    background-size: cover;
    }
</style>

</head>

<body>

    <!--*******************
        Preloader start
    ********************-->
    <div id="preloader">
        <div class="sk-three-bounce">
            <div class="sk-child sk-bounce1"></div>
            <div class="sk-child sk-bounce2"></div>
            <div class="sk-child sk-bounce3"></div>
        </div>
    </div>
    <!--*******************
        Preloader end
    ********************-->


    <!--**********************************
        Main wrapper start
    ***********************************-->
    <div id="main-wrapper">

        <!--**********************************
            Nav header start
        ***********************************-->
        <div class="nav-header">
             
            <a href="/" class="brand-logo">
              
                <!--<img class="brand-title" src="https://nexustech.online/asset/home/demo_cropped.png" alt="">-->
                <img class="logo-abbr" src="https://nexustech.online/asset/home/demo_cropped.png" alt="">
                <img class="logo-compact" src="https://nexustech.online/asset/home/demo_cropped.png" alt="">
                <img class="brand-title" src="/asset/home/nexus_logo.png" alt="" >
            </a>
          

            <div class="nav-control">
                <div class="hamburger">
                    <span class="line"></span><span class="line"></span><span class="line"></span>
                </div>
            </div>
        </div>
        <!--**********************************
            Nav header end
        ***********************************-->
        
       


        
        
        <!--**********************************
            Header start
        ***********************************-->
        <div class="header">
            <div class="header-content">
                <nav class="navbar navbar-expand">
                    <div class="collapse navbar-collapse justify-content-between">
                        <div class="header-left">
                            <div class="dashboard_bar">
                                <?php echo (isset($main_title)? $main_title: '');?>
                            </div>
                        </div>
                         <ul class="navbar-nav header-right">
                            
                           
                             <li class="nav-item dropdown notification_dropdown">
                                <a class="nav-link" href="javascript:void(0)" data-toggle="dropdown">
                                   <i class="fa fa-shopping-basket" style="color:rgb(76, 129, 71);"></i>
                                    <span class="badge light text-white bg-primary rounded-circle hidden" data-cart-wrapper>1</span>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right rounded">
                                    <div id="DZ_W_TimeLine11Home" class="widget-timeline dz-scroll style-1 p-3 height370 ps ps--active-y">
                                        <form action="/checkout" method="POST">
                                            @csrf
                                            <ul class="contacts" data-cart>
                                        
                                            </ul>
                                            <div class="shopping-cart mt-3 text-right">
                                           
                                                     <button type="submit" style="background:#434345;border-color: #434345;color:#fff;" class="btn subtotal-preview-btn" data-submit><i class="fa fa-shopping-basket mr-2"></i>Checkout</button>
                                              
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </li>
                            <li class="hide-not-for-small nav-item dropdown header-profile">
                                <a class="nav-link" href="javascript:void(0)" role="button" data-toggle="dropdown">
                                    <img src="/asset/front/images/profile/profile-circle.svg" width="20" alt=""/>
                                    <div class="header-info">
                                        <span class="text-black"><strong>Brian Lee</strong></span>
                                        <p class="fs-12 mb-0">Admin</p>
                                    </div>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <a href="/product" class="dropdown-item ai-icon">
                                        <svg id="icon-user1" xmlns="http://www.w3.org/2000/svg" class="text-primary" width="18" height="18" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><path d="M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2"></path><circle cx="12" cy="7" r="4"></circle></svg>
                                        <span class="ml-2">Product </span>
                                    </a>
                                    <a href="/blog" class="dropdown-item ai-icon">
                                        <svg id="icon-inbox" xmlns="http://www.w3.org/2000/svg" class="text-success" width="18" height="18" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><path d="M4 4h16c1.1 0 2 .9 2 2v12c0 1.1-.9 2-2 2H4c-1.1 0-2-.9-2-2V6c0-1.1.9-2 2-2z"></path><polyline points="22,6 12,13 2,6"></polyline></svg>
                                        <span class="ml-2">Blog </span>
                                    </a>
                                     @guest
                                     <a href="/login" class="dropdown-item ai-icon">
                                        <svg id="icon-logout" xmlns="http://www.w3.org/2000/svg" class="text-danger" width="18" height="18" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><path d="M9 21H5a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h4"></path><polyline points="16 17 21 12 16 7"></polyline><line x1="21" y1="12" x2="9" y2="12"></line></svg>
                                        <span class="ml-2">Login </span>
                                    </a>
                                    <a href="/register" class="dropdown-item ai-icon">
                                        <svg id="icon-logout" xmlns="http://www.w3.org/2000/svg" class="text-danger" width="18" height="18" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><path d="M9 21H5a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h4"></path><polyline points="16 17 21 12 16 7"></polyline><line x1="21" y1="12" x2="9" y2="12"></line></svg>
                                        <span class="ml-2">Register </span>
                                    </a>
                                     @else
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                                       onclick="event.preventDefault();
                                                                     document.getElementById('logout-form').submit();" class="dropdown-item ai-icon">
                                        <svg id="icon-logout" xmlns="http://www.w3.org/2000/svg" class="text-danger" width="18" height="18" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><path d="M9 21H5a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h4"></path><polyline points="16 17 21 12 16 7"></polyline><line x1="21" y1="12" x2="9" y2="12"></line></svg>
                                        <span class="ml-2">Logout </span>
                                    </a>
                                    @endguest
                                </div>
                            </li>
                           
    
                             <li class="hide-for-small nav-item">
                                <a class="nav-link" href="/product">{{ __('Product') }}</a>
                            </li>
                            <li class="hide-for-small nav-item">
                                <a class="nav-link" href="/blog">{{ __('Blog') }}</a>
                            </li>
                           
               
                                        <!-- Authentication Links -->
                                        @guest
                                            @if (Route::has('login'))
                                                <li class="hide-for-small nav-item">
                                                    <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                                                </li>
                                            @endif

                                            @if (Route::has('register'))
                                                <li class="hide-for-small nav-item">
                                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                                </li>
                                            @endif
                                        @else
                                            <li class="hide-for-small nav-item dropdown">
                                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                                    {{ Auth::user()->name }}
                                                </a>

                                                <div class="dropdown-menu dropdown-menu-end" aria-labelledby="navbarDropdown">
                                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                                       onclick="event.preventDefault();
                                                                     document.getElementById('logout-form').submit();">
                                                        {{ __('Logout') }}
                                                    </a>

                                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                                        @csrf
                                                    </form>
                                                </div>
                                            </li>
                                        @endguest
                                   
                       
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
        <!--**********************************
            Header end ti-comment-alt
        ***********************************-->

        <!--**********************************
            Sidebar start
        ***********************************-->
        <div class="deznav">
           <div class="widget-timeline style-1">
                
                                         
                <ul class="timeline" id="menu">
                    
                     <li>
                         @guest
                         
                        @else
                         <div class="timeline-badge info"></div>
                         <a class="text-muted" href="https://nexustech.online/home" aria-expanded="false">
                          
                            <span class="nav-text"><h6 class="mb-0">Dashboard</h6></span>
                        </a>
                        @endguest
                        
                    </li>

                     <?php
                        if (isset($cats)) {
                            echo $cats;
                        } ?>
                    
                </ul>
                
                <div class="add-menu-sidebar">
                    <img src="https://nexustech.online/asset/front/images/food-serving.png" alt="">
                    <p class="mb-3">Kick start your e-commerce journey with ease.</p>
                    <span class="fs-12 d-block mb-3">Let us handle the technical part of it</span>
                    <a href="whatsapp://send?phone=+601123380451&text=Hello%2C%20Nexus! I like to know about the starter kit" class="btn btn-secondary btn-rounded">Contact Us</a>
                </div>
                <div class="copyright">
                    <p>© 2023 All Rights Reserved</p>
                    <p>Made with <span class="heart"></span> by Nexus Tech</p>
                </div>

            </div>
            
        </div>
        <!--**********************************
            Sidebar end
        ***********************************-->

        <!--**********************************
            Content body start
        ***********************************-->
        <div class="content-body">
            <div class="container-fluid">
                <!-- Add Order -->
                <div class="modal fade" id="addOrderModalside">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Add Menus</h5>
                                <button type="button" class="close" data-dismiss="modal"><span>&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <form>
                                    <div class="form-group">
                                        <label class="text-black font-w500">Food Name</label>
                                        <input type="text" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label class="text-black font-w500">Order Date</label>
                                        <input type="date" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label class="text-black font-w500">Food Price</label>
                                        <input type="text" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <button type="button" class="btn btn-primary">Submit</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <!--<div class="page-titles">-->
                <!--    <ol class="breadcrumb">-->
                        <?php 
                        // if(isset($bread)) {
                           
                        //     foreach($bread as $key=>$value) {
                        //         foreach($value as $k=>$v) {
                            ?>
                             <!--<li class="breadcrumb-item"><a href="<?php #echo $k;?>"><?php #echo $v;?></a></li>-->
                            <?php
                        //         }
                        //     }
                        // }
                        
                        ?>
                       
                        
                <!--    </ol>-->
                <!--</div>-->
            
                <div id="app">
                   

                    <main class="py-12">
                       
                            @yield('content')
                   
                         
                    </main>

                </div>
    
            </div>
        </div>
        <!--**********************************
            Content body end
        ***********************************-->


        <!--**********************************
            Footer start
        ***********************************-->
        <div class="footer">
            <div class="copyright">
                <p>Copyright © Designed &amp; Developed by <a href="http://nexustech.online/" target="_blank">Nexus Tech</a> 2023</p>
            </div>
        </div>
        <!--**********************************
            Footer end
        ***********************************-->

        <!--**********************************
           Support ticket button start
        ***********************************-->

        <!--**********************************
           Support ticket button end
        ***********************************-->


</div>
    <!--**********************************
        Main wrapper end
    ***********************************-->

    <!--**********************************
        Scripts
    ***********************************-->

    <script type="text/javascript">
      var csrfToken = "{{ csrf_token() }}";

    </script>


    <!-- Required vendors -->
    {{Html::script('app-assets/vendors/js/vendors.min.js')}}
    {{Html::script('app-assets/fonts/LivIconsEvo/js/LivIconsEvo.tools.js')}}
    {{Html::script('app-assets/fonts/LivIconsEvo/js/LivIconsEvo.defaults.js')}}
    {{Html::script('app-assets/fonts/LivIconsEvo/js/LivIconsEvo.min.js')}}
    {{Html::script('app-assets/vendors/js/forms/repeater/jquery.repeater.min.js')}}
    {{Html::script('app-assets/js/scripts/configs/vertical-menu-dark.js')}}

    {{Html::script('app-assets/js/scripts/components.js')}}


    {{Html::script('assets/theme/vendor/aos/aos.js')}}
    {{Html::script('assets/theme/vendor/glightbox/js/glightbox.min.js')}}
    {{Html::script('assets/theme/vendor/isotope-layout/isotope.pkgd.min.js')}}
    {{Html::script('assets/theme/vendor/swiper/swiper-bundle.min.js')}}
    {{Html::script('assets/theme/vendor/waypoints/noframework.waypoints.js')}} 
    {{Html::script('assets/theme/js/main.js')}}


      <!--do not change this-->
      <script src="{{ 'https://nexustech.online/asset/front/vendor/global/global_modified.min.js'}}" defer></script>
       <script src="{{ 'https://nexustech.online/asset/front/vendor/bootstrap-select/dist/js/bootstrap-select.min.js'}}" defer></script> 
      <script src="{{ 'https://nexustech.online/asset/front/js/custom.min.js'}}" defer></script>
      <script src="{{ 'https://nexustech.online/asset/front/js/deznav-init.js'}}" defer></script>
<script>
    $(function() {
        
        shoppingCart();
        
        function shoppingCart()
        {
            if(JSON.parse(localStorage.getItem("cart")) != null) {
      
         
            $('[data-cart-wrapper]').removeClass('hidden').text(JSON.parse(localStorage.getItem("cart")).length);
           
            
            var cartTable = '<table style="border-collapse: collapse;">';
            var grandTotal = 0;
            
            $.each(JSON.parse(localStorage.getItem("cart")), function(k, v) {
               
                $.each(v.product, function(k2, v2) {
                console.log(v2.id);
                     cartTable += '<tr><th><input style="color:#eb1b23;" type="text" name="product['+v2.id+'][product]" value="'+v2.name+'" readonly></th></tr>';
                    
                     $.each(v2.variant, function(k3, v3) {
                          cartTable += '<tr>';

                        cartTable += '<td><div class="timeline-badge info">';
                        cartTable += '</div>';
                        cartTable += '<a class="timeline-panel text-muted" href="#">';
                        cartTable += '<span><input type="hidden" name="product['+v2.id+'][variant]['+v3[0].item+'][name]" value="'+v3[0].item+'"><input type="text" name="product['+v2.id+'][variant]['+v3[0].item+'][value]" value="'+v3[0].value+'" readonly></span>';
        
                        cartTable +='</a></td>';
                        cartTable += '<td><input style="text-align:right;" type="text" name="product['+v2.id+'][variant]['+v3[0].item+'][regular]" value="'+v3[0].regular+'" readonly></td>';
                        cartTable += '</tr>';
                     });
                     
                     cartTable += '<tr><th>Quantity</th><th style="text-align:right;"><input style="text-align:right;" type="number"  name="product['+v2.id+'][qty]"value="'+v2.qty+'"></th></tr>';
                     cartTable += '<tr><th>Sub Total</th><th style="text-align:right;">'+v2.per_product+'</th></tr>';
                     cartTable += '<tr><th>Total</th><th style="text-align:right;">'+v2.sub+'</th></tr>';
                     grandTotal = v2.sub.toFixed(2);
                    
                });
                
            });
            
            
            cartTable += '<tr><th>Grand Total</th><th style="text-align:right;">'+grandTotal+'</th></tr>';
            cartTable += '<table>';
            
            $('[data-cart]').html(cartTable);
            
            }
        } 
    });
</script>
@yield('scripts')




</body>

</html>