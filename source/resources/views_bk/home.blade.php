<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
    <meta name="description" content="Frest admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities.">
    <meta name="keywords" content="admin template, Frest admin template, dashboard template, flat admin template, responsive admin template, web app">
    <meta name="author" content="NEXUS">
    <title>Dashboard - NEXUS</title>
    <link rel="apple-touch-icon" href="/asset/home/nexus_fav.png">
    <link rel="shortcut icon" type="image/x-icon" href="/asset/home/nexus_fav.png">
    <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500,600%7CIBM+Plex+Sans:300,400,500,600,700" rel="stylesheet">

    {{Html::style('app-assets/vendors/css/vendors.min.css')}}
    {{Html::style('app-assets/vendors/css/charts/apexcharts.css')}}
    {{Html::style('app-assets/vendors/css/extensions/swiper.min.css')}}

    {{Html::style('app-assets/css/bootstrap.css')}}
    {{Html::style('app-assets/css/bootstrap-extended.css')}}
    {{Html::style('app-assets/css/colors.css')}}
    {{Html::style('app-assets/css/components.css')}}

    {{Html::style('app-assets/css/themes/dark-layout.css')}}
    {{Html::style('app-assets/css/themes/semi-dark-layout.css')}}
    {{Html::style('app-assets/css/core/menu/menu-types/vertical-menu.css')}}
    <!--{{Html::style('app-assets/css/pages/dashboard-ecommerce.css')}}-->

    <!--{{Html::style('app-assets/vendors/css/editors/quill/katex.min.css')}}-->
    <!--{{Html::style('app-assets/vendors/css/editors/quill/monokai-sublime.min.css')}}-->
    <!--{{Html::style('app-assets/vendors/css/editors/quill/quill.snow.css')}}-->
    <!--{{Html::style('app-assets/vendors/css/editors/quill/quill.bubble.css')}}-->


    {{Html::style('assets/css/style.css')}}

 <style type="text/css">

  div[data-key-count] {
    width: 30px;
    border-radius: 15px;
    position: relative;
    left: 20px;
    background-color: red;
    padding: 0;
    color: white;
    font-size: 12px;
    text-align: center;
    top: -30px;
}
          /*--------------------------------------------------------------
# Portfolio
--------------------------------------------------------------*/
#portfolio-flters {
  list-style: none;
  /*margin-bottom: 20px;*/
}

#portfolio-flters li {
 cursor: pointer;
    display: inline-block;
    margin: 10px 5px;
    font-size: 15px;
    font-weight: 500;
    line-height: 1;
    color: #444444;
    transition: all 0.3s;
/*    padding: 2px;*/
/*    border-radius: 50px;*/
    font-family: "Poppins", sans-serif;
    text-align: center;
/*    height: 30px;*/
    background-color: darkgrey;
}

#portfolio-flters li:hover, #portfolio-flters li.filter-active {
  background: #47b2e4;
  color: #fff;
}

.portfolio .portfolio-item {
  margin-bottom: 30px;
}

.portfolio .portfolio-item .portfolio-img {
  overflow: hidden;
}

.portfolio .portfolio-item .portfolio-img img {
  transition: all 0.6s;
}

.portfolio .portfolio-item .portfolio-info {
  opacity: 0;
  position: absolute;
  left: 15px;
  bottom: 0;
  z-index: 3;
  right: 15px;
  transition: all 0.3s;
  background: rgba(55, 81, 126, 0.8);
  padding: 10px 15px;
}

.portfolio .portfolio-item .portfolio-info h4 {
  font-size: 18px;
  color: #fff;
  font-weight: 600;
  color: #fff;
  margin-bottom: 0px;
}

.portfolio .portfolio-item .portfolio-info p {
  color: #f9fcfe;
  font-size: 14px;
  margin-bottom: 0;
}

.portfolio .portfolio-item .portfolio-info .preview-link, .portfolio .portfolio-item .portfolio-info .details-link {
  position: absolute;
  right: 40px;
  font-size: 24px;
  top: calc(50% - 18px);
  color: #fff;
  transition: 0.3s;
}

.portfolio .portfolio-item .portfolio-info .preview-link:hover, .portfolio .portfolio-item .portfolio-info .details-link:hover {
  color: #47b2e4;
}

.portfolio .portfolio-item .portfolio-info .details-link {
  right: 10px;
}

.portfolio .portfolio-item:hover .portfolio-img img {
  transform: scale(1.15);
}

.portfolio .portfolio-item:hover .portfolio-info {
  opacity: 1;
}

/*--------------------------------------------------------------
# Portfolio Details
--------------------------------------------------------------*/
.portfolio-details {
  padding-top: 40px;
}

.portfolio-details .portfolio-details-slider img {
  width: 100%;
}

.portfolio-details .portfolio-details-slider .swiper-pagination {
  margin-top: 20px;
  position: relative;
}

.portfolio-details .portfolio-details-slider .swiper-pagination .swiper-pagination-bullet {
  width: 12px;
  height: 12px;
  background-color: #fff;
  opacity: 1;
  border: 1px solid #47b2e4;
}

.portfolio-details .portfolio-details-slider .swiper-pagination .swiper-pagination-bullet-active {
  background-color: #47b2e4;
}

.portfolio-details .portfolio-info {
  padding: 30px;
  box-shadow: 0px 0 30px rgba(55, 81, 126, 0.08);
}

.portfolio-details .portfolio-info h3 {
  font-size: 22px;
  font-weight: 700;
  margin-bottom: 20px;
  padding-bottom: 20px;
  border-bottom: 1px solid #eee;
}

.portfolio-details .portfolio-info ul {
  list-style: none;
  padding: 0;
  font-size: 15px;
}

.portfolio-details .portfolio-info ul li + li {
  margin-top: 10px;
}

.portfolio-details .portfolio-description {
  padding-top: 30px;
}

.portfolio-details .portfolio-description h2 {
  font-size: 26px;
  font-weight: 700;
  margin-bottom: 20px;
}

.portfolio-details .portfolio-description p {
  padding: 0;
}

.hidden {
    display: none;
    
}
.strike {
    text-decoration: line-through;
    font-size:20px;
}
.front thead, .front .dt-buttons.btn-group.flex-wrap {
     display: none;
}
.fullheight {
height: 100% !important;
}

a.linkcolor {
color: #727E8C !important;
text-decoration: underline;
}
tr.red a.linkcolor {
color: #fff !important;
}


/**mobile only*/
@media only screen and (max-width: 600px){
table.dataTable tbody td {

    display: block;
}
.hide-for-small {
    display:none !important;
}

.hide-not-for-small {
    display: flex;
}
.mobile-title.hide-not-for-small {
    display: block;
}
.header-right .notification_dropdown .nav-link i {
    font-size: 28px;
}
}


#target_wrapper table.dataTable, #target_wrapper .dataTables_scrollHeadInner {
    width: 100% !important;
}

/**checkbox**/


input[id^=ship_] {
  position: absolute;
  left: -9999px;
}

label.data-check-num {
  display: block;
  position: relative;
  /*margin: 20px;*/
  padding: 5px 5px 5px 52px;
  border: 3px solid #fff;
  border-radius: 100px;
  color: #fff;
  background-color: #A3AFBD;
  white-space: nowrap;
  cursor: pointer;
  user-select: none;
  transition: background-color .2s, box-shadow .2s;
}

label.data-check-num::before {
  content: '';
  display: block;
  position: absolute;
  top: 10px;
  bottom: 10px;
  left: 10px;
  width: 32px;
  border: 3px solid #fff;
  border-radius: 100px;
  transition: background-color .2s;
}

label.data-check-num:first-of-type {
  /*transform: translateX(-40px);*/
}

label.data-check-num:last-of-type {
  /*transform: translateX(40px);*/
}

label.data-check-num:hover, input[id^=ship_]:focus + label.data-check-num {

  background-color: #39DA8A;
    box-shadow: 0 2px 6px 0 rgb(57 218 138 / 60%);
}

input[id^=ship_]:checked + label.data-check-num {
  background-color: #39DA8A;
  box-shadow: 0 2px 6px 0 rgb(57 218 138 / 60%);
}

input[id^=ship_]:checked + label.data-check-num::before {
  background-color: #fff;
}
</style>
 <style type="text/css">
    .table .thead-dark th {
        background-color:#fff;
        border-color:#3457D5;
        color: #3457D5;
        text-transform: capitalize;
        font-size: 14px;
        font-weight: 500;
    }
    input[id^=ship_]:checked + label.data-check-num {
        background-color:#3457D5;
        box-shadow: none;
    }
    .btn-secondary {
        background: none !important;
        color: #3457D5;
        border: 1px solid #3457D5;
    }
    body.semi-dark-layout .main-menu {
     background-color: none; 
    background-image: url(https://nexustech.online/asset/home/sidebar.png);
    background-size: cover;
   }
   .main-menu.menu-dark .navigation > li {
    background: rgba(90, 141, 238, 0.15);
    margin-bottom: 10px;
   }
   .main-menu.menu-dark .navigation li a {
       color:#ccc;
   }
   .main-menu.menu-dark .navigation > li > ul > li:first-child > a {
       border-top:none;
   }
   .btn-secondary {
       border-color:#3457D5 !important;
   }
   .main-menu .main-menu-content {
       top:30px;
   }
   .main-menu .navbar-header .navbar-brand .brand-text, i.toggle-icon.bx-disc.font-medium-4.d-none.d-xl-block.primary.bx {
       color:#ccc;
   }
   #portfolio-flters li:hover, #portfolio-flters li.filter-active {
       background-color:#3457D5;
   }
   #portfolio-flters li {
       background: rgba(90, 141, 238, 0.15);
       color: #3457D5;
   }

   .vertical-layout.vertical-menu-modern .main-menu.menu-dark {
       border: 1px solid #3457D5 !important;
   }
   .header-navbar.fixed-top {
       background-color:#3457D5;
   }
   div.dataTables_wrapper div.dataTables_filter {
    text-align: right;
    position: relative;
    top: -10px;
    }
    #target_wrapper .dt-buttons.btn-group.flex-wrap {
            position: inherit;
        top: 50px;
    }
    .header-navbar .navbar-wrapper {
     background-image:url(https://nexustech.online/asset/home/sidebar.png);
     background-size:cover;
    }
    .dataTables_filter input[type=search] {
    padding:20px !important;
    border-color:#3457D5;
}
.dataTables_filter label{
    color:#3457D5;
}
div.dataTables_wrapper div.dataTables_filter {
    top:-20px;
}
.pagination .page-item.previous.disabled .page-link, .pagination .page-item.next.disabled .page-link, .pagination .page-item.first.disabled .page-link, .pagination .page-item.last.disabled .page-link,.pagination .page-item:nth-last-child(-n+2) .page-link {
    background:none;
    color: #3457D5 !important;
    border:none;
    border-bottom: 1px solid #3457D5 !important;
    border-radius:0;
}

.pagination .page-item.active, .pagination .page-item.active .page-link, .pagination .page-item.active .page-link:hover {
    background-color:#3457D5 !important;
    color:#fff !important;
}
.add-item-row {
    width:100%;
}
.btn-success {
    color: #3457D5;
}
.btn-secondary:hover, .btn-secondary.hover {
    background-color:#3457D5 !important;
}
.btn-success:hover, .btn-success.hover {
    color: #3457D5;
}

.vertical-layout.vertical-menu-modern.menu-expanded .footer {
    margin-left:0;
}
footer.footer {
    padding-left:0;
}
.header-navbar .navbar-container ul.nav li > a.nav-link {
    color:#fff;
}
    /**mobile only*/
@media only screen and (max-width: 600px){
    #target_wrapper .dt-buttons.btn-group.flex-wrap,div.dataTables_wrapper div.dataTables_filter{
        top:0 !important;
    } 
    div.dataTables_wrapper div.dataTables_filter {
        text-align: left;
    }
    #portfolio-flters li {
        width: 100%;
    padding: 0.8rem 1.6rem;
    clear: both;
    font-weight: 400;
    color: #475F7B;
    text-align: inherit;
    white-space: nowrap;
    background-color: transparent;
    border: 0;
    position: relative;
    left:-15%;
    text-transform: capitalize;
}
#portfolio-flters li:hover, #portfolio-flters li.filter-active {
    background:none;
    color: #3457D5;
}
.add-item {
    position: relative;
    top: -37px;
    right: -55%;
}
.add-item-row {
    width:auto;
}

}

</style>
    @yield('assets')

</head>
<!-- END: Head-->

<!-- BEGIN: Body-->

 @if(Request::segment(1) != 'login' && Request::segment(1) != 'register'&& Request::segment(1) != 'password')

        <body
        class="vertical-layout vertical-menu-modern semi-dark-layout 2-columns navbar-sticky footer-static"
        data-open="click"
        data-menu="vertical-menu-modern"
        data-col="2-columns">
    @else
        <body
        class="vertical-layout vertical-menu-modern boxicon-layout no-card-shadow content-left-sidebar todo-application navbar-sticky footer-static loginBg"
        data-open="click"
        data-menu="vertical-menu-modern"
        data-col="content-left-sidebar">


    @endif

    <!-- BEGIN: Header-->
    <div class="header-navbar-shadow"></div>
    <nav class="header-navbar main-header-navbar navbar-expand-lg navbar navbar-with-menu fixed-top ">
        <div class="navbar-wrapper">
            <div class="navbar-container content">
                <div class="navbar-collapse" id="navbar-mobile">
                    <div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
                        <ul class="nav navbar-nav">
                            <li class="nav-item mobile-menu d-xl-none mr-auto"><a class="nav-link nav-menu-main menu-toggle hidden-xs" href="javascript:void(0);"><i class="ficon bx bx-menu"></i></a></li>
                        </ul>
                        
                    </div>
                    <ul class="nav navbar-nav float-right" style="background: rgba(90, 141, 238, 0.15);color: #fff;border-radius: 25px;">
                        
                         @guest
                                @if (Route::has('login'))
                                   <li class="dropdown dropdown-language nav-item">
                                        <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                                    </li>
                                @endif

                                @if (Route::has('register'))
                                   <li class="dropdown dropdown-language nav-item">
                                        <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                    </li>
                                @endif
                            @else
                        <li class="dropdown dropdown-user nav-item"><a class="dropdown-toggle nav-link dropdown-user-link" href="javascript:void(0);" data-toggle="dropdown">
        <div class="user-nav d-sm-flex d-none"><span class="user-name">{{ Auth::user()->name }}</span>
        <span class="user-status text-muted">Admin</span></div><span><img class="round" src="https://nexustech.online/asset/assets/images/portrait/small/avatar-s-11.jpg" alt="avatar" height="40" width="40"></span>
    </a>

                            <div class="dropdown-menu dropdown-menu-right pb-0"><a class="dropdown-item" href=""><i class="bx bx-user mr-50"></i> Edit Profile</a>
                                <div class="dropdown-divider mb-0"></div><a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();"><i class="bx bx-power-off mr-50"></i> Logout</a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                        @csrf
                                    </form>
                            </div>
                        </li>
                            @endguest

                       
                        
                    </ul>
                </div>
            </div>
        </div>
    </nav>
    <!-- END: Header-->

    <!-- BEGIN: Main Menu-->
    <div class="main-menu menu-fixed menu-dark menu-accordion menu-shadow" data-scroll-to-active="true">
             @include('layouts.side-menu')
        
        <div class="shadow-bottom"></div>
       
    </div>
    <!-- END: Main Menu-->

    <!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="content-wrapper">
            <div class="content-header row">
            </div>
            <div class="content-body">
                 <!-- Dashboard Analytics Start -->
                <section id="dashboard-analytics">
                    <div class="row">
                        <!-- Website Analytics Starts-->
                        <div class="col-md-6 col-sm-12">
                            <div class="card">
                                <div class="card-header d-flex justify-content-between align-items-center">
                                    <h4 class="card-title">Website Analytics</h4>
                                    <i class="bx bx-dots-vertical-rounded font-medium-3 cursor-pointer"></i>
                                </div>
                                <div class="card-body pb-1">
                                    <div class="d-flex justify-content-around align-items-center flex-wrap">
                                        <div class="user-analytics mr-2">
                                            <i class="bx bx-user mr-25 align-middle"></i>
                                            <span class="align-middle text-muted">Users</span>
                                            <div class="d-flex">
                                                <div id="radial-success-chart"></div>
                                                <h3 class="mt-1 ml-50">61K</h3>
                                            </div>
                                        </div>
                                        <div class="sessions-analytics mr-2">
                                            <i class="bx bx-trending-up align-middle mr-25"></i>
                                            <span class="align-middle text-muted">Sessions</span>
                                            <div class="d-flex">
                                                <div id="radial-warning-chart"></div>
                                                <h3 class="mt-1 ml-50">92K</h3>
                                            </div>
                                        </div>
                                        <div class="bounce-rate-analytics">
                                            <i class="bx bx-pie-chart-alt align-middle mr-25"></i>
                                            <span class="align-middle text-muted">Bounce Rate</span>
                                            <div class="d-flex">
                                                <div id="radial-danger-chart"></div>
                                                <h3 class="mt-1 ml-50">72.6%</h3>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="analytics-bar-chart" class="my-75"></div>
                                </div>
                            </div>

                        </div>
                        <div class="col-xl-3 col-md-6 col-sm-12 dashboard-referral-impression">
                            <div class="row">
                                <!-- Referral Chart Starts-->
                                <div class="col-xl-12 col-12">
                                    <div class="card">
                                        <div class="card-body text-center pb-0">
                                            <h2>$32,690</h2>
                                            <span class="text-muted">Referral 40%</span>
                                            <div id="success-line-chart"></div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Impression Radial Chart Starts-->
                                <div class="col-xl-12 col-12">
                                    <div class="card">
                                        <div class="card-body donut-chart-wrapper">
                                            <div id="donut-chart" class="d-flex justify-content-center"></div>
                                            <ul class="list-inline d-flex justify-content-around mb-0">
                                                <li> <span class="bullet bullet-xs bullet-primary mr-50"></span>Social</li>
                                                <li> <span class="bullet bullet-xs bullet-info mr-50"></span>Email</li>
                                                <li> <span class="bullet bullet-xs bullet-warning mr-50"></span>Search</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-3 col-md-12 col-sm-12">
                            <div class="row">
                                <!-- Conversion Chart Starts-->
                                <div class="col-xl-12 col-md-6 col-12">
                                    <div class="card">
                                        <div class="card-header d-flex justify-content-between pb-xl-0 pt-xl-1">
                                            <div class="conversion-title">
                                                <h4 class="card-title">Conversion</h4>
                                                <p>60%
                                                    <i class="bx bx-trending-up text-success font-size-small align-middle mr-25"></i>
                                                </p>
                                            </div>
                                            <div class="conversion-rate">
                                                <h2>89k</h2>
                                            </div>
                                        </div>
                                        <div class="card-body text-center">
                                            <div id="bar-negative-chart" class="negative-bar-chart"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-12 col-md-6 col-12">
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="card">
                                                <div class="card-body d-flex align-items-center justify-content-between">
                                                    <div class="d-flex align-items-center">
                                                        <div class="avatar bg-rgba-primary m-0 p-25 mr-75 mr-xl-2">
                                                            <div class="avatar-content">
                                                                <i class="bx bx-user text-primary font-medium-2"></i>
                                                            </div>
                                                        </div>
                                                        <div class="total-amount">
                                                            <h5 class="mb-0">$38,566</h5>
                                                            <small class="text-muted">Conversion</small>
                                                        </div>
                                                    </div>
                                                    <div id="primary-line-chart"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="card">
                                                <div class="card-body d-flex align-items-center justify-content-between">
                                                    <div class="d-flex align-items-center">
                                                        <div class="avatar bg-rgba-warning m-0 p-25 mr-75 mr-xl-2">
                                                            <div class="avatar-content">
                                                                <i class="bx bx-dollar text-warning font-medium-2"></i>
                                                            </div>
                                                        </div>
                                                        <div class="total-amount">
                                                            <h5 class="mb-0">$53,659</h5>
                                                            <small class="text-muted">Income</small>
                                                        </div>
                                                    </div>
                                                    <div id="warning-line-chart"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <!-- Activity Card Starts-->
                        <div class="col-xl-3 col-md-6 col-12 activity-card">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Activity</h4>
                                </div>
                                <div class="card-body pt-1">
                                    <div class="d-flex activity-content">
                                        <div class="avatar bg-rgba-primary m-0 mr-75">
                                            <div class="avatar-content">
                                                <i class="bx bx-bar-chart-alt-2 text-primary"></i>
                                            </div>
                                        </div>
                                        <div class="activity-progress flex-grow-1">
                                            <small class="text-muted d-inline-block mb-50">Total Sales</small>
                                            <small class="float-right">$8,125</small>
                                            <div class="progress progress-bar-primary progress-sm">
                                                <div class="progress-bar" role="progressbar" aria-valuenow="50" style="width:50%"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="d-flex activity-content">
                                        <div class="avatar bg-rgba-success m-0 mr-75">
                                            <div class="avatar-content">
                                                <i class="bx bx-dollar text-success"></i>
                                            </div>
                                        </div>
                                        <div class="activity-progress flex-grow-1">
                                            <small class="text-muted d-inline-block mb-50">Income Amount</small>
                                            <small class="float-right">$18,963</small>
                                            <div class="progress progress-bar-success progress-sm">
                                                <div class="progress-bar" role="progressbar" aria-valuenow="80" style="width:80%"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="d-flex activity-content">
                                        <div class="avatar bg-rgba-warning m-0 mr-75">
                                            <div class="avatar-content">
                                                <i class="bx bx-stats text-warning"></i>
                                            </div>
                                        </div>
                                        <div class="activity-progress flex-grow-1">
                                            <small class="text-muted d-inline-block mb-50">Total Budget</small>
                                            <small class="float-right">$14,150</small>
                                            <div class="progress progress-bar-warning progress-sm">
                                                <div class="progress-bar" role="progressbar" aria-valuenow="60" style="width:60%"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="d-flex mb-75">
                                        <div class="avatar bg-rgba-danger m-0 mr-75">
                                            <div class="avatar-content">
                                                <i class="bx bx-check text-danger"></i>
                                            </div>
                                        </div>
                                        <div class="activity-progress flex-grow-1">
                                            <small class="text-muted d-inline-block mb-50">Completed Tasks</small>
                                            <small class="float-right">106</small>
                                            <div class="progress progress-bar-danger progress-sm">
                                                <div class="progress-bar" role="progressbar" aria-valuenow="30" style="width:30%"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Profit Report Card Starts-->
                        <div class="col-xl-3 col-md-6 col-12 profit-report-card">
                            <div class="row">
                                <div class="col-md-12 col-sm-6">
                                    <div class="card">
                                        <div class="card-header d-flex justify-content-between align-items-center">
                                            <h4 class="card-title">Profit Report</h4>
                                            <i class="bx bx-dots-vertical-rounded font-medium-3 cursor-pointer"></i>
                                        </div>
                                        <div class="card-body d-flex justify-content-around">
                                            <div class="d-inline-flex mr-xl-2">
                                                <div id="profit-primary-chart"></div>
                                                <div class="profit-content ml-50 mt-50">
                                                    <h5 class="mb-0">$12k</h5>
                                                    <small class="text-muted">2020</small>
                                                </div>
                                            </div>
                                            <div class="d-inline-flex">
                                                <div id="profit-info-chart"></div>
                                                <div class="profit-content ml-50 mt-50">
                                                    <h5 class="mb-0">$64k</h5>
                                                    <small class="text-muted">2019</small>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 col-sm-6">
                                    <div class="card">
                                        <div class="card-header">
                                            <h4 class="card-title">Registrations</h4>
                                        </div>
                                        <div class="card-body">
                                            <div class="d-flex align-items-end justify-content-around">
                                                <div class="registration-content mr-xl-1">
                                                    <h4 class="mb-0">56.3k</h4>
                                                    <i class="bx bx-trending-up success align-middle"></i>
                                                    <span class="text-success">12.8%</span>
                                                </div>
                                                <div id="registration-chart"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Sales Chart Starts-->
                        <div class="col-xl-3 col-md-6 col-12 sales-card">
                            <div class="card">
                                <div class="card-header d-flex justify-content-between align-items-center">
                                    <div class="card-title-content">
                                        <h4 class="card-title">Sales</h4>
                                        <small class="text-muted">Calculated in last 7 days</small>
                                    </div>
                                    <i class="bx bx-dots-vertical-rounded font-medium-3 cursor-pointer"></i>
                                </div>
                                <div class="card-body">
                                    <div id="sales-chart" class="mb-2"></div>
                                    <div class="d-flex justify-content-between my-1">
                                        <div class="sales-info d-flex align-items-center">
                                            <i class='bx bx-up-arrow-circle text-primary font-medium-5 mr-50'></i>
                                            <div class="sales-info-content">
                                                <h6 class="mb-0">Best Selling</h6>
                                                <small class="text-muted">Sunday</small>
                                            </div>
                                        </div>
                                        <h6 class="mb-0">28.6k</h6>
                                    </div>
                                    <div class="d-flex justify-content-between mt-2">
                                        <div class="sales-info d-flex align-items-center">
                                            <i class='bx bx-down-arrow-circle icon-light font-medium-5 mr-50'></i>
                                            <div class="sales-info-content">
                                                <h6 class="mb-0">Lowest Selling</h6>
                                                <small class="text-muted">Thursday</small>
                                            </div>
                                        </div>
                                        <h6 class="mb-0">986k</h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Growth Chart Starts-->
                        <div class="col-xl-3 col-md-6 col-12 growth-card">
                            <div class="card">
                                <div class="card-body text-center">
                                    <div class="dropdown">
                                        <button class="btn btn-sm btn-outline-secondary dropdown-toggle" type="button" id="dropdownMenuButtonSec" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            2020
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButtonSec">
                                            <a class="dropdown-item" href="javascript:void(0);">2020</a>
                                            <a class="dropdown-item" href="javascript:void(0);">2019</a>
                                            <a class="dropdown-item" href="javascript:void(0);">2018</a>
                                        </div>
                                    </div>
                                    <div id="growth-Chart"></div>
                                    <h6 class="mt-2"> 62% Growth in 2020</h6>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <!-- Task Card Starts -->
                        <div class="col-lg-7">
                            <div class="row">
                                <div class="col-12">
                                    <div class="card widget-todo">
                                        <div class="card-header border-bottom d-flex justify-content-between align-items-center flex-wrap">
                                            <h4 class="card-title d-flex mb-25 mb-sm-0">
                                                <i class='bx bx-check font-medium-5 pl-25 pr-75'></i>Tasks
                                            </h4>
                                            <ul class="list-inline d-flex mb-25 mb-sm-0">
                                                <li class="d-flex align-items-center">
                                                    <i class='bx bx-filter font-medium-3 mr-50'></i>
                                                    <div class="dropdown">
                                                        <div class="dropdown-toggle mr-1 cursor-pointer" role="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Filter</div>
                                                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                                                            <a class="dropdown-item" href="javascript:void(0);">My Tasks</a>
                                                            <a class="dropdown-item" href="javascript:void(0);">Due this week</a>
                                                            <a class="dropdown-item" href="javascript:void(0);">Due next week</a>
                                                            <a class="dropdown-item" href="javascript:void(0);">Custom Filter</a>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li class="d-flex align-items-center">
                                                    <i class='bx bx-sort mr-50 font-medium-3'></i>
                                                    <div class="dropdown">
                                                        <div class="dropdown-toggle cursor-pointer" role="button" id="dropdownMenuButton2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Sort</div>
                                                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton2">
                                                            <a class="dropdown-item" href="javascript:void(0);">None</a>
                                                            <a class="dropdown-item" href="javascript:void(0);">Alphabetical</a>
                                                            <a class="dropdown-item" href="javascript:void(0);">Due Date</a>
                                                            <a class="dropdown-item" href="javascript:void(0);">Assignee</a>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="card-body px-0 py-1">
                                            <ul class="widget-todo-list-wrapper" id="widget-todo-list">
                                                <li class="widget-todo-item">
                                                    <div class="widget-todo-title-wrapper d-flex justify-content-between align-items-center mb-50">
                                                        <div class="widget-todo-title-area d-flex align-items-center">
                                                            <i class='bx bx-grid-vertical mr-25 font-medium-4 cursor-move'></i>
                                                            <div class="checkbox checkbox-shadow">
                                                                <input type="checkbox" class="checkbox__input" id="checkbox1">
                                                                <label for="checkbox1"></label>
                                                            </div>
                                                            <span class="widget-todo-title ml-50">Add SCSS and JS files if required</span>
                                                        </div>
                                                        <div class="widget-todo-item-action d-flex align-items-center">
                                                            <div class="badge badge-pill badge-light-success mr-1">frontend</div>
                                                            <div class="avatar bg-rgba-primary m-0 mr-50">
                                                                <div class="avatar-content">
                                                                    <span class="font-size-base text-primary">RA</span>
                                                                </div>
                                                            </div>
                                                            <div class="dropdown">
                                                                <span class="bx bx-dots-vertical-rounded font-medium-3 dropdown-toggle nav-hide-arrow cursor-pointer icon-light" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" role="menu"></span>
                                                                <div class="dropdown-menu dropdown-menu-right">
                                                                    <a class="dropdown-item" href="javascript:void(0);">View Details</a>
                                                                    <a class="dropdown-item" href="javascript:void(0);">Duplicate Task</a>
                                                                    <a class="dropdown-item" href="javascript:void(0);">Delete Task</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li class="widget-todo-item">
                                                    <div class="widget-todo-title-wrapper d-flex justify-content-between align-items-center mb-50">
                                                        <div class="widget-todo-title-area d-flex align-items-center">
                                                            <i class='bx bx-grid-vertical mr-25 font-medium-4 cursor-move'></i>
                                                            <div class="checkbox checkbox-shadow">
                                                                <input type="checkbox" class="checkbox__input" id="checkbox2">
                                                                <label for="checkbox2"></label>
                                                            </div>
                                                            <span class="widget-todo-title ml-50">Check your changes, before commiting</span>
                                                        </div>
                                                        <div class="widget-todo-item-action d-flex align-items-center">
                                                            <div class="badge badge-pill badge-light-danger mr-1">backend</div>
                                                            <div class="avatar m-0 mr-50">
                                                                <img src="../../../app-assets/images/profile/user-uploads/social-2.jpg" alt="img placeholder" height="32" width="32">
                                                            </div>
                                                            <div class="dropdown">
                                                                <span class="bx bx-dots-vertical-rounded font-medium-3 dropdown-toggle nav-hide-arrow cursor-pointer icon-light" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" role="menu"></span>
                                                                <div class="dropdown-menu dropdown-menu-right">
                                                                    <a class="dropdown-item" href="javascript:void(0);">View Details</a>
                                                                    <a class="dropdown-item" href="javascript:void(0);">Duplicate Task</a>
                                                                    <a class="dropdown-item" href="javascript:void(0);">Delete Task</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li class="widget-todo-item completed">
                                                    <div class="widget-todo-title-wrapper d-flex justify-content-between align-items-center mb-50">
                                                        <div class="widget-todo-title-area d-flex align-items-center">
                                                            <i class='bx bx-grid-vertical mr-25 font-medium-4 cursor-move'></i>
                                                            <div class="checkbox checkbox-shadow">
                                                                <input type="checkbox" class="checkbox__input" id="checkbox3" checked>
                                                                <label for="checkbox3"></label>
                                                            </div>
                                                            <span class="widget-todo-title ml-50">Dribble, Behance, UpLabs & Pinterest Post</span>
                                                        </div>
                                                        <div class="widget-todo-item-action d-flex align-items-center">
                                                            <div class="badge badge-pill badge-light-primary mr-1">UI/UX</div>
                                                            <div class="avatar bg-rgba-primary m-0 mr-50">
                                                                <div class="avatar-content">
                                                                    <span class="font-size-base text-primary">JP</span>
                                                                </div>
                                                            </div>
                                                            <div class="dropdown">
                                                                <span class="bx bx-dots-vertical-rounded font-medium-3 dropdown-toggle nav-hide-arrow cursor-pointer icon-light" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" role="menu"></span>
                                                                <div class="dropdown-menu dropdown-menu-right">
                                                                    <a class="dropdown-item" href="javascript:void(0);">View Details</a>
                                                                    <a class="dropdown-item" href="javascript:void(0);">Duplicate Task</a>
                                                                    <a class="dropdown-item" href="javascript:void(0);">Delete Task</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li class="widget-todo-item">
                                                    <div class="widget-todo-title-wrapper d-flex justify-content-between align-items-center mb-50">
                                                        <div class="widget-todo-title-area d-flex align-items-center">
                                                            <i class='bx bx-grid-vertical mr-25 font-medium-4 cursor-move'></i>
                                                            <div class="checkbox checkbox-shadow">
                                                                <input type="checkbox" class="checkbox__input" id="checkbox4">
                                                                <label for="checkbox4"></label>
                                                            </div>
                                                            <span class="widget-todo-title ml-50">Fresh Design Web & Responsive Miracle</span>
                                                        </div>
                                                        <div class="widget-todo-item-action d-flex align-items-center">
                                                            <div class="badge badge-pill badge-light-info mr-1">Design</div>
                                                            <div class="avatar m-0 mr-50">
                                                                <img src="../../../app-assets/images/profile/user-uploads/user-05.jpg" alt="img placeholder" height="32" width="32">
                                                            </div>
                                                            <div class="dropdown">
                                                                <span class="bx bx-dots-vertical-rounded font-medium-3 dropdown-toggle nav-hide-arrow cursor-pointer icon-light" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" role="menu"></span>
                                                                <div class="dropdown-menu dropdown-menu-right">
                                                                    <a class="dropdown-item" href="javascript:void(0);">View Details</a>
                                                                    <a class="dropdown-item" href="javascript:void(0);">Duplicate Task</a>
                                                                    <a class="dropdown-item" href="javascript:void(0);">Delete Task</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li class="widget-todo-item">
                                                    <div class="widget-todo-title-wrapper d-flex justify-content-between align-items-center mb-50">
                                                        <div class="widget-todo-title-area d-flex align-items-center">
                                                            <i class='bx bx-grid-vertical mr-25 font-medium-4 cursor-move'></i>
                                                            <div class="checkbox checkbox-shadow">
                                                                <input type="checkbox" class="checkbox__input" id="checkbox5">
                                                                <label for="checkbox5"></label>
                                                            </div>
                                                            <span class="widget-todo-title ml-50">Add Calendar page, source and credit page in
                                                                documentation</span>
                                                        </div>
                                                        <div class="widget-todo-item-action d-flex align-items-center">
                                                            <div class="badge badge-pill badge-light-warning mr-1">Javascript</div>
                                                            <div class="avatar bg-rgba-primary m-0 mr-50">
                                                                <div class="avatar-content">
                                                                    <span class="font-size-base text-primary">AK</span>
                                                                </div>
                                                            </div>
                                                            <div class="dropdown">
                                                                <span class="bx bx-dots-vertical-rounded font-medium-3 dropdown-toggle nav-hide-arrow cursor-pointer icon-light" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" role="menu"></span>
                                                                <div class="dropdown-menu dropdown-menu-right">
                                                                    <a class="dropdown-item" href="javascript:void(0);">View Details</a>
                                                                    <a class="dropdown-item" href="javascript:void(0);">Duplicate Task</a>
                                                                    <a class="dropdown-item" href="javascript:void(0);">Delete Task</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li class="widget-todo-item">
                                                    <div class="widget-todo-title-wrapper d-flex justify-content-between align-items-center mb-50">
                                                        <div class="widget-todo-title-area d-flex align-items-center">
                                                            <i class='bx bx-grid-vertical mr-25 font-medium-4 cursor-move'></i>
                                                            <div class="checkbox checkbox-shadow">
                                                                <input type="checkbox" class="checkbox__input" id="checkbox6">
                                                                <label for="checkbox6"></label>
                                                            </div>
                                                            <span class="widget-todo-title ml-50">Add Angular Starter-kit</span>
                                                        </div>
                                                        <div class="widget-todo-item-action d-flex align-items-center">
                                                            <div class="badge badge-pill badge-light-primary mr-1">UI/UX</div>
                                                            <div class="avatar m-0 mr-50">
                                                                <img src="../../../app-assets/images/profile/user-uploads/user-05.jpg" alt="img placeholder" height="32" width="32">
                                                            </div>
                                                            <div class="dropdown">
                                                                <span class="bx bx-dots-vertical-rounded font-medium-3 dropdown-toggle nav-hide-arrow cursor-pointer icon-light" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" role="menu"></span>
                                                                <div class="dropdown-menu dropdown-menu-right">
                                                                    <a class="dropdown-item" href="javascript:void(0);">View Details</a>
                                                                    <a class="dropdown-item" href="javascript:void(0);">Duplicate Task</a>
                                                                    <a class="dropdown-item" href="javascript:void(0);">Delete Task</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Daily Financials Card Starts -->
                        <div class="col-lg-5">
                            <div class="card ">
                                <div class="card-header">
                                    <h4 class="card-title">
                                        Order Timeline
                                    </h4>
                                </div>
                                <div class="card-body">
                                    <ul class="timeline mb-0">
                                        <li class="timeline-item timeline-icon-primary active">
                                            <div class="timeline-time">September, 16</div>
                                            <h6 class="timeline-title">1983, orders, $4220</h6>
                                            <p class="timeline-text">2 hours ago</p>
                                            <div class="timeline-content">
                                                <img src="../../../app-assets/images/icon/pdf.png" alt="document" height="23" width="19" class="mr-50">New Order.pdf
                                            </div>
                                        </li>
                                        <li class="timeline-item timeline-icon-primary active">
                                            <div class="timeline-time">September, 17</div>
                                            <h6 class="timeline-title">12 Invoices have been paid</h6>
                                            <p class="timeline-text">25 minutes ago</p>
                                            <div class="timeline-content">
                                                <img src="../../../app-assets/images/icon/pdf.png" alt="document" height="23" width="19" class="mr-50">Invoices.pdf
                                            </div>
                                        </li>
                                        <li class="timeline-item timeline-icon-primary active pb-0">
                                            <div class="timeline-time">September, 18</div>
                                            <h6 class="timeline-title">Order #37745 from September</h6>
                                            <p class="timeline-text">4 minutes ago</p>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- Dashboard Analytics end -->
            </div>
        </div>
    </div>
    <!-- END: Content-->

    <!-- demo chat-->
    
    <div class="sidenav-overlay"></div>
    <div class="drag-target"></div>

    <!-- BEGIN: Footer-->
    <footer class="footer footer-static footer-light">
        <p class="clearfix mb-0"><span class="float-left d-inline-block">2023 &copy; Nexus Tech</span><span class="float-right d-sm-inline-block d-none">Crafted with<i class="bx bxs-heart pink mx-50 font-small-3"></i>by<a class="text-uppercase" href="#" target="_blank">Nexus Tech</a></span>
            <button class="btn btn-primary btn-icon scroll-top" type="button"><i class="bx bx-up-arrow-alt"></i></button>
        </p>
    </footer>
    <!-- END: Footer-->

    <script type="text/javascript">
      var csrfToken = "{{ csrf_token() }}";

    </script>

    {{Html::script('app-assets/vendors/js/vendors.min.js')}}
    {{Html::script('app-assets/fonts/LivIconsEvo/js/LivIconsEvo.tools.js')}}
    {{Html::script('app-assets/fonts/LivIconsEvo/js/LivIconsEvo.defaults.js')}}
    {{Html::script('app-assets/fonts/LivIconsEvo/js/LivIconsEvo.min.js')}}

    {{Html::script('app-assets/js/scripts/configs/vertical-menu-dark.js')}}
    {{Html::script('app-assets/js/core/app-menu.js')}}
    {{Html::script('app-assets/js/core/app.js')}}
    {{Html::script('app-assets/js/scripts/components.js')}}
    {{Html::script('app-assets/js/scripts/footer.js')}}

    {{Html::script('app-assets/js/scripts/pages/dashboard-analytics.js')}}

    @yield('scripts')



</body>
<!-- END: Body-->

</html>